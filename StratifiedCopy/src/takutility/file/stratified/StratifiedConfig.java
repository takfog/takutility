package takutility.file.stratified;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import takutility.file.Files;

public class StratifiedConfig implements FileFilter {

	private static final String PLACEHOLDER_FILE = "stratcopy.ph";
	private Collection<FileFilter> fileFilters = null;
	private Collection<FilenameFilter> filenameFilters = null;
	private long keepSpace = 0;
	private boolean placeholder = false;
	
	public StratifiedConfig() {
		addFilter(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return !name.equals(PLACEHOLDER_FILE);
			}
		});
	}
	
	/**
	 * @param keepSpace the space thath should not be used by files
	 */
	public StratifiedConfig(long keepSpace) {
		this();
		this.keepSpace = keepSpace;
	}
	
	/**
	 * @param keepSpace the space thath should not be used by files
	 * @param filter a filter to apply to select the files
	 */
	public StratifiedConfig(long keepSpace, FileFilter filter) {
		this(keepSpace);
		addFilter(filter);
	}
	
	/**
	 * @param keepSpace the space thath should not be used by files
	 * @param filter a filter to apply to select the files
	 */
	public StratifiedConfig(long keepSpace, FilenameFilter filter) {
		this(keepSpace);
		addFilter(filter);
	}
	
	/**
	 * Adds a filter to apply to select the files
	 * @param fileFilter a filter to apply to select the files
	 */
	public StratifiedConfig addFilter(FileFilter fileFilter) {
		if (fileFilters == null) 
			fileFilters = new ArrayList<FileFilter>();
		fileFilters.add(fileFilter);
		return this;
	}
	
	/**
	 * Adds a filter to apply to select the files
	 * @param fileFilter a filter to apply to select the files
	 */
	public StratifiedConfig addFilter(FilenameFilter filenameFilter) {
		if (filenameFilters == null) 
			filenameFilters = new ArrayList<FilenameFilter>();
		filenameFilters.add(filenameFilter);
		return this;
	}
	
	public long getKeepSpace() {
		return keepSpace;
	}
	
	public StratifiedConfig setKeepSpace(long keepSpace) {
		this.keepSpace = keepSpace;
		return this;
	}
	
	public boolean usePlaceholder() {
		return placeholder;
	}
	
	public StratifiedConfig setPlaceholder(boolean placeholder) {
		this.placeholder = placeholder;
		return this;
	}
	
	public File getPlaceholder(File dir) {
		if (!placeholder) return null;
		return new File(dir, PLACEHOLDER_FILE);
	}
	
	public File getPlaceholderContent(File dir) {
		if (!placeholder) return null;
		File ph = getPlaceholder(dir);
		try {
			String cont = Files.fileToString(ph);
			if (cont == null || cont.trim().isEmpty()) 
				return null;
			return new File(dir, cont.replaceAll("[\n\r]+", ""));
		} catch (IOException e) {
			return null;
		}
	}
	
	public Collection<FileFilter> getFileFilters() {
		return fileFilters != null ? fileFilters 
				: Collections.<FileFilter>emptyList();
	}
	
	public Collection<FilenameFilter> getFilenameFilters() {
		return filenameFilters != null ? filenameFilters 
				: Collections.<FilenameFilter>emptyList();
	}
	
	public StratifiedCopy build() {
		return new StratifiedCopy(this);
	}
	
	public boolean accept(File file) {
		String name = file.getName();
		File dir = file.getParentFile();
		for (FilenameFilter f : getFilenameFilters()) {
			if (!f.accept(dir, name))
				return false;
		}
		for (FileFilter f : getFileFilters()) {
			if (!f.accept(file))
				return false;
		}
		return true;
	}
}
