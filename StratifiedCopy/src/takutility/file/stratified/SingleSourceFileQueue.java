package takutility.file.stratified;

import java.io.File;
import java.util.Collection;

public class SingleSourceFileQueue extends FileQueue {
	private File source;
	
	SingleSourceFileQueue(File source, StratifiedConfig config) {
		this.source = source;
		fillQueue(source, source, config);
	}
	
	@Override
	public File getSource(File dir) {
		return source;
	}
	
	@Override
	public float getWeight(File dir) {
		return 1;
	}
	
	void addDir(File dir, Collection<File> files) {
		map.addAll(dir, files);
	}

	@Override
	protected void addDir(File dir, File root, Collection<File> files) {
		map.addAll(dir, files);
	}
	
}