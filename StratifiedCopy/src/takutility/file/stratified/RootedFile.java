package takutility.file.stratified;

import java.io.File;
import java.io.IOException;

import takutility.file.Files;

public class RootedFile {
	private File file, root;

	public RootedFile(File file, File root) {
		this.file = file;
		this.root = root;
	}

	public File file() {
		return file;
	}

	public File root() {
		return root;
	}
	
	public long length() {
		return file.length();
	}
	
	public String getRelativePath() {
		String rootPath = root.getAbsolutePath();
		int rootLen = rootPath.length();
		if (!rootPath.endsWith(File.separator))
			rootLen++;
		return file.getAbsolutePath().substring(rootLen);
	}
	
	public File transplant(File dest) {
		return Files.transplant(file, root, dest);
	}
	
	public File copy(File destDir) throws IOException {
		File dest = transplant(destDir);
		File parent = dest.getParentFile();
		parent.mkdirs();
		File tmp = new File(parent, dest.getName()+".tmp");
		Files.copy(file, tmp);
		if (dest.exists()) dest.delete();
		tmp.renameTo(dest);
		return dest;
	}
	
	@Override
	public String toString() {
		return file.toString();
	}
}
