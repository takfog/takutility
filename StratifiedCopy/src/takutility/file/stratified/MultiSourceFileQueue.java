package takutility.file.stratified;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import takutility.TakUtility;
import takutility.file.Files;

public class MultiSourceFileQueue extends FileQueue {
	private Map<File, File> roots = new HashMap<File, File>();
	private Map<File, Float> weights = new HashMap<File, Float>();
	private StratifiedConfig config;
	
	public MultiSourceFileQueue(StratifiedConfig config, File... dirs) {
		this.config = config;
		if (dirs.length > 0) {
			File root = Files.commonRoot(dirs);
			for (File dir : dirs) {
				addDir(dir, root);
			}
		}
	}
	
	public MultiSourceFileQueue(StratifiedConfig config, RootedFile... dirs) {
		this.config = config;
		if (dirs.length > 0) {
			for (RootedFile dir : dirs) {
				addDir(dir.file(), dir.root());
			}
		}
	}

	@Override
	public File getSource(File dir) {
		return roots.get(dir);
	}
	
	@Override
	public float getWeight(File dir) {
		return TakUtility.optional(weights.get(dir), new Integer(1)).floatValue();
	}
	
	public void setWeight(File dir, float weight) {
		if (weight >= 1f || weight <= 0f)
			weights.remove(dir);
		else
			weights.put(dir, weight);
	}
	
	public void addDir(File dir, File root) {
		fillQueue(dir, root, config);
	}

	@Override
	protected void addDir(File dir, File root, Collection<File> files) {
		map.addAll(dir, files);
		roots.put(dir, root);
	}
	
	public void merge(FileQueue other) {
		for (File dir : other.getDirs()) {
			addDir(dir, getSource(dir), other.map.get(dir));
		}
	}
}
