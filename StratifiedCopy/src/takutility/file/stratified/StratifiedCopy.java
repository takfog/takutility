package takutility.file.stratified;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import takutility.Comparators;
import takutility.ProgressNotifier;
import takutility.TakUtility;
import takutility.collections.MultiProperties;
import takutility.file.Files;
import takutility.ptr.Lazy;

/**
 * Selects files one for directory stopping when there is no more space
 *
 */
public class StratifiedCopy {
	private StratifiedConfig config;
	
	public StratifiedCopy() {
		config = new StratifiedConfig();
	}
	
	public StratifiedCopy(StratifiedConfig config) {
		this.config = config;
	}
	
	/**
	 * @param keepSpace the space thath should not be used by files
	 */
	public StratifiedCopy(long keepSpace) {
		this.config = new StratifiedConfig(keepSpace);
	}
	
	/**
	 * @param keepSpace the space thath should not be used by files
	 * @param filter a filter to apply to select the files
	 */
	public StratifiedCopy(long keepSpace, FileFilter filter) {
		this.config = new StratifiedConfig(keepSpace, filter);
	}
	
	/**
	 * @param keepSpace the space thath should not be used by files
	 * @param filter a filter to apply to select the files
	 */
	public StratifiedCopy(long keepSpace, FilenameFilter filter) {
		this.config = new StratifiedConfig(keepSpace, filter);
	}
	
	public StratifiedConfig getConfig() {
		return config;
	}

	/**
	 * Get the files that can be copied, ignoring existing files
	 * @param source the source directory
	 * @param dest the destination directory
	 * @return the files that can be copied
	 */
	public List<RootedFile> getFiles(File source, File dest) {
		return getFiles(source, dest, getUsableSpace(dest));
	}

	/**
	 * Get the files that can be copied, ignoring existing files
	 * @param queues the file queues from which take the files to copy
	 * @param dest the destination directory
	 * @return the files that can be copied
	 */
	public List<RootedFile> getFiles(FileQueue queues, File dest) {
		return getFiles(queues, dest, getUsableSpace(dest));
	}
	
	private long getUsableSpace(File dir) {
		if (dir.exists())
			return dir.getUsableSpace();
		else
			return getUsableSpace(dir.getParentFile());
	}
	
	/**
	 * Get the files that can be copied
	 * @param source the source directory
	 * @param availableSpace the available space in the destination
	 * @return the files that can be copied
	 */
	public List<RootedFile> getFiles(File source, long availableSpace) {
		return getFiles(source, null, availableSpace);
	}
	
	/**
	 * Get the files that can be copied
	 * @param queues the file queues from which take the files to copy
	 * @param availableSpace the available space in the destination
	 * @return the files that can be copied
	 */
	public List<RootedFile> getFiles(FileQueue queues, long availableSpace) {
		return getFiles(queues, null, availableSpace);
	}

	/**
	 * Get the files that can be copied, ignoring existing files
	 * @param source the source directory
	 * @param dest the destination directory
	 * @param availableSpace the available space in the destination
	 * @return the files that can be copied
	 */
	public List<RootedFile> getFiles(File source, File dest, long availableSpace) {
		FileQueue queues = buildQueues(source);
		return getFiles(queues, dest, availableSpace);
	}

	/**
	 * Builds the file queues from which take the files to copy
	 * @param source the source directory
	 * @return the queues relative to the source
	 */
	public FileQueue buildQueues(File source) {
		return new SingleSourceFileQueue(source, config);
	}	
	

	/**
	 * Builds the file queues from which take the files to copy. 
	 * If the destination subdirectory already exists and is not empty,
	 * the file before the existing files will be excluded 
	 * @param source the source directory
	 * @param dest the destination directory
	 * @return the queues relative to the source
	 */
	public FileQueue buildQueues(File source, File dest) {
		FileQueue queues = buildQueues(source);
		queues.startFrom(dest, config);
		return queues;
	}
	
	public MultiSourceFileQueue buildMultifileQueues(File... dirs) {
		return new MultiSourceFileQueue(config, dirs);
	}
	
	public MultiSourceFileQueue buildMultifileQueues(RootedFile... dirs) {
		return new MultiSourceFileQueue(config, dirs);
	}

	/**
	 * Get the files that can be copied, ignoring existing files
	 * @param queues the file queues from which take the files to copy
	 * @param dest the destination directory
	 * @param availableSpace the available space in the destination
	 * @return the files that can be copied
	 */
	public List<RootedFile> getFiles(FileQueue queues, File dest, long availableSpace) {
		SortedSet<RootedFile> level = new TreeSet<>(new Comparator<RootedFile>() {
			private final Random rand = new Random();
			@Override
			public int compare(RootedFile o1, RootedFile o2) {
				int c = Comparators.compare(o1.length(), o2.length());
				if (c != 0) return c;
				return rand.nextBoolean() ? -1 : 1;
			}
		});
		ArrayList<RootedFile> files = new ArrayList<>();
		long remainingSpace = availableSpace - config.getKeepSpace();
		Lazy<Map<File, Float>> weights = new Lazy<>(HashMap.class);
		while(remainingSpace > 0) {
			boolean noMoreFiles = true;
			level.clear();
			queues.removeEmptyDirs();
			for (File dir : queues.getDirs()) {
				noMoreFiles = false;
				float w = queues.getWeight(dir);
				if (w < 1) {
					Float acc = weights.get().get(dir);
					if (acc == null)
						w = 1f;
					else 
						w += acc;
					
					if (w >= 1f)
						weights.get().put(dir, w - 1f);
					else {
						weights.get().put(dir, w);
						continue;
					}
				}
				level.add(new RootedFile(queues.pop(dir), queues.getSource(dir)));
			}
			if (noMoreFiles)
				break;
			for (RootedFile file : level) {
				if (dest != null && file.transplant(dest).exists())
					continue;
				remainingSpace -= file.length();
				if (remainingSpace >= 0)
					files.add(file);
				else
					break;
			}
		}
		return files;
	}
	
	/**
	 * Copies the files
	 * @param source the source directory
	 * @param dest the destination directory
	 * @throws IOException
	 */
	public void copy(File source, File dest) throws IOException {
		copy(source, dest, null);
	}
	
	public void copy(File source, File dest, ProgressNotifier<RootedFile> notifier) throws IOException {
		List<RootedFile> files = getFiles(source, dest);
		copy(files, dest, notifier);
	}
	
	/**
	 * Copies the files
	 * @param queues the file queues from which take the files to copy
	 * @param dest the destination directory
	 * @throws IOException
	 */
	public void copy(FileQueue queues, File dest) throws IOException {
		copy(queues, dest, null);
	}

	public void copy(FileQueue queues, File dest, ProgressNotifier<RootedFile> notifier) throws IOException {
		List<RootedFile> files = getFiles(queues, dest);
		copy(files, dest, notifier);
	}

	public void copy(List<RootedFile> files, File dest, ProgressNotifier<RootedFile> notifier) throws IOException {
		long prog=0, tot=0;
		boolean detailed = notifier != null && files.size() < 1000;
		float frac = 1;
		if (detailed) {
			for (RootedFile file : files) {
				tot += file.length();
			}
			detailed = tot > 0;
			if (tot*100L > Integer.MAX_VALUE)
				frac = (float)(Integer.MAX_VALUE - 100) / (tot*100L); 
		} 
		if (!detailed) {
			tot = files.size();
		}
		Map<File, String> lastCopied = null;
		if (config.usePlaceholder())
			lastCopied = new HashMap<>();
		for (RootedFile file : files) {
			if (notifier != null)
				notifier.notify(file, (int)(prog*frac), (int)(tot*frac));
			File fileDest = file.copy(dest);
			if (detailed)
				prog += file.length();
			else
				prog++;
			if (lastCopied != null)
				lastCopied.put(fileDest.getParentFile(), fileDest.getName());
		}
		if (lastCopied != null) {
			for (Entry<File, String> e : lastCopied.entrySet()) {
				Files.stringToFile(config.getPlaceholder(e.getKey()), e.getValue());
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		char[] units = new char[] {'K', 'M', 'G', 'T'};
		
		if (args.length == 0) {
			System.err.println("Missing config file");
			return;
		}
		File file = new File(args[0]);
		if (!file.exists()) {
			System.err.println("Config file not found: "+file.getAbsolutePath());
			return;
		}
		
		StratifiedConfig config = new StratifiedConfig();
		MultiProperties prop = new MultiProperties(file);
		if (prop.containsKey("keepSpace")) {
			Pattern ksRegex = Pattern.compile("(\\d+)\\s*([TGMK]?)B?", Pattern.CASE_INSENSITIVE);
			Matcher m = ksRegex.matcher(prop.get("keepSpace"));
			if (m.matches()) {
				long mult=1;
				if (!m.group(2).isEmpty()) {
					char kunit = m.group(2).toUpperCase().charAt(0);
					for (int i = 0; i < units.length; i++) {
						if (kunit == units[i]) {
							mult = (long)Math.pow(1024, i+1);
							break;
						}
					}
				}
				config.setKeepSpace(TakUtility.parseLong(m.group(1)) * mult);
			}
		}
		if (prop.containsKey("placeholder")) {
			config.setPlaceholder(Boolean.parseBoolean(prop.get("placeholder")));
		}
		StratifiedCopy sc = config.build();
		
		if (!prop.containsSingleKey("output")) {
			System.err.println("Missing single property 'output'");
			return;
		}
		File dest = new File(prop.get("output"));
		FileQueue queue;
		if (prop.containsKey("folder")) {
			File defRoot = new File(prop.get("input",""));
			ArrayList<RootedFile> dirs = new ArrayList<>();
			Lazy<Map<File, Float>> weights = new Lazy<>(HashMap.class);
			for (String path : prop.getMulti("folder")) {
				int split = path.indexOf('?');
				File root;
				String filePath;
				if (split <= 0) {
					if (split == 1) path = path.substring(1);
					root = defRoot;
					filePath = path;
				} else {
					root = new File(path.substring(0,split));
					filePath = path.substring(split+1);
				}
				
				float weight = -1f;
				split = filePath.indexOf('*');
				if (split > 0) {
					weight = TakUtility.parseFloat(filePath.substring(split+1), -1f);
					filePath = filePath.substring(0, split);
				}
				
				RootedFile rFile = new RootedFile(new File(root, filePath), root);
				dirs.add(rFile);
				if (weight > 0f)
					weights.get().put(rFile.file(), weight);
			}
			MultiSourceFileQueue mq = sc.buildMultifileQueues(dirs.toArray(new RootedFile[dirs.size()]));
			queue = mq;
			if (weights.isCreated()) {
				Set<Entry<File, Float>> entrySet = weights.get().entrySet();
				float max = -1f;
				for (Entry<File, Float> e : entrySet) {
					if (max < e.getValue())
						max = e.getValue();
				}
				if (max <= 0f)
					max = 1f;
				else
					max = 1/max;
				for (Entry<File, Float> e : entrySet) {
					mq.setWeight(e.getKey(), e.getValue()*max);
				}
			}
		} else if (prop.containsSingleKey("input")) {
			queue = sc.buildQueues(new File(prop.get("input")), dest);
		} else {
			System.err.println("Missing property 'input' or 'folder' list");
			return;
		}
		
		if ("true".equalsIgnoreCase(prop.getProperty("ignoreStart", ""))){
			queue.startFrom(dest, config);
		}
		
		List<RootedFile> files = sc.getFiles(queue, dest);
		
		ArrayList<RootedFile> sortf = new ArrayList<>(files);
		Collections.sort(sortf, new Comparator<RootedFile>() {
			@Override
			public int compare(RootedFile o1, RootedFile o2) {
				int c = o1.root().compareTo(o2.root());
				if (c != 0) return c;
				return o1.file().compareTo(o2.file());
			}
		});
		System.out.println("Files that will be copied:");
		float tot = 0;
		for (RootedFile f : sortf) {
			System.out.println(f.getRelativePath());
			tot += f.length();
		}
		String totunit = "B";
		if (tot > 1024) {
			for (int i = 0; i < units.length && tot >= 1024; i++) {
				tot /= 1024;
				totunit = units[i]+"B";
			}
		}
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		System.out.printf("Total size: %s %s%n",df.format(tot), totunit);
		
		sc.copy(files, dest, new ProgressNotifier<RootedFile>() {
			@Override
			public void notify(RootedFile elem, int pos, int tot) {
				System.out.printf("%d%%\t%s%n", pos*100/tot, elem.getRelativePath());
			}
		});
		System.out.println("Done");
	}
}
