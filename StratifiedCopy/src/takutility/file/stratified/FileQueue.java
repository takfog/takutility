package takutility.file.stratified;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.Map.Entry;
import java.util.Set;

import takutility.collections.multimap.DequeMultiMap;
import takutility.file.Files;
import takutility.ptr.Lazy;

public abstract class FileQueue {
	static final Comparator<File> nameComp = new Comparator<File>() {
		@Override
		public int compare(File o1, File o2) {
			return o1.getName().compareTo(o2.getName());
		}
	};
	protected DequeMultiMap<File, File> map = new DequeMultiMap<>();
	
	public abstract File getSource(File dir);
	
	public abstract float getWeight(File dir);
	
	public Set<File> getDirs() {
		return Collections.unmodifiableSet(map.keySet());
	}
	
	public Collection<Deque<File>> getQueues() {
		return map.values();
	}
	
	protected void fillQueue(File base, File root, StratifiedConfig config) {
		Lazy<ArrayList<File>> qFiles = new Lazy<>(ArrayList.class);
		for (File file : base.listFiles()) {
			if (file.isDirectory())
				fillQueue(file, root, config);
			else if(config.accept(file)) {
				qFiles.get().add(file);
			}
		}
		if (qFiles.isCreated()) {
			Collections.sort(qFiles.get(), nameComp);
			addDir(base, root, qFiles.get());
		}
	}
	
	public File current(File dir) {
		Deque<File> queue = map.getIfPresent(dir);
		return (queue == null) ? null : queue.peek();
	}
	
	void removeEmptyDirs() {
		map.removeEmpty();
	}
	
	protected abstract void addDir(File dir, File root, Collection<File> files);
	
	public File pop(File dir) {
		Deque<File> queue = map.getIfPresent(dir);
		if (queue == null) 
			return null;
		return queue.pop();
	}
	
	public File next(File dir) {
		Deque<File> queue = map.getIfPresent(dir);
		if (queue == null) 
			return null;
		queue.pop();
		return queue.peek();
	}
	
	/**
	 * For each queue, if the destination subdirectory already exists and is not empty,
	 * the file before the existing files will be excluded 
	 * @param dest the destination directory
	 */
	public void startFrom(File dest, StratifiedConfig config) {
		for (Entry<File, Deque<File>> e : map.entrySet()) {
			File subDest = Files.transplant(e.getKey(), getSource(e.getKey()), dest);
			if (!subDest.isDirectory()) continue;
			File[] dFiles = subDest.listFiles(config);
			File ph = config.getPlaceholderContent(subDest);
			if (ph != null) {
				if (dFiles.length == 0)
					dFiles = new File[] { ph };
				else {
					boolean found = false;
					for (File file : dFiles) {
						if (file.equals(ph)){
							ph = null;
							found = true;
							break;
						}
					}
					if (!found) {
						File[] tmp = new File[dFiles.length+1];
						System.arraycopy(dFiles, 0, tmp, 0, dFiles.length);
						tmp[dFiles.length] = ph;
						dFiles = tmp;
					}
				}
			}
			if (dFiles.length == 0) continue;
			Arrays.sort(dFiles, nameComp);
			
			boolean found = false;
			int di = 0; 
			fileFor:
			for (File sFile : e.getValue()) {
				int c;
				do {
					c = nameComp.compare(sFile, dFiles[di]);
					if (c == 0) {
						found = true;
						break fileFor;
					} else if (c < 0) {
						continue fileFor;
					} else {
						di++;
					}
				} while(c > 0 && di < dFiles.length);
			}
			if (found) {
				while(nameComp.compare(e.getValue().peek(), dFiles[0]) < 0) {
					e.getValue().pop();
				}
				if (ph != null && nameComp.compare(e.getValue().peek(), ph) == 0) {
					e.getValue().pop();
				}
			}
		}
	}
}