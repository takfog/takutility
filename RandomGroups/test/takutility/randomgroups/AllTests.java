package takutility.randomgroups;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
		RandomIteratorBaseLayerTest.class, 
		RandomIteratorDoubleLayerTest.class, 
		SingleElementTest.class,
		SingleIteratorTest.class, 
		RandomIterableTest.class ,
		RandomListIteratorTest.class,
		MultiElementTest.class
	})
public class AllTests {

}
