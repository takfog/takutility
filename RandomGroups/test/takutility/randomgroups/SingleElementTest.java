package takutility.randomgroups;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

public class SingleElementTest {

	@Test
	public void test() {
		String obj = "prova";
		SingleElement<String> el = new SingleElement<>(obj);
		assertSame(obj, el.get());
		Iterator<String> it = el.iterator();
		assertTrue(it.hasNext());
		assertSame(obj, it.next());
		assertFalse(it.hasNext());
		it = el.iterator();
		assertTrue(it.hasNext());
		assertSame(obj, it.next());
		assertFalse(it.hasNext());
	}

}
