package takutility.randomgroups;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SingleIteratorTest {

	private <T> RandomIterator<T> create(T elem) {
		return new SingleElement<>(elem).randomIterator();
	}
	@Test
	public void testHasNext() {
		RandomIterator<Object> el = create(new Object());
		assertTrue(el.hasNext());
		el.next();
		assertFalse(el.hasNext());
	}

	@Test
	public void testNext() {
		Object cont = new Object();
		RandomIterator<Object> el = create(cont);
		assertSame(cont, el.next());
	}

	@Test
	public void testReset() {
		RandomIterator<Object> el = create(new Object());
		el.next();
		assertFalse(el.hasNext());
		el.reset();
		assertTrue(el.hasNext());
		
		el = create(new Object());
		assertTrue(el.hasNext());
		el.reset();
		assertTrue(el.hasNext());

	}
	

}
