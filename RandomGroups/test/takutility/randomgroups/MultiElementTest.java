package takutility.randomgroups;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

public class MultiElementTest {

	@Test
	public void testHasNext() {
		Object o = new Object();
		MultiElement<Object> el = new MultiElement<>(o, 0);
		assertFalse(el.iterator().hasNext());
		
		el = new MultiElement<Object>(o, 1);
		Iterator<Object> it = el.iterator();
		assertTrue(it.hasNext());
		it.next();
		assertFalse(it.hasNext());
		
		el = new MultiElement<Object>(o, 3);
		it = el.iterator();
		assertTrue(it.hasNext());
		it.next();
		assertTrue(it.hasNext());
		it.next();
		assertTrue(it.hasNext());
		it.next();
		assertFalse(it.hasNext());
	}

	@Test
	public void testNext() {
		Object o = new Object();
		MultiElement<Object> el = new MultiElement<>(o, 5);
		for (Object eo : el) {
			assertSame(o, eo);
		}
	}

	@Test
	public void testHasPrevious() {
		Object o = new Object();
		MultiElement<Object> el = new MultiElement<>(o, 0);
		assertFalse(el.iterator().hasNext());
		
		el = new MultiElement<Object>(o, 3);
		RandomListIterator<Object> it = el.randomListIterator();
		assertFalse(it.hasPrevious());
		it.next();
		assertTrue(it.hasPrevious());
	}

	@Test
	public void testPrevious() {
		Object o = new Object();
		MultiElement<Object> el = new MultiElement<>(o, 3);
		RandomListIterator<Object> it = el.randomListIterator();
		it.next();
		it.next();
		it.next();
		assertSame(o, it.previous());
		assertSame(o, it.previous());
		assertSame(o, it.previous());
	}

	@Test
	public void testNextIndex() {
		Object o = new Object();
		MultiElement<Object> el = new MultiElement<>(o, 3);
		RandomListIterator<Object> it = el.randomListIterator();
		assertEquals(0, it.nextIndex());
		it.next();
		assertEquals(1, it.nextIndex());
		it.next();
		assertEquals(2, it.nextIndex());
		it.next();
		assertEquals(3, it.nextIndex());
	}

	@Test
	public void testPreviousIndex() {
		Object o = new Object();
		MultiElement<Object> el = new MultiElement<>(o, 3);
		RandomListIterator<Object> it = el.randomListIterator();
		assertEquals(-1, it.previousIndex());
		it.next();
		assertEquals(0, it.previousIndex());
		it.next();
		assertEquals(1, it.previousIndex());
		it.next();
		assertEquals(2, it.previousIndex());
	}

}
