package takutility.randomgroups;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class RandomIteratorBaseLayerTest {
	private Random rand;
	
	@Before
	public void setUp() throws Exception {
		rand = new Random(0);
	}
	
	@Test
	public void testHasNext() {
		DefaultRandomIterator<Integer> rc = DefaultRandomIterator.of(rand, 1, 2, 3);
		assertTrue(rc.hasNext());
		rc.next();
		assertTrue(rc.hasNext());
		rc.next();
		assertTrue(rc.hasNext());
		rc.next();
		assertFalse(rc.hasNext());
	}

	@Test
	public void testNext() {
		Collection<Integer> list = new HashSet<>(Arrays.asList(1,2,3,4,5));
		DefaultRandomIterator<Integer> rc = DefaultRandomIterator.of(list, rand);
		while(rc.hasNext()) {
			assertTrue(list.remove(rc.next()));
		}
		assertTrue(list.isEmpty());
	}

	@Test
	public void testReset() {
		Collection<Integer> list = new HashSet<>(Arrays.asList(1,2,3,4,5));
		DefaultRandomIterator<Integer> rc = DefaultRandomIterator.of(list, rand);
		while(rc.hasNext()) {
			rc.next();
		}
		assertFalse(rc.hasNext());
		rc.reset();
		assertTrue(rc.hasNext());
		int c=0;
		while(rc.hasNext()) {
			rc.next();
			c++;
		}
		assertEquals(list.size(), c);
	}

}
