package takutility.randomgroups;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class RandomIterableTest {

	@Test
	public void testBaseLayer() {
		List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
		RandomIterable<Integer> rc = RandomIterable.of(list);
		for (Integer i : rc) {
			assertTrue(list.remove(i));
		}
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void testDoubleLayer() {
		List<Integer> list = new ArrayList<>();
		RandomIterable<Integer> rc = new RandomIterable<>();
		create(list, rc, 1,2,3,4);
		create(list, rc, 5,6);
		create(list, rc, 7,8,9);
		create(list, rc, 10);
		create(list, rc, 11,12,13);
		
		for (Integer i : rc) {
			assertTrue(list.remove(i));
		}
		assertTrue(list.isEmpty());
	}

	private <T> RandomElement<T> create(List<T> list, RandomIterable<T> rc, 
			@SuppressWarnings("unchecked") T... els) 
	{
		for (T e : els) {
			list.add(e);
		}
		RandomElement<T> re;
		if (els.length == 1)
			re = new SingleElement<T>(els[0]);
		else
			re = RandomIterable.of(els);
		rc.add(re);
		return re;
	}
}
