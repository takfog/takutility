package takutility.randomgroups;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class RandomListIteratorTest {
	private DefaultRandomListIterator<Integer> rc;
	private List<Integer> elements;

	@Before
	public void setUp() {
		Random rand = new Random(0);
		ArrayList<RandomIterator<Integer>> sub = new ArrayList<>();
		sub.add(DefaultRandomIterator.of(rand, 1, 2, 3));
		sub.add(DefaultRandomIterator.of(rand, 4,5,6,7,8,9));
		sub.add(new SingleElement<>(17).randomIterator());
		sub.add(DefaultRandomIterator.of(rand, 10,11));
		sub.add(DefaultRandomIterator.of(rand, 12,13,14,15));
		sub.add(new SingleElement<>(16).randomIterator());
		
		elements = new ArrayList<>(Arrays.asList(
				1, 2, 3,
				4,5,6,7,8,9,
				17,
				10,11,
				12,13,14,15,
				16
			));
		
		rc = new DefaultRandomListIterator<>(sub);
	}

	@Test
	public void testPrevious() {
		ArrayList<Integer> sel = new ArrayList<>();
		while(rc.hasNext()) {
			sel.add(rc.next());
		}
		assertEquals(elements.size(), sel.size());
		for(int i=sel.size()-1; i>=0; i--) {
			assertEquals(sel.get(i), rc.previous());
		}
	}

	@Test
	public void testIteration() {
		Random dir = new Random(1);
		ArrayList<Integer> sel = new ArrayList<>();
		int x = -1;
		for(int i=0; i<50; i++) {
			if (!rc.hasNext() || (rc.hasPrevious() && dir.nextBoolean())) {
				Integer v = rc.previous();
				assertSame(sel.get(x), v);
				x--;
			} else {
				Integer v = rc.next();
				x++;
				if (x < sel.size())
					assertSame(sel.get(x), v);
				else
					sel.add(v);
			}
		}
	}

	@Test
	public void testHasPrevious() {
		assertFalse(rc.hasPrevious());
		rc.next();
		assertTrue(rc.hasPrevious());
	}

	@Test
	public void testNextIndex() {
		assertEquals(0, rc.nextIndex());
		rc.next();
		assertEquals(1, rc.nextIndex());
		rc.next();
		assertEquals(2, rc.nextIndex());
		rc.next();
		assertEquals(3, rc.nextIndex());
		rc.previous();
		assertEquals(2, rc.nextIndex());
		rc.previous();
		assertEquals(1, rc.nextIndex());
		rc.next();
		assertEquals(2, rc.nextIndex());
		rc.next();
		assertEquals(3, rc.nextIndex());
	}

	@Test
	public void testPreviousIndex() {
		assertEquals(-1, rc.previousIndex());
		rc.next();
		assertEquals(0, rc.previousIndex());
		rc.next();
		assertEquals(1, rc.previousIndex());
		rc.next();
		assertEquals(2, rc.previousIndex());
		rc.previous();
		assertEquals(1, rc.previousIndex());
		rc.previous();
		assertEquals(0, rc.previousIndex());
		rc.next();
		assertEquals(1, rc.previousIndex());
	}

}
