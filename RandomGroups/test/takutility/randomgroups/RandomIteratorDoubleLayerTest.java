package takutility.randomgroups;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class RandomIteratorDoubleLayerTest {
	private Random rand;
	private DefaultRandomIterator<Integer> rc;
	private List<Integer> elements;
	
	@Before
	public void setUp() throws Exception {
		rand = new Random(0);
		ArrayList<RandomIterator<Integer>> sub = new ArrayList<>();
		sub.add(DefaultRandomIterator.of(rand, 1, 2, 3));
		sub.add(DefaultRandomIterator.of(rand, 4,5,6,7,8,9));
		sub.add(new SingleElement<>(17).randomIterator());
		sub.add(DefaultRandomIterator.of(rand, 10,11));
		sub.add(DefaultRandomIterator.of(rand, 12,13,14,15));
		sub.add(new SingleElement<>(16).randomIterator());
		
		elements = new ArrayList<>(Arrays.asList(
				1, 2, 3,
				4,5,6,7,8,9,
				17,
				10,11,
				12,13,14,15,
				16
			));
		
		rc = new DefaultRandomIterator<>(sub);
	}
	
	@Test
	public void testHasNext() {
		for(int i=0; i<elements.size(); i++) {
			assertTrue(rc.hasNext());
			rc.next();
		}
		assertFalse(rc.hasNext());
	}

	@Test
	public void testNext() {
		while(rc.hasNext()) {
			assertTrue(elements.remove(rc.next()));
		}
		assertTrue(elements.toString(), elements.isEmpty());
	}

	@Test
	public void testReset() {
		while(rc.hasNext()) {
			rc.next();
		}
		assertFalse(rc.hasNext());
		rc.reset();
		assertTrue(rc.hasNext());
		int c=0;
		while(rc.hasNext()) {
			rc.next();
			c++;
		}
		assertEquals(elements.size(), c);
	}

}
