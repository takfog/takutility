package takutility.randomgroups;

import java.util.NoSuchElementException;

public class MultiIterator<T> implements RandomListIterator<T> {
	private T elem;
	private int index = 0;
	private final int max;
	
	public MultiIterator(T elem, int count) {
		this.elem = elem;
		this.max = count;
	}

	@Override
	public boolean hasNext() {
		return index < max;
	}

	@Override
	public T next() {
		if (index >= max)
			throw new NoSuchElementException();
		index++;
		return elem;
	}

	@Override
	public boolean hasPrevious() {
		return index > 0;
	}

	@Override
	public T previous() {
		if (index <= 0)
			throw new NoSuchElementException();
		index--;
		return elem;
	}

	@Override
	public int nextIndex() {
		return index;
	}

	@Override
	public int previousIndex() {
		return index-1;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void set(T e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(T e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void reset() {
		index = 0;
	}

}