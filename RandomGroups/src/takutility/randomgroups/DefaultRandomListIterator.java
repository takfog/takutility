package takutility.randomgroups;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.stream.Stream;

public class DefaultRandomListIterator<T> extends DefaultRandomIterator<T>
	implements RandomListIterator<T> 
{
	private List<T> selected = new ArrayList<>();
	/**
	 * The index of the next value
	 */
	private int selIndex = 0;
	
	public DefaultRandomListIterator(Iterable<RandomIterator<T>> c, Random rand) {
		super(c, rand);
	}

	public DefaultRandomListIterator(Iterable<RandomIterator<T>> c) {
		super(c);
	}

	public DefaultRandomListIterator(Stream<RandomIterator<T>> c, Random rand) {
		super(c, rand);
	}

	public DefaultRandomListIterator(Stream<RandomIterator<T>> c) {
		super(c);
	}
	
	@Override
	public T next() {
		T next;
		if (selIndex < selected.size()) {
			next = selected.get(selIndex);
		} else {
			next = super.next();
			selected.add(next);
		}
		selIndex++;
		return next;
	}

	@Override
	public boolean hasPrevious() {
		return selIndex > 0;
	}

	@Override
	public T previous() {
		if (selIndex <= 0)
			throw new  NoSuchElementException();
		selIndex--;
		return selected.get(selIndex);
	}

	@Override
	public int nextIndex() {
		return selIndex;
	}

	@Override
	public int previousIndex() {
		return selIndex-1;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void set(T e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(T e) {
		throw new UnsupportedOperationException();
	}

	

}
