package takutility.randomgroups;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DefaultRandomIterator<T> implements RandomIterator<T> {
	private List<RandomIterator<T>> list;
	private int frontier = -1;
	private Random rand = new Random();
	private boolean hasNext = true;

	@SafeVarargs
	public static <T> DefaultRandomIterator<T> of(Random rand, T... elements) {
		return of(Arrays.asList(elements), rand);
	}

	public static <T> DefaultRandomIterator<T> of(Collection<? extends T> c, Random rand) {
		return new DefaultRandomIterator<>(c.stream()
				.map(e -> new SingleIterator<T>(e)), rand);
	}

	public static <T> DefaultRandomIterator<T> of(Collection<? extends T> c) {
		return of(c, null);
	}
	
	public DefaultRandomIterator(Iterable<RandomIterator<T>> c) {
		this(c, null);
	}
	
	public DefaultRandomIterator(Stream<RandomIterator<T>> c) {
		this(c, null);
	}
	
	public DefaultRandomIterator(Stream<RandomIterator<T>> c, Random rand) {
		list = c.collect(Collectors.toList());
		init(rand);
	}

	public DefaultRandomIterator(Iterable<RandomIterator<T>> c, Random rand) {
		list = new ArrayList<>();
		for (RandomIterator<T> el : c) {
			list.add(el);
		}
		init(rand);
	}
	
	private void init(Random rand) {
		this.rand = rand == null ? new Random() : rand;
		if (list.size() > 0)
			moveNext();
		else 
			hasNext = false;
	}
	
	@Override
	public boolean hasNext() {
		return hasNext;
	}
	
	private void moveNext() {
		boolean reset = false;
		do {
			int idx;
			if (frontier <= 0) {
				if (reset) {
					hasNext = false;
					return;
				} else {
					frontier = list.size();
					reset = true;
				}
			}
			
			if (frontier == 1)
				idx = 0;
			else
				idx = rand.nextInt(frontier);
			frontier--;
			if (frontier > 0 && idx != frontier) {
				RandomIterator<T> re = list.get(idx);
				list.set(idx, list.get(frontier));
				list.set(frontier, re);
			}
		} while(frontier < 0 || !list.get(frontier).hasNext()) ;
	}

	@Override
	public T next() {
		if (!hasNext)
			throw new NoSuchElementException();
		RandomIterator<T> el = list.get(frontier);
		T next = el.next();
		moveNext();
		return next;
	}

	@Override
	public void reset() {
		for (RandomIterator<T> el : list) {
			el.reset();
		}
		hasNext = list.size() > 0;
		moveNext();
	}

	@Override
	public String toString() {
		return list.toString();
	}

}
