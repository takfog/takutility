package takutility.randomgroups;

public class SingleIterator<T> extends AbstractSingleIterator<T> {
	private T elem;
	
	public SingleIterator(T elem) {
		this.elem = elem;
	}
	
	@Override
	public T get() {
		return elem;
	}

}
