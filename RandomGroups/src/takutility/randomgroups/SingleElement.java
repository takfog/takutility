package takutility.randomgroups;

public class SingleElement<T> implements RandomElement<T> {
	private T elem;
	
	public SingleElement(T elem) {
		this.elem = elem;
	}
	
	public T get() {
		return elem;
	}
	
	public void set(T elem) {
		this.elem = elem;
	}

	@Override
	public String toString() {
		return elem+"";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((elem == null) ? 0 : elem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SingleElement<?> other = (SingleElement<?>) obj;
		if (elem == null) {
			if (other.elem != null)
				return false;
		} else if (!elem.equals(other.elem))
			return false;
		return true;
	}

	@Override
	public RandomIterator<T> randomIterator() {
		return randomListIterator();
	}
	
	@Override
	public RandomListIterator<T> randomListIterator() {
		return new AbstractSingleIterator<T>() {
			@Override
			public T get() {
				return elem;
			}
		};
	}
}
