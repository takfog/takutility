package takutility.randomgroups;

import java.util.NoSuchElementException;

public abstract class AbstractSingleIterator<T> implements RandomListIterator<T> {
	private boolean active = true;

	public abstract T get();
	
	@Override
	public void reset() {
		active = true;
	}
	
	@Override
	public boolean hasNext() {
		return active;
	}
	
	@Override
	public T next() {
		if (!active)
			throw new NoSuchElementException();
		active = false;
		return get();
	}

	@Override
	public boolean hasPrevious() {
		return !active;
	}

	@Override
	public T previous() {
		if (active)
			throw new NoSuchElementException();
		active = true;
		return get();
	}

	@Override
	public int nextIndex() {
		return active ? 1 : 0;
	}

	@Override
	public int previousIndex() {
		return active ? 0 : -1;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void set(T e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(T e) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		if (active)
			return get()+"";
		else
			return "("+get()+")";
	}
}