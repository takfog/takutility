package takutility.randomgroups;

public class MultiElement<T> extends SingleElement<T> {
	private int count;
	
	public MultiElement(T elem, int count) {
		super(elem);
		this.count = count;
	}

	@Override
	public RandomIterator<T> randomIterator() {
		return randomListIterator();
	}
	
	@Override
	public RandomListIterator<T> randomListIterator() {
		return new MultiIterator<>(get(), count);
	}
}
