package takutility.randomgroups;

import java.util.Iterator;

public interface RandomElement<T> extends Iterable<T> {
	RandomIterator<T> randomIterator();
	RandomListIterator<T> randomListIterator();
	
	@Override
	default Iterator<T> iterator() {
		return randomIterator();
	}
}
