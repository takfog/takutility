package takutility.randomgroups;

import java.util.ListIterator;

public interface RandomListIterator<T> 
	extends RandomIterator<T>, ListIterator<T> {
}
