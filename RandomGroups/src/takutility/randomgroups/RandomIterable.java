package takutility.randomgroups;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomIterable<T> implements RandomElement<T> {
	private Collection<RandomElement<T>> elements;
	private Random rand;
	
	@SafeVarargs
	public static <T> RandomIterable<T> of(T... elements) {
		return of(null, elements);
	}

	@SafeVarargs
	public static <T> RandomIterable<T> of(Random rand, T... elements) {
		return of(rand, Arrays.asList(elements));
	}

	public static <T> RandomIterable<T> of(Collection<? extends T> c) {
		return of(null, c);
	}
	
	public static <T> RandomIterable<T> of(Random rand, Collection<? extends T> c) {
		return new RandomIterable<>(rand, c.stream().map(e -> new SingleElement<>(e)));
	}
	
	public RandomIterable() {
		this((Random)null);
	}
	
	public RandomIterable(Random rand) {
		this(rand, new ArrayList<>());
	}
	
	public RandomIterable(Stream<RandomElement<T>> s) {
		this(null, s);
	}
	
	public RandomIterable(Collection<RandomElement<T>> c) {
		this(c.stream());
	}
	
	public RandomIterable(Random rand, Collection<RandomElement<T>> c) {
		this(rand, c.stream());
	}
	
	public RandomIterable(Random rand, Stream<RandomElement<T>> s) {
		this.rand = rand;
		elements = s.collect(Collectors.toList());
	}
	
	public boolean add(T elem) {
		return add(new SingleElement<>(elem));
	}

	public boolean add(RandomElement<T> elem) {
		return elements.add(elem);
	}
	
	public void addAll(Collection<? extends T> c) {
		for (T e : c) {
			add(e);
		}
	}
	
	public boolean remove(T elem) {
		return remove(new SingleElement<>(elem));
	}

	public boolean remove(RandomElement<T> elem) {
		return elements.remove(elem);
	}
	
	@Override
	public RandomIterator<T> randomIterator() {
		return new DefaultRandomIterator<>(elements.stream()
					.map(RandomElement::randomIterator),
				rand);
	}
	
	@Override
	public RandomListIterator<T> randomListIterator() {
		return new DefaultRandomListIterator<>(elements.stream()
					.map(RandomElement::randomIterator),
				rand);
	}

}
