package takutility.randomgroups;

import java.util.Iterator;

public interface RandomIterator<T> extends Iterator<T> {

	void reset();
}
