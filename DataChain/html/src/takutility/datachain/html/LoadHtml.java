package takutility.datachain.html;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

public class LoadHtml extends AbstractLoad<Document> {

	public LoadHtml() {}

	public LoadHtml(HtmlConfig defParam) {
		super(defParam);
	}

	public LoadHtml(String id) {
		super(id);
	}

	public LoadHtml(String id, HtmlConfig defParam) {
		super(id, defParam);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		super.verify(data);
		data.setCurrentType(Document.class);
	}

	@Override
	public void verifyCli(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(Document.class);
	}

	@Override
	public Document convert(InnerChainParams params, String url, DataPersistence pers) {
		try {
			Connection conn = createConnection(params, url, pers);
			return Jsoup.parse(conn.execute().body());
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		HtmlConfig config = scriptConfig(params);
		return ParseUtils.simpleArgs(args, LoadHtml::new, 
				() -> new LoadHtml(config),
				s -> {
					if ("get".equalsIgnoreCase(s))
						config.setMethodGet(true);
					else if ("post".equalsIgnoreCase(s))
						config.setMethodGet(false);
					return new LoadHtml(config);
				});
	}
}
