package takutility.datachain.html;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiConsumer;
import java.util.function.IntSupplier;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;

import takutility.datachain.ParamElem;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.filter.Converter;
import takutility.datachain.filter.ParametrizedImpl;
import takutility.datachain.in.CommandLineInput;
import takutility.datachain.parse.ParseUtils;

abstract class AbstractLoad<O> extends ParametrizedImpl<HtmlConfig> implements Converter<String, O>, CommandLineInput<String, O> {

	public AbstractLoad() {
		super(null, null);
	}

	public AbstractLoad(HtmlConfig defParam) {
		super(null, defParam);
	}

	public AbstractLoad(String id) {
		super(id, null);
	}

	public AbstractLoad(String id, HtmlConfig defParam) {
		super(id, defParam);
	}

	private void fillConn(String url, InnerChainParams params, DataPersistence pers, Connection conn,
			Map<String, ParamElem> entrySet, BiConsumer<String, String> consumer) 
	{
		for (Entry<String, ParamElem> e : entrySet.entrySet()) {
			consumer.accept(e.getKey(), e.getValue().get(params, url, pers).toString());
		}
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(String.class, this);
	}

	@Override
	public DataContainer get(String params, IntSupplier idSupplier) {
		return new InputDataContainer(idSupplier, Collections.singleton(convert(null, params, null)));
	}

	@Override
	public DataContainer getFromCli(String[] args) {
		return get(args[0], null);
	}

	public Connection createConnection(InnerChainParams params, String url, DataPersistence pers) {
		HtmlConfig config = getParam(params);
		Connection conn = Jsoup.connect(url);
		if (config != null) {
			fillConn(url, params, pers, conn, config.getCookies(), conn::cookie);			
			fillConn(url, params, pers, conn, config.getData(), conn::data);			
			fillConn(url, params, pers, conn, config.getHeaders(), conn::header);			
			if (config.getReferrer() != null)
				conn.referrer(config.getReferrer());
			if (config.usePost())
				conn.method(Method.POST);
		}
		return conn;
	}
	
	@Override
	protected HtmlConfig getParam(InnerChainParams params) {
		if (params == null)
			return getDefaultParam();
		return HtmlConfig.merge(super.getParam(params), getDefaultParam());
	}

//	@ScriptParse
//	public static Object parse(String args, List<String> params) {
//		scriptConfig(params);
//		return ParseUtils.simpleArgs(args, LoadWeb::new, 
//				() -> new LoadWeb(config),
//				s -> {
//					if ("get".equalsIgnoreCase(s))
//						config.setMethodGet(true);
//					else if ("post".equalsIgnoreCase(s))
//						config.setMethodGet(false);
//					return new LoadWeb(config);
//				});
//	}

	protected static HtmlConfig scriptConfig(List<String> params) {
		HtmlConfig config = new HtmlConfig();
		for (String param : params) {
			String[] split = ParseUtils.argsToArray(param);
			switch (split[0].toLowerCase()) {
			case "header":
				config.addHeader(split[1], ParamElem.deserialize(split[2]));
				break;
			case "cookie":
				config.addCookie(split[1], ParamElem.deserialize(split[2]));
				break;
			case "data":
				config.addData(split[1], ParamElem.deserialize(split[2]));
				break;
			case "referrer":
				config.setReferrer(split[1]);
				break;
//			case "proxy":
//				break;
			case "get":
				config.setMethodGet(true);
				break;
			case "post":
				config.setMethodGet(false);
				break;
			}
		}
		return config;
	}
}
