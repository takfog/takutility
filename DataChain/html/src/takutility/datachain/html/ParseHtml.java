package takutility.datachain.html;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.filter.Converter;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class ParseHtml implements Converter<String, Document> {

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(String.class, this);
		data.setCurrentType(Document.class);
	}

	@Override
	public Document convert(InnerChainParams params, String in, DataPersistence pers) {
		return Jsoup.parse(in);
	}

}
