package takutility.datachain.html;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Element;

import takutility.datachain.ParamElem;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InnerDataContainer;
import takutility.datachain.filter.FullConverter;
import takutility.datachain.parse.ScriptParse;

public class HtmlAttribute implements FullConverter<Element, String> {
	private ParamElem key;
	
	public HtmlAttribute(ParamElem key) {
		this.key = key;
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(Element.class, this);
		data.setCurrentType(String.class);
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		ArrayList<Data> attrData = new ArrayList<>(in.get().size());
		for (Data data : in.get()) {
			Element elem = (Element)data.get();
			String attrKey = key.get(params, data).toString();
			if (elem.hasAttr(attrKey)) {
				data.set(elem.attr(attrKey));
				attrData.add(data);
			}
		}
		return new InnerDataContainer(attrData);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return new HtmlAttribute(ParamElem.deserialize(args));
	}
}
