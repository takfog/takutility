package takutility.datachain.html;

import org.jsoup.nodes.Element;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.filter.Converter;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class HtmlText implements Converter<Element, String> {

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(Element.class, this);
		data.setCurrentType(String.class);
	}

	@Override
	public String convert(InnerChainParams params, Element in, DataPersistence pers) {
		return in.text();
	}

}
