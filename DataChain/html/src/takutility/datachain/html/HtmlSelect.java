package takutility.datachain.html;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import takutility.datachain.ParamElem;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InnerDataContainer;
import takutility.datachain.filter.FullConverter;
import takutility.datachain.parse.ScriptParse;

public class HtmlSelect implements FullConverter<Element, Element> {
	private ParamElem path;
	
	public HtmlSelect(ParamElem path) {
		this.path = path;
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(Element.class, this);
		data.setCurrentType(Element.class);
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		ArrayList<Data> selected = new ArrayList<>();
		for (Data data : in.get()) {
			String p = path.get(params, data.get(), data).toString();
			Elements sel = ((Element)data.get()).select(p);
			Iterator<Element> it = sel.iterator();
			if (it.hasNext()) {
				data.set(it.next());
				selected.add(data);
				while (it.hasNext()) {
					Data tmp = in.newData(it.next(), data);
					selected.add(tmp);
				}
			}
		}
		return new InnerDataContainer(selected);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return new HtmlSelect(ParamElem.deserialize(args));
	}
}
