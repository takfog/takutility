package takutility.datachain.html;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import takutility.datachain.ParamElem;
import takutility.ptr.Lazy;

public class HtmlConfig {
	private Boolean useGet = null;
	private Lazy<Map<String, ParamElem>> header, cookie, data;
	private String referrer;
	
	public static HtmlConfig merge(HtmlConfig main, HtmlConfig def) {
		if (def == null || main == def)
			return main;
		HtmlConfig merge = new HtmlConfig();
		merge.header = mergeMap(main.header, def.header);
		merge.cookie = mergeMap(main.cookie, def.cookie);
		merge.data = mergeMap(main.data, def.data);
		merge.referrer = main.referrer == null ? def.referrer : main.referrer;
		merge.useGet = main.useGet == null ? def.useGet : main.useGet;
		return merge;
	}
	
	private static Lazy<Map<String,ParamElem>> mergeMap(Lazy<Map<String,ParamElem>> main, 
			Lazy<Map<String,ParamElem>> def) 
	{
		if (!def.isCreated())
			return main;
		if (!main.isCreated())
			return def;
		Lazy<Map<String,ParamElem>> merge = new Lazy<>(HashMap::new);
		merge.get().putAll(def.get());
		merge.get().putAll(main.get());
		return merge;
	}
	
	public HtmlConfig() {
		header = new Lazy<>(HashMap::new);
		cookie = new Lazy<>(HashMap::new);
		data = new Lazy<>(HashMap::new);
	}
	
	public boolean useGet() {
		return useGet != Boolean.FALSE;
	}
	
	public boolean usePost() {
		return useGet == Boolean.FALSE;
	}
	
	public void setMethodGet(boolean useGet) {
		this.useGet = useGet;
	}
	
	public Map<String, ParamElem> getHeaders() {
		return header.getOrDefault(Collections::emptyMap);
	}
	
	public void addHeader(String key, ParamElem value) {
		header.get().put(key, value);
	}
	
	public Map<String, ParamElem> getCookies() {
		return cookie.getOrDefault(Collections::emptyMap);
	}
	
	public void addCookie(String key, ParamElem value) {
		cookie.get().put(key, value);
	}
	
	public Map<String, ParamElem> getData() {
		return data.getOrDefault(Collections::emptyMap);
	}
	
	public void addData(String key, ParamElem value) {
		data.get().put(key, value);
	}
	
	public String getReferrer() {
		return referrer;
	}
	
	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}
	
}
