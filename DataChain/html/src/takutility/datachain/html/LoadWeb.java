package takutility.datachain.html;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.nodes.Document;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

public class LoadWeb extends AbstractLoad<String> {

	public LoadWeb() {}

	public LoadWeb(HtmlConfig defParam) {
		super(defParam);
	}

	public LoadWeb(String id) {
		super(id);
	}

	public LoadWeb(String id, HtmlConfig defParam) {
		super(id, defParam);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		super.verify(data);
		data.setCurrentType(Document.class);
	}

	@Override
	public void verifyCli(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(Document.class);
	}

	@Override
	public String convert(InnerChainParams params, String url, DataPersistence pers) {
		try {
			Connection conn = createConnection(params, url, pers);
			conn.ignoreContentType(true);
			return conn.execute().body();
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		HtmlConfig config = scriptConfig(params);
		return ParseUtils.simpleArgs(args, LoadWeb::new, 
				() -> new LoadWeb(config),
				s -> {
					if ("get".equalsIgnoreCase(s))
						config.setMethodGet(true);
					else if ("post".equalsIgnoreCase(s))
						config.setMethodGet(false);
					return new LoadWeb(config);
				});
	}
}
