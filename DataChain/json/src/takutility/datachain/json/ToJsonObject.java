package takutility.datachain.json;
import org.json.JSONObject;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.filter.Converter;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class ToJsonObject implements Converter<String, JSONObject> {

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(String.class, this);
		data.setCurrentType(JSONObject.class);
	}

	@Override
	public JSONObject convert(InnerChainParams params, String in, DataPersistence pers) {
		return new JSONObject(in);
	}

}
