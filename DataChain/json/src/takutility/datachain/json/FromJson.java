package takutility.datachain.json;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import takutility.datachain.chain.ChainType;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.filter.Converter;
import takutility.datachain.parse.ScriptParse;

public class FromJson<I, O> implements Converter<I, O> {
	private static final Pattern arrRegex = Pattern.compile("^\\[(\\d+)\\]/?");
	private static final Pattern objRegex = 
			Pattern.compile("^(?:\"((?:\\\\|\\\"|[^\\\\])+)\"|([^\\[\"/][^/\"\\[]*))(?:$|/|(?=\\[))");

	private List<Object> steps;
	
	public static <O> FromJson<JSONObject,O> object(String path) {
		if (path.startsWith("["))
			throw new IllegalArgumentException("not an object path");
		return new FromJson<>(path);
	}
	
	public static <O> FromJson<JSONObject,O> array(String path) {
		if (!path.startsWith("["))
			throw new IllegalArgumentException("not an array path");
		return new FromJson<>(path);
	}
	
	private FromJson(String path) {
		steps = new ArrayList<>();
		for(int i=0; i<path.length(); ) {
			if (path.charAt(i) == '[') {
				//array
				Matcher m = arrRegex.matcher(path);
				m.region(i, path.length());
				m.useAnchoringBounds(true);
				if (!m.find())
					throw new IllegalArgumentException("Invalid sequence at "+i+": "+path.substring(i));
				Integer idx = Integer.parseInt(m.group(1));
				steps.add(idx);
				i = m.end();
			} else {
				//object
				Matcher m = objRegex.matcher(path);
				m.region(i, path.length());
				m.useAnchoringBounds(true);
				if (!m.find())
					throw new IllegalArgumentException("Invalid sequence at "+i+": "+path.substring(i));
				if (m.group(1) != null) {
					String step = m.group(1);
					step = step.replace("\\\"", "\"").replace("\\\\", "\\");
					steps.add(step);
				} else {
					steps.add(m.group(2));
				}
				i = m.end();
			}
		}
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		if (steps.get(0).getClass() == String.class)
			data.verifyType(JSONObject.class, this);
		else
			data.verifyType(JSONArray.class, this);
		data.setCurrentType(ChainType.ANY);
	}

	@SuppressWarnings("unchecked")
	@Override
	public O convert(InnerChainParams params, I in, DataPersistence pers) {
		Object elem = in;
		for (Object step : steps) {
			if (step.getClass() == Integer.class) {
				elem = ((JSONArray)elem).get(((Integer)step).intValue());
			} else {
				elem = ((JSONObject)elem).get(step.toString());
			}
		}
		return (O)elem;
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return new FromJson<>(args);
	}
}
