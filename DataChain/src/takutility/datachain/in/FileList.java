package takutility.datachain.in;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.IntSupplier;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InnerDataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.filter.FullConverter;
import takutility.datachain.in.FileList.FileListConfig;
import takutility.datachain.parse.ScriptParse;

/**
 * Input element that generate a list of files as specified in the
 * {@link FileListConfig} input object.
 */
@ScriptParse
public class FileList implements CommandLineInput<FileListConfig,File>, FullConverter<FileListConfig, File> {

	public static FullConverter<File, File> convertFile(boolean recursive) {
		FileList conv = new FileList();
		return new FullConverter<File, File>() {
			@Override
			public DataContainer fullConvert(InnerChainParams p, DataContainer d) {
				d.dataStream().forEach(v -> v.set(new FileListConfig((File)v.get(), recursive)));
				return conv.fullConvert(p,d);
			};
			
			@Override
			public void verify(VerificationData data) throws ChainVerificationException {
				data.verifyType(File.class, conv);
			}
		};
	}
	
	@Override
	public DataContainer get(FileListConfig config, IntSupplier idSupplier) {
		List<File> files;
		files = get(config);
		return new InputDataContainer(idSupplier, files);
	}
	
	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		Collection<Data> newData = new ArrayList<>();
		for (Data data : in.get()) {
			FileListConfig config = data.get();
			List<File> files = get(config);
			if (files.size() > 0) {
				data.set(files.get(0));
				newData.add(data);
				for (int i = 1; i < files.size(); i++) {
					newData.add(in.newData(files.get(i), data));
				}
			}
		}
		return new InnerDataContainer(newData);
	}

	private List<File> get(FileListConfig config) {
		List<File> files;
		if (config.base.isDirectory()) {
			files = new ArrayList<>();
			getRecursive(config, config.base, files);
		} else if(config.base.isFile()) {
			files = Collections.singletonList(config.base);
		} else {
			files = Collections.emptyList();
		}
		return files;
	}
	
	private void getRecursive(FileListConfig config, File dir, List<File> files) {
		for (File file : dir.listFiles()) {
			if (file.isDirectory()) {
				if (config.includeFolders)
					files.add(file);
				if (config.recursive)
					getRecursive(config, file, files);
			} else if (config.includeFiles) {
				files.add(file);
			}
		}
	}
	
	@Override
	public DataContainer getFromCli(String[] args) {
		String file = args[0];
		boolean recursive = file.endsWith(">");
		if (recursive)
			file = file.substring(0, file.length()-1);
		FileListConfig config = new FileListConfig(new File(file), recursive);
		if (args.length > 1) {
			switch (args[1].toUpperCase()) {
			case "A":
				config.setInclude(true, true);
				break;
			case "D":
				config.setInclude(false, true);
				break;
			case "F":
				config.setInclude(true, false);
				break;
			}
		}
		return get(config, null);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(FileListConfig.class, this);
		data.setCurrentType(File.class);
	}
	
	@Override
	public void verifyCli(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(File.class);
	}
	
	/**
	 * Configuration class for {@link FileList}.
	 * If the file is a regulare file, it will be the only element of the chain.
	 * If it's a folder, all its content will be included, possibly recursively.
	 * The file list can contain folder, files or both depending on the filters.
	 */
	public static class FileListConfig {
		private File base;
		private boolean recursive;
		private boolean includeFolders;
		private boolean includeFiles = true;

		public FileListConfig(File base) {
			this(base, false, false);
		}

		public FileListConfig(File base, boolean recursive) {
			this(base, recursive, false);
		}

		public FileListConfig(File base, boolean recursive, boolean includeFolders) {
			this.base = base;
			this.recursive = recursive;
			this.includeFolders = includeFolders;
		}
		
		public FileListConfig(File base, boolean recursive, boolean includeFolders, boolean includeFiles) {
			this.base = base;
			this.recursive = recursive;
			this.includeFolders = includeFolders;
			this.includeFiles = includeFiles;
		}
		
		public FileListConfig(String base, String recursive, String includeFolders, String includeFiles) {
			this(new File(base), Boolean.parseBoolean(recursive),
					Boolean.parseBoolean(includeFolders), 
					Boolean.parseBoolean(includeFiles));
		}

		public File getBase() {
			return base;
		}

		public void setBase(File base) {
			this.base = base;
		}

		public boolean isRecursive() {
			return recursive;
		}

		public void setRecursive(boolean recursive) {
			this.recursive = recursive;
		}

		public boolean includesFolders() {
			return includeFolders;
		}
		
		public void setInclude(boolean files,boolean folders) {
			includeFiles = files;
			includeFolders = folders;
		}

		public void setIncludeFolders(boolean includeFolders) {
			this.includeFolders = includeFolders;
		}

		public boolean includesFiles() {
			return includeFiles;
		}

		public void setIncludeFiles(boolean includeFiles) {
			this.includeFiles = includeFiles;
		}

		@Override
		public String toString() {
			char incLetter = 'X';
			if (includeFiles) {
				if (includeFolders) {
					incLetter = 'A';
				} else {
					incLetter = 'F';
				}
			} else if (includeFolders) {
				incLetter = 'D';
			}
			return base.getAbsolutePath() 
					+ (recursive ? File.separator+"*" : "")
					+ " ["+incLetter+"]";
		}
	}

}
