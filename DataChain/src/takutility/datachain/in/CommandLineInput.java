package takutility.datachain.in;

import java.util.Arrays;
import java.util.List;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

public interface CommandLineInput<P, T> extends Input<P, T> {
	DataContainer getFromCli(String args[]);
	
	void verifyCli(VerificationData data) throws ChainVerificationException;
	
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		String[] arr = ParseUtils.argsToArray(args);
		try {
			Class<?> clazz = Class.forName(arr[0]);
			CommandLineInput<?,?> inst = (CommandLineInput<?,?>)clazz.newInstance();
			return (Input<?,?>)((p,id) -> 
				inst.getFromCli(Arrays.copyOfRange(arr, 1, args.length()))
			);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}
}
