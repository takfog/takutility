package takutility.datachain.in;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.net.URL;
import java.util.Collections;
import java.util.function.IntSupplier;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.filter.Converter;
import takutility.datachain.parse.ScriptParse;

/**
 * Element that uses the content of the url as chain data.
 * Can be used either as input as a converter.
 */
@ScriptParse
public class ReadUrl implements Converter<String, String>, CommandLineInput<String, String> {

	private String readUrl(String url) {
		StringBuilder sb = new StringBuilder();
		try (Reader r = new InputStreamReader(new URL(url).openStream())) {
			char[] buffer = new char[4096];
			int n;
			while ((n = r.read(buffer)) > 0) {
			    sb.append(buffer, 0, n);
			}
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
		return sb.toString();
	}

	@Override
	public String convert(InnerChainParams params, String in, DataPersistence pers) {
		return readUrl(in);
	}

	@Override
	public DataContainer get(String file, IntSupplier idSupplier) {
		String content = readUrl(file);
		return new InputDataContainer(idSupplier, Collections.singleton(content));
	}

	@Override
	public DataContainer getFromCli(String[] args) {
		return get(args[0], null);
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(String.class, this);
		data.setCurrentType(String.class);
	}
	
	@Override
	public void verifyCli(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(String.class);
	}
}
