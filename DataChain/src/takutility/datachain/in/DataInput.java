package takutility.datachain.in;

import java.util.Collection;
import java.util.function.IntSupplier;

import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InnerDataContainer;
import takutility.datachain.parse.ScriptParse;

/**
 * Input element that uses the collection of chain data objects
 * given in input directly as output. For internal use. 
 * @param <T> type of the elements contained in the {@link Data} objects
 */
@ScriptParse
public class DataInput<T> implements Input<Collection<Data>, T> {

	@Override
	public DataContainer get(Collection<Data> data, IntSupplier idSupplier) {
		return new InnerDataContainer(data);
	}
}
