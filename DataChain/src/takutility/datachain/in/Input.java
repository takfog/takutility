package takutility.datachain.in;

import java.util.function.IntSupplier;

import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.chain.link.Verifiable;
import takutility.datachain.data.DataContainer;

public interface Input<P,T> extends Verifiable  {
	default DataContainer get(ChainParams<P> params) {
		return get(params.getInput(), params.getIdSupplier());
	}

	DataContainer get(P params, IntSupplier idSupplier);
	
	@Override
	default void verify(VerificationData data) throws ChainVerificationException {
	}
}
