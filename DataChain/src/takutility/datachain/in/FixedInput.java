package takutility.datachain.in;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.IntSupplier;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.util.CollectionType;

/**
 * Input element that uses the collection given to the constructor
 * directly as output. <br>
 * <b>Script syntax:</b><br>
 * the params will be the otput of the element. If not set, 
 * the output will be the argument element only. 
 * @param <T> type of the elements
 */
public class FixedInput<T> implements CommandLineInput<Void, T> {
	private Collection<T> cont;
	
	public FixedInput(Collection<T> cont) {
		this.cont = cont;
	}

	@Override
	public DataContainer get(Void params, IntSupplier idSupplier) {
		return new InputDataContainer(idSupplier, cont);
	}

	@Override
	public DataContainer getFromCli(String[] args) {
		return new InputDataContainer(cont);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(CollectionType.get(cont));
	}
	
	@Override
	public void verifyCli(VerificationData data) throws ChainVerificationException {
		verify(data);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		if (params.size() > 0)
			return new FixedInput<String>(params);
		else
			return new FixedInput<String>(Collections.singleton(args));
	}
}
