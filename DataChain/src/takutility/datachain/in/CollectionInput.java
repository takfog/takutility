package takutility.datachain.in;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.IntSupplier;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.parse.ScriptParse;

/**
 * Input element that uses the collection given in input directly as output. 
 * @param <T> type of the elements
 */
@ScriptParse
public class CollectionInput<T> implements CommandLineInput<Collection<T>, T> {

	@Override
	public DataContainer get(Collection<T> params, IntSupplier idSupplier) {
		return new InputDataContainer(idSupplier, params);
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataContainer getFromCli(String[] args) {
		return get((Collection<T>)Arrays.asList(args), null);
	}
	
	@Override
	public void verifyCli(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(String.class);
	}
}
