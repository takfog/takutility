package takutility.datachain.in;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.function.IntSupplier;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.parse.ScriptParse;

/**
 * Input element that uses the lines of the input file as chain data.
 */
@ScriptParse
public class FileLines implements CommandLineInput<File,String> {

	@Override
	public DataContainer get(File file, IntSupplier idSupplier) {
		if (!file.isFile())
			return new InputDataContainer(idSupplier, Collections.emptyList());
		DataContainer lines;
		try(BufferedReader r = new BufferedReader(new FileReader(file))) {
			lines = new InputDataContainer(idSupplier, r.lines());
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
		return lines;
	}

	@Override
	public DataContainer getFromCli(String[] args) {
		return get(new File(args[0]), null);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(File.class, this);
		data.setCurrentType(String.class);
	}
	
	@Override
	public void verifyCli(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(String.class);
	}
}
