package takutility.datachain.in;

import java.util.function.IntSupplier;
import java.util.stream.IntStream;

import takutility.composite.Pair;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.util.PairChainType;

/**
 * Input element that generate the sequence on numbers in the 
 * selected range (extremes included)
 */
@ScriptParse
public class Range implements CommandLineInput<Pair<Integer,Integer>, Integer> {

	@Override
	public DataContainer get(Pair<Integer,Integer> params, IntSupplier idSupplier) {
		return new InputDataContainer(IntStream
				.rangeClosed(params.a(), params.b()).boxed());
	}

	@Override
	public DataContainer getFromCli(String[] args) {
		return get(new Pair<>(Integer.parseInt(args[0]), 
				Integer.parseInt(args[1])), null);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(new PairChainType(Integer.class), this);
		data.setCurrentType(Integer.class);
	}
	
	@Override
	public void verifyCli(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(Integer.class);
	}
}
