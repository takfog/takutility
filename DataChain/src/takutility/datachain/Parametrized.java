package takutility.datachain;

public interface Parametrized<P> {
	public static String defaultId(Parametrized<?> elem) {
		return defaultId(elem.getClass());
	}
	
	@SuppressWarnings("rawtypes")
	public static String defaultId(Class<? extends Parametrized> clazz) {
		return clazz.getSimpleName();
	}
	
	String getLinkId();
	boolean isParamRequired();

}
