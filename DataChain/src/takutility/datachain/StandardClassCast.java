package takutility.datachain;

import java.io.File;
import java.util.regex.Pattern;

import takutility.composite.Pair;

import static takutility.datachain.ClassCast.standard;

public abstract class StandardClassCast {
	
	@SuppressWarnings("rawtypes")
	private static final ClassCast NO_CAST = new ClassCast() {
		@Override
		public Object cast(Object in) {
			return in;
		}
		@Override
		public boolean canCast(Class type) {
			return true;
		}
	};
	
	@SuppressWarnings("unchecked")
	public static <E> ClassCast<E> noCast() {
		return NO_CAST;
	}
	
	public static final TypedClassCast<String> STRING = new TypedClassCast<String>() {
		@Override
		public String cast(Object in) {
			return in.toString();
		}
		@Override
		public boolean canCast(Class<?> type) {
			return true;
		}
		@Override
		public Class<String> castClass() {
			return String.class;
		}
	};
	
	public static final TypedClassCast<File> FILE = standard(File.class);
	
	public static final TypedClassCast<ParamElem> PARAM = new TypedClassCast<ParamElem>() {
		@Override
		public ParamElem cast(Object in) {
			if (in instanceof ParamElem)
				return (ParamElem) in;
			return ParamElem.deserialize(in.toString());
		}
		@Override
		public boolean canCast(Class<?> type) {
			return true;
		}
		@Override
		public Class<ParamElem> castClass() {
			return ParamElem.class;
		}
	};
	
	public static final TypedClassCast<TypedParamElem> TYPED_PARAM = new TypedClassCast<TypedParamElem>() {
		@Override
		public TypedParamElem cast(Object in) {
			if (in instanceof TypedParamElem)
				return (TypedParamElem) in;
			return TypedParamElem.deserialize(in.toString());
		}
		@Override
		public boolean canCast(Class<?> type) {
			return true;
		}
		@Override
		public Class<TypedParamElem> castClass() {
			return TypedParamElem.class;
		}
	};
	
	public static final TypedClassCast<Pattern> PATTERN = new TypedClassCast<Pattern>() {
		@Override
		public Pattern cast(Object in) {
			if (in instanceof Pattern)
				return (Pattern) in;
			return Pattern.compile(in.toString());
		}
		@Override
		public boolean canCast(Class<?> type) {
			return true;
		}
		@Override
		public Class<Pattern> castClass() {
			return Pattern.class;
		}
	};

	public static final <A,B> ClassCast<Pair<A,A>> pair(TypedClassCast<A> cast) {
		return pair(cast, cast);
	}

	public static final <A,B> ClassCast<Pair<A,B>> pair(TypedClassCast<A> castA, TypedClassCast<B> castB) {
		return pair(castA.castClass(), castA, castB.castClass(), castB);
	}
	
	public static final <A,B> ClassCast<Pair<A,B>> pair(Class<A> classA, ClassCast<A> castA, Class<B> classB, ClassCast<B> castB) {
		return new ClassCast<Pair<A,B>>() {
			@SuppressWarnings("unchecked")
			@Override
			public Pair<A,B> cast(Object in) {
				if (!(in instanceof Pair))
					return null;
				Pair<?,?> inp = (Pair<?, ?>) in;
				if (classA.isInstance(inp.a())) {
					if (classB.isInstance(inp.b()))
						return (Pair<A,B>) inp;
					else
						return new Pair<>(classA.cast(inp.a()), castB.cast(inp.b().toString()));
				} else {
					if (classB.isInstance(inp.b()))
						return new Pair<>(castA.cast(inp.a().toString()), classB.cast(inp.b()));
					else
						return new Pair<>(castA.cast(inp.a().toString()), castB.cast(inp.b().toString()));
				}
			}
			@Override
			public boolean canCast(Class<?> type) {
				return Pair.class.isAssignableFrom(type);
			}
		};
	}
	
	
	
	private StandardClassCast() {}
}
