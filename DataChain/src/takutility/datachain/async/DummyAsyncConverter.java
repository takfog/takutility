package takutility.datachain.async;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;

public class DummyAsyncConverter<I, O> extends AsyncConverter<I, O> {

	@Override
	public void run() {
		next();
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {}

}
