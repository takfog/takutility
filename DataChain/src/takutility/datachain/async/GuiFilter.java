package takutility.datachain.async;

import java.util.Collection;

import takutility.datachain.data.Data;
import takutility.datachain.data.InnerDataContainer;
import takutility.datachain.gui.DataSelectionFrame;
import takutility.datachain.gui.DataSelectionFrame.DialogListener;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class GuiFilter<E> extends AsyncConverter<E, E> {

	@Override
	public void run() {
		if (getInput().get().isEmpty()) {
			next();
			return;
		}
		DataSelectionFrame frame = new DataSelectionFrame();
		frame.setData(getInput());
		frame.addDialogListener(new DialogListener() {
			@Override
			public void dialogClosed(Collection<Data> selected, boolean cancelled) {
				if (!cancelled)
					setOutput(new InnerDataContainer(selected));
			}
		});
		frame.setVisible(true);
	}

}
