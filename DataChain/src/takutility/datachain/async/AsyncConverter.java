package takutility.datachain.async;

import java.util.function.BiConsumer;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.out.FullOutput;

public abstract class AsyncConverter<I,O> implements FullOutput<I>, Runnable {
	private DataContainer input;
	private BiConsumer<InnerChainParams, DataContainer> listener = null;
	private InnerChainParams params;
	
	public void setListener(BiConsumer<InnerChainParams, DataContainer> listener) {
		this.listener = listener;
	}
	
	@Override
	public void acceptAll(InnerChainParams params, DataContainer out) {
		this.input = out;
		this.params = params;
		this.run();
	}
	
	public DataContainer getInput() {
		return input;
	}
	
	public InnerChainParams getParams() {
		return params;
	}
	
	public void setOutput(DataContainer output) {
		if (listener != null)
			listener.accept(params, output);
	}
	
	public void next() {
		setOutput(input);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
	}
}
