package takutility.datachain.async;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.filter.FullConverter;

public class ResynchConverter<I, O> extends AsyncConverter<I, O> {
	private FullConverter<I, O> conv;
	
	public ResynchConverter(FullConverter<I, O> conv) {
		this.conv = conv;
	}

	@Override
	public void run() {
		setOutput(conv.fullConvert(getParams(), getInput()));
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		conv.verify(data);
	}
}
