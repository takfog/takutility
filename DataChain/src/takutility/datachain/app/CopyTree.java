package takutility.datachain.app;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import takutility.composite.Pair;
import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.chain.link.DefaultStartLink;
import takutility.datachain.chain.link.StartLink;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.filter.Cast;
import takutility.datachain.filter.FilePath;
import takutility.datachain.filter.MkDir;
import takutility.datachain.filter.RegexReplace;
import takutility.datachain.in.FileList;
import takutility.datachain.in.FileList.FileListConfig;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class CopyTree implements ChainApp<Pair<File,File>, File> {
	private static final String NEW_ROOT = "newRoot";
	private static final String REPLACE = "replace";
	private StartLink<FileListConfig> chain;
	
	public CopyTree() {
		DefaultStartLink<FileListConfig,File> start;
		chain = start = new DefaultStartLink<>(new FileList());
		start
			.join(FilePath.fullPath())
			.join(new RegexReplace(REPLACE))
			.join(Cast.fromInput(File.class, String.class))
			.join(new MkDir())
			.close();
	}
	
	public void run(File src, File dest) {
		FileListConfig input = new FileListConfig(src, true);
		input.setInclude(false, true);
		
		Pattern regex = Pattern.compile("^"+Pattern.quote(src.getAbsolutePath()));
		ChainParams<FileListConfig> params = new ChainParams<>(input);
		params.set(NEW_ROOT, dest);
		params.set(REPLACE, new Pair<>(regex, 
				Matcher.quoteReplacement(dest.getAbsolutePath())));
		
		chain.start(params);
	}
	
	@Override
	public File convert(InnerChainParams params, Pair<File, File> in, DataPersistence pers) {
		run(in.a(), in.b());
		return in.b();
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(File.class, this);
	}
	
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("Usage: source_dir dest_dir");
			return;
		}
		File src = new File(args[0]);
		File dest = new File(args[1]);
		new CopyTree().run(src, dest);
	}
}
