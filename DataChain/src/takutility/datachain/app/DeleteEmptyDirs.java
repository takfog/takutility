package takutility.datachain.app;

import java.io.File;

import takutility.datachain.chain.Chain;
import takutility.datachain.chain.ChainBuilder;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.filter.FileFilter;
import takutility.datachain.filter.Filter;
import takutility.datachain.in.FileList;
import takutility.datachain.in.FileList.FileListConfig;
import takutility.datachain.out.DeleteFile;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class DeleteEmptyDirs implements ChainApp<File, File> {
	private Chain<FileListConfig> chain;
	
	public DeleteEmptyDirs() {
		ChainBuilder<FileListConfig> builder = new ChainBuilder<>();
		builder.open(new FileList())
			.join(FileFilter.directoryOnly())
			.join((Filter<File>)((p,f,x) -> f.list().length == 0))
			.close(new DeleteFile());
		chain = builder.getChain();
	}
	
	public void run(File base) {
		chain.start(new FileListConfig(base, true, true));
	}
	
	@Override
	public File convert(InnerChainParams params, File val, DataPersistence pers) {
		run(val);
		return val;
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(File.class, this);
	}
	
	public static void main(String[] args) {
		if (args.length < 0) {
			System.err.println("Usage: base_folder");
			return;
		}
		new DeleteEmptyDirs().run(new File(args[0]));
	}
}
