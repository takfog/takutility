package takutility.datachain.app;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;

import takutility.composite.Pair;
import takutility.datachain.IdSupplier;
import takutility.datachain.ParamElem;
import takutility.datachain.chain.Chain;
import takutility.datachain.chain.ChainBuilder;
import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.MultiChainBuilder;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.filter.Concat;
import takutility.datachain.filter.FilePath;
import takutility.datachain.filter.Persist;
import takutility.datachain.filter.RegexFilter;
import takutility.datachain.filter.RegexReplace;
import takutility.datachain.filter.ToPair;
import takutility.datachain.in.FileList;
import takutility.datachain.in.FileList.FileListConfig;
import takutility.datachain.out.Zip;
import takutility.datachain.parse.RegexParser;
import takutility.datachain.parse.ScriptParse;

public class Comic2Cbz implements ChainApp<Comic2CbzConfig, File> {
	private static final String 
			PERS_ORIG = "orig",
			PERS_NAME = "filename";
	private static final String 
			ID_SUBFILTER = "subfilter",
			ID_SUBREPLACE = "subrep";
	private static final String VAR_COVER_NAME = "coverName";
	
	private String subfolderRegex;
	private String subfolderReplace;
	private Chain<FileListConfig> fileChain, coverChain;
	
	public Comic2Cbz(String subfolderRegex, String subfolderReplace) {
		this.subfolderRegex = subfolderRegex;
		this.subfolderReplace = subfolderReplace;
		createChain();
	}

	private void createChain() {
		ChainBuilder<FileListConfig> fileBuilder = new ChainBuilder<>();
		fileBuilder.open(new FileList())
				.join(new Persist<>(PERS_ORIG))
				.join(new Persist<>(PERS_NAME, File::getName))
				.join(FilePath.parent())
				.join(new RegexFilter(ID_SUBFILTER))
				.join(new RegexReplace(ID_SUBREPLACE))
				.join(new Concat<>(ParamElem.in, ParamElem.pers(PERS_NAME)))
				;
		ChainBuilder<FileListConfig> coverBuilder = new ChainBuilder<>();
		coverBuilder.open(new FileList())
				.join(new Persist<>(PERS_ORIG))
				.join(FilePath.extension())
				.join(new Concat<>(ParamElem.var(VAR_COVER_NAME), ParamElem.in))
				;

		MultiChainBuilder<String> multiBuilder = new MultiChainBuilder<>(String.class);
		multiBuilder.build()
				.join(ToPair.fillA(File.class, ParamElem.pers(PERS_ORIG)))
				.close(new Zip());
		multiBuilder.attachSources(fileBuilder, coverBuilder);
		
		fileChain = fileBuilder.getChain();
		coverChain = coverBuilder.getChain();
	}
	
	public void run(File input, File coverFile, File output) {
		run(new Comic2CbzConfig(input, coverFile, output));
	}
	
	public void run(Comic2CbzConfig config) {
		IdSupplier idSup = new IdSupplier();
		String base = config.getInput().getAbsolutePath();
		Pattern regex = Pattern.compile(Pattern.quote(base)+subfolderRegex);
		
		FileListConfig in = new FileListConfig(config.getInput(), true, false);
		ChainParams<FileListConfig> params = new ChainParams<>(in, idSup);
		params.set(ID_SUBFILTER, regex);
		params.set(ID_SUBREPLACE, new Pair<>(regex,subfolderReplace));
		params.set(Zip.class, config.getOutput());
		fileChain.start(params);
		params = new ChainParams<>(new FileListConfig(config.getCoverFile()), idSup);
		params.set(VAR_COVER_NAME, config.getCoverName());
		coverChain.start(params);
	}
	
	@Override
	public File convert(InnerChainParams params, Comic2CbzConfig in, DataPersistence pers) {
		run(in);
		return in.getOutput();
	};
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(Comic2CbzConfig.class, this);
		data.setCurrentType(File.class);
	}
	
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return RegexParser.parse(args, Comic2Cbz::new);
	}
	
	public static void main(String[] args) {
		String base = "ignore";
		new Comic2Cbz("[^\\/]*(\\d{3}).*$", "$1-")
		.run(
				new File(base,"comic"),
				new File(base, "comic cover.png"),
				new File(base,"comic\\comic.zip"));
	}
}
