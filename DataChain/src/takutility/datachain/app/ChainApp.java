package takutility.datachain.app;

import java.util.Collections;
import java.util.function.IntSupplier;

import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.in.Input;
import takutility.datachain.out.ProducerOutput;

public interface ChainApp<I, O> extends Input<I, O>, ProducerOutput<I, O> {

	@Override
	default DataContainer get(ChainParams<I> params) {
		O val = convert(params, params.getInput(), DataPersistence.empty());
		return new InputDataContainer(params.getIdSupplier(), 
				Collections.singleton(val));
	}
	
	@Override
	default DataContainer get(I params, IntSupplier idSupplier) {
		return get(new ChainParams<I>(params, idSupplier));
	}
	
	@Override
	default void verify(VerificationData data) throws ChainVerificationException {
	}
}
