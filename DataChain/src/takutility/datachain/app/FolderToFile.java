package takutility.datachain.app;

import java.io.File;

import takutility.composite.Pair;
import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.chain.link.DefaultStartLink;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.filter.FilePath;
import takutility.datachain.in.FileList;
import takutility.datachain.in.FileList.FileListConfig;
import takutility.datachain.out.ToFile;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.util.PairChainType;

@ScriptParse
public class FolderToFile implements ChainApp<Pair<File,File>, File> {
	private DefaultStartLink<FileListConfig, File> start;
	private ToFile close;
	
	public FolderToFile() {
		FileList in = new FileList();
		close = new ToFile();
		
		start = new DefaultStartLink<>(in);
		start.join(FilePath.fileName())
			.close(close);
	}

	public void run(File dir, File out) {
		ChainParams<FileListConfig> params = new ChainParams<>();
		params.setInput(new FileListConfig(dir, false, true));
		params.set(close, out);
		start.start(params);
	}
	
	@Override
	public File convert(InnerChainParams params, Pair<File, File> in, DataPersistence pers) {
		run(in.a(), in.b());
		return in.a();
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(new PairChainType(File.class), this);
		data.setCurrentType(File.class);
	}
	
	public static void main(String[] args) {
		new FolderToFile().run(
				new File("C:/Users/f.debenedictis/Downloads/app/eclipse"),
				new File("text.txt"));
	}
}
