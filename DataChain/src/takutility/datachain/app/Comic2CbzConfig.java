package takutility.datachain.app;

import java.io.File;

public class Comic2CbzConfig {
	private File input;
	private File coverFile;
	private String coverName = "000 cover";
	private File output;

	public Comic2CbzConfig(String input, String coverFile, String output) {
		this(new File(input), new File(coverFile), new File(output));
	}
	
	public Comic2CbzConfig(File input, File coverFile, String coverName, File output) {
		this.input = input;
		this.coverFile = coverFile;
		this.coverName = coverName;
		this.output = output;
	}

	public Comic2CbzConfig(File input, File coverFile, File output) {
		this.input = input;
		this.coverFile = coverFile;
		this.output = output;
	}
	
	public File getInput() {
		return input;
	}
	
	public File getOutput() {
		return output;
	}
	
	public File getCoverFile() {
		return coverFile;
	}
	
	public String getCoverName() {
		return coverName;
	}

	@Override
	public String toString() {
		return "Comic2CbzConfig [input=" + input + ", coverFile=" + coverFile + ", coverName=" + coverName
				+ ", output=" + output + "]";
	}
	
}