package takutility.datachain;

public interface ClassCast<O> {
	
	O cast(Object in);
	boolean canCast(Class<?> type);
	
	public static <O> TypedClassCast<O> standard(Class<O> clazz) {
		return new TypedClassCast<O>() {
			@Override
			public O cast(Object in) {
				if (clazz.isInstance(in))
					return clazz.cast(in);
				try {
					return clazz.getConstructor(in.getClass()).newInstance(in);
				} catch (Exception e) {
					return null;
				}
			}

			@Override
			public boolean canCast(Class<?> type) {
				if (clazz.isAssignableFrom(type))
					return true;
				try {
					clazz.getConstructor(type);
					return true;
				} catch (Exception e) {
					return false;
				}
			}

			@Override
			public Class<O> castClass() {
				return clazz;
			}
		};
	}
}
