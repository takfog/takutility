package takutility.datachain;

public interface TypedClassCast<O> extends ClassCast<O> {
	Class<O> castClass();
}
