package takutility.datachain.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.IntSupplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InnerDataContainer extends DataContainer implements Cloneable 
{
	private Collection<Data> val;
	private IntSupplier idSupplier;

	public InnerDataContainer(Stream<Data> stream) {
		this(stream.collect(Collectors.toList()));
	}

	public InnerDataContainer(Collection<Data> val) {
		this(null, val);
	}

	public InnerDataContainer(IntSupplier idSupplier, Collection<Data> val) {
		this.val = val;
		this.idSupplier = idSupplier == null ? new LocalIdSupplier(val) : idSupplier;
	}
	
	@Override
	public Collection<Data> get() {
		return val;
	}
	
	@Override
	public Data newData(Object obj, Data src) {
		int id = idSupplier.getAsInt();
		if (src == null)
			return new Data(id, obj);
		else
			return src.clone(id, obj);
	}

	@Override
	public String toString() {
		return val.toString();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new InnerDataContainer(idSupplier, new ArrayList<>(val));
	}
	
	private static class LocalIdSupplier implements IntSupplier {
		private int minId;

		public LocalIdSupplier(Collection<Data> val) {
			for (Data data : val) {
				if (minId < data.getId())
					minId = data.getId();
			}
		}
		
		@Override
		public synchronized int getAsInt() {
			minId++;
			return minId;
		}
	}
}
