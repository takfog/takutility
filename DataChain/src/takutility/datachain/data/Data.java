package takutility.datachain.data;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import takutility.datachain.ClassCast;

public class Data implements DataPersistence {
	private int id;
	private Object obj;
	private Map<String, Object> persistent = null;
	
	public Data(int id, Object obj) {
		this.id = id;
		this.obj = obj;
	}
	
	public Data clone(int id, Object obj) {
		Data n = new Data(id,obj);
		if (persistent != null) 
			n.persistent = new HashMap<>(persistent);
		return n;
	}
	
	public int getId() {
		return id;
	}
	
	@SuppressWarnings("unchecked")
	public <E> E get() {
		return (E)obj;
	}
	
	@SuppressWarnings("unchecked")
	public <E> E get(ClassCast<E> classCast) {
		if (classCast == null)
			return (E)obj;
		return classCast.cast(obj);
	}
	
	public void set(Object newVal) {
		obj = newVal;
	}
	
	@SuppressWarnings("unchecked")
	public <T> void persist(String key, Function<T,?> func) {
		if (persistent == null)
			persistent = new HashMap<>();
		persistent.put(key, func.apply((T)obj));
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <E> E getPersisted(String key) {
		if (persistent == null)
			return null;
		return (E)persistent.get(key);
	}
	
	public void recover(String key) {
		obj = persistent.get(key);
	}
	
	@Override
	public int hashCode() {
		return id; //perfect hash assumption in equals()
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (hashCode() != obj.hashCode())
			return false;
		if (getClass() != obj.getClass())
			return false;
		//perfect hash assumption
//		Data other = (Data) obj;
//		if (id != other.id)
//			return false;
		return true;
	}

	@Override
	public String toString() {
		return obj == null ? "null" : obj.toString();
	}
}
