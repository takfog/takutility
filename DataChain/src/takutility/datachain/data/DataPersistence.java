package takutility.datachain.data;

public interface DataPersistence {
	<E> E getPersisted(String key);
	
	public static DataPersistence empty() {
		return new DataPersistence() {
			@Override
			public <E> E getPersisted(String key) {
				return null;
			}
		};
	}
}
