package takutility.datachain.data;

import java.util.Collection;
import java.util.function.IntSupplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import takutility.datachain.IdSupplier;

public class InputDataContainer extends InnerDataContainer {

	private static final Collection<Data> toData(IntSupplier idSupplier, 
			Stream<?> stream) 
	{
		IntSupplier idS = idSupplier == null ? new IdSupplier() : idSupplier;
		return stream
				.map(v -> new Data(idS.getAsInt(), v))
				.collect(Collectors.toList());
	}

	public InputDataContainer(Stream<?> stream) {
		this(null, stream);
	}

	public InputDataContainer(Collection<?> val) {
		this(val.stream());
	}

	public InputDataContainer(IntSupplier idSupplier, Stream<?> stream) {
		super(idSupplier, toData(idSupplier, stream));
	}

	public InputDataContainer(IntSupplier idSupplier, Collection<?> val) {
		this(idSupplier, val.stream());
	}
}
