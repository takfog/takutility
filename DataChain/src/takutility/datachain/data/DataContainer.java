package takutility.datachain.data;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Stream;

import takutility.datachain.ClassCast;

public abstract class DataContainer {
	
	public abstract Collection<Data> get();
	
	public Stream<Data> dataStream() {
		return get().stream();
	}
	
	public <T> Stream<T> stream() {
		return get().stream().map(Data::get);
	}
	
	public <T> Stream<T> stream(ClassCast<T> classCast) {
		return get().stream().map(d -> d.get(classCast));
	}
	
	public <T> void persist(String key, Function<T,?> func) {
		dataStream().forEach(d -> d.persist(key, func));
	}
	
	public void recover(String perKey, String actKey) {
		dataStream().forEach(d -> {
			if (actKey != null)
				d.persist(actKey, Function.identity());
			d.recover(perKey);
		});
	}
	
	public abstract Data newData(Object obj, Data src);
	
	public Data newData(Object obj) {
		return newData(obj, null);
	}
}
