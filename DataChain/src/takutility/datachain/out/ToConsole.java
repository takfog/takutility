package takutility.datachain.out;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ScriptParse;

/**
 * Prints the data to the console.
 * @param <T> the expected type of the data
 */
@ScriptParse
public class ToConsole<T> implements ProducerOutput<T,T> {

	@Override
	public T convert(InnerChainParams params, T val, DataPersistence pers) {
		System.out.println(val);
		return val;
	}

}
