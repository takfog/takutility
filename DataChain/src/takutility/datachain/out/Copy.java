package takutility.datachain.out;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import takutility.composite.Pair;
import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.util.PairChainType;

/**
 * Copies the first file in the input pair to the second one.
 * Returns the output file.
 */
@ScriptParse
public class Copy implements ProducerOutput<Pair<File,File>, File> {

	@Override
	public File convert(InnerChainParams params, Pair<File, File> in, DataPersistence pers) {
		try {
			Files.copy(in.a().toPath(), in.b().toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
		return in.b();
	}
	
	@Override
	public ClassCast<Pair<File, File>> inputAutoCast() {
		return StandardClassCast.pair(StandardClassCast.FILE);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(new PairChainType(File.class), 
				StandardClassCast.pair(StandardClassCast.FILE),
				this);
		data.setCurrentType(File.class);
	}
}
