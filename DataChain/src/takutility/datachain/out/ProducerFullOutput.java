package takutility.datachain.out;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.filter.FullConverter;

public interface ProducerFullOutput<I, O> extends FullOutput<I>, FullConverter<I, O> {

	@Override
	default void acceptAll(InnerChainParams params, DataContainer in) {
		fullConvert(params, in);
	}
}
