package takutility.datachain.out;

import java.io.IOException;

import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class Run implements Output<String> {

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
	}
	
	@Override
	public ClassCast<String> inputAutoCast() {
		return StandardClassCast.STRING;
	}

	@Override
	public void accept(InnerChainParams params, String val, DataPersistence pers) {
		ProcessBuilder pb = new ProcessBuilder(ParseUtils.argsToArray(val));
		try {
			pb.start();
		} catch (IOException e) {
			throw new RuntimeException("Error while running "+val, e);
		}
	}

}
