package takutility.datachain.out;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;

import takutility.composite.Pair;
import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.util.PairChainType;

/**
 * Renames the file in the first position of the pair as the file in the second position.
 */
@ScriptParse
public class Rename implements ProducerOutput<Pair<File,File>, File> {

	@Override
	public File convert(InnerChainParams params, Pair<File, File> val, DataPersistence pers) {
		try {
			Files.move(val.a().toPath(), val.b().toPath());
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
		return val.b();
	}
	
	@Override
	public ClassCast<Pair<File, File>> inputAutoCast() {
		return StandardClassCast.pair(StandardClassCast.FILE);
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(new PairChainType(File.class), inputAutoCast(), this);
		data.setCurrentType(File.class);
	}
}
