package takutility.datachain.out;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.parse.ScriptParse;

/**
 * It does nothing.
 * @param <T> the expected type in the data
 */
@ScriptParse
public class EmptyOutput<T> implements FullOutput<T> {

	@Override
	public void acceptAll(InnerChainParams params, DataContainer in) {
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
	}

}
