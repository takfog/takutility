package takutility.datachain.out;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import takutility.composite.Pair;
import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InnerDataContainer;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.util.PairChainType;

public class Download implements ProducerFullOutput<Pair<String,File>, File> {
	public static enum OnError {
		THROW, SKIP, NULL;
	}
	
	private OnError onError = OnError.THROW;
	
	public Download() {}
	
	public Download(OnError onError) {
		this.onError = onError;
	}
	
	public Download(int retry, OnError onError) {
		
	}
	
	@Override
	public ClassCast<Pair<String, File>> inputAutoCast() {
		return StandardClassCast.pair(StandardClassCast.STRING, StandardClassCast.FILE);
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		Set<Data> keep;
		if (onError == OnError.SKIP)
			keep = Collections.synchronizedSet(new HashSet<>());
		else
			keep = null;
		
		in.dataStream().parallel().forEach(data -> {
			Pair<String,File> p = data.get();
			URL url;
			try {
				url = new URL(p.a());
			} catch (MalformedURLException e) {
				throw new RuntimeException(e);
			}
			try(InputStream is = url.openStream()) {
				Files.copy(is, p.b().toPath());
				data.set(p.b());
				if (keep != null)
					keep.add(data);
			} catch (IOException e) {
				switch (onError) {
				case NULL:
					data.set(null);
					break;
				case SKIP:
					break;
				case THROW:
					throw new UncheckedIOException(e);
				}
			}
		});
		if (keep != null && keep.size() != in.get().size()) {
			ArrayList<Data> arr = new ArrayList<>(keep.size());
			for (Data data : in.get()) {
				if (keep.contains(data))
					arr.add(data);
			}
			return new InnerDataContainer(arr);
		}
		return in;
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(new PairChainType(String.class, File.class),
				inputAutoCast(), this);
		data.setCurrentType(File.class);
	}
	
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		if (args.isEmpty())
			return new Download();
		else if (args.toLowerCase().startsWith("retry ")) {
			String[] split = args.split(" ",3);
			return new Download(Integer.parseInt(split[1]), OnError.valueOf(split[2].toUpperCase()));
		} else
			return new Download(OnError.valueOf(args));
	}
}
