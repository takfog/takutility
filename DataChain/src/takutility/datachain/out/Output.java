package takutility.datachain.out;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.DataPersistence;

public interface Output<T> extends FullOutput<T> {
	@Override
	default void acceptAll(InnerChainParams params, DataContainer data) {
		data.get().stream().forEach(v -> accept(params, v.<T>get(inputAutoCast()), v));
	}
	
	void accept(InnerChainParams params, T val, DataPersistence pers);
}
