package takutility.datachain.out;

import java.io.File;
import java.util.List;

import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

/**
 * Deletes the files given as input. 
 * If recursive is set to false, non empty folders won't be deleted.
 */
@ScriptParse
public class DeleteFile implements Output<File> {
	private boolean recursive = true;
	
	public DeleteFile() {}
	
	public DeleteFile(boolean recursive) {
		this.recursive = recursive;
	}
	
	@Override
	public ClassCast<File> inputAutoCast() {
		return StandardClassCast.FILE;
	}
	
	@Override
	public void accept(InnerChainParams params, File val, DataPersistence pers) {
		if (recursive && val.isDirectory())
			deleteAll(val);
		val.delete();
	}

	private void deleteAll(File dir) {
		for (File f : dir.listFiles()) {
			if (f.isDirectory())
				deleteAll(f);
			f.delete();
		}
	}
	
	public static final Object parse(String args, List<String> params) {
		return ParseUtils.simpleArgs(args, null, DeleteFile::new, 
				s -> new DeleteFile(Boolean.parseBoolean(s)));
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(StandardClassCast.FILE, this);
		data.setCurrentType(File.class);
	}
}
