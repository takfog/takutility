package takutility.datachain.out;

import takutility.datachain.filter.Converter;

public interface ProducerOutput<I, O> extends ProducerFullOutput<I,O>, Converter<I, O> {
}
