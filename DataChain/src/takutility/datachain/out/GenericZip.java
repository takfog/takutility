package takutility.datachain.out;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import takutility.composite.Pair;
import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.filter.ParametrizedImpl;
import takutility.datachain.util.PairChainType;

public class GenericZip<I> extends ParametrizedImpl<File> implements ProducerFullOutput<Pair<I,String>, File> {
	private Function<I,InputStream> toStream;
	
	public GenericZip(String id, File zipFile, Function<I, InputStream> toStream) {
		super(id, zipFile);
		this.toStream = toStream;
	}
	
	@Override
	protected ClassCast<File> paramAutoCast() {
		return StandardClassCast.FILE;
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		File zipFile = getParam(params);
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		boolean end = false;
		try {
			fos = new FileOutputStream(zipFile);
			ZipOutputStream zos2 = zos = new ZipOutputStream(fos);
			in.<Pair<I,String>>stream(inputAutoCast())
					.forEach(f -> addToZipFile(f, zos2));
			end = true;
		} catch(IOException e) {
			throw new UncheckedIOException(e);
		} finally {
			try {
				if (zos != null) zos.close();
			} catch (IOException e) {
				if (end) throw new UncheckedIOException(e);
			}
		}
		
		return new InputDataContainer(Collections.singleton(zipFile));
	}

	private void addToZipFile(Pair<I,String> fileToZip, ZipOutputStream zos) {
		try {
			InputStream fis = toStream.apply(fileToZip.a());
			ZipEntry zipEntry = new ZipEntry(fileToZip.b());
			zos.putNextEntry(zipEntry);

			byte[] buffer = new byte[4096];
			int n;
			while ((n = fis.read(buffer)) > 0) {
			    zos.write(buffer, 0, n);
			}
			
			zos.closeEntry();
			fis.close();
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
	
	protected void verifyZipParam(VerificationData data, Class<I> inputType) throws ChainVerificationException {
		data.verifyType(new PairChainType(inputType, File.class), 
				inputAutoCast(), this);
		verifyParam(data, File.class);
		data.setCurrentType(File.class);
	}
}
