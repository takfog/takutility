package takutility.datachain.out;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.link.Verifiable;
import takutility.datachain.data.DataContainer;
import takutility.datachain.filter.CastableInput;

public interface FullOutput<T> extends Verifiable, CastableInput<T> {
	void acceptAll(InnerChainParams params, DataContainer in);
}
