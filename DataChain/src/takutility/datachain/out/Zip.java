package takutility.datachain.out;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UncheckedIOException;
import java.util.List;

import takutility.composite.Pair;
import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

/**
 * Saves the files in a zip file, given as parameter.
 * The first element of the pair is the source file,
 * teh second is the path insize the zip.<br>
 * <b>Script syntax:</b><br>
 * if the args start with {@code ?}, the rest is the ID of the element,
 * otherwise is the path of the zip file.
 */
public class Zip extends GenericZip<File> {

	public Zip() {
		this(null, null);
	}

	public Zip(String id) {
		this(id, null);
	}

	public Zip(File zipFile) {
		this(null, zipFile);
	}
	
	protected Zip(String id, File zipFile) {
		super(id, zipFile, f -> {
			try {
				return new FileInputStream(f);
			} catch (FileNotFoundException e) {
				throw new UncheckedIOException(e);
			}
		});
	}
	
	@Override
	public ClassCast<Pair<File, String>> inputAutoCast() {
		return StandardClassCast.pair(StandardClassCast.FILE, StandardClassCast.STRING);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		super.verifyZipParam(data, File.class);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return ParseUtils.simpleArgs(args, Zip::new, Zip::new, 
				s -> new Zip(new File(args)));
	}
}
