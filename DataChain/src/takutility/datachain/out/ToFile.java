package takutility.datachain.out;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.List;

import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.filter.ParametrizedImpl;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

/**
 * Saves the data to a file given as parameter.<br>
 * <b>Script syntax:</b><br>
 * if the args start with {@code ?}, the rest is the ID of the element,
 * otherwise is the path of the output file.
 */
public class ToFile extends ParametrizedImpl<File>
	implements ProducerFullOutput<String,File> 
{
	public ToFile() {
		super(null, null);
	}

	public ToFile(File file) {
		super(null, file);
	}

	public ToFile(String id) {
		super(id, null);
	}
	
	@Override
	public ClassCast<String> inputAutoCast() {
		return StandardClassCast.STRING;
	}
	
	@Override
	public ClassCast<File> paramAutoCast() {
		return StandardClassCast.FILE;
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer out) {
		File file = getParam(params);
		try(PrintWriter w = new PrintWriter(file)) {
			out.stream().forEach(w::println);
		} catch (FileNotFoundException e) {
			throw new UncheckedIOException(e);
		}
		return new InputDataContainer(Collections.singleton(file));
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		verifyParam(data, File.class);
		data.setCurrentType(File.class);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return ParseUtils.simpleArgs(args, ToFile::new, ToFile::new, 
				s -> new ToFile(new File(args)));
	}
}
