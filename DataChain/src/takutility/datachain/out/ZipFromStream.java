package takutility.datachain.out;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.function.Function;

import takutility.composite.Pair;
import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

/**
 * Saves the files in a zip file, given as parameter.
 * The first element of the pair is the input stream,
 * teh second is the path insize the zip.<br>
 * <b>Script syntax:</b><br>
 * if the args start with {@code ?}, the rest is the ID of the element,
 * otherwise is the path of the zip file.
 */
public class ZipFromStream extends GenericZip<InputStream> {

	public ZipFromStream() {
		this(null, null);
	}

	public ZipFromStream(String id) {
		this(id, null);
	}

	public ZipFromStream(File zipFile) {
		this(null, zipFile);
	}
	
	protected ZipFromStream(String id, File zipFile) {
		super(id, zipFile, Function.identity());
	}
	
	@Override
	public ClassCast<Pair<InputStream, String>> inputAutoCast() {
		return StandardClassCast.pair(ClassCast.standard(InputStream.class),
				StandardClassCast.STRING);
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		super.verifyZipParam(data, InputStream.class);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return ParseUtils.simpleArgs(args, ZipFromStream::new, ZipFromStream::new, 
				s -> new ZipFromStream(new File(args)));
	}
}
