package takutility.datachain.out;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.parse.ScriptParse;

/**
 * Saves the data internally without elaboration.
 * @param <T> the expected type in the data
 */
@ScriptParse
public class DataStorage<T> implements ProducerFullOutput<T,T> {
	private DataContainer data = null;
	private boolean filled = false;
	
	public DataContainer getData() {
		return data;
	}

	public boolean isFilled() {
		return filled;
	}
	
	public void reset() {
		filled = false;
		data = null;
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		this.data = in;
		filled = true;
		return in;
	}
}
