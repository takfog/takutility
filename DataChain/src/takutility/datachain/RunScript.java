package takutility.datachain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.parse.CommandLineScriptParser;
import takutility.datachain.parse.ScriptOption;
import takutility.datachain.parse.ScriptParser;
import takutility.datachain.parse.ScriptOption.Type;

public class RunScript {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		boolean verify = false;
		int i=0;
		if (args[0].equals("-verify")) {
			verify = true;
			i = 1;
		}
		Map<String, String> argParams = new HashMap<>();
		for(; i<args.length && args[i].startsWith("-P"); i++) {
			String[] p = args[i].split("=",2);
			argParams.put(p[0].substring(2), p.length > 1 ? p[1] : "");
		}
		File script = new File(args[i]);
		
		ScriptParser<String[]> parser = new CommandLineScriptParser();
		parser.load(script);
		if (verify) {
			verify(argParams, parser);
		} else {
			ChainParams<String[]> params = new ChainParams<>();
			for (Entry<String, String> e : argParams.entrySet()) {
				params.set(e.getKey(), 
						e.getValue().isEmpty() ? e.getValue() : true);
			}
			params.setInput(Arrays.copyOfRange(args, i+1, args.length));
			if (checkOptions(parser, params))
				parser.getChain().start(params);
		}
	}

	protected static boolean checkOptions(ScriptParser<String[]> parser, ChainParams<String[]> params) {
		for (ScriptOption opt : parser.getOptions(Type.VAR)) {
			if (!params.contains(opt.value())) {
				System.err.println("Missing parameter: "+opt.value());
				return false;
			}
		}
		return true;
	}

	protected static void verify(Map<String, String> argParams, ScriptParser<String[]> parser) {
		VerificationData data = new VerificationData();
		for (ScriptOption opt : parser.getOptions(Type.VAR)) {
			data.saveVariable(opt.value(), String.class);
		}
		for (ScriptOption opt : parser.getOptions(Type.INPUT)) {
			data.saveVariable(opt.value(), String.class);
		}
		for (Entry<String, String> e : argParams.entrySet()) {
			Class<?> type;
			if (e.getValue().isEmpty())
				type = Boolean.class;
			else 
				type = String.class;
			data.saveVariable(e.getKey(), type);
		}
		try {
			parser.getChain().verify(data);
			System.out.println("The chain is correct");
		} catch (ChainVerificationException e1) {
			e1.printStackTrace();
		}
	}
}
