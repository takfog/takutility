package takutility.datachain.util;

import java.util.function.Function;
import java.util.regex.Pattern;

import takutility.composite.Pair;
import takutility.datachain.ClassCast;
import takutility.datachain.parse.RegexParser;

public class RegexUtil {
	
	public static <E> ClassCast<Pair<Pattern, E>> castReplace(Class<E> secondClass, 
			Function<String, E> secondConv) 
	{
		return new ClassCast<Pair<Pattern,E>>() {
			@SuppressWarnings("unchecked")
			@Override
			public Pair<Pattern, E> cast(Object in) {
				if (in instanceof Pair) {
					Pair<?,?> inp = (Pair<?,?>)in;
					if (inp.a() instanceof Pattern && secondClass.isInstance(inp.b()))
						return (Pair<Pattern, E>)inp;
				}
				return RegexParser.parseCompile(in.toString(), 
						(p,r) -> new Pair<>(p, secondConv.apply(r)));
			}
			@Override
			public boolean canCast(Class<?> type) {
				return true;
			}
		};
	}
}
