package takutility.datachain.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import takutility.datachain.chain.ChainType;

public class CollectionType {
	public static ChainType get(Collection<?> coll) {
		Set<Class<?>> subTypes = new HashSet<>();
		contLoop:
				for (Object e : coll) {
					Class<? extends Object> ec = e.getClass();
					Iterator<Class<?>> it = subTypes.iterator();
					while(it.hasNext()) {
						Class<?> st = it.next();
						if (st == ec || st.isAssignableFrom(ec))
							continue contLoop;
						if (ec.isAssignableFrom(st))
							it.remove();
					}
					subTypes.add(ec);
				}
				if (subTypes.size() == 1)
					return new ChainType(subTypes.iterator().next());
				else
					return ChainType.ANY;
	}
}
