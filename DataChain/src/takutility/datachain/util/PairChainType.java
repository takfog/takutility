package takutility.datachain.util;

import takutility.composite.Pair;
import takutility.datachain.chain.ChainType;

public class PairChainType extends ChainType {

	public PairChainType() {
		super(Pair.class, ChainType.ANY, ChainType.ANY);
	}

	public PairChainType(Class<?> type) {
		super(Pair.class, type, type);
	}

	public PairChainType(Class<?> typeA, Class<?> typeB) {
		super(Pair.class, typeA, typeB);
	}

	public PairChainType(ChainType typeA, ChainType typeB) {
		super(Pair.class, typeA, typeB);
	}

}
