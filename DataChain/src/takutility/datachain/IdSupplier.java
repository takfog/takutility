package takutility.datachain;

import java.util.function.IntSupplier;

public class IdSupplier implements IntSupplier {
	private int id = Integer.MIN_VALUE;
	
	@Override
	public int getAsInt() {
		return id++;
	}
}
