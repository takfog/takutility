package takutility.datachain.chain;

import java.util.HashMap;
import java.util.Map;

import takutility.datachain.Parametrized;

public class InnerChainParams {
	private Map<String, Object> params = new HashMap<>();
	 
	@SuppressWarnings("unchecked")
	public <P> P get(Parametrized<P> elem) {
		String id = elem.getLinkId();
		if (elem.isParamRequired() && !params.containsKey(id))
			throw throwMissing(id);
		return (P)params.get(id);
	}
	
	public boolean contains(Parametrized<?> elem) {
		return params.containsKey(elem.getLinkId());
	}
	
	public boolean contains(String id) {
		return params.containsKey(id);
	}
	 
	@SuppressWarnings("unchecked")
	public <P> P getOrDefault(Parametrized<P> elem, P defVal) {
		String id = elem.getLinkId();
		if (!elem.isParamRequired()) {
			return (P)params.getOrDefault(id, defVal);
		} else if (params.containsKey(id)) {
			return (P)params.get(id);
		} else if (defVal != null) {
			return defVal;
		}
		throw throwMissing(id);
	}

	protected RuntimeException throwMissing(String id) {
		return new IllegalStateException("Missing param: "+id);
	}
	
	public Object get(String id) {
		return params.get(id);
	}
	
	public <P> void set(Parametrized<P> elem, P value) {
		params.put(elem.getLinkId(), value);
	}
	
	@SuppressWarnings("rawtypes")
	public void set(Class<? extends Parametrized> clazz, Object value) {
		params.put(Parametrized.defaultId(clazz), value);
	}

	public void set(String id, Object value) {
		params.put(id, value);
	}
	
	public void setAll(InnerChainParams other) {
		params.putAll(other.params);
	}
	
	@Override
	public String toString() {
		return params.toString();
	}
}
