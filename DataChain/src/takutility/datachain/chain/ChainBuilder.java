package takutility.datachain.chain;

import takutility.datachain.chain.link.ChainBuilding;
import takutility.datachain.chain.link.ChainLink;
import takutility.datachain.chain.link.DefaultStartLink;
import takutility.datachain.chain.link.StartLink;
import takutility.datachain.in.Input;

public class ChainBuilder<I> extends AbstractChainBuilder<I> {
	protected StartLink<I> start = null;

	public ChainBuilder<I> clear() {
		innerClear();
		start = null;
		return this;
	}
	
	public <N> ChainBuilding<N> open(Input<I, N> input) {
		return open(new DefaultStartLink<>(input));
	}

	public <N,C extends StartLink<I> & ChainBuilding<N>> 
		ChainBuilding<N> open(C startLink) 
	{
		setStart(startLink);
		BuilderBuilding<N> bb = new BuilderBuilding<>(startLink);
		this.last = bb;
		return bb;
	}

	protected void setStart(StartLink<I> start) {
		super.setFirst(start);
		this.start = start;
	}

	@Override
	protected void setFirst(ChainLink<I> first) {
		super.setFirst(first);
		if (first == null)
			start = null;
		else if (first instanceof StartLink)
			start = (StartLink<I>) first;
	}

	public Chain<I> getChain() {
		if (start == null) {
			if (getFirst() == null)
				throw new IllegalStateException("Chain not initialized");
			else
				throw new IllegalStateException("This is a subchain");
		}
		return new Chain<I>(start);
	}

	
}
