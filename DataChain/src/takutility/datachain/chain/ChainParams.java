package takutility.datachain.chain;

import java.util.function.IntSupplier;

public class ChainParams<T> extends InnerChainParams {
	private T input;
	private IntSupplier idSupplier;
	
	public ChainParams() {}
	
	public ChainParams(T input) {
		this(input, null);
	}
	
	public ChainParams(T input, IntSupplier idSupplier) {
		this.input = input;
		this.idSupplier = idSupplier;
	}
	
	public T getInput() {
		return (T)input;
	}
	
	public void setInput(T input) {
		this.input = input;
	}
	
	public IntSupplier getIdSupplier() {
		return idSupplier;
	}
	
	public void setIdSupplier(IntSupplier idSupplier) {
		this.idSupplier = idSupplier;
	}
	
	@Override
	public String toString() {
		return input + " " + super.toString();
	}
}
