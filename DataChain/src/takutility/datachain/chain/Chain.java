package takutility.datachain.chain;

import takutility.datachain.chain.link.StartLink;

public class Chain<I> extends AbstractChain<I> implements StartLink<I> {
	
	public Chain(StartLink<I> start) {
		super(start);
	}

	@Override
	public void start(ChainParams<I> params) {
		((StartLink<I>)getStart()).start(params);
	}
}
