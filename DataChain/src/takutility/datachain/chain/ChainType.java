package takutility.datachain.chain;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ChainType {
	public static final ChainType NULL = new ChainType(false);
	public static final ChainType ANY = new ChainType(true);
	
	private Class<?> type;
	private ChainType[] sub = new ChainType[0];
	
	private ChainType(boolean any) {
		type = null;
		if (!any) sub = null;
	}
	
	public ChainType(Class<?> type, Class<?> sub1, Class<?>... sub) {
		this(type, Stream.concat(Stream.of(sub1), Arrays.stream(sub))
				.map(t -> new ChainType(t))
				.toArray(n -> new ChainType[n])
			);
	}

	public ChainType(Class<?> type, ChainType... sub) {
		Objects.requireNonNull(type);
		if (sub == null) sub = new ChainType[0];
		this.type = type;
		this.sub = sub;
	}

	public Class<?> getType() {
		return type;
	}

	public ChainType[] getSubTypes() {
		return sub;
	}

	public ChainType getSubType(int index) {
		return sub[index];
	}
	
	public boolean isNull() {
		return type == null && sub == null;
	}
	
	public boolean isAny() {
		return type == null && sub != null;
	}

	public boolean canBeAssignedTo(ChainType other) {
		if (other == null)
			return isNull();
		return other.isAssignableFrom(this);
	}

	public boolean canBeAssignedTo(Class<?> otherType, Class<?>... otherSub) {
		if (otherType == null)
			return isNull();
		if (isNull())
			return false;
		if (isAny())
			return true;
		if (!otherType.isAssignableFrom(type))
			return false;
		if (sub.length != otherSub.length)
			return sub.length == 0 || otherSub.length == 0;
		for(int i=0; i<sub.length; i++) {
			if (!sub[i].canBeAssignedTo(otherSub[i]))
				return false;
		}
		return true;
	}

	public boolean isAssignableFrom(Class<?> otherType, Class<?>... otherSub) {
		if (otherType == null)
			return isNull();
		if (isNull())
			return false;
		if (isAny())
			return true;
		if (!type.isAssignableFrom(otherType))
			return false;
		if (sub.length != otherSub.length)
			return sub.length == 0 || otherSub.length == 0;
		for(int i=0; i<sub.length; i++) {
			if (!sub[i].isAssignableFrom(otherSub[i]))
				return false;
		}
		return true;
	}

	public boolean isAssignableFrom(ChainType other) {
		if (other == null || other.isNull())
			return isNull();
		if (isNull())
			return false;
		if (isAny() || other.isAny())
			return true;
		if (!type.isAssignableFrom(other.type))
			return false;
		if (sub.length != other.sub.length)
			return sub.length == 0 || other.sub.length == 0;
		for(int i=0; i<sub.length; i++) {
			if (!sub[i].isAssignableFrom(other.sub[i]))
				return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(sub);
		result = prime * result + ((type == null) ? 0 : type.getCanonicalName().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChainType other = (ChainType) obj;
		if (!Arrays.equals(sub, other.sub))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		if (isNull())
			return "null";
		if (isAny())
			return "any";
		switch (sub.length) {
		case 0:
			return type.getCanonicalName();
		case 1:
			return type.getCanonicalName() + "<" + sub[0] + ">";
		default:
			return type.getCanonicalName() + Arrays.stream(sub).map(Object::toString)
				.collect(Collectors.joining(", ","<",">"));
		}
	}
}
