package takutility.datachain.chain;

import takutility.datachain.chain.link.ChainLink;
import takutility.datachain.data.DataContainer;

public abstract class AbstractChain<I> implements ChainLink<I> {
	private ChainLink<I> start;
	
	public AbstractChain(ChainLink<I> start) {
		this.start = start;
	}

	public ChainLink<I> getStart() {
		return start;
	}

	@Override
	public void printChain() {
		start.printChain();
	}

	@Override
	public void next(InnerChainParams params, DataContainer data) {
		start.next(params, data);
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		start.verify(data);
	}
}
