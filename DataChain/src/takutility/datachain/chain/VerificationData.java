package takutility.datachain.chain;

import java.util.HashMap;
import java.util.Map;

import takutility.datachain.ClassCast;
import takutility.datachain.ParamElem;
import takutility.datachain.Parametrized;
import takutility.datachain.TypedClassCast;
import takutility.datachain.TypedParamElem;

public class VerificationData {
	private ChainType currentType = ChainType.NULL;
	private Map<String, ChainType> persTypes, varTypes;
	
	public VerificationData() {
		persTypes = new HashMap<>();
		varTypes = new HashMap<>();
	}
	
	public ChainType getCurrentType() {
		return currentType;
	}
	
	public void setCurrentType(Class<?> type) {
		currentType = new ChainType(type);
	}
	
	public void setCurrentType(Class<?> type, Class<?> subType) {
		currentType = new ChainType(type, subType);
	}

	public void verifyType(Class<?> type, Object obj) throws ChainVerificationException {
		if (!currentType.canBeAssignedTo(type)) {
			throwVerificationException(obj, "invalid input");
		}
	}
	
	public void verifyType(TypedClassCast<?> cast, Object obj) throws ChainVerificationException {
		verifyType(cast.castClass(), cast, obj);
	}
	
	public void verifyType(Class<?> type, ClassCast<?> cast, Object obj) throws ChainVerificationException {
		if (!currentType.canBeAssignedTo(type) && !cast.canCast(type)) {
			throwVerificationException(obj, "invalid input");
		}
	}
	
	public void verifyType(ChainType type, Object obj) throws ChainVerificationException {
		if (!currentType.canBeAssignedTo(type)) {
			throwVerificationException(obj, "invalid input");
		}
	}

	public void verifyType(ChainType type, ClassCast<?> cast, Object obj) throws ChainVerificationException {
		if (!currentType.canBeAssignedTo(type) && !cast.canCast(type.getType())) {
			throwVerificationException(obj, "invalid input");
		}
	}

	private void throwVerificationException(Object obj, String message) throws ChainVerificationException {
		if (obj instanceof Parametrized)
			throw new ChainVerificationException((Parametrized<?>)obj, message);
		else
			throw new ChainVerificationException(obj, message);
	}

	public void verify(TypedParamElem elem, Object obj) throws ChainVerificationException {
		verify(elem, elem.getType(), obj);
	}

	public void verify(ParamElem elem, TypedClassCast<?> classCast, Object obj) throws ChainVerificationException {
		ChainType elemType = getTypeFromParam(elem);
		if (elemType.isNull())
			throwVerificationException(obj, "missing parameter "+elem);
		Class<?> elTypeClass = elemType.getType();
		if (!classCast.canCast(elTypeClass))
			throwVerificationException(obj, "wrong parameter "+elem
					+" - expected "+classCast.castClass()+", found "+elTypeClass);		
	}

	public void verify(ParamElem elem, Class<?> paramType, Object obj) throws ChainVerificationException {
		ChainType elemType = getTypeFromParam(elem);
		if (elemType.isNull())
			throwVerificationException(obj, "missing parameter "+elem);
		Class<?> elTypeClass = elemType.getType();
		if (!paramType.isAssignableFrom(elTypeClass))
			throwVerificationException(obj, "wrong parameter "+elem
					+" - expected "+paramType+", found "+elTypeClass);
	}

	public ChainType getTypeFromParam(ParamElem elem) {
		if (elem.isFixed())
			return new ChainType(elem.get(null).getClass());
		return (ChainType)elem.get(this::getVariable, 
				() -> currentType, this::getPersisted);
	}
	
	public void setCurrentType(ChainType newType) {
		this.currentType = newType == null 
				? ChainType.NULL : newType;
	}
	
	public void persist(String key) {
		persTypes.put(key, currentType);
	}
	
	public ChainType getPersisted(String key) {
		return persTypes.getOrDefault(key, ChainType.NULL);
	}
	
	public boolean containPersisted(String key) {
		return persTypes.containsKey(key);
	}

	public void saveVariable(String key, Class<?> type) {
		varTypes.put(key, new ChainType(type));
	}

	public void saveVariable(String key, Class<?> type, Class<?> sub) {
		varTypes.put(key, new ChainType(type, sub));
	}

	public void saveVariable(String key, ChainType type) {
		varTypes.put(key, type);
	}
	
	public ChainType getVariable(String key) {
		return varTypes.getOrDefault(key, ChainType.NULL);
	}
	
	public ChainType getVariable(Parametrized<?> elem) {
		return getVariable(elem.getLinkId());
	}
	
	public boolean containsVariable(String key) {
		return varTypes.containsKey(key);
	}
	
	public boolean containsVariable(Parametrized<?> elem) {
		return varTypes.containsKey(elem.getLinkId());
	}
	
	@Override
	public String toString() {
		return currentType.toString();
	}
}
