package takutility.datachain.chain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Supplier;

import takutility.datachain.chain.link.ChainBuilding;
import takutility.datachain.chain.link.JoinLink;

public class MultiChainBuilder<T> extends AbstractChainBuilder<T> {
	private Collection<Supplier<ChainBuilding<T>>> sources = new ArrayList<>();
	private Class<T> inputType;
	
	public MultiChainBuilder(Class<T> inputType) {
		this.inputType = inputType;
	}
	
	@SafeVarargs
	public final void addSource(Supplier<ChainBuilding<T>>... newSources) {
		for (Supplier<ChainBuilding<T>> s : newSources) {
			sources.add(s);
		}
	}
	
	@SafeVarargs
	public final void addSource(ChainBuilder<?>...  srcBuilders) {
		for (ChainBuilder<?> s : srcBuilders) {
			sources.add(() -> s.build(inputType));
		}
	}
	
	@SafeVarargs
	public final void addSource(ChainBuilding<T>...  srcLinks) {
		for (ChainBuilding<T> s : srcLinks) {
			sources.add(() -> s);
		}
	}

	@SafeVarargs
	public final void attachSources(Supplier<ChainBuilding<T>>... newSources) {
		addSource(newSources);
		attachSources();
	}
	
	@SafeVarargs
	public final void attachSources(ChainBuilder<?>...  srcBuilders) {
		addSource(srcBuilders);
		attachSources();
	}
	
	@SafeVarargs
	public final void attachSources(ChainBuilding<T>...  srcLinks) {
		addSource(srcLinks);
		attachSources();
	}

	public void attachSources() {
		JoinLink<T> link = new JoinLink<>();
		for (Supplier<ChainBuilding<T>> src : sources) {
			link.addInput(src.get());
		}
		link.attach(getFirst());
	}
}
