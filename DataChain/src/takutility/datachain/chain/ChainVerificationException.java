package takutility.datachain.chain;

import takutility.datachain.Parametrized;

public class ChainVerificationException extends Exception {
	private static final long serialVersionUID = -5385558130000659563L;

	public ChainVerificationException() {}

	public ChainVerificationException(Object obj, String message) {
		this(obj.getClass().getSimpleName()+": "+message);
	}
	
	public ChainVerificationException(Parametrized<?> par, String message) {
		this(par.getLinkId()+": "+message);
	}

	public ChainVerificationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ChainVerificationException(String message) {
		super(message);
	}

	
}
