package takutility.datachain.chain.link;

import takutility.datachain.async.AsyncConverter;
import takutility.datachain.filter.FullConverter;
import takutility.datachain.out.EmptyOutput;
import takutility.datachain.out.FullOutput;

public interface ChainBuilding<T> {

	void attach(ChainLink<T> next);

	default <N,L extends ChainLink<T> & ChainBuilding<N>> ChainBuilding<N> join(L next) {
		attach(next);
		return next;
	}

	default <N> ChainBuilding<N> join(FullConverter<T,N> conv) {
		return this.join(new InnerLink<>(conv));
	}
	
	default <N> ChainBuilding<N> join(AsyncConverter<T,N> conv) {
		return this.join(new AsyncLink<>(conv));
	}

	default void close(ChainLink<T> end) {
		attach(end);
	}

	default void close(FullOutput<T> out) {
		close(new EndLink<>(out));
	}
	
	default void close() {
		close(new EmptyOutput<>());
	}
	
}
