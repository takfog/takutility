package takutility.datachain.chain.link;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;

public interface Verifiable {

	void verify(VerificationData data) throws ChainVerificationException;

}