package takutility.datachain.chain.link;

import takutility.datachain.async.AsyncConverter;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;

public class AsyncLink<I,O> extends AbstractInnerLink<I, O> {
	private AsyncConverter<I, O> converter;
	
	public AsyncLink(AsyncConverter<I, O> converter) {
		this.converter = converter;
		converter.setListener(this::asyncNext);
	}

	@Override
	public void next(InnerChainParams params, DataContainer data) {
		converter.acceptAll(params, data);
	}

	private void asyncNext(InnerChainParams params, DataContainer data) {
		next.next(params, data);
	}
	
	@Override
	public void printChain() {
		PrintableLink.printChain(converter, next);
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		converter.verify(data);
	}
}
