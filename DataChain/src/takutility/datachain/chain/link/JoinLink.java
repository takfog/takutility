package takutility.datachain.chain.link;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InnerDataContainer;
import takutility.datachain.out.DataStorage;

public class JoinLink<T> implements ChainBuilding<T>, PrintableLink {
	private Collection<JoinStorage> ends = new ArrayList<>();
	private ChainLink<T> next;
	private InnerChainParams params;

	@Override
	public void printChain() {
		PrintableLink.printChain(this, next);
	}
	
	public JoinLink<T> addInput(ChainBuilding<T> chain) {
		ends.add(new JoinStorage(chain));
		return this;
	}
	
	public void clearInput() {
		for (JoinStorage st : ends) {
			st.remove();
		}
		ends.clear();
	}
	
	public void setParams(InnerChainParams params) {
		this.params = params;
	}

	private void nextIfReady() {
		for (JoinStorage st : ends) {
			if (!st.isFilled())
				return;
		}
		if (ends.size() == 1) {
			JoinLink<T>.JoinStorage st = ends.iterator().next();
			next.next(params != null ? params : st.params, st.getData());
		} else {
			boolean createParams = false;
			InnerChainParams localPar = params;
			if (params == null) {
				localPar = new InnerChainParams();
				createParams = true;
			}
			Set<Data> concat = new LinkedHashSet<>();
			for (JoinStorage st : ends) {
				concat.addAll(st.getData().get());
				if (createParams)
					localPar.setAll(st.params);
			}
			next.next(localPar, new InnerDataContainer(concat));
		}
		for (JoinStorage st : ends) {
			st.reset();
		}
	}
	
	private class JoinStorage extends DataStorage<T> {
		InnerChainParams params;
		private boolean active = true;
		
		public JoinStorage(ChainBuilding<T> chain) {
			chain.close(this);
		}
		
		private void remove() {
			active = false;
		}

		@Override
		public void acceptAll(InnerChainParams params, DataContainer data) {
			super.acceptAll(params, data);
			if (active) {
				this.params = params;
				nextIfReady();
			}
		}
	}

	@Override
	public void attach(ChainLink<T> next) {
		this.next = next;
	}
}
