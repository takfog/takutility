package takutility.datachain.chain.link;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;

public interface ChainLink<T> extends PrintableLink, Verifiable {
	void next(InnerChainParams params, DataContainer data);
}
