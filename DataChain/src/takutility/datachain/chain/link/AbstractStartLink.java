package takutility.datachain.chain.link;

import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.in.Input;

public abstract class AbstractStartLink<P,T,S, I extends Input<P,T>> 
	implements PrintableLink, ChainBuilding<T>, StartLink<S> 
{
	protected I in;
	private ChainLink<T> next;
	
	public AbstractStartLink(I in) {
		this.in = in;
	}

	@Override
	public void start(ChainParams<S> params) {
		next.next(params, createData(params));
	}

	protected abstract DataContainer createData(ChainParams<S> params);

	@Override
	public void attach(ChainLink<T> next) {
		this.next = next;
	}
	
	@Override
	public void printChain() {
		PrintableLink.printChain(in, next);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		in.verify(data);
		verifyNext(data);
	}

	protected void verifyNext(VerificationData data) throws ChainVerificationException {
		next.verify(data);
	}
}
