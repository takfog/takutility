package takutility.datachain.chain.link;

import java.util.Arrays;

import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.filter.FullConverter;
import takutility.datachain.in.CommandLineInput;

public class ImplicitCommandLineStartLink<T> extends 
	AbstractStartLink<String,T,String[],CommandLineInput<String, T>> 
{
	private FullConverter<String, T> conv;
	
	public ImplicitCommandLineStartLink(FullConverter<String, T> conv) {
		super(null);
		this.conv = conv;
	}

	@Override
	protected DataContainer createData(ChainParams<String[]> params) {
		InputDataContainer data = new InputDataContainer(params.getIdSupplier(), Arrays.asList(params.getInput()));
		return conv.fullConvert(params, data);
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(String.class);
		conv.verify(data);
		verifyNext(data);
	}
}
