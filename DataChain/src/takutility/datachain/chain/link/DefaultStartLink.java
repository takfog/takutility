package takutility.datachain.chain.link;

import takutility.datachain.chain.ChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.in.Input;

public class DefaultStartLink<P,T> extends AbstractStartLink<P, T, P, Input<P,T>> {
	
	public DefaultStartLink(Input<P,T> in) {
		super(in);
	}
	
	@Override
	protected DataContainer createData(ChainParams<P> params) {
		return in.get(params);
	}
}
