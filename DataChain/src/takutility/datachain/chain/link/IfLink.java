package takutility.datachain.chain.link;

import java.util.List;

import takutility.datachain.Parametrized;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.filter.FullConverter;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.parse.ScriptUtils;

public class IfLink<T> extends AbstractInnerLink<T,T>
	implements Parametrized<Boolean>
{
	private String id;
	private FullConverter<T,T> conv;
	
	public IfLink(String id, FullConverter<T,T> conv) {
		this.id = id;
		this.conv = conv;
	}

	@Override
	public void next(InnerChainParams params, DataContainer data) {
		if (params.get(this))
			data = conv.fullConvert(params, data);
		next.next(params, data);
	}

	@Override
	public String getLinkId() {
		return id;
	}

	@Override
	public boolean isParamRequired() {
		return true;
	}

	@Override
	public void printChain() {
		PrintableLink.printChain("If "+id+" "+conv, next);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		FullConverter conv = 
				ScriptUtils.parseSingleSub("", params, FullConverter.class);
		return new IfLink(args, conv);
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.getVariable(this).canBeAssignedTo(Boolean.class);
		conv.verify(data);
	}
}
