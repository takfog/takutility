package takutility.datachain.chain.link;

import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;

public class InputSaveStartLink<T> implements StartLinkDecorator<T> {
	private StartLink<T> sublink;
	private String id; 
	
	public InputSaveStartLink(String id, StartLink<T> sublink) {
		this.id = id;
		this.sublink = sublink;
	}

	@Override
	public void start(T params) {
		start(new ChainParams<T>(params));
	}
	
	@Override
	public void start(ChainParams<T> params) {
		params.set(id, params.getInput());
		sublink.start(params);
	}
	
	@Override
	public StartLink<T> getSublink() {
		return sublink;
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.saveVariable(id, data.getCurrentType());
		sublink.verify(data);
	}
}
