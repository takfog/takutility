package takutility.datachain.chain.link;

import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import takutility.composite.Pair;
import takutility.datachain.IdSupplier;
import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.in.Input;

public class MultiStartLink<P,T> extends JoinLink<T> implements StartLink<Collection<P>>  {
	private Supplier<Input<P,T>> startBuilder;
	
	public MultiStartLink(Supplier<Input<P, T>> startBuilder) {
		this.startBuilder = startBuilder;
	}

	@Override
	public void start(ChainParams<Collection<P>> params) {
		setParams(params);
		Collection<P> inputParams = params.getInput();
		clearInput();
		
		List<Pair<P, DefaultStartLink<P, T>>> starts = inputParams.stream()
			.map(p -> new Pair<>(p, new DefaultStartLink<>(startBuilder.get())))
			.peek(e -> addInput(e.b()))
			.collect(Collectors.toList());
		
		IdSupplier idSupp = new IdSupplier();
		for (Pair<P, DefaultStartLink<P, T>> pair : starts) {
			pair.b().start(new ChainParams<>(pair.a(),idSupp));
		}
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		startBuilder.get().verify(data); //TODO
	}

}
