package takutility.datachain.chain.link;

public interface ChainBuildingLink<I,O> 
	extends ChainLink<I>, ChainBuilding<O>  
{}
