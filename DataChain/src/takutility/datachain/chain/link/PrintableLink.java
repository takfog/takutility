package takutility.datachain.chain.link;

public interface PrintableLink {

	void printChain();

	static void printChain(Object me, PrintableLink next) {
		PrintableLink.printChain(me);
		next.printChain();
	}

	static void printChain(Object me) {
		System.out.println(me);
	}

	static void printChain(Object me, String extra) {
		System.out.println(me+" - "+extra);
	}

}