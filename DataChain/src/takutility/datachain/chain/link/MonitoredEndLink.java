package takutility.datachain.chain.link;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.out.FullOutput;

public class MonitoredEndLink<T> extends EndLink<T> {
	private Runnable listener;
	
	public MonitoredEndLink(FullOutput<T> out, Runnable listener) {
		super(out);
		this.listener = listener;
	}
	
	@Override
	public void next(InnerChainParams params, DataContainer data) {
		super.next(params, data);
		listener.run();
	}

}
