package takutility.datachain.chain.link;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;

public abstract class AbstractInnerLink<I, O> implements ChainBuildingLink<I,O> {
	protected ChainLink<O> next;

	@Override
	public void attach(ChainLink<O> next) {
		this.next = next;
	}

	@Override
	public void printChain() {
		PrintableLink.printChain(this, next);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		next.verify(data);
	}
}