package takutility.datachain.chain.link;

import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.in.CommandLineInput;

public class CommandLineStartLink<P,T> extends 
	AbstractStartLink<P,T,String[],CommandLineInput<P, T>> 
{
	
	public CommandLineStartLink(CommandLineInput<P, T> in) {
		super(in);
	}

	@Override
	protected DataContainer createData(ChainParams<String[]> params) {
		return in.getFromCli(params.getInput());
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		in.verifyCli(data);
		verifyNext(data);
	}
}
