package takutility.datachain.chain.link;

import java.util.Iterator;

import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;

public interface StartLink<P> extends ChainLink<P> {

	@Override
	default void next(InnerChainParams params, DataContainer data) {
		Iterator<Data> it = data.get().iterator();
		if (!it.hasNext())
			return;
		
		ChainParams<P> newPar = new ChainParams<>(it.next().get());
		newPar.setAll(params);
		start(newPar);
	}
	
	default void start(P params) {
		start(new ChainParams<>(params));
	}

	void start(ChainParams<P> params);

}