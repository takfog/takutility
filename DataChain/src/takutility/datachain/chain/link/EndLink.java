package takutility.datachain.chain.link;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.out.FullOutput;

public class EndLink<T> implements ChainLink<T> {
	private FullOutput<T> out;

	public EndLink(FullOutput<T> out) {
		this.out = out;
	}

	@Override
	public void next(InnerChainParams params, DataContainer data) {
		out.acceptAll(params, data);
	}
	
	@Override
	public void printChain() {
		PrintableLink.printChain(out);
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		out.verify(data);
	}
}
