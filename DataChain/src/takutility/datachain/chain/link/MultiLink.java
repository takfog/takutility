package takutility.datachain.chain.link;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;

//TODO make data independent between links (both in next and in verify)
public class MultiLink<T> implements ChainLink<T>, ChainBuilding<T>  {
	private List<ChainLink<T>> nextList = new ArrayList<>();
	
	public MultiLink() {}
	
	@SafeVarargs
	public MultiLink(ChainLink<T>... chainLinks) {
		nextList = new ArrayList<>(chainLinks.length);
		for (ChainLink<T> l : chainLinks) {
			nextList.add(l);
		}
	}

	public MultiLink(Collection<ChainLink<T>> chainLinks) {
		nextList = new ArrayList<>(chainLinks);
	}
	
	@Override
	public void next(InnerChainParams params, DataContainer data) {
		for (ChainLink<T> link : nextList) {
			link.next(params, data);
		}
	}

	@Override
	public void attach(ChainLink<T> next) {
		nextList.add(next);
	}
	
	@Override
	public void printChain() {
		int i=0;
		for (ChainLink<T> chainLink : nextList) {
			i++;
			PrintableLink.printChain(this, " - Chain "+i);
			chainLink.printChain();
		}
		
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		for (ChainLink<T> link : nextList) {
			link.verify(data);
		}
	}

}
