package takutility.datachain.chain.link;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import takutility.datachain.Parametrized;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.filter.FullConverter;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;
import takutility.ptr.Ptr;

public class SwitchLink<I,O,T> extends AbstractInnerLink<I, O>
	implements Parametrized<T>
{
	private String id;
	private Map<T, ? extends FullConverter<I, O>> convs;
	private FullConverter<I, O> defConv = null;

	public SwitchLink(Map<T, ? extends FullConverter<I, O>> convs) {
		this(null, convs, null);
	}

	public SwitchLink(String id, Map<T, ? extends FullConverter<I, O>> convs) {
		this(id, convs, null);
	}

	public SwitchLink(Map<T, ? extends FullConverter<I, O>> convs, FullConverter<I, O> defConv) {
		this(null, convs, defConv);
	}

	public SwitchLink(String id, Map<T, ? extends FullConverter<I, O>> convs, FullConverter<I, O> defConv) {
		this.id = (id != null ? id : Parametrized.defaultId(this));
		this.convs = convs;
		this.defConv = defConv;
	}

	@Override
	public void next(InnerChainParams params, DataContainer data) {
		T key = params.get(this);
		FullConverter<I, O> elem;
		if (key == null || !convs.containsKey(key))
			elem = defConv;
		else 
			elem = convs.get(key);
		next.next(params, elem.fullConvert(params, data));		
	}

	@Override
	public String getLinkId() {
		return id;
	}

	@Override
	public boolean isParamRequired() {
		return defConv == null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		ArrayList<String> cleanParams = new ArrayList<>(params.size());
		Queue<String> keys = new ArrayDeque<>();
		for (String p : params) {
			if (!p.startsWith("-")){
				int i = p.indexOf("=");
				if (i<1)
					keys.add(null);
				else
					keys.add(p.substring(0,i));
				cleanParams.add(p.substring(i+1));
			} else
				cleanParams.add(p);
		}
		Map<String, FullConverter<?, ?>> map = new HashMap<>();
		Ptr<FullConverter<?, ?>> def = new Ptr<>();
		ParseUtils.parse(cleanParams, (e,i) -> {
			Object obj = e.toObject();
			if (!(obj instanceof FullConverter))
				throw new IllegalArgumentException("Not a converter");
			String key = keys.remove();
			if (key == null)
				def.set((FullConverter<?, ?>) obj);
			else
				map.put(key, (FullConverter<?, ?>) obj);
		});
		return new SwitchLink(args.isEmpty() ? null : args, map, def.val);
	}
}
