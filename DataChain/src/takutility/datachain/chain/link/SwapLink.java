package takutility.datachain.chain.link;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;

public class SwapLink<T> implements ChainLink<T> {
	private ChainLink<T> next = null;
	
	public SwapLink() {}
	
	public SwapLink(ChainLink<T> next) {
		this.next = next;
	}

	public ChainLink<T> getNext() {
		return next;
	}

	public void setNext(ChainLink<T> next) {
		this.next = next;
	}

	@Override
	public void next(InnerChainParams params, DataContainer data) {
		if (next != null)
			next.next(params, data);
	}

	@Override
	public void printChain() {
		PrintableLink.printChain(this);
		if (next != null)
			next.printChain();
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		if (next != null)
			next.verify(data);
	}
}
