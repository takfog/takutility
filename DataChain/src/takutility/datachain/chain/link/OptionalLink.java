package takutility.datachain.chain.link;

import java.util.List;

import takutility.datachain.Parametrized;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.filter.FullConverter;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.parse.ScriptUtils;

public class OptionalLink<T,E extends Parametrized<?> & FullConverter<T,T>> 
	extends AbstractInnerLink<T,T> 
{
	private E elem;
	
	public OptionalLink(E elem) {
		this.elem = elem;
	}

	@Override
	public void next(InnerChainParams params, DataContainer data) {
		if (params.contains(elem))
			data = elem.fullConvert(params, data);
		next.next(params, data);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return new OptionalLink((Parametrized) ScriptUtils.parseSingleSub(args, params, 
				FullConverter.class, Parametrized.class));
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		if (data.containsVariable(elem))
			elem.verify(data);
		super.verify(data);
	}
}
