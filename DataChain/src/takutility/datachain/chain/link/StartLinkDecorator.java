package takutility.datachain.chain.link;

import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;

public interface StartLinkDecorator<T> extends StartLink<T> {

	StartLink<T> getSublink();
	
	@Override
	default void printChain() {
		getSublink().printChain();
	}
	
	@Override
	default void start(T params) {
		getSublink().start(params);
	}
	
	@Override
	default void start(ChainParams<T> params) {
		getSublink().start(params);
	}
	
	@Override
	default void verify(VerificationData data) throws ChainVerificationException {
		getSublink().verify(data);
	}
	
	@Override
	default void next(InnerChainParams params, DataContainer data) {
		getSublink().next(params, data);
	}
	
	
}
