package takutility.datachain.chain.link;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.filter.FullConverter;

public class InnerLink<I,O> extends AbstractInnerLink<I, O> {
	private FullConverter<I, O> converter;
	
	public InnerLink(FullConverter<I, O> converter) {
		this.converter = converter;
	}

	@Override
	public void next(InnerChainParams params, DataContainer data) {
		next.next(params, converter.fullConvert(params, data));
	}
	
	@Override
	public String toString() {
		return converter.toString();
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		converter.verify(data);
		next.verify(data);
	}
}
