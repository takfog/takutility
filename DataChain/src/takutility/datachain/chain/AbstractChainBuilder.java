package takutility.datachain.chain;

import takutility.datachain.chain.link.ChainBuilding;
import takutility.datachain.chain.link.ChainLink;

class AbstractChainBuilder<I> {
	private ChainLink<I> first = null;
	protected BuilderBuilding<?> last = null;

	protected void innerClear() {
		setFirst(null);
		last = null;
	}
	
	protected ChainLink<I> getFirst() {
		return first;
	}
	
	protected void setFirst(ChainLink<I> first) {
		this.first = first;
	}

	public boolean isInitialized() {
		return first != null;
	}

	public ChainBuilding<I> build() {
		return build((Class<I>)null);
	}
	
	protected ChainBuilding<?> getLast() {
		return last == null ? null : last.real;
	}
	
	public <N> ChainBuilding<N> setLast(ChainBuilding<N> next) {
		if (next == null) {
			last = null;
			return null;
		}
		BuilderBuilding<N> bb = new BuilderBuilding<>(next);
		last = bb;
		return bb;
	}

	@SuppressWarnings("unchecked")
	public <N> ChainBuilding<N> build(Class<N> clazz) {
		if (last == null)
			return new BuilderBuilding<>();
		return (ChainBuilding<N>) last;
	}

	@SuppressWarnings("unchecked")
	public <O> SubChain<I,O> getSubChain() {
		return new SubChain<I,O>(first, (ChainBuilding<O>) last.real);
	}

	class BuilderBuilding<S> implements ChainBuilding<S> {
		ChainBuilding<S> real;

		public BuilderBuilding() {}

		public BuilderBuilding(ChainBuilding<S> real) {
			this.real = real;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public <N, L extends ChainLink<S> & ChainBuilding<N>> 
			ChainBuilding<N> join(L next) 
		{
			if (real == null)
				setFirst((ChainLink<I>) next);
			else
				real.join(next);
			return setNext(next);
		}

		private <N> ChainBuilding<N> setNext(ChainBuilding<N> next) {
			return setLast(next);
		}

		@SuppressWarnings("unchecked")
		@Override
		public void attach(ChainLink<S> next) {
			if (real == null) {
				setFirst((ChainLink<I>) next);
			} else
				real.attach(next);
			if (next instanceof ChainBuilding) 
				setNext((ChainBuilding<?>) next);
		}
		
	}
}