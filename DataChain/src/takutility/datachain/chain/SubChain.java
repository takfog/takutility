package takutility.datachain.chain;

import takutility.datachain.chain.link.ChainBuilding;
import takutility.datachain.chain.link.ChainLink;
import takutility.datachain.chain.link.InnerLink;
import takutility.datachain.filter.Identity;
import takutility.datachain.out.FullOutput;

public class SubChain<I,O> extends AbstractChain<I> {
	private ChainBuilding<O> end;
	
	public static <T> SubChain<T, T> empty() {
		InnerLink<T,T> link = new InnerLink<>(new Identity<>());
		return new SubChain<>(link, link);
	}
	
	public SubChain(ChainLink<I> start, ChainBuilding<O> end) {
		super(start);
		this.end = end;
	}
	
 	public ChainBuilding<O> getEnd() {
		return end;
	}
	
	public void setClosure(FullOutput<O> output) {
		end.close(output);
	}
}
