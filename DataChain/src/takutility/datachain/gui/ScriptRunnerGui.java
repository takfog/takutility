package takutility.datachain.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import takutility.console.ConsolePanel;
import takutility.datachain.chain.Chain;
import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.parse.CommandLineScriptParser;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ParsingMap;
import takutility.datachain.parse.ScriptOption;
import takutility.gui.JMessageDialog;
import takutility.gui.textpopup.TextPopupFactory;

public class ScriptRunnerGui extends JFrame {
	private static final long serialVersionUID = -3449352731627402688L;
	
	private List<ParameterPanel> params = new ArrayList<>();
	private Chain<String[]> _chain = null; 
	private String _lastScript = null;

	private JPanel contentPane;
	private JTextField txtScriptFile;
	private JPanel panParams;
	private JButton btnValidate;
	private JButton btnRun;
	private JTextArea txtArgs;
	private ConsolePanel console;
	private JTabbedPane tabbedPane;
	private JScrollPane panParamsScroll;
	private JPanel panel_2;
	private JScrollPane scrollPane;
	private JTextArea scriptArea;
	private JCheckBox chckbxSplitArgs;
	private JSplitPane splitPane;
	private JScrollPane scrollPane_1;
	private JList<String> lstLinks;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					ScriptRunnerGui frame = new ScriptRunnerGui();
					frame.setVisible(true);
					if (args.length > 0) {
						frame.txtScriptFile.setText(args[0]);
						frame.loadScript(new File(args[0]));
						frame.tabbedPane.setSelectedComponent(frame.panParamsScroll);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ScriptRunnerGui() {
		setTitle("DataChain Script Runner");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 453, 466);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWeights = new double[]{1.0, 0.0, 0.0};
		gbl_contentPane.rowWeights = new double[]{1.0, 0.0, 0.0};
		contentPane.setLayout(gbl_contentPane);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				getChain();
			}
		});
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridwidth = 3;
		gbc_tabbedPane.insets = new Insets(0, 0, 5, 0);
		gbc_tabbedPane.gridx = 0;
		gbc_tabbedPane.gridy = 0;
		contentPane.add(tabbedPane, gbc_tabbedPane);
		
		panel_2 = new JPanel();
		tabbedPane.addTab("Script", null, panel_2, null);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel_2.add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(5, 0));
		
		JLabel lblFile = new JLabel("File:");
		panel.add(lblFile, BorderLayout.WEST);
		
		txtScriptFile = new JTextField();
		txtScriptFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				loadScript(new File(txtScriptFile.getText()));
			}
		});
		panel.add(txtScriptFile);
		txtScriptFile.setColumns(10);
		
		JButton btnOpenScript = new JButton("...");
		btnOpenScript.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				openScriptDialog();
			}
		});
		panel.add(btnOpenScript, BorderLayout.EAST);
		
		panParamsScroll = new JScrollPane();
		tabbedPane.addTab("Parameters", null, panParamsScroll, null);
		panParamsScroll.setBorder(null);
		panParamsScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		panParams = new JPanel();
		panParams.setBorder(new EmptyBorder(5, 5, 5, 5));
		panParamsScroll.setViewportView(panParams);
		panParams.setLayout(new BoxLayout(panParams, BoxLayout.Y_AXIS));
		
		console = new ConsolePanel(true, true);
		tabbedPane.addTab("Console", null, console, null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridwidth = 3;
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		contentPane.add(panel_1, gbc_panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		txtArgs = new JTextArea();
		txtArgs.setRows(1);
		txtArgs.setWrapStyleWord(true);
		txtArgs.setLineWrap(true);
		panel_1.add(txtArgs);
		
		btnValidate = new JButton("Validate");
		btnValidate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				verify();
			}
		});
		
		chckbxSplitArgs = new JCheckBox("Split args");
		chckbxSplitArgs.setSelected(true);
		GridBagConstraints gbc_chckbxSplitArgs = new GridBagConstraints();
		gbc_chckbxSplitArgs.anchor = GridBagConstraints.WEST;
		gbc_chckbxSplitArgs.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxSplitArgs.gridx = 0;
		gbc_chckbxSplitArgs.gridy = 2;
		contentPane.add(chckbxSplitArgs, gbc_chckbxSplitArgs);
		GridBagConstraints gbc_btnValidate = new GridBagConstraints();
		gbc_btnValidate.insets = new Insets(0, 0, 0, 5);
		gbc_btnValidate.gridx = 1;
		gbc_btnValidate.gridy = 2;
		contentPane.add(btnValidate, gbc_btnValidate);
		
		btnRun = new JButton("Run");
		btnRun.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				runChain();
			}
		});
		GridBagConstraints gbc_btnRun = new GridBagConstraints();
		gbc_btnRun.gridx = 2;
		gbc_btnRun.gridy = 2;
		contentPane.add(btnRun, gbc_btnRun);
		
		unsetChain();
		TextPopupFactory popup = TextPopupFactory.all().singleUndo();
		popup.attachTo(txtArgs).attachTo(txtScriptFile);
		
		splitPane = new JSplitPane();
		splitPane.setOneTouchExpandable(true);
		panel_2.add(splitPane, BorderLayout.CENTER);
		
		scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);
		
		scriptArea = new JTextArea();
		scrollPane.setViewportView(scriptArea);
		
		scrollPane_1 = new JScrollPane();
		splitPane.setLeftComponent(scrollPane_1);
		
		lstLinks = new JList<String>();
		lstLinks.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2)
					addScriptKey();
			}
		});
		lstLinks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_1.setViewportView(lstLinks);
		
		splitPane.setDividerLocation(0);
		splitPane.setLastDividerLocation(-1);
		lstLinks.setModel(scriptKeys());
		lstLinks.getInputMap(JList.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("ENTER"), "action");
		lstLinks.getActionMap().put("action", new AbstractAction() {
			private static final long serialVersionUID = -5834787641895722548L;
			@Override
            public void actionPerformed(ActionEvent e) {
                addScriptKey();
            }
        });
	}
	
	private void addScriptKey() {
		String value = lstLinks.getSelectedValue();
		if (value != null)
			scriptArea.insert(value+"\n", scriptArea.getCaretPosition());
	}
	
	private static ListModel<String> scriptKeys() {
		String[] keys = ParsingMap.validKeys().toArray(new String[0]);
		Arrays.sort(keys);
		DefaultListModel<String> model = new DefaultListModel<>();
		for (String k : keys) {
			model.addElement(k);
		}
		return model;
	}

	private void openScriptDialog() {
		JFileChooser fc = new JFileChooser(txtScriptFile.getText());
		if (fc.showOpenDialog(this) != JFileChooser.APPROVE_OPTION)
			return;
		txtScriptFile.setText(fc.getSelectedFile().getAbsolutePath());
		loadScript(fc.getSelectedFile());
	}
	
	private void loadScript(File file) {
		String script;
		try {
			script = Files.lines(file.toPath()).collect(Collectors.joining("\n"));
		} catch (IOException e) {
			console.clear();
			JMessageDialog.error(this, e, "Error loading script");
			return;
		}
		scriptArea.setText(script);
		_getChain(true, true);
	}

	private Chain<String[]> getChain() {
		return getChain(false);
	}

	private Chain<String[]> getChain(boolean showErr) {
		return _getChain(showErr, false);
	}

	private Chain<String[]> _getChain(boolean showErr, boolean updateSplit) {
		if (scriptArea == null)
			return null;
		String script = scriptArea.getText();
		if ((_chain != null || !showErr) && script.equals(_lastScript))
			return _chain;
		
		_lastScript = script;
		_chain = null;
		CommandLineScriptParser parser = new CommandLineScriptParser();
		parser.setEndListener(() -> JOptionPane.showMessageDialog(this, "Chain completed"));
		try {
			parser.load(script);
		} catch (Exception e) {
			if (showErr) {
				e.printStackTrace();
				tabbedPane.setSelectedComponent(console);
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error loading script", JOptionPane.ERROR_MESSAGE);
			}
			return null;
		}
		panParams.removeAll();
		Map<String, String> oldParam = new HashMap<>();
		for (ParameterPanel par : params) {
			oldParam.put(par.getPropertyName(), par.getText());
		}
		params.clear();
		addParams(parser, oldParam, ScriptOption.Type.VAR);
		addParams(parser, oldParam, ScriptOption.Type.OPT_VAR);
		if (updateSplit)
			chckbxSplitArgs.setSelected(!parser.hasOption(ScriptOption.Type.NO_SPLIT));
		for (ParameterPanel p : params) {
			panParams.add(p);
		}
		panParams.validate();
		_chain = parser.getChain();
		return _chain;
	}

	private void addParams(CommandLineScriptParser parser, Map<String, String> oldParam,
			takutility.datachain.parse.ScriptOption.Type type) {
		for (ScriptOption opt : parser.getOptions(type)) {
			ParameterPanel paramPanel = new ParameterPanel();
			paramPanel.setProperyName(opt.value(),true);
			paramPanel.setText(oldParam.getOrDefault(opt.value(), ""));
			params.add(paramPanel);
		}
	}
	
	private void unsetChain() {
		_chain = null;
		_lastScript = null;
	}
	
	private void verify() {
		Chain<String[]> chain = getChain(true);
		if (chain == null) return;
		VerificationData data = new VerificationData();
		for (ParameterPanel param : params) {
			if (param.isBoolean())
				data.saveVariable(param.getPropertyName(), Boolean.class);
			else if (param.isRequired() || !param.getText().isEmpty())
				data.saveVariable(param.getPropertyName(), String.class);
		}
		try {
			chain.verify(data);
			JOptionPane.showMessageDialog(this, "The chain is correct");
		} catch (ChainVerificationException e) {
			console.clear();
			JMessageDialog.warning(this, e, "Invalid chain");
		}
	}
	
	private void runChain() {
		Chain<String[]> chain = getChain(true);
		if (chain == null) return;
		tabbedPane.setSelectedComponent(console);
		console.clear();
		ChainParams<String[]> chainp = new ChainParams<>();
		for (ParameterPanel param : params) {
			Object val = param.getPropertyValue();
			if (val != null)
				chainp.set(param.getPropertyName(), val);
		}
		if (chckbxSplitArgs.isSelected())
			chainp.setInput(ParseUtils.argsToArray(txtArgs.getText()));
		else
			chainp.setInput(new String[] {txtArgs.getText()});
		new Thread(() -> {
			try {
				chain.start(chainp);
			} catch(Exception e) {
				JMessageDialog.error(this, e, "Error running the chain");
			}
		}).start();
	}
}
