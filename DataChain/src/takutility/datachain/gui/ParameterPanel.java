package takutility.datachain.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ParameterPanel extends JPanel {
	private static final long serialVersionUID = 1672625669427507209L;
	
	private String name;
	private boolean required;
	
	private JTextField txtValue;
	private JLabel lblProp;
	private JCheckBox chkBool;

	/**
	 * Create the panel.
	 */
	public ParameterPanel() {
		setLayout(new BorderLayout(5, 0));
		
		txtValue = new JTextField();
		add(txtValue, BorderLayout.CENTER);
		txtValue.setColumns(10);
		
		lblProp = new JLabel("Prop:");
		add(lblProp, BorderLayout.WEST);
		
		chkBool = new JCheckBox("");
		chkBool.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				txtValue.setEnabled(!chkBool.isSelected());
			}
		});
		add(chkBool, BorderLayout.EAST);

	}
	
	public boolean isRequired() {
		return required;
	}
	
	public String getPropertyName() {
		return name;
	}
	
	public void setRequired(boolean required) {
		setProperyName(name, required);
	}

	public void setProperyName(String name) {
		setProperyName(name, required);
	}

	public void setProperyName(String name, boolean required) {
		this.name = name;
		this.required = required;
		lblProp.setText(name + (required ? " (*)" : ""));
		
	}
	
	public String getText() {
		return txtValue.getText();
	}
	
	public void setText(String val) {
		txtValue.setText(val);
	}
	
	@Override
	public Dimension getMaximumSize() {
		return new Dimension(Integer.MAX_VALUE, txtValue.getPreferredSize().height);
	}
	
	public boolean isBoolean() {
		return chkBool.isSelected();
	}
	
	public Object getPropertyValue() {
		if (chkBool.isSelected())
			return true;
		else if(required || !txtValue.getText().isEmpty())
			return txtValue.getText();
		else
			return null;
	}
}
