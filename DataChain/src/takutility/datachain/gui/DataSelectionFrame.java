package takutility.datachain.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.gui.checkedlist.CheckedList;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class DataSelectionFrame extends JFrame {
	private static final long serialVersionUID = -593227836745004122L;
	
	private JPanel contentPane;
	private CheckedList list;
	private JButton btnOk;
	private JButton btnCancel;
	private Collection<DialogListener> listeners = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					DataSelectionFrame frame = new DataSelectionFrame();
					frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
					frame.setData(new InputDataContainer(Arrays.asList("a","b","c","d","e","f")));
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DataSelectionFrame() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				onDialogClosed(true);
			}
		});
		setTitle("Data selection - DataChain");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		list = new CheckedList();
		scrollPane.setViewportView(list);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		JButton btnSelectAll = new JButton("Select all");
		btnSelectAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				list.checkAll();
			}
		});
		panel.add(btnSelectAll);
		
		JButton btnDeselectAll = new JButton("Deselect all");
		btnDeselectAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				list.uncheckAll();
			}
		});
		panel.add(btnDeselectAll);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		panel.add(horizontalGlue);
		
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				onDialogClosed(true);
				dispose();
			}
		});
		panel.add(btnCancel);
		
		btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				onDialogClosed(false);
				dispose();
			}
		});
		panel.add(btnOk);
	}

	public void setData(DataContainer data) {
		Collection<Data> dataList = data.get();
		list.setListData(dataList.toArray());
		list.checkAll();
	}
	
	public Collection<Data> getSelectedData() {
		return list.getSelectedValuesList();
	}
	
	public void addDialogListener(DialogListener listener) {
		if (listeners == null)
			listeners = new ArrayList<DialogListener>();
		listeners.add(listener);
	}
	
	public void removeDialogListener(DialogListener listener) {
		if (listener != null)
			listeners.remove(listener);
	}
	
	private void onDialogClosed(boolean cancelled) {
		if (listeners == null) return;
		Collection<Data> data = null;
		if (!cancelled) 
			data = getSelectedData();
		for (DialogListener listener : listeners) {
			listener.dialogClosed(data, cancelled);
		}
	}
	
	public static interface DialogListener {
		void dialogClosed(Collection<Data> selected, boolean cancelled);
	}
}
