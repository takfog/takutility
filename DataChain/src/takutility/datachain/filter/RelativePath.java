package takutility.datachain.filter;

import java.io.File;
import java.util.List;

import takutility.datachain.ClassCast;
import takutility.datachain.ParamElem;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

public class RelativePath implements Converter<File, String> {
	private ParamElem elem;
	
	public RelativePath(String id) {
		this(ParamElem.var(id));
	}

	public RelativePath(File base) {
		this(new ParamElem(base));
	}
	
	public RelativePath(ParamElem base) {
		this.elem = base;
	}
	
	@Override
	public ClassCast<File> inputAutoCast() {
		return StandardClassCast.FILE;
	}

	@Override
	public String convert(InnerChainParams params, File in, DataPersistence pers) {
		File baseFile = (File) elem.get(params, in, pers);
		String base = baseFile.getAbsolutePath() + File.separator;
		String f = in.getAbsolutePath();
		if (f.startsWith(base))
			return f.substring(base.length());
		else
			return f;
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(StandardClassCast.FILE, this);
		data.verify(elem, StandardClassCast.FILE, this);
		data.setCurrentType(String.class);
	}
	
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return ParseUtils.simpleArgs(args, RelativePath::new, null, 
				s -> {
					ParamElem param = ParamElem.deserialize(s);
					if (param.isFixed())
						return new RelativePath(new File(param.get(null).toString()));
					else
						return new RelativePath(param);
				});
	}
}
