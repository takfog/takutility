package takutility.datachain.filter;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ScriptParse;

@FunctionalInterface
public interface FileFilter extends Filter<File> {

	public static FileFilter fileOnly() {
		return File::isFile;
	}

	public static FileFilter directoryOnly() {
		return File::isDirectory;
	}

	public static FileFilter extension(final String ext) {
		if (ext.startsWith("."))
			return f -> f.getName().endsWith(ext);
		else
			return extension("."+ext);
	}

	@Override
	default boolean filter(InnerChainParams params, File in, DataPersistence pers) {
		return filter(in);
	}
	
	boolean filter(File in);
	
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		String[] split = args.trim().split(" ");
		try {
			Method method = FileFilter.class.getDeclaredMethod(split[0], 
					IntStream.range(1, split.length)
					.mapToObj(i -> String.class)
					.toArray(n -> new Class<?>[n]));
			if (split.length == 1) 
				return method.invoke(null);
			else 
				return method.invoke(null, (Object[])Arrays.copyOfRange(split, 1, split.length));
		} catch (NoSuchMethodException | SecurityException e) {
			throw new IllegalArgumentException(split[0] + " is not a valid method", e);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new IllegalArgumentException("Error invoking "+args, e);
		}
	}
}
