package takutility.datachain.filter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InnerDataContainer;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class Distinct<T> implements FullConverter<T, T> {

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		Set<Object> unique = new HashSet<>();
		ArrayList<Data> newData = new ArrayList<>(in.get().size());
		for (Data data : in.get()) {
			if (!unique.contains(data.get())) {
				unique.add(data.get());
				newData.add(data);
			}
		}
		if (newData.size() == in.get().size())
			return in;
		else
			return new InnerDataContainer(newData);
	}

}
