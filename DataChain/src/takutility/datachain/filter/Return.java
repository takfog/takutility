package takutility.datachain.filter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import takutility.composite.Pair;
import takutility.datachain.TypedParamElem;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

public class Return<I,O>  implements Converter<I,O> {
	private Method method;
	private TypedParamElem[] elements;

	public Return(Class<I> clazz, String name, Class<O> retType, TypedParamElem... elements) {
		Method localMethod;
		try {
			localMethod = clazz.getMethod(name, Arrays.stream(elements)
					.map(TypedParamElem::getType)
					.toArray(n -> new Class[n]));
		} catch (NoSuchMethodException | SecurityException e) {
			throw new IllegalArgumentException(e);
		}
		init(localMethod, retType, elements);
	}
	
	private Return(Class<I> clazz, String name, TypedParamElem... elements) {
		this(clazz, name, null, elements);
	}
	
	public Return(Method method, Class<O> retType, TypedParamElem... elements) {
		init(method, retType, elements);
	}
	
	private void init(Method method, Class<O> retType, TypedParamElem[] elements) {
		if (retType != null && method.getReturnType() != retType)
			throw new IllegalArgumentException("Invalid return type");
		this.method = method;
		this.elements = elements;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public O convert(InnerChainParams params, I in, DataPersistence pers) {
		try {
			Object obj;
			if (Modifier.isStatic(method.getModifiers()))
				obj = null;
			else
				obj = in;
			
			return (O) method.invoke(obj, Arrays.stream(elements)
					.map(e -> e.get(params, in, pers))
					.toArray());
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		for (TypedParamElem elem : elements) {
			data.verify(elem, this);
		}
		data.setCurrentType(method.getReturnType());
	}
	
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		Pair<Class<?>, String> clMeth = ParseUtils.deserializeMethod(args);
		if (clMeth.a() == null)
			throw new IllegalArgumentException("No class defined");
		if (clMeth.b() == null)
			throw new IllegalArgumentException("No method defined");
		
		return new Return<>(clMeth.a(), clMeth.b(), 
				ParseUtils.typedParamElems(params));
	}
}
