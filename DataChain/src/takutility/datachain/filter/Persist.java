package takutility.datachain.filter;

import java.util.List;
import java.util.function.Function;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.parse.ScriptParse;

public class Persist<T> implements FullConverter<T,T> {
	private String key;
	private Function<T,?> partPersist;
	
	public Persist(String key) {
		this(key, Function.identity());
	}
	
	public Persist(String key, Function<T,?> partPersist) {
		this.key = key;
		this.partPersist = partPersist;
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		in.persist(key, partPersist);
		return in;
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.persist(key);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return new Persist<>(args);
	}
}
