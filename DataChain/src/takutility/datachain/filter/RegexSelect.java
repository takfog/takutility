package takutility.datachain.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import takutility.composite.Pair;
import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InnerDataContainer;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.RegexParser;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.util.PairChainType;
import takutility.datachain.util.RegexUtil;

public class RegexSelect extends ParametrizedImpl<Pair<Pattern,Integer>> 
	implements FullConverter<String, String>
{

	public RegexSelect(Pattern pattern) {
		this(pattern, 0);
	}

	public RegexSelect(Pattern pattern, int group) {
		super(null, new Pair<>(pattern, group));
	}

	public RegexSelect(String id) {
		super(id, null);
	}
	
	@Override
	public ClassCast<String> inputAutoCast() {
		return StandardClassCast.STRING;
	}
	
	@Override
	protected ClassCast<Pair<Pattern, Integer>> paramAutoCast() {
		return RegexUtil.castReplace(Integer.class, s -> Integer.parseInt(s));
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		Pair<Pattern, Integer> param = getParam(params);
		Pattern regex = param.a();
		int group = param.b();
		Collection<Data> oldData = in.get();
		ArrayList<Data> newData = new ArrayList<>(oldData.size());
		for (Data data : oldData) {
			boolean first = true;
			Matcher m = regex.matcher(data.get(inputAutoCast()));
			while(m.find()) {
				if (first)
					first = false;
				else
					data = in.newData(null, data);
				data.set(m.group(group));
				newData.add(data);
			}
		}
		return new InnerDataContainer(newData);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		verifyParam(data, new PairChainType(Pattern.class, Integer.class));
		data.setCurrentType(String.class);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return ParseUtils.simpleArgs(args, RegexSelect::new, 
				null, RegexSelect::parseRegex);
	}
	
	private static RegexSelect parseRegex(String str) {
		if (!str.startsWith("/"))
			return new RegexSelect(Pattern.compile(str));
		
		return RegexParser.parseCompile(str, (p,r) -> {
			r = r.trim();
			if (r.isEmpty())
				return new RegexSelect(p);
			else
				return new RegexSelect(p, Integer.parseInt(r));
		});
	}
}
