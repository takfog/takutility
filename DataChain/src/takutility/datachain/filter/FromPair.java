package takutility.datachain.filter;

import java.util.List;

import takutility.composite.Pair;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.util.PairChainType;

public abstract class FromPair {

	public static <A,B> Converter<Pair<A,B>, A> keepA() {
		return new Converter<Pair<A,B>, A>() {
			@Override
			public A convert(InnerChainParams params, Pair<A, B> in, DataPersistence pers) {
				return in.a();
			}
			@Override
			public void verify(VerificationData data) throws ChainVerificationException {
				FromPair.verify(data, this, 0);
			}
		};
	}

	public static <A,B> Converter<Pair<A,B>, B> keepB() {
		return new Converter<Pair<A,B>, B>() {
			@Override
			public B convert(InnerChainParams params, Pair<A, B> in, DataPersistence pers) {
				return in.b();
			}
			@Override
			public void verify(VerificationData data) throws ChainVerificationException {
				FromPair.verify(data, this, 1);
			}
		};
	}
	
	private static void verify(VerificationData data, Object obj, int idx) throws ChainVerificationException {
		data.verifyType(new PairChainType(), obj);
		data.setCurrentType(data.getCurrentType().getSubType(idx));
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		switch (args.trim().toLowerCase()) {
		case "a":
			return keepA();
		case "b":
			return keepB();
		default:
			throw new IllegalArgumentException("Argument not valid");
		}
	}
}
