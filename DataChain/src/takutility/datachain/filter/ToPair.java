package takutility.datachain.filter;

import java.util.List;

import takutility.composite.Pair;
import takutility.datachain.ParamElem;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.util.PairChainType;

public class ToPair<I,A,B> implements Converter<I, Pair<A, B>> {
	private ParamElem pa,pb;

	public static <A,B> ToPair<B, A, B> fillA(Class<A> classA, ParamElem a) {
		return new ToPair<>(a, ParamElem.in);
	}

	public static <A,B> ToPair<A, A, B> fillB(Class<B> classB, ParamElem b) {
		return new ToPair<>(ParamElem.in, b);
	}
	
	public ToPair(ParamElem a, ParamElem b) {
		this.pa = a;
		this.pb = b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Pair<A, B> convert(InnerChainParams params, I in, DataPersistence pers) {
		A a = (A) pa.get(params, in, pers);
		B b = (B) pb.get(params, in, pers);
		return new Pair<>(a, b);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(new PairChainType(
				data.getTypeFromParam(pa),
				data.getTypeFromParam(pb)));
	}
	
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		if (params.size() != 2) 
			throw new IllegalArgumentException("Must set 2 params");
		ParamElem[] elems = ParseUtils.paramElems(params);
		return new ToPair<>(elems[0], elems[1]);
	}
}
