package takutility.datachain.filter;

import java.util.List;

import takutility.datachain.chain.ChainType;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

public class Recover<I,O> implements FullConverter<I,O> {
	private String perKey, actKey;

	public Recover(String key) {
		this(key, null);
	}

	public Recover(String perKey, String actKey) {
		this.perKey = perKey;
		this.actKey = actKey;
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		in.recover(perKey, actKey);
		return in;
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		if (!data.containPersisted(perKey)) {
			throw new ChainVerificationException(this, "no persisted data "+perKey);
		}
		ChainType type = data.getPersisted(perKey);
		if (actKey != null)
			data.persist(actKey);
		data.setCurrentType(type);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		String[] arr = ParseUtils.argsToArray(args);
		switch (arr.length) {
		case 1:
			return new Recover<>(arr[0], null);
		case 2:
			return new Recover<>(arr[0], arr[1]);
		default:
			throw new IllegalArgumentException("Invalid recover args");
		}
	}
}
