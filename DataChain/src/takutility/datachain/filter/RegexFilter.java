package takutility.datachain.filter;

import java.util.List;
import java.util.regex.Pattern;

import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

public class RegexFilter extends ParametrizedImpl<Pattern> 
	implements Filter<String>
{

	public RegexFilter(Pattern regex) {
		super(null, regex);
	}

	public RegexFilter(String id) {
		super(id, null);
	}
	
	@Override
	public ClassCast<String> inputAutoCast() {
		return StandardClassCast.STRING;
	}
	
	@Override
	protected ClassCast<Pattern> paramAutoCast() {
		return StandardClassCast.PATTERN;
	}
	
	@Override
	public boolean filter(InnerChainParams params, String in, DataPersistence pers) {
		return getParam(params).matcher(in).matches();
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		verifyParam(data, Pattern.class);
	}
	
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return ParseUtils.simpleArgs(args, RegexFilter::new, null,
				s -> new RegexFilter(Pattern.compile(s)));
	}
}
