package takutility.datachain.filter;

import java.util.List;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ScriptParse;

/**
 * Converts an object to string. If a format is specified,
 * uses that specific format
 * @param <T> the expected type of the input elements 
 */
public class ToString<T> implements Converter<T, String> {
	private String format = null;
	
	public ToString() {}
	
	public ToString(String format) {
		this.format = format;
	}

	@Override
	public String convert(InnerChainParams params, T in, DataPersistence pers) {
		if (format == null)
			return in.toString();
		else
			return String.format(format, in);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(String.class);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		if (args.isEmpty())
			return new ToString<>();
		else
			return new ToString<>(args);
	}
}
