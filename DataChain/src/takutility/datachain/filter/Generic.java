package takutility.datachain.filter;

import java.util.Collection;
import java.util.function.Consumer;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;

public class Generic<T> implements FullConverter<T, T> {
	private Consumer<Collection<Data>> func;
	
	public Generic(Consumer<Collection<Data>> func) {
		this.func = func;
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		func.accept(in.get());
		return in;
	}

}
