package takutility.datachain.filter;

import java.io.File;

import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class MkDir implements Elaborator<File> {

	@Override
	public void elaborate(InnerChainParams params, File val, DataPersistence pers) {
		val.mkdirs();
	}
	
	@Override
	public ClassCast<File> inputAutoCast() {
		return StandardClassCast.FILE;
	}
}
