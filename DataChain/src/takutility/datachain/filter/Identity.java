package takutility.datachain.filter;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class Identity<T> implements FullConverter<T, T> {

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		return in;
	}

}
