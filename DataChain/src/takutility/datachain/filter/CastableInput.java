package takutility.datachain.filter;

import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;

public interface CastableInput<I> {

	default ClassCast<I> inputAutoCast() {
		return StandardClassCast.noCast();
	}

}