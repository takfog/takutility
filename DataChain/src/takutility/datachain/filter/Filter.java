package takutility.datachain.filter;

import java.util.stream.Collectors;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.data.InnerDataContainer;

public interface Filter<T> extends FullConverter<T, T> {
	@Override
	default DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		return new InnerDataContainer(in.dataStream()
				.filter(i ->filter(params, i.<T>get(inputAutoCast()), i))
				.collect(Collectors.toList())
		);
	}
	
	boolean filter(InnerChainParams params, T in, DataPersistence pers);

}
