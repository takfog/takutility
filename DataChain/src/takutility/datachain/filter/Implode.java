package takutility.datachain.filter;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import takutility.datachain.chain.ChainType;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class Implode<E> implements FullConverter<E, List<E>> {

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(new ChainType(List.class, data.getCurrentType()));
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		return new InputDataContainer(Collections.singletonList(
				in.stream().collect(Collectors.toList())));
	}

}
