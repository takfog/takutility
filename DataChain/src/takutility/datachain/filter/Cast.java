package takutility.datachain.filter;

import java.lang.reflect.Constructor;
import java.util.List;

import takutility.datachain.ParamElem;
import takutility.datachain.TypedParamElem;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;
import takutility.ptr.Ptr;

public class Cast<I, O> extends ParametrizedImpl<ParamElem[]> implements Converter<I, O> {
	private Constructor<O> ctor;
	private boolean required;

	public static <I, O>  Cast<I,O> fromParams(Class<O> clazz, TypedParamElem... defParam) {
		return fromParams(clazz, null, defParam);
	}

	public static <I, O>  Cast<I,O> fromParams(Class<O> clazz, String id, TypedParamElem... defParam) {
		try {
			if (defParam == null || defParam.length == 0)
				return new Cast<>(clazz.getConstructor());
			@SuppressWarnings("rawtypes")
			Class[] types = new Class[defParam.length];
			for (int i = 0; i < defParam.length; i++) {
				types[i] = defParam[i].getType();
			}
			return new Cast<>(clazz.getConstructor(types), id, defParam);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static <I, O>  Cast<I,O> fromInput(Class<O> clazz, Class<I> inputType) {
		try {
			return new Cast<>(clazz.getConstructor(inputType), ParamElem.in);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Cast(Constructor<O> ctor) {
		this(ctor, null, (ParamElem[])null);
	}

	public Cast(Constructor<O> ctor, String id) {
		this(ctor, id, (ParamElem[])null);
	}

	public Cast(Constructor<O> ctor, ParamElem... defParam) {
		this(ctor, null, defParam);
	}

	public Cast(Class<O> clazz) {
		this(clazz, null);
	}

	public Cast(Class<O> clazz, String id) {
		this(ctorFromClass(clazz), id, (ParamElem[])null);
	}
		
	public Cast(Constructor<O> ctor, String id, ParamElem... defParam) {
		super(id, defParam);
		this.ctor = ctor;
		if (ctor.getParameterCount() == 0)
			required = false;
		else if (defParam == null || defParam.length < ctor.getParameterCount())
			required = true;
		else
			required = false;
	}

	
	@Override
	public O convert(InnerChainParams params, I in, DataPersistence pers) {
		ParamElem[] defaultParam = getDefaultParam();
		ParamElem[] paramEls = getParam(params);
		Object[] par;
		if (paramEls == null) {
			par = new Object[0];
		} else {
			par = new Object[paramEls.length];
			for (int i = 0; i < paramEls.length; i++) {
				ParamElem el = null;
				if (paramEls[i] != null) 
					el = paramEls[i];
				else if (defaultParam.length > i)
					el = defaultParam[i];
				
				if (el != null)
					par[i] = el.get(params, in, pers);
			}
		}
		try {
			return ctor.newInstance(par);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean isParamRequired() {
		return required;
	}
	
	@SuppressWarnings("unchecked")
	private static <T> Constructor<T> ctorFromClass(Class<T> clazz) {
		Constructor<?>[] ctors = clazz.getConstructors();
		if (ctors.length == 0) 
			throw new IllegalArgumentException("No public construtors for "+clazz);
		if (ctors.length > 1)
			throw new IllegalArgumentException("Too many public construtors for "+clazz);
		return (Constructor<T>) ctors[0];
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		super.verifyParam(data, ParamElem[].class);
		ParamElem[] paramElems = getDefaultParam();
		if (paramElems != null) {
			Class<?>[] types = ctor.getParameterTypes();
			if (types.length != paramElems.length)
				throw new ChainVerificationException(this, "wrong number of params");
			for (int i = 0; i < types.length; i++) {
				data.verify(paramElems[i], types[i], this);
			}
		}
		data.setCurrentType(ctor.getDeclaringClass());
	}
	
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		try {
			String[] argArr = args.split(" ",2);
			Class<?> dest = Class.forName(argArr[0]);
			Ptr<String> id = new Ptr<>();
			if (argArr.length > 1) {
				Class<?> inType = ParseUtils.simpleArgs(argArr[1], v -> {
							id.set(v);
							return null;
						}, 
						() -> null, 
						s -> {
							try {
								return Class.forName(s);
							} catch (Exception e) {
								throw new IllegalArgumentException(e);
							}
						});
				if (inType != null)
					return fromInput(dest, inType);
			}
			if (params.isEmpty())
				return new Cast<>(dest, id.get());
			
			return fromParams(dest, id.get(), ParseUtils.typedParamElems(params));
			
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException(e);
		}
	}
}
