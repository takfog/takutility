package takutility.datachain.filter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import takutility.datachain.ParamElem;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

public class Concat<T> implements Converter<T, String> 
{
	private ParamElem[] elems;
	private String delimiter;
	
	public Concat(ParamElem... elems) {
		this("", elems);
	}
	
	public Concat(String delimiter, ParamElem... elems) {
		this.elems = elems;
		this.delimiter = delimiter;
	}

	@Override
	public String convert(InnerChainParams params, T in, DataPersistence pers) {
		return Arrays.stream(elems)
			.map(e -> e.get(params, in, pers))
			.map(Object::toString)
			.collect(Collectors.joining(delimiter));
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(String.class);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		ParamElem[] paramElems = ParseUtils.paramElems(params);
		args = args.trim();
		if (args.isEmpty()) 
			return new Concat<>(paramElems);
		
		String delim;
		if ((args.endsWith("'") || args.endsWith("\"")) 
				&& args.charAt(0) == args.charAt(args.length()-1))
		{
			delim = args.substring(1, args.length()-1)
					.replace("\\"+args.charAt(0), args.charAt(0)+"")
					.replace("\\\\", "\\");
		} else 
			delim = args;
		return new Concat<>(delim, paramElems);
	}
}
