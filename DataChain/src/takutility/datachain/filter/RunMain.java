package takutility.datachain.filter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import takutility.datachain.ParamElem;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.parse.ScriptParse;

public class RunMain<T> implements FullConverter<T, T> {
	private ParamElem classParam;
	private ParamElem[] args;
	
	
	public RunMain(String classVar, ParamElem... args) {
		classParam = ParamElem.var(classVar);
		this.args = args;
	}
	
	public RunMain(Class<?> clazz, ParamElem... args) {
		classParam = new ParamElem(clazz);
		this.args = args;
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		Object classObj = classParam.get(params);
		Class<?> clazz;
		if (classObj instanceof Class)
			clazz = (Class<?>) classObj;
		else
			try {
				clazz = Class.forName(classObj.toString());
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}
		Method main;
		try {
			main = clazz.getDeclaredMethod("main", String[].class);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
		String[] mainArgs;
		if (args.length == 0) {
			mainArgs = new String[0];
		} else {
			mainArgs = Arrays.stream(args)
					.flatMap(p -> p.getStream(params, in))
					.map(Object::toString).toArray(n -> new String[n]);
		}
		try {
			main.invoke(null, (Object)mainArgs);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		return in;
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return new RunMain<>(args, ParamElem.deserializeArray(params));
	}
}
