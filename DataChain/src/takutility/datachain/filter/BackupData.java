package takutility.datachain.filter;

import java.util.List;

import takutility.datachain.chain.ChainType;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InnerDataContainer;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

/**
 * Backup and restores the data container. If out is null, outputs the input data. <br>
 * <b>Script syntax:</b><br>
 * specify {@code in=} to save the data, {@code out=} to recover saved data
 */
public class BackupData<I,O> implements FullConverter<I,O> {
	private String in, out;
	
	public BackupData(String in, String out) {
		this.in = in;
		this.out = out;
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer old) {
		if (in != null) {
			DataContainer bk;
			if (out == null)
				bk = new InnerDataContainer(old.get());
			else
				bk = old;
			params.set(in, bk);
		}
		if (out == null)
			return old;
		else
			return (DataContainer) params.get(out);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		int inPos = args.indexOf("in=");
		int outPos = args.indexOf("out=");
		String in = null, out = null;
		if (inPos >= 0)
			in = varValue(args, inPos+3);
		if (outPos >= 0)
			out = varValue(args, outPos+4);
		return new BackupData<>(in, out);
	}
	
	private static String varValue(String args, int start) {
		if (args.charAt(start) == '"') {
			String[] array = ParseUtils.argsToArray(args.substring(start));
			return array[0];
		} else {
			int space = args.indexOf(" ", start);
			if (space == 0)
				return null;
			else if (space < 0)
				return args.substring(start);
			else
				return args.substring(start, space); 
		}
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		if (in != null) {
			data.saveVariable(in, new ChainType(Data.class, data.getCurrentType()));
		}
		if (out != null) {
			ChainType outType = data.getVariable(out);
			outType.canBeAssignedTo(new ChainType(Data.class, ChainType.ANY));
			data.setCurrentType(outType.getSubType(0));
		}
	}
}
