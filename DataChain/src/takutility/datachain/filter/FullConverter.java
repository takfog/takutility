package takutility.datachain.filter;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.chain.link.Verifiable;
import takutility.datachain.data.DataContainer;

@FunctionalInterface
public interface FullConverter<I,O> extends Verifiable, CastableInput<I> {
	DataContainer fullConvert(InnerChainParams params, DataContainer in);
	
	@Override
	default void verify(VerificationData data) throws ChainVerificationException {
	}
}
