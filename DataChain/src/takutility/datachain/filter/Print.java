package takutility.datachain.filter;

import java.util.List;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.parse.ScriptParse;

public class Print<T> implements FullConverter<T, T> {
	private String value;
	
	public Print(String value) {
		this.value = value;
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		System.out.println(value);
		return in;
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return new Print<>(args);
	}
}
