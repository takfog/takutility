package takutility.datachain.filter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import takutility.datachain.ClassCast;
import takutility.datachain.ParamElem;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InnerDataContainer;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

/**
 * Looks for files with the specified pattern. 
 * It outputs the files contained in the directory set as parameter 
 * that matches the input patterns. If a file matches more than one
 * pattern, it will appear more than once 
 */
public class Find extends ParametrizedImpl<ParamElem> 
	implements FullConverter<String, File> 
{

	public Find() {
		super(null, null);
	}
	
	public Find(ParamElem base) {
		super(null, base);
	}

	public Find(String id) {
		super(id, null);
	}
	
	@Override
	public ClassCast<String> inputAutoCast() {
		return StandardClassCast.STRING;
	}
	
	@Override
	protected ClassCast<ParamElem> paramAutoCast() {
		return StandardClassCast.PARAM;
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		List<Data> newData = new ArrayList<>();
		for (Data data : in.get()) {
			ParamElem baseEl = getParam(params);
			File base;
			{
				Object obj = baseEl.get(params, null, data);
				if (obj instanceof File)
					base = (File)obj;
				else
					base = new File(obj.toString());
			}
			File[] found = base.listFiles((f,n) -> n.matches(data.get(inputAutoCast())));
			if (found != null && found.length > 0) {
				data.set(found[0]);
				newData.add(data);
				for(int i=1; i<found.length; i++) {
					newData.add(in.newData(found[i], data));
				}
			}
		}
		return new InnerDataContainer(newData);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		verifyParam(data, ParamElem.class);
		data.setCurrentType(File.class);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return ParseUtils.simpleArgs(args, Find::new, Find::new,
				v -> new Find(ParamElem.deserialize(args)));
	}
}
