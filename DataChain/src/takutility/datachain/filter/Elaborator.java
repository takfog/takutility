package takutility.datachain.filter;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.DataPersistence;

public interface Elaborator<T> extends FullConverter<T,T> {
	@Override
	default DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		in.dataStream().forEach(i ->
			elaborate(params, i.<T>get(inputAutoCast()), i)
		);
		return in;
	}
	
	void elaborate(InnerChainParams params, T val, DataPersistence pers);
}
