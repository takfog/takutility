package takutility.datachain.filter;

import takutility.datachain.ClassCast;
import takutility.datachain.Parametrized;
import takutility.datachain.chain.ChainType;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;

public abstract class ParametrizedImpl<P> implements Parametrized<P> {
	private P defParam;
	private String id;
	
	protected ParametrizedImpl(String id, P defParam) {
		this.defParam = defParam;
		if (id == null)
			this.id = Parametrized.defaultId(this);
		else
			this.id = id;
	}
	
	protected P getDefaultParam() {
		return defParam;
	}

	protected P getParam(InnerChainParams params) {
		P param = params.getOrDefault(this, defParam);
		if (param == null)
			return null;
		ClassCast<P> autoCast = paramAutoCast();
		if (autoCast != null) {
			return autoCast.cast(param);
		}
		return param;
	}

	@Override
	public String getLinkId() {
		return id;
	}
	
	protected ClassCast<P> paramAutoCast() {
		return null;
	}
	
	@Override
	public boolean isParamRequired() {
		return defParam == null;
	}
	
	protected void verifyParam(VerificationData data, ChainType paramType) throws ChainVerificationException {
		ChainType type = getParamForVerification(data);
		if (type == null) return;
		if (type.canBeAssignedTo(paramType)) return;
		castParamOrFailVerification(type);
	}
	
	protected void verifyParam(VerificationData data, Class<?> paramType) throws ChainVerificationException {
		ChainType type = getParamForVerification(data);
		if (type == null) return;
		if (type.canBeAssignedTo(paramType)) return;
		castParamOrFailVerification(type);
	}

	private void castParamOrFailVerification(ChainType type) throws ChainVerificationException {
		ClassCast<P> autoCast = paramAutoCast();
		if (autoCast == null || !autoCast.canCast(type.getType()))
			throw new ChainVerificationException(this, "invalid param type: "+type);
	}
	
	private ChainType getParamForVerification(VerificationData data) throws ChainVerificationException {
		if (data.containsVariable(this)) {
			return data.getVariable(this);
		} else if (isParamRequired()) {
			throw new ChainVerificationException(this, "missing param");
		}
		return null;
	}
}
