package takutility.datachain.filter;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.DataPersistence;

public interface Converter<I,O> extends FullConverter<I,O> {
	@Override
	default DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		in.dataStream().forEach(i ->
			i.set(convert(params, i.<I>get(inputAutoCast()), i))
		);
		return in;
	}
	
	O convert(InnerChainParams params, I in, DataPersistence pers);
}
