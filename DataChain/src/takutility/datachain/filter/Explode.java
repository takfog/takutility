package takutility.datachain.filter;

import java.util.ArrayList;

import takutility.datachain.chain.ChainType;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class Explode<E> implements FullConverter<Iterable<E>, E> {

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(new ChainType(Iterable.class,  ChainType.ANY), this);
		data.setCurrentType(data.getCurrentType().getSubType(0));
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		ArrayList<E> elems = new ArrayList<>();
		for (Data data : in.get()) {
			for (E e : (Iterable<E>)data.get()) {
				elems.add(e);
			}
		}
		return new InputDataContainer(elems);
	}

}
