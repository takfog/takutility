package takutility.datachain.filter;

import java.util.List;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ScriptParse;

public class NullFilter<T> implements Filter<T> {
	private boolean keep = false;
	
	public NullFilter() {}
	
	public NullFilter(boolean keep) {
		this.keep = keep;
	}
	
	@Override
	public boolean filter(InnerChainParams params, T in, DataPersistence pers) {
		return keep == (in == null);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		if (args.isEmpty())
			return new NullFilter<>();
		else if ("keep".equals(args))
			return new NullFilter<>(true);
		else
			return new NullFilter<>(Boolean.parseBoolean(args));
	}
}
