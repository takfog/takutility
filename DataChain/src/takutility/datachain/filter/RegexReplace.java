package takutility.datachain.filter;

import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import takutility.composite.Pair;
import takutility.datachain.ClassCast;
import takutility.datachain.StandardClassCast;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.RegexParser;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.util.PairChainType;
import takutility.datachain.util.RegexUtil;

public class RegexReplace extends ParametrizedImpl<Pair<Pattern, String>> 
	implements Converter<String,String> 
{
	public RegexReplace(String regex, String replace) {
		this(Pattern.compile(regex), replace);
	}
	
	public RegexReplace(Pattern regex, String replace) {
		super(null, new Pair<>(regex, replace));
	}
	
	public RegexReplace(String id) {
		super(id, null);
	}
	
	
	@Override
	public ClassCast<String> inputAutoCast() {
		return StandardClassCast.STRING;
	}
	
	@Override
	protected ClassCast<Pair<Pattern, String>> paramAutoCast() {
		return RegexUtil.castReplace(String.class, Function.identity());
	}


	@Override
	public String convert(InnerChainParams params, String in, DataPersistence pers) {
		Pair<Pattern, String> p = getParam(params);
		Matcher m = p.a().matcher(in);
		return m.replaceAll(p.b());
	}
	
	@Override
	public String toString() {
		Pair<Pattern, String> p = getDefaultParam();
		return getClass().getSimpleName()+"@"+Integer.toHexString(hashCode()) 
			+ (p == null ? "" : " "+p);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		verifyParam(data, new PairChainType(Pattern.class, String.class));
		data.setCurrentType(String.class);
	}
	
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		return ParseUtils.simpleArgs(args, RegexReplace::new, 
				null, str -> RegexParser.parseCompile(str, 
						(p,r) -> new RegexReplace(p, r)));
	}
}
