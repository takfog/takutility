package takutility.datachain.filter;

import java.util.Iterator;
import java.util.List;

import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.in.Input;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.parse.ScriptUtils;

public class InnerInput<I,O> implements FullConverter<I, O> {
	private Input<I, O> input;
	
	public InnerInput(Input<I, O> input) {
		this.input = input;
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		Iterator<Data> it = in.get().iterator();
		if (!it.hasNext())
			return in;
		
		ChainParams<I> newPar = new ChainParams<>(it.next().get());
		newPar.setAll(params);
		return input.get(newPar);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		Input input = ScriptUtils.parseSingleSub(args, params, Input.class);
		return new InnerInput<>(input);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		input.verify(data);
	}
}
