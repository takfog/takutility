package takutility.datachain.filter;

import java.lang.reflect.Method;
import java.util.List;

import takutility.composite.Pair;
import takutility.datachain.TypedParamElem;
import takutility.datachain.chain.ChainType;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ParseUtils;
import takutility.datachain.parse.ScriptParse;

public class Call<T>  extends Return<T, T> {

	public Call(Class<T> clazz, String name, TypedParamElem... elements) {
		super(clazz, name, null, elements);
	}
	
	public Call(Method method, TypedParamElem... elements) {
		super(method, null, elements);
	}
	
	@Override
	public T convert(InnerChainParams params, T in, DataPersistence pers) {
		super.convert(params, in, pers);
		return in;
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		ChainType currentType = data.getCurrentType();
		super.verify(data);
		data.setCurrentType(currentType);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		Pair<Class<?>, String> clMeth = ParseUtils.deserializeMethod(args);
		if (clMeth.a() == null)
			throw new IllegalArgumentException("No class defined");
		if (clMeth.b() == null)
			throw new IllegalArgumentException("No method defined");
		
		return new Call<>(clMeth.a(), clMeth.b(), 
				ParseUtils.typedParamElems(params));
	}
}
