package takutility.datachain.filter;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import takutility.datachain.chain.ChainType;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class Collect<T> implements FullConverter<T, List<T>> {

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		List<Object> list = in.stream().collect(Collectors.toList());
		return new InputDataContainer(Collections.singleton(list));
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(new ChainType(List.class, data.getCurrentType()));
	}
}
