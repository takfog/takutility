package takutility.datachain.filter;

import java.util.regex.Pattern;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class RegexQuote implements Converter<String, String> {

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(String.class, this);
	}

	@Override
	public String convert(InnerChainParams params, String in, DataPersistence pers) {
		return Pattern.quote(in);
	}

}
