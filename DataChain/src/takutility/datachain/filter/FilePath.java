package takutility.datachain.filter;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import takutility.composite.Pair;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.util.PairChainType;

public interface FilePath extends Converter<File, String> {
	
	public static FilePath fullPath() {
		return f -> f.getAbsolutePath();
	}
	
	public static FilePath fileName() {
		return f -> f.getName();
	}
	
	public static FilePath nameWithoutExtension() {
		return FilePath::nameWithoutExtensionFromFile;
	}
	
	public static FilePath extension() {
		return FilePath::extensionFromFile;
	}

	public static String nameWithoutExtensionFromFile(File file) {
		String name = file.getName();
		int ext = name.lastIndexOf('.');
		if (ext < 1)
			return name;
		else
			return name.substring(0, ext);
	}

	public static String extensionFromFile(File file) {
		String name = file.getName();
		int ext = name.lastIndexOf('.');
		if (ext < 1)
			return "";
		else
			return name.substring(ext);
	}

	public static FilePath parent() {
		return f -> f.getParentFile().getAbsolutePath();
	}
	
	public static Converter<File, Pair<String,String>> parentAndName() {
		return new Converter<File, Pair<String,String>>() {
			@Override
			public Pair<String, String> convert(InnerChainParams p, File f, DataPersistence r) {
				return new Pair<>(
						f.getParentFile().getAbsolutePath(), 
						f.getName());
			}
			@Override
			public void verify(VerificationData data) throws ChainVerificationException {
				data.verifyType(File.class, this);
				data.setCurrentType(new PairChainType(String.class));
			}
		};
	}
	
	@Override
	default String convert(InnerChainParams params, File in, DataPersistence pers) {
		return convert(in);
	}
	
	String convert(File in);
	
	@Override
	default void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(File.class, this);
		data.setCurrentType(String.class);
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		try {
			Method method = FilePath.class.getDeclaredMethod(args);
			return method.invoke(null);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new IllegalArgumentException(args + " is not a valid method");
		}
	}
}
