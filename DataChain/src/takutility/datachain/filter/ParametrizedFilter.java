package takutility.datachain.filter;

import java.util.function.Predicate;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataPersistence;

public class ParametrizedFilter<T> extends ParametrizedImpl<Predicate<T>> implements Filter<T>
{

	public ParametrizedFilter(Predicate<T> filter) {
		super(null, filter);
	}

	public ParametrizedFilter(String id) {
		super(id, null);
	}

	@Override
	public boolean filter(InnerChainParams params, T in, DataPersistence pers) {
		return getParam(params).test(in);
	}

}
