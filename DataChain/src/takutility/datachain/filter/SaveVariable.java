package takutility.datachain.filter;

import java.util.List;
import java.util.Optional;

import takutility.datachain.ParamElem;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.DataPersistence;
import takutility.datachain.parse.ScriptParse;

public class SaveVariable<T> implements FullConverter<T, T> {
	private String var;
	private ParamElem element;
	private boolean replace;

	public SaveVariable(String var, ParamElem element) {
		this(var, element, true);
	}

	public SaveVariable(String var, ParamElem element, boolean replace) {
		this.var = var;
		this.element = element;
		this.replace = replace;
	}
	
	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		if (replace || !params.contains(var)) {
			Optional<Data> first = in.dataStream().findFirst();
			if (first.isPresent()) 
				params.set(var, element.get(params, first.get().get(), DataPersistence.empty()));
			else
				params.set(var, element.get(params, null, first.get()));
		}
		return in;
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.saveVariable(var, data.getTypeFromParam(element));
	}
	
	@SuppressWarnings("rawtypes")
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		String[] split = args.split(" ",2);
		String var = split[0].trim();
		boolean replace = true;
		if (var.startsWith("?")) {
			var = var.substring(1);
			replace = false;
		}
		return new SaveVariable(var, ParamElem.deserialize(split[1]), replace);
		
	}

}
