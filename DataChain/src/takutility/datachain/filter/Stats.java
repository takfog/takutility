package takutility.datachain.filter;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.DataContainer;
import takutility.datachain.parse.ScriptParse;

@ScriptParse
public class Stats<T> implements FullConverter<T, T> {

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		System.out.println("Count: "+in.get().size());
		return in;
	}

}
