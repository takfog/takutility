package takutility.datachain.filter;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InputDataContainer;
import takutility.datachain.parse.ScriptParse;
import takutility.datachain.util.CollectionType;

/**
 * Element that uses the collection given to the constructor
 * directly as output, ignoring and discarding the input data. <br>
 * <b>Script syntax:</b><br>
 * the params will be the otput of the element. If not set, 
 * the output will be the argument element only.
 * @param <I> expected input type 
 * @param <O> type of the elements
 */
public class FixedData<I, O> implements FullConverter<I, O> {
	private Collection<O> cont;
	
	public FixedData(Collection<O> cont) {
		this.cont = cont;
	}
	
	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		return new InputDataContainer(cont);
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.setCurrentType(CollectionType.get(cont));
	}

	@ScriptParse
	public static Object parse(String args, List<String> params) {
		if (params.size() > 0)
			return new FixedData<>(params);
		else
			return new FixedData<>(Collections.singleton(args));
	}
}
