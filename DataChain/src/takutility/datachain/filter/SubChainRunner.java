package takutility.datachain.filter;

import takutility.datachain.chain.ChainType;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.SubChain;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.data.DataContainer;
import takutility.datachain.out.DataStorage;

public class SubChainRunner<I,O> extends ParametrizedImpl<SubChain<I, O>>
	implements FullConverter<I, O> //Parametrized<SubChain<I, O>>
{
	
	public SubChainRunner() {
		super(null, null);
	}
	
	public SubChainRunner(SubChain<I, O> chain) {
		super(null, chain);
	}
	
	public SubChainRunner(String id) {
		super(id, null);
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		DataStorage<O> storage = new DataStorage<>();
		SubChain<I, O> chain = getParam(params);
		chain.setClosure(storage);
		
		chain.next(params, in);
		return storage.getData();
	}
	
	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		verifyParam(data, new ChainType(SubChain.class, ChainType.ANY, ChainType.ANY));
	}
}
