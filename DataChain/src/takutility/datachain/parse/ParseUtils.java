package takutility.datachain.parse;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import takutility.composite.Pair;
import takutility.datachain.ParamElem;
import takutility.datachain.TypedParamElem;
import takutility.datachain.chain.link.ChainLink;
import takutility.datachain.out.EmptyOutput;

public class ParseUtils {
	private static final Pattern argsRegex = 
			Pattern.compile("^\\s*(\"((?:\\\\.|[^\"\\\\])*)\"|(?<=^|\\s)[^\\s\"]\\S*)");
	private static final Pattern paramRegex = Pattern.compile("\\s*-\\s*(\\S.*|)");

	public static String[] argsToArray(String args) {
		args = args.trim();
		if (args.isEmpty())
			return new String[0];
		ArrayList<String> arr = new ArrayList<>();
		Matcher m = argsRegex.matcher(args);
		m.useTransparentBounds(false);
		while(m.find()) {
			if (m.group(2) == null)
				arr.add(m.group(1));
			else
				arr.add(m.group(2).replace("\\\"", "\"").replace("\\\\", "\\"));
			m.region(m.end(), m.regionEnd());
		}
		if (arr.size() == 0 || m.regionStart() != args.length()) 
			throw new IllegalArgumentException("Invalid argument list");
		
		return arr.toArray(new String[arr.size()]);
	}
	
	public static <T> T simpleArgs(String args, Function<String, T> withId, 
			Supplier<T> empty,
			Function<String, T> otherwise) 
	{
		String trim = args.trim();
		if (trim.isEmpty()) {
			if (empty != null)
				return empty.get();
		} else {
			if (trim.startsWith("?") && withId != null)
				return withId.apply(trim.substring(1));
		}
		return otherwise.apply(args);
	}
	
	public static ParamElem[] paramElems(List<String> params) {
		return params.stream()
				.map(ParamElem::deserialize)
				.toArray(n -> new ParamElem[n]);
	}
	
	public static TypedParamElem[] typedParamElems(List<String> params) {
		return params.stream()
				.map(TypedParamElem::deserialize)
				.toArray(n -> new TypedParamElem[n]);
	}
	
	public static Class<?> deserializeClass(String className) {
			try {
				if (!className.contains("."))
					className = "java.lang."+className;
				return Class.forName(className);
			} catch (ClassNotFoundException e) {
				throw new IllegalArgumentException(e);
			}
	}
	
	public static Pair<Class<?>,String> deserializeMethod(String str) {
		int split = str.indexOf("::");
		if (split < 0 || split == str.length()-2)
			return new Pair<>(deserializeClass(str), null);
		else if (split == 0)
			return new Pair<>(null, str);
		else  {
			Class<?> clazz = deserializeClass(str.substring(0, split));
			return new Pair<>(clazz, str.substring(split+2));
		}
			
	}

	public static void parse(Iterable<String> lines, Consumer<ChainLink<?>> consumer) { 
		parse(lines, consumer, null);
	}

	public static void parse(Iterable<String> lines, Consumer<ChainLink<?>> consumer, 
			Consumer<ScriptOption> options) 
	{ 
		parse(lines, (e,i) -> consumer.accept(e.toLink(i==0, i==2)), options);
	}

	public static void parse(Iterable<String> lines, 
			BiConsumer<ParsingElement, Integer> consumer)
	{
		parse(lines, consumer, null);
	}

	public static void parse(Iterable<String> lines, 
			BiConsumer<ParsingElement, Integer> consumer,
			Consumer<ScriptOption> options)
	{
		ArrayList<ParsingElement> elements = new ArrayList<>();
		ParsingElement el = null;
		for (String line : lines) {
			String trim = line.trim();
			if (trim.startsWith("#opt ")) {
				if (options != null) {
					options.accept(ScriptOption.parse(trim.substring(5)));
				}
				continue;
			}
			if (trim.isEmpty() || trim.startsWith("#"))
				continue;
			Matcher m = paramRegex.matcher(line);
			if (m.matches()) {
				//param
				if (el == null)
					throw new IllegalStateException("No active element for param: "+line);
				el.add(m.group(1));
			} else {
				//new element
				if (el != null)
					elements.add(el);
				el = ParsingElement.parse(line);
			}
		}
		if (el != null)
			elements.add(el);
		
		if (elements.size() > 0)
			consumer.accept(elements.get(0), 0);
		for(int i=1; i<elements.size()-1; i++) {
			consumer.accept(elements.get(i), 1);
		}
		if (elements.size() > 1)
			consumer.accept(elements.get(elements.size()-1), 2);
		else
			consumer.accept(ParsingElement.parse(EmptyOutput.class.getCanonicalName()), 2);
	}
	
	public static Object hasParse(Class<?> clazz) {
		ScriptParse annotation = clazz.getAnnotation(ScriptParse.class);
		if (annotation != null) {
			return clazz;
		} else {
			for (Method method : clazz.getDeclaredMethods()) {
				if (method.isAnnotationPresent(ScriptParse.class)
						&& Modifier.isStatic(method.getModifiers()))
				{
					@SuppressWarnings("rawtypes")
					Class[] types = method.getParameterTypes();
					if (types.length == 2 
							&& types[0] == String.class
							&& types[1] == List.class)
					{
						return method;
					}
				}
			}
		}
		return null;
	}
}
