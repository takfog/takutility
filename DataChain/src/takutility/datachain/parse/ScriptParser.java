package takutility.datachain.parse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

import takutility.collections.AutoMap;
import takutility.datachain.async.AsyncConverter;
import takutility.datachain.chain.Chain;
import takutility.datachain.chain.ChainBuilder;
import takutility.datachain.chain.link.AsyncLink;
import takutility.datachain.chain.link.ChainLink;
import takutility.datachain.chain.link.CommandLineStartLink;
import takutility.datachain.chain.link.DefaultStartLink;
import takutility.datachain.chain.link.EndLink;
import takutility.datachain.chain.link.ImplicitCommandLineStartLink;
import takutility.datachain.chain.link.InnerLink;
import takutility.datachain.chain.link.InputSaveStartLink;
import takutility.datachain.chain.link.MonitoredEndLink;
import takutility.datachain.filter.FullConverter;
import takutility.datachain.in.CommandLineInput;
import takutility.datachain.in.Input;
import takutility.datachain.out.FullOutput;
import takutility.io.UTF8BufferedReader;

public class ScriptParser<T> {
	private Chain<T> chain;
	private Map<ScriptOption.Type, Collection<ScriptOption>> options;
	private boolean cli = false;
	private Runnable endListener = null;
	
	public ScriptParser() {
		options = new AutoMap<>(k -> new HashSet<>(), 
				new EnumMap<>(ScriptOption.Type.class));
	}
	
	protected ScriptParser(boolean cli) {
		this();
		this.cli = cli;
	}
	
	public void setEndListener(Runnable endListener) {
		this.endListener = endListener;
	}
	
	public void load(File file) throws FileNotFoundException, IOException {
		try (UTF8BufferedReader r = new UTF8BufferedReader(file)) {
			load((Iterable<String>)r);
		}
	}
	
	public void load(String script) throws FileNotFoundException, IOException {
		load(new StringReader(script));
	}
	
	public void load(Reader r) throws FileNotFoundException, IOException {
		load(new BufferedReader(r).lines().collect(Collectors.toList()));
	}
	
	@SuppressWarnings("unchecked")
	public void load(Iterable<String> lines) {
		final ChainBuilder<T> builder = new ChainBuilder<>();
		ParseUtils.parse(lines, (e,i) -> {
				builder.build().attach(elToLink(e, i));
			}, o -> options.get(o.getType()).add(o));
		if (options.containsKey(ScriptOption.Type.INPUT)) {
			ScriptOption opt = options.get(ScriptOption.Type.INPUT).iterator().next();
			chain = new Chain<>(new InputSaveStartLink<>(opt.value(), builder.getChain()));
		} else {
			chain = builder.getChain();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ChainLink elToLink(ParsingElement e, int i) {
		Object obj = e.toObject();
		if (obj instanceof ChainLink) {
			return (ChainLink<?>) obj;
		} else {
			//parse element
			if (i == 0) {
				if (cli) {
					if (obj instanceof CommandLineInput) {
						return new CommandLineStartLink<>(
								(CommandLineInput<?,?>)obj);
					} else {
						//add default input
						return new ImplicitCommandLineStartLink<>(
								(FullConverter<String,?>)obj);
					}
				} else
					return new DefaultStartLink<>((Input<?,?>)obj);
			} else if (i == 2) {
				if (endListener == null)
					return new EndLink<>((FullOutput<?>)obj);
				else
					return new MonitoredEndLink<>((FullOutput<?>)obj, endListener);
			} else {
				if (obj instanceof FullConverter)
					return new InnerLink<>((FullConverter<?, ?>)obj);
				else if (obj instanceof AsyncConverter)
					return new AsyncLink<>((AsyncConverter<?, ?>)obj);
				else
					throw new IllegalStateException("Invalid inner element: "+obj.getClass());
			}
		}
	}
	
	public Chain<T> getChain() {
		return chain;
	}
	
	public Collection<ScriptOption> getOptions(ScriptOption.Type type) {
		if (options.containsKey(type))
			return options.get(type);
		else
			return Collections.emptyList();
	}
	
	public boolean hasOption(ScriptOption.Type type) {
		return options.containsKey(type);
	}

}
