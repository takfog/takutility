package takutility.datachain.parse;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

public class ParsingMap {
	private static Map<String, Parsable> map = null;
	
	private static void init() {
		if (map != null) return;
		map = new HashMap<>();
		Properties prop = null;
		String filePath = "datachain/script-classes.properties";
		try {
			Enumeration<URL> urls = ParsingMap.class.getClassLoader().getResources(filePath);
			if (!urls.hasMoreElements()) {
				throw new FileNotFoundException("Cannot find script class file "+filePath);
			}
			while (urls.hasMoreElements()) {
				URL url = urls.nextElement();
				InputStream stream = url.openStream();
				prop = new Properties();
				prop.load(stream);
				stream.close();
				for (Entry<Object, Object> e : prop.entrySet()) {
					addClass(e.getKey().toString(), e.getValue().toString());
				}
			}
		} catch (IOException e) {
			System.err.println("Error loading script class file "+filePath);
			return;
		}
		for (Entry<Object, Object> e : prop.entrySet()) {
			addClass(e.getKey().toString(), e.getValue().toString());
		}
	}
	
	public static Parsable get(String key) {
		init();
		if (map.containsKey(key))
			return map.get(key);
		else {
			addClass(key,key);
			if (map.containsKey(key))
				return map.get(key);
			else
				throw new RuntimeException("Script class not found: "+key);
		}
	}
	
	private static void addClass(String key, String className) {
		init();
		try {
			Object parse = ParseUtils.hasParse(Class.forName(className));
			if (parse == null) return;
			if (parse.getClass() == Class.class) {
				map.put(key,(a,p) -> {
					try {
						return ((Class<?>)parse).newInstance();
					} catch (Exception ex) {
						throw new RuntimeException(ex);
					}
				});
			} else if (parse.getClass() == Method.class) {
				map.put(key, (a,p) -> {
					try {
						return ((Method)parse).invoke(null, a, p);
					} catch(IllegalAccessException|IllegalArgumentException ex) {
						throw new RuntimeException(ex);
					} catch(InvocationTargetException ex) {
						Throwable cause = ex.getCause();
						if (cause.getCause() == null 
								|| !cause.getMessage().isEmpty())
						{
							throw new RuntimeException(cause);
						} else {
							throw new RuntimeException(cause.getCause());
						}
					}
				});
			}
		} catch (ClassNotFoundException ex) {}
	}
	
	public static Set<String> validKeys() {
		init();
		return Collections.unmodifiableSet(map.keySet());
	}
}
