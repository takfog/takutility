package takutility.datachain.parse;

import java.util.List;

public interface Parsable {
	Object parse(String args, List<String> params);
}
