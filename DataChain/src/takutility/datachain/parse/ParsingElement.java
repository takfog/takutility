package takutility.datachain.parse;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import takutility.datachain.chain.link.ChainLink;
import takutility.datachain.chain.link.DefaultStartLink;
import takutility.datachain.chain.link.EndLink;
import takutility.datachain.chain.link.InnerLink;
import takutility.datachain.filter.FullConverter;
import takutility.datachain.in.Input;
import takutility.datachain.out.FullOutput;

public class ParsingElement {
	private static final Pattern argsRegex = Pattern.compile("(\\S+)\\s*(.*)");
	
	private String className;
	private String args;
	private List<String> params = null;
	
	public static ParsingElement parse(String firstLine) {
		Matcher m = argsRegex.matcher(firstLine);
		if (!m.matches())
			return null;
		else
			return new ParsingElement(m);
	}
		
	private ParsingElement(Matcher m) {
		className = m.group(1);
		args = m.group(2);
	}
	
	public void add(String param) {
		if (params == null)
			params = new ArrayList<>();
		params.add(param);
	}
	
	public ChainLink<?> toLink(boolean start, boolean end) {
		Object obj = toObject();
		if (obj instanceof ChainLink) {
			return (ChainLink<?>) obj;
		} else {
			//parse element
			if (start) {
				return new DefaultStartLink<>((Input<?,?>)obj);
			} else if (end) {
				return new EndLink<>((FullOutput<?>)obj);
			} else {
				return new InnerLink<>((FullConverter<?, ?>)obj);
			}
		}
	}

	public Object toObject() {
		try {
			return ScriptUtils.parse(className, args, params);
		}catch(Exception e) {
			throw new RuntimeException("Error while parsing "
					+className+" "+args, e);
		}
	}
	
	
}