package takutility.datachain.parse;

public class ScriptOption {
	private Type type;
	private String value;
	
	public static ScriptOption parse(String value) {
		value = value.trim();
		String[] split = value.split(" ",2);
		Type type = Type.deserialize(split[0]);
		if (type == null)
			throw new IllegalArgumentException("Not a valid type in: "+value);
		return new ScriptOption(type, split.length > 1 ? split[1] : null);
	}
	
	public ScriptOption(Type type, String value) {
		this.type = type;
		this.value = value;
	}

	public Type getType() {
		return type;
	}

	public String value() {
		return value;
	}
	
	@Override
	public String toString() {
		return type + " " + value;
	}

	public static enum Type {
		VAR("var"), OPT_VAR("?var"), INPUT("input"), NO_SPLIT("no-split");
		
		private String ser;
		
		private Type(String ser) {
			this.ser = ser;
		}
		
		public static Type deserialize(String val) {
			for (Type t : Type.values()) {
				if (t.ser.equals(val))
					return t;
			}
			return null;
		}
	}
}
