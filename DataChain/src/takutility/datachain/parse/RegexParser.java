package takutility.datachain.parse;

import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexParser {
	private static final Pattern regexParser = 
			Pattern.compile("\\s*/(|(?:\\\\\\\\|\\\\.|[^/\\\\])*)/((?:[^/]|\\\\/)*)/\\s*");

	public static <T> T parseCompile(String str, BiFunction<Pattern, String, T> conv) {
		return parse(str, (p,r) -> conv.apply(Pattern.compile(p), r));
	}

	public static <T> T parse(String str, BiFunction<String, String, T> conv) {
		Matcher m = regexParser.matcher(str);
		if (!m.matches())
			throw new IllegalArgumentException("Not a valid regex: "+str);
		return conv.apply(m.group(1), m.group(2));
	}
}
