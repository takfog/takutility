package takutility.datachain.parse;

import java.util.Collections;
import java.util.List;

import takutility.datachain.chain.InnerChainParams;
import takutility.ptr.Ptr;

public class ScriptUtils {

	public static Object parse(String className, String args, List<String> params) {
		return ParsingMap.get(className).parse(args == null ? "" : args,
				params == null ? Collections.emptyList() : params);
	}

	public static Object parseClass(String className) {
		return ParsingMap.get(className).parse("", Collections.emptyList());
	}

	public static Object parseSingleSub(String args, List<String> params) {
		if (params.size() > 0) {
			Ptr<Object> in = new Ptr<>();
			ParseUtils.parse(params, (e,i) -> {
				if (i != 0)
					throw new IllegalArgumentException(InnerChainParams.class.getSimpleName()
							+ " accepts only a single input element");
				in.val = e.toObject();
			});
			return in.val;
		} else if (!args.isEmpty()) {
			String[] parts = args.split(" ",2);
			if (parts.length == 1)
				return parseClass(parts[0]);
			else
				return parse(parts[0], parts[1], null);
		}
		return null;
	}

	public static <T> T parseSingleSub(String args, List<String> params, Class<T> type) {
		Object sub = parseSingleSub(args, params);
		if (sub == null)
			throw new IllegalArgumentException("No sub element set");
		if (!type.isInstance(sub))
			throw new IllegalArgumentException("Sub element is not a "+type);
		return type.cast(sub);
	}

	public static Object parseSingleSub(String args, List<String> params,
			Class<?> type, Class<?>... types) 
	{
		Object sub = parseSingleSub(args, params, type);
		for (Class<?> t : types) {
			if (!t.isInstance(sub))
				throw new IllegalArgumentException("Sub element is not a "+t);
		}
		return sub;
	}

}
