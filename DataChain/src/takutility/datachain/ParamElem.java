package takutility.datachain;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.DataPersistence;

public class ParamElem {
	protected enum Source { OBJECT, VAR, PERS, IN }
	
	public static final ParamElem in = new ParamElem();
	
	private Object obj;
	private Source source;
	
	public static ParamElem var(String varName) {
		return new ParamElem(varName, Source.VAR);
	}
	
	public static ParamElem pers(String varName) {
		return new ParamElem(varName, Source.PERS);
	}

	public static ParamElem[] deserializeArray(List<String> ser) {
		return ser.stream()
			.map(ParamElem::deserialize)
			.toArray(n -> new ParamElem[n]);
	}

	public static ParamElem deserialize(String ser) {
		if (ser.startsWith("=")) {
			return new ParamElem(ser.substring(1));
		}
		String[] parts = ser.split(" ",2);
		if (parts.length == 0)
			throw new IllegalArgumentException("Empty string");
		else {
			switch (parts[0].toLowerCase()) {
			case "in":
				return in;
			case "pers":
				return pers(parts[1]);
			case "var":
				return var(parts[1]);
			default:
				throw new IllegalArgumentException("Invalid param type: "+parts[0]);
			}
		}
	}
	
	private ParamElem() {
		this(null, Source.IN);
	}

	public ParamElem(Object obj) {
		this(obj, Source.OBJECT);
	}

	protected ParamElem(Object obj, Source source) {
		this.obj = obj;
		this.source = source;
	}

	public Object get(InnerChainParams params) {
		return get(params, null, DataPersistence.empty());
	}
	public Object get(InnerChainParams params, Data in) {
		return get(params, in.get(), in);
	}

	public Object get(InnerChainParams params, Object in, DataPersistence pers) {
		switch (source) {
		case OBJECT:
			return obj;
		case VAR:
			return params.get(obj.toString());
		case PERS:
			return pers.getPersisted(obj.toString());
		case IN:
			return in;
		}
		return null;
	}

	public Object get(Function<String,?> params, Supplier<Object> in, Function<String,?> pers) {
		switch (source) {
		case OBJECT:
			return obj;
		case VAR:
			return params.apply(obj.toString());
		case PERS:
			return pers.apply(obj.toString());
		case IN:
			return in.get();
		}
		return null;
	}

	public Stream<Object> getStream(InnerChainParams params, DataContainer data) {
		switch (source) {
		case OBJECT:
			return Stream.of(obj);
		case VAR:
			return Stream.of(params.get(obj.toString()));
		case PERS:
			return data.dataStream().map(d -> d.getPersisted(obj.toString()));
		case IN:
			return data.stream();
		}
		return null;
	}
	
	public boolean isInput() {
		return source == Source.IN;
	}
	
	public boolean isFixed() {
		return source == Source.OBJECT;
	}
	
	@Override
	public String toString() {
		switch (source) {
		case OBJECT:
			return "="+obj.toString();
		case VAR:
			return "[variable]="+obj.toString();
		case PERS:
			return "[pers]="+obj.toString();
		case IN:
			return "[input]";
		}
		return super.toString();
	}
}
