package takutility.datachain;

import takutility.datachain.parse.ParseUtils;

public class TypedParamElem extends ParamElem {
	private Class<?> type;
	
	public static TypedParamElem var(String varName, Class<?> type) {
		return new TypedParamElem(varName, Source.VAR, type);
	}
	
	public static TypedParamElem pers(String varName, Class<?> type) {
		return new TypedParamElem(varName, Source.PERS, type);
	}
	
	public static TypedParamElem type(Class<?> type) {
		return new TypedParamElem(null, Source.OBJECT, type);
	}
	
	public static TypedParamElem in(Class<?> type) {
		return new TypedParamElem(null, Source.IN, type);
	}
	
	public static TypedParamElem deserialize(String ser) {
		if (ser.startsWith("=")) {
			return new TypedParamElem(ser.substring(1));
		}
		String[] parts = ser.split(" ",3);
		if (parts.length == 0)
			throw new IllegalArgumentException("Empty string");
		else {
			Class<?> type;
			type = ParseUtils.deserializeClass(parts[0]);
			if (parts.length < 3)
				return in(type);
			switch (parts[1].toLowerCase()) {
			case "pers":
				return pers(parts[2], type);
			case "var":
				return var(parts[2], type);
			default:
				throw new IllegalArgumentException("Invalid param type: "+parts[1]);
			}
		}
	}
	
	public TypedParamElem(Object obj) {
		this(obj, Source.OBJECT, obj.getClass());
	}
	
	public <T> TypedParamElem(T obj, Class<? extends T> type) {
		this(obj, Source.OBJECT, type);
	}
	
	private TypedParamElem(Object obj, Source var, Class<?> type) {
		super(obj, var);
		this.type = type;
	}
	
	public Class<?> getType() {
		return type;
	}
	
	@Override
	public String toString() {
		return super.toString()+" : "+type;
	}
}
