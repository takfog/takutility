package takutility.datachain.meta;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import takutility.datachain.IdSupplier;
import takutility.datachain.ParamElem;
import takutility.datachain.TypedParamElem;
import takutility.datachain.chain.ChainBuilder;
import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.MultiChainBuilder;
import takutility.datachain.filter.Concat;
import takutility.datachain.filter.Distinct;
import takutility.datachain.filter.Filter;
import takutility.datachain.filter.Persist;
import takutility.datachain.filter.RegexReplace;
import takutility.datachain.filter.RelativePath;
import takutility.datachain.filter.Return;
import takutility.datachain.in.CollectionInput;
import takutility.datachain.in.FileList;
import takutility.datachain.out.ToFile;
import takutility.datachain.parse.ParseUtils;

public class ScriptClassListBuilder {
	private static final String FULL_CLASS = "fullClass";

	@SuppressWarnings("rawtypes")
	public static void main(String[] args) {
		if (args.length > 0 && args[0].startsWith("-h")) {
			System.out.println("Usage: [-o output_dir] [input_dir [input_dir2...]]");
			System.out.println("                       default input dir: src");
			return;
		}
		File out;
		Collection<File> src;
		int idx = 0;
		if (args.length > 1 && args[0].equals("-o")) {
			out = new File(args[1], "script-classes.properties");
			idx = 2; 
		} else {
			out = new File("bin/datachain/script-classes.properties");
		}
		if (args.length > idx) {
			src = IntStream.range(idx, args.length)
					.mapToObj(i -> new File(args[i]))
					.collect(Collectors.toList());
		} else {
			src = Collections.singleton(new File("src"));
		}
		
		Method forName;
		try {
			forName = Class.class.getDeclaredMethod("forName", String.class);
		} catch (NoSuchMethodException | SecurityException e) {
			return;
		}
		
//		ChainBuilder<File> oldFileBuilder = new ChainBuilder<>();
//		oldFileBuilder.open(new FileLines());
		
		ChainBuilder<Collection<File>> fromClassBuilder = new ChainBuilder<>();
		fromClassBuilder.open(new CollectionInput<File>())
				.join(new Persist<>("base"))
				.join(FileList.convertFile(true))
				.join(new RelativePath(ParamElem.pers("base")))
				.join(new RegexReplace("\\.java", ""))
				.join(new RegexReplace(Pattern.quote(File.separator),"."))
				.join(new Persist<>(FULL_CLASS))
				.join(new Return<>(forName, Class.class, TypedParamElem.in(String.class)))
				.join((Filter<Class>)(
						(p,v,x)-> ParseUtils.hasParse(v) != null))
				.join(new Return<>(Class.class, "getSimpleName", String.class))
				.join(new Concat<>(
						ParamElem.in, 
						new ParamElem(" = "),
						ParamElem.pers(FULL_CLASS)))
				;

		MultiChainBuilder<String> builder = new MultiChainBuilder<>(String.class);
		builder.build()
				.join(new Distinct<>())
				.close(new ToFile(out));
//		builder.addSource(oldFileBuilder);
		builder.addSource(fromClassBuilder);
		builder.attachSources();
		
		IdSupplier idSupplier = new IdSupplier();
		fromClassBuilder.getChain().start(new ChainParams<>(src, idSupplier));
//		oldFileBuilder.getChain().start(new ChainParams<>(out,idSupplier));
	}
}
