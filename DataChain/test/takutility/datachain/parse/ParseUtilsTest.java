package takutility.datachain.parse;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;
import static takutility.datachain.parse.ParseUtils.argsToArray;

import java.util.Arrays;

import org.junit.Test;

public class ParseUtilsTest {

	@Test
	public void testArgsToArray() {
		assertArrayEquals(new String[0], argsToArray(""));
		assertArrayEquals(new String[0], argsToArray("   "));

		assertArrayEquals(new String[] {
				"primo", "secondo", "contorno"
		}, argsToArray("primo secondo contorno"));
		
		assertArrayEquals(new String[] {
				"primo", "secondo vegano", "contorno"
		}, argsToArray("primo \"secondo vegano\" contorno"));
		
		assertArrayEquals(new String[] {"con \" apici \" in mezzo"},
				argsToArray("\"con \\\" apici \\\" in mezzo\""));
		
		assertArrayEquals(new String[] {"con \\ slash"},
				argsToArray("\"con \\\\ slash\""));
		
		try {
			String[] arr = argsToArray("\"sbilanciato\\\"");
			fail("sbilanciato "+Arrays.toString(arr));
		}catch(IllegalArgumentException e) {}
		
		try {
			String[] arr = argsToArray("secondo \"sbilanciato\\\" seguito");
			fail("sbilanciato "+Arrays.toString(arr));
		}catch(IllegalArgumentException e) {}

	}

	@Test
	public void testSimpleArgs() {
//		fail("Not yet implemented");
	}

}
