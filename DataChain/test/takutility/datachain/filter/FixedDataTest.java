package takutility.datachain.filter;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.VerificationData;

public class FixedDataTest {

	@Test
	public void testVerify() {
		try {
			FixedData<?, ? extends Object> fixed;
			VerificationData data = new VerificationData();
			
			fixed = new FixedData<>(Arrays.asList("a", "b", "c"));
			fixed.verify(data);
			assertEquals(String.class, data.getCurrentType().getType());
			
			fixed = new FixedData<>(Arrays.asList("asa", 5, 's'));
			fixed.verify(data);
			assertTrue(data.getCurrentType().isAny());
			
			fixed = new FixedData<>(Arrays.asList(new Object(), "asa", 5));
			fixed.verify(data);
			assertEquals(Object.class, data.getCurrentType().getType());
			
		} catch (ChainVerificationException e) {
			fail(e.getMessage());
		}
	}

}
