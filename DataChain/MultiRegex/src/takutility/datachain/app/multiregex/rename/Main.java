package takutility.datachain.app.multiregex.rename;

import java.util.Arrays;

import takutility.datachain.app.multiregex.rename.gui.RegexRenameGui;

public class Main {
	public static void main(String[] args) throws Exception {
		if (args.length == 0)
			RegexRenameGui.main(args);
		else if (args[0].equals("-gui")) 
			RegexRenameGui.main(Arrays.copyOfRange(args, 1, args.length));
		else
			RegexRename.main(args);
	}
}
