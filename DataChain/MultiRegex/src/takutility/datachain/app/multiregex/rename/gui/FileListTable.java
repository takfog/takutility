package takutility.datachain.app.multiregex.rename.gui;

import java.io.File;
import java.util.function.Supplier;

public class FileListTable extends MyTable<File,FileListTableModel> {
	private static final long serialVersionUID = 4945307574333928604L;

	public FileListTable(Supplier<Boolean> recursiveDrop) {
		super(new FileListTableModel());
		setTransferHandler(new FileListTransferHandler(model,recursiveDrop));
		setAutoCreateRowSorter(true);
	}
}
