package takutility.datachain.app.multiregex.rename.gui;

import java.io.File;
import java.util.function.Consumer;
import java.util.function.Predicate;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import takutility.datachain.app.multiregex.rename.gui.RegexTableModel.Type;

public class RegexTable extends MyTable<RegexElement,RegexTableModel> {
	private static final long serialVersionUID = 1L;

	public RegexTable(Consumer<File> fileLoader) {
		super(new RegexTableModel());
		setTransferHandler(new RegexTransferHandler(fileLoader));
		
		JComboBox<Type> cmbTypes = new JComboBox<>(model.types());
		TableColumnModel colModel = getColumnModel();
		TableColumn colType = colModel.getColumn(RegexTableModel.COL_TYPE);
		colType.setCellEditor(new DefaultCellEditor(cmbTypes));
		colType.sizeWidthToFit();
		colType.setCellRenderer(new ValidTableCellRenderer(
				row -> isValid(row, RegexElement::isTypeValid)));
		
		TableColumn colPat = colModel.getColumn(RegexTableModel.COL_PATTERN);
		colPat.setCellRenderer(new ValidTableCellRenderer(
				row -> isValid(row, RegexElement::isRegexValid)));
		TableColumn colRep = colModel.getColumn(RegexTableModel.COL_REPLACE);
		colRep.setCellRenderer(new ValidTableCellRenderer(
			row -> isValid(row, RegexElement::isGroupValid)));

	}

	private boolean isValid(int row, Predicate<RegexElement> pred) {
		RegexElement regexElement = model.get(row);
		if (regexElement == null)
			return true;
		return pred.test(regexElement);
	}
}
