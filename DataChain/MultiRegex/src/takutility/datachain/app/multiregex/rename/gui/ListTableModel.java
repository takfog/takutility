package takutility.datachain.app.multiregex.rename.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import javax.swing.table.AbstractTableModel;

public abstract class ListTableModel<T> extends AbstractTableModel {
	private static final long serialVersionUID = 4841956387736633339L;
	
	private List<T> list = new ArrayList<>();

	@Override
	public int getRowCount() {
		return list.size();
	}

	public int size() {
		return list.size();
	}

	public void add(T elem) {
		if (innerAdd(elem))
			fireTableRowsInserted(list.size()-1, list.size()-1);
	}
	
	protected final boolean innerAdd(T elem) {
		return innerAdd(list.size(), elem);
	}

	protected boolean innerAdd(int pos, T elem) {
		if (pos < list.size()) {
			list.add(pos, elem);
			return true;
		} else
			return list.add(elem);
	}

	public void addAll(Collection<T> newElems) {
		addAll(newElems.stream());
	}

	public void addAll(Stream<T> newElems) {
		int oldSize = list.size();
		newElems.forEach(elem -> innerAdd(elem));
		if (oldSize < list.size()) 
			fireTableRowsInserted(oldSize, list.size()-1);
	}
	
	public int find(T elem) {
		return list.indexOf(elem);
	}

	public void remove(T elem) {
		if (elem != null)
			remove(list.indexOf(elem));
	}

	public void remove(int index) {
		if (innerRemove(index))
			fireTableRowsDeleted(index, index);
	}

	protected boolean innerRemove(int index) {
		list.remove(index);
		return true;
	}
	
	public void insert(int index, T elem) {
		if (innerAdd(index, elem))
			fireTableRowsInserted(index, index);
	}

	public void clear() {
		int size = list.size();
		list.clear();
		fireTableRowsDeleted(0, size-1);
	}

	public T get(int row) {
		if (row < list.size())
			return list.get(row);
		else
			return null;
	}

	public Stream<T> stream() {
		return list.stream();
	}
}