package takutility.datachain.app.multiregex.rename;

import java.io.File;
import java.util.stream.Collectors;

import takutility.composite.Pair;
import takutility.datachain.async.ResynchConverter;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.data.InnerDataContainer;
import takutility.datachain.filter.FullConverter;
import takutility.datachain.filter.ParametrizedImpl;
import takutility.datachain.in.FileList.FileListConfig;
import takutility.datachain.out.DataStorage;

public class RegexRenameLink extends ParametrizedImpl<RegexRenameConfig>
	implements FullConverter<FileListConfig, File>
{

	public RegexRenameLink(String id) {
		super(id, null);
	}

	public RegexRenameLink(RegexRenameConfig config) {
		super(null, config);
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		RegexRenameConfig config = getParam(params);
		DataStorage<Pair<File,File>> data = new DataStorage<>();
		RegexRename regexRename = new RegexRename(new ResynchConverter<>(data));
		regexRename.run(in.<FileListConfig>stream()
				.collect(Collectors.toList()), config);
		
		return new InnerDataContainer(data.getData().dataStream()
				.map(d -> new Data(d.getId(), d.<Pair<File,File>>get().b())));
	}
}
