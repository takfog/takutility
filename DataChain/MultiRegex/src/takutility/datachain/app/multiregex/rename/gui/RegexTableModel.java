package takutility.datachain.app.multiregex.rename.gui;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import takutility.datachain.app.multiregex.MultiRegexStep;

public class RegexTableModel extends ListTableModel<RegexElement> {
	private static final long serialVersionUID = -311383876727206944L;
	public static final int COL_ORD = 0;
	public static final int COL_TYPE = 1;
	public static final int COL_PATTERN = 2;
	public static final int COL_REPLACE = 3;
	
	private String[] cols = new String[] {
		"Ord", "Type", "Pattern", "Replace"	
	};
	
	@Override
	public String getColumnName(int column) {
		return cols[column];
	}
	
	@Override
	public int getColumnCount() {
		return cols.length;
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case COL_ORD:
			return Integer.class;
		case COL_TYPE:
			return Type.class;
		default:
			return String.class;
		}
	}
	
	@Override
	public int getRowCount() {
		return super.getRowCount()+1;
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex >= size())
			return null;
		if (columnIndex == COL_ORD)
			return rowIndex+1;
		RegexElement e = get(rowIndex);
		switch (columnIndex) {
		case COL_TYPE:
			return e.getType();
		case COL_PATTERN:
			return e.getRegex();
		case COL_REPLACE:
			return e.getReplace();
		default:
			return null;
		}
	}

	public Type[] types() {
		return Type.values();
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (columnIndex == COL_ORD) {
			moveAt((Integer)aValue-1, rowIndex);
			return;
		}
		RegexElement el;
		if (rowIndex < size())
			el = get(rowIndex);
		else {
			if (aValue == null || aValue.toString().isEmpty())
				return;
			el = new RegexElement();
			add(el);
		}
		switch (columnIndex) {
		case COL_TYPE:
			el.setType((Type) aValue);
			break;
		case COL_PATTERN:
			el.setRegex((String) aValue);
			break;
		case COL_REPLACE:
			el.setReplace((String) aValue);
			break;
		default:
			return;
		}
		fireTableCellUpdated(rowIndex, columnIndex);
	}
	
	private void moveAt(int pos, int rowIndex) {
		if (rowIndex == pos)
			return;
		if (pos < 0) pos = 0;
		else if (pos > size()) pos = size();
		
		if (rowIndex < size()) {
			RegexElement el = get(rowIndex);
			if (pos < rowIndex) {
				remove(rowIndex);
				insert(pos, el);
			} else {
				insert(pos, el);
				remove(rowIndex);
			}
		} else {
			insert(pos, new RegexElement());	
		}
	}
	
	public List<MultiRegexStep> getSteps() throws ConversionException {
		Optional<String> err = stream().map(RegexElement::verify)
			.filter(v -> v != null)
			.findFirst();
		if (err.isPresent())
			throw new ConversionException(err.get());
		
		return stream().map(RegexElement::toStep)
				.collect(Collectors.toList());
	}
	
	public static enum Type {
		REPLACE("Replace"), 
		SELECT("Select");
		
		private String str;
		
		Type(String str) {
			this.str = str; 
		}
		
		@Override
		public String toString() {
			return str;
		}
	}
	
	public static class ConversionException extends Exception {
		private static final long serialVersionUID = 1L;
		
		public ConversionException(String message) {
			super(message);
		}
	}
}
