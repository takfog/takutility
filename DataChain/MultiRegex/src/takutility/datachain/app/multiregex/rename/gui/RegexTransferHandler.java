package takutility.datachain.app.multiregex.rename.gui;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import javax.swing.TransferHandler;

public class RegexTransferHandler extends TransferHandler {
	private static final long serialVersionUID = 1L;
	private Consumer<File> loader;
	
	public RegexTransferHandler(Consumer<File> loader) {
		this.loader = loader;
	}

	@Override
	public boolean canImport(TransferSupport support) {
		if (!support.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
			return false;
		try {
			List<File> files = getFiles(support);
			if (files.size() != 1)
				return false;
			if (!files.get(0).isFile())
				return false;
		} catch (Exception e) {}
		return true;
	}
	
	@Override
	public boolean importData(TransferSupport support) {
		if (!canImport(support)) {
            return false;
        }

		List<File> files;
		try {
			files = getFiles(support);
		} catch (UnsupportedFlavorException | IOException e) {
			e.printStackTrace();
			return false;
		}
		
		loader.accept(files.get(0));
		return true;
	}

	@SuppressWarnings("unchecked")
	private List<File> getFiles(TransferSupport support) throws UnsupportedFlavorException, IOException {
		List<File> files;
		files = (List<File>)support.getTransferable().
				getTransferData(DataFlavor.javaFileListFlavor);
		return files;
	}
}
