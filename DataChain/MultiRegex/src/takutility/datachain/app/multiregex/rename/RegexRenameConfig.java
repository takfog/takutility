package takutility.datachain.app.multiregex.rename;

import java.io.PrintWriter;

import takutility.datachain.app.multiregex.MultiRegexConfig;

public class RegexRenameConfig extends MultiRegexConfig {
	private static final String ID_NAMETYPE = "N";
	private FileNameType nameType = FileNameType.NAME;
	
	public FileNameType getNameType() {
		return nameType;
	}

	public void setNameType(FileNameType nameType) {
		this.nameType = nameType;
	}
	
	@Override
	protected void saveProperties(PrintWriter w) {
		super.saveProperties(w);
		writeProperty(w, ID_NAMETYPE, nameType.name());
	}

	@Override
	protected void loadProperty(String id, String value) {
		if (ID_NAMETYPE.equals(id))
			nameType = FileNameType.valueOf(value);
		else
			super.loadProperty(id, value);
	}
}
