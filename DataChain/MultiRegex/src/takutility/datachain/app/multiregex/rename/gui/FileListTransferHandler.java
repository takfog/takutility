package takutility.datachain.app.multiregex.rename.gui;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

import javax.swing.TransferHandler;

public class FileListTransferHandler extends TransferHandler {
	private static final long serialVersionUID = 1L;
	private ListTableModel<File> model;
	private Supplier<Boolean> recursive;
	
	public FileListTransferHandler(ListTableModel<File> model, Supplier<Boolean> recursive) {
		this.model = model;
		this.recursive = recursive;
	}

	@Override
	public boolean canImport(TransferSupport support) {
		return support.isDataFlavorSupported(DataFlavor.javaFileListFlavor);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean importData(TransferSupport support) {
		if (!canImport(support)) {
            return false;
        }

		List<File> files;
		try {
			files = (List<File>)support.getTransferable().
					getTransferData(DataFlavor.javaFileListFlavor);
		} catch (UnsupportedFlavorException | IOException e) {
			e.printStackTrace();
			return false;
		}
		
		model.addAll(getFiles(files.stream()));
		return true;
	}
	
	private Stream<File> getFiles(Stream<File> orig) {
		return orig.flatMap(file -> {
			if (file.isDirectory()) {
				if (recursive.get())
					return getFiles(Arrays.stream(file.listFiles()));
				else
					return Arrays.stream(
						file.listFiles(sf -> !sf.isDirectory()));
			} else {
				return Stream.of(file);
			}
		});
	}
}
