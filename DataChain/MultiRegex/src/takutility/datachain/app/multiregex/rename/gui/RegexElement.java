package takutility.datachain.app.multiregex.rename.gui;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import takutility.datachain.app.multiregex.MultiRegexReplaceStep;
import takutility.datachain.app.multiregex.MultiRegexSelectStep;
import takutility.datachain.app.multiregex.MultiRegexStep;
import takutility.datachain.app.multiregex.rename.gui.RegexTableModel.Type;

public class RegexElement {
	private Type type;
	private String regex;
	private Pattern validRegex;
	private String replace;
	private Integer group = null;

	public RegexElement(MultiRegexStep step) {
		setRegex(step.getPattern().pattern());
		if (step instanceof MultiRegexReplaceStep) {
			setType(Type.REPLACE);
			setReplace(((MultiRegexReplaceStep)step).getReplace());
		} else if (step instanceof MultiRegexSelectStep) {
			setType(Type.SELECT);
			int g = ((MultiRegexSelectStep)step).getGroup();
			if (g > 0)
				setReplace(g+"");
		} else
			throw new IllegalArgumentException("Unknown step type "+step.getClass());
	}

	public RegexElement(Type type, String regex, String replace) {
		setType(type);
		setRegex(regex);
		setReplace(replace);
	}
	
	public RegexElement() {
		setType(Type.REPLACE);
	}
	
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
		if (type == Type.SELECT)
			setReplace(replace);
	}
	
	public boolean isTypeValid() {
		return type != null;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
		try {
			validRegex = Pattern.compile(regex);
		} catch (PatternSyntaxException e) {
			validRegex = null;
		}
	}

	public String getReplace() {
		return replace;
	}
	
	public boolean isRegexValid() {
		return validRegex != null;
	}

	public void setReplace(String replace) {
		this.replace = replace;
		group = null;
		if (type == Type.SELECT) {
			if (replace != null && !replace.isEmpty()) {
				try {
					int g = Integer.parseInt(replace);
					if (g >= 0)
						group = g;
				} catch(NumberFormatException e) {}
			} else {
				group = 0;
			}
		}
	}
	
	public boolean isGroupValid() {
		return type != Type.SELECT || group != null;
	}

	public String verify() {
		if (!isTypeValid())
			return "Missing type";
		if (!isRegexValid())
			return "Invalid pattern: "+regex;
		if (!isGroupValid())
			return "Invalid group number: "+replace;
		return null;
	}
	
	public MultiRegexStep toStep() {
		switch (type) {
		case REPLACE:
			return new MultiRegexReplaceStep(validRegex, 
					replace == null ? "" : replace);
		case SELECT:
			if (replace.isEmpty())
				return new MultiRegexSelectStep(validRegex);
			else
				return new MultiRegexSelectStep(validRegex, group);
		default:
			return null;	
		}
	}
}