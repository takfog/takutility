package takutility.datachain.app.multiregex.rename.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;

import takutility.StringWrap;
import takutility.composite.Pair;
import takutility.datachain.app.multiregex.rename.FileNameType;
import takutility.datachain.app.multiregex.rename.RegexRename;
import takutility.datachain.app.multiregex.rename.RegexRenameConfig;
import takutility.datachain.app.multiregex.rename.gui.RegexTableModel.ConversionException;
import takutility.datachain.async.AsyncConverter;
import takutility.datachain.in.FileList.FileListConfig;

public class RegexRenameGui extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private RegexRename chain;
	private boolean preview;
	private JFileChooser fc;
	
	private JPanel contentPane;
	private FileListTable tblFile;
	private RegexTable tblRegex;
	private JTextField txtFilter;
	private JCheckBox chckbxFilter;
	private JComboBox<StringWrap<FileNameType>> cmbNameType;
	private JCheckBox chckbxRecursiveFolders;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					RegexRenameGui frame = new RegexRenameGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	private void init() {
		chain = new RegexRename(new Preview());
		cmbNameType.setModel(new DefaultComboBoxModel<>(new StringWrap[] {
				new StringWrap<>(FileNameType.NAME, "Name"),
				new StringWrap<>(FileNameType.FULL, "Full path"),
				new StringWrap<>(FileNameType.NO_EXTENSION, "No ext."),
		}));
		fc = new JFileChooser();
		fc.addChoosableFileFilter(new FileFilter() {
			@Override
			public String getDescription() {
				return "MultiRegexRename files (*.mrr)";
			}
			@Override
			public boolean accept(File f) {
				if (f.isDirectory())
					return true;
				return f.getName().endsWith(".mrr");
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public RegexRenameGui() {
		setTitle("MultiRegexRename");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 569, 471);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panToolbar = new JPanel();
		contentPane.add(panToolbar, BorderLayout.NORTH);
		panToolbar.setLayout(new BoxLayout(panToolbar, BoxLayout.X_AXIS));
		
		JButton btnTest = new JButton("Test");
		btnTest.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				runPreview();
			}
		});
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});
		
		JButton btnLoad = new JButton("Load");
		btnLoad.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				load();
			}
		});
		panToolbar.add(btnLoad);
		panToolbar.add(btnSave);
		
		chckbxRecursiveFolders = new JCheckBox("Recursive folders");
		panToolbar.add(chckbxRecursiveFolders);
		panToolbar.add(btnTest);
		
		JButton btnRename = new JButton("Rename");
		btnRename.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				rename();
			}
		});
		panToolbar.add(btnRename);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		contentPane.add(splitPane, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		splitPane.setLeftComponent(scrollPane);
		
		tblFile = new FileListTable(chckbxRecursiveFolders::isSelected);
		scrollPane.setViewportView(tblFile);
		
		JPanel panel = new JPanel();
		splitPane.setRightComponent(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panFilter = new JPanel();
		panFilter.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel.add(panFilter, BorderLayout.NORTH);
		GridBagLayout gbl_panFilter = new GridBagLayout();
		gbl_panFilter.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0};
		gbl_panFilter.rowWeights = new double[]{0.0};
		panFilter.setLayout(gbl_panFilter);
		
		cmbNameType = new JComboBox<>();
		GridBagConstraints gbc_cmbNameType = new GridBagConstraints();
		gbc_cmbNameType.insets = new Insets(0, 0, 0, 5);
		gbc_cmbNameType.fill = GridBagConstraints.HORIZONTAL;
		gbc_cmbNameType.gridx = 0;
		gbc_cmbNameType.gridy = 0;
		panFilter.add(cmbNameType, gbc_cmbNameType);
		
		chckbxFilter = new JCheckBox("Filter");
		GridBagConstraints gbc_chckbxFilter = new GridBagConstraints();
		gbc_chckbxFilter.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxFilter.anchor = GridBagConstraints.WEST;
		gbc_chckbxFilter.gridx = 1;
		gbc_chckbxFilter.gridy = 0;
		panFilter.add(chckbxFilter, gbc_chckbxFilter);
		
		txtFilter = new JTextField();
		txtFilter.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				chckbxFilter.setSelected(!txtFilter.getText().isEmpty());
			}
		});
		GridBagConstraints gbc_txtFilter = new GridBagConstraints();
		gbc_txtFilter.insets = new Insets(0, 0, 0, 5);
		gbc_txtFilter.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtFilter.gridx = 2;
		gbc_txtFilter.gridy = 0;
		panFilter.add(txtFilter, gbc_txtFilter);
		txtFilter.setColumns(10);
		
		JButton btnTestFilter = new JButton("Test");
		btnTestFilter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				testFilter();
			}
		});
		GridBagConstraints gbc_btnTestFilter = new GridBagConstraints();
		gbc_btnTestFilter.gridx = 3;
		gbc_btnTestFilter.gridy = 0;
		panFilter.add(btnTestFilter, gbc_btnTestFilter);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel.add(scrollPane_1);
		
		tblRegex = new RegexTable(this::load);
		scrollPane_1.setViewportView(tblRegex);
		splitPane.setResizeWeight(0.5);
		
		init();
	}

	private void runPreview() {
		run(true, Integer.MAX_VALUE);
	}
	
	private void testFilter() {
		run(false, 0);
	}

	private void rename() {
		run(false, Integer.MAX_VALUE);
		JOptionPane.showMessageDialog(this, "Rename done");
	}

	private void run(boolean preview, int maxRegex) {
		RegexRenameConfig config = getConfig(maxRegex);
		List<FileListConfig> files = tblFile.getModel().stream()
			.map(FileListConfig::new)
			.collect(Collectors.toList());
		this.preview = preview;
		chain.run(files, config);
	}

	private RegexRenameConfig getConfig(int maxRegex) {
		RegexRenameConfig config = new RegexRenameConfig();
		if (maxRegex > 0) {
			try {
				config.setRegexes(tblRegex.getModel().getSteps());
			} catch (ConversionException e) {
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				return null;
			}
		}
		if (chckbxFilter.isSelected()) {
			Pattern filter = getFilter();
			if (filter == null) return null;
			config.setFilter(filter);
		}
		config.setNameType(getNameType());
		return config;
	}

	private FileNameType getNameType() {
		return cmbNameType.getItemAt(cmbNameType.getSelectedIndex()).get();
	}

	private Pattern getFilter() {
		try {
			return Pattern.compile(txtFilter.getText());
		} catch(PatternSyntaxException e) {
			JOptionPane.showMessageDialog(this, "Error while parsing filter", "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}
	
	private void save() {
		JFileChooser fc = new JFileChooser();
		fc.addChoosableFileFilter(new FileFilter() {
			@Override
			public String getDescription() {
				return "MultiRegexRename files (*.mrr)";
			}
			@Override
			public boolean accept(File f) {
				return f.getName().endsWith(".mrr");
			}
		});
		int resp = fc.showSaveDialog(this);
		if (resp != JFileChooser.APPROVE_OPTION)
			return;
		
		RegexRenameConfig config = getConfig(Integer.MAX_VALUE);
		try {
			config.save(fc.getSelectedFile());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Error while saving file: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void load() {
		int resp = fc.showOpenDialog(this);
		if (resp != JFileChooser.APPROVE_OPTION)
			return;
		load(fc.getSelectedFile());
	}

	private void load(File file) {
		RegexRenameConfig config = new RegexRenameConfig();
		try {
			config.load(file);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Error while loading file: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		
		cmbNameType.setSelectedItem(config.getNameType());
		if (config.getFilter() != null) {
			chckbxFilter.setSelected(true);
			txtFilter.setText(config.getFilter().pattern());
		} else {
			chckbxFilter.setSelected(false);
			txtFilter.setText("");
		}
		RegexTableModel model = tblRegex.getModel();
		model.clear();
		model.addAll(config.getRegexes().stream().map(RegexElement::new));
	}

	private class Preview extends AsyncConverter<Pair<File,File>, Pair<File,File>> {
		@Override
		public void run() {
			Function<File,String> file2str;
			if (getNameType() == FileNameType.FULL)
				file2str = File::getAbsolutePath;
			else
				file2str = File::getName;
			
			FileListTableModel model = tblFile.getModel();
			model.clearNewNames();
			getInput().<Pair<File,File>>stream()
				.forEach(p ->
					model.setNewName(p.a(), file2str.apply(p.b())));
			if (!preview)
				next();
		}
		
	}
}
