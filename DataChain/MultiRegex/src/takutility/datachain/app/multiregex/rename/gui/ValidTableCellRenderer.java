package takutility.datachain.app.multiregex.rename.gui;

import java.awt.Color;
import java.awt.Component;
import java.util.function.Predicate;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class ValidTableCellRenderer implements TableCellRenderer {
	private Predicate<Integer> validator;
	private DefaultTableCellRenderer valid, invalid;

	public ValidTableCellRenderer(Predicate<Integer> validator) {
		this.validator = validator;
		valid = new DefaultTableCellRenderer();
		invalid = new DefaultTableCellRenderer();
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) 
	{
		if (validator.test(row))
			return valid.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		else {
			Component comp = invalid.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			comp.setBackground(Color.ORANGE);
			return comp;
		}
	}
}
