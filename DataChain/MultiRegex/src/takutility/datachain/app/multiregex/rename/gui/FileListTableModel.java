package takutility.datachain.app.multiregex.rename.gui;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import takutility.datachain.filter.FilePath;

public class FileListTableModel extends ListTableModel<File> {
	private static final long serialVersionUID = -1617770324016387155L;
	
	private static final int COL_NEW_NAME = 3;
	
	private String[] cols = new String[] {
			"Path",
			"Name",
			"Ext.",
			"New name"
	};
	Map<File,String> newNames = new HashMap<>();
	
	@Override
	public String getColumnName(int column) {
		return cols[column];
	}
	
	@Override
	public int getColumnCount() {
		return cols.length;
	}
	
	@Override
	protected boolean innerAdd(int pos, File file) {
		if (newNames.containsKey(file))
			return false;
		
		newNames.put(file,"");
		return super.innerAdd(pos, file);
	}
	
	@Override
	public boolean innerRemove(int index) {
		newNames.remove(get(index));
		return super.innerRemove(index);
	}
	
	@Override
	public void clear() {
		newNames.clear();
		super.clear();
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		File f = get(rowIndex);
		switch (columnIndex) {
		case 0:
			return f.getParent();
		case 1:
			return FilePath.nameWithoutExtensionFromFile(f);
		case 2:
			String ext = FilePath.extensionFromFile(f);
			if (ext.startsWith("."))
				ext = ext.substring(1);
			return ext;
		case 3:
			return newNames.get(f);
		default:
			return "";
		}
	}
	
	public void clearNewNames() {
		newNames.clear();
		for(int i=0; i<size(); i++)
			fireTableCellUpdated(i, COL_NEW_NAME);
	}
	
	public void setNewName(File orig, String newFile) {
		int index = find(orig);
		if (index < 0) return;
		newNames.put(orig, newFile);
		fireTableCellUpdated(index, COL_NEW_NAME);
	}
}
