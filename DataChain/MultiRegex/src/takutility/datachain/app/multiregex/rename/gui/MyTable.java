package takutility.datachain.app.multiregex.rename.gui;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JTable;
import javax.swing.KeyStroke;

public class MyTable<T,M extends ListTableModel<T>> extends JTable {
	private static final long serialVersionUID = -3358124846065115530L;
	
	protected M model;

	public MyTable(M model) {
		setFillsViewportHeight(true);
		this.model = model;
		setModel(model);
		deleteOnKeyPress();
	}

	private void deleteOnKeyPress() {
		InputMap inputMap = getInputMap(WHEN_FOCUSED);
		ActionMap actionMap = getActionMap();

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
		actionMap.put("delete", new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent evt) {
				int[] rows = getSelectedRows();
				Object[] elems = new Object[rows.length];
				for (int i = 0; i < rows.length; i++) {
					if (rows[i] >= 0)
						elems[i] = model.get(convertRowIndexToModel(rows[i]));
				}
				for (Object e : elems) {
					model.remove((T)e);
				}
				getSelectionModel().clearSelection();
			}
		});
	}

	@Override
	public String getToolTipText(MouseEvent e) {
		java.awt.Point p = e.getPoint();
		int rowIndex = rowAtPoint(p);
		if (rowIndex < 0)
			return null;
		int colIndex = columnAtPoint(p);
		if (colIndex < 0)
			return null;
		int realColumnIndex = convertColumnIndexToModel(colIndex);
		int realRowIndex = convertRowIndexToModel(rowIndex);
		Object val = model.getValueAt(realRowIndex, realColumnIndex);
		if (val == null || val.toString().isEmpty())
			return null;
		return val.toString();
	}

	@Override
	public M getModel() {
		return model;
	}
}
