package takutility.datachain.app.multiregex.rename;

public enum FileNameType {
	FULL, NAME, NO_EXTENSION
}
