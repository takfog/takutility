package takutility.datachain.app.multiregex.rename;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import takutility.composite.Pair;
import takutility.datachain.ParamElem;
import takutility.datachain.TypedParamElem;
import takutility.datachain.app.multiregex.MultiRegex;
import takutility.datachain.async.AsyncConverter;
import takutility.datachain.async.DummyAsyncConverter;
import takutility.datachain.chain.ChainBuilder;
import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.SubChain;
import takutility.datachain.chain.link.ChainBuilding;
import takutility.datachain.chain.link.MultiStartLink;
import takutility.datachain.filter.Cast;
import takutility.datachain.filter.Concat;
import takutility.datachain.filter.FilePath;
import takutility.datachain.filter.Persist;
import takutility.datachain.filter.SubChainRunner;
import takutility.datachain.filter.ToPair;
import takutility.datachain.in.FileList;
import takutility.datachain.in.FileList.FileListConfig;
import takutility.datachain.out.Rename;
import takutility.datachain.parse.ScriptParse;

public class RegexRename extends MultiRegex<Collection<FileListConfig>,MultiStartLink<FileListConfig, File>> 
{
	private static final String PER_ORIG = "orig";
	private static final String PER_PARENT = "parent";
	private static final String PER_EXT = "ext";
	private static final String ID_FILE_TO_STRING = "file2string";
	private static final String ID_STRING_TO_FILE = "string2file";

	private AsyncConverter<Pair<File, File>, Pair<File, File>> preview;
	private SubChain<File, String> fullInputChain, nameInputChain, noextInputChain;
	private SubChain<String, File> fullOutputChain, nameOutputChain, noextOutputChain;

	public RegexRename() {
		this(new DummyAsyncConverter<>());
	}

	public RegexRename(AsyncConverter<Pair<File, File>, Pair<File, File>> preview) {
		super(preview);
		createInputChains();
		createOutputChains();
	}
	
	private void createInputChains() {
		ChainBuilder<File> builder = new ChainBuilder<>();
		Persist<File> persistParent = new Persist<>(PER_PARENT, File::getParent);
		
		builder.build().join(FilePath.fullPath());
		fullInputChain = builder.getSubChain();
		
		builder.clear().build()
				.join(persistParent)
				.join(FilePath.fileName());
		nameInputChain = builder.getSubChain();
		
		builder.clear().build()
				.join(persistParent)
				.join(new Persist<>(PER_EXT, FilePath::extensionFromFile))
				.join(FilePath.nameWithoutExtension());
		noextInputChain = builder.getSubChain();
	}

	private void createOutputChains() {
		ChainBuilder<String> builder = new ChainBuilder<>();
		Cast<String,File> toFile = Cast.fromParams(File.class,
				TypedParamElem.pers(PER_PARENT, String.class),
				TypedParamElem.in(String.class));
		
		builder.build().join(Cast.fromInput(File.class, String.class));
		fullOutputChain = builder.getSubChain();
		
		builder.clear().build().join(toFile);
		nameOutputChain = builder.getSubChain();
		
		builder.clear().build()
				.join(new Concat<>(ParamElem.in, ParamElem.pers(PER_EXT)))
				.join(toFile);
		noextOutputChain = builder.getSubChain();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void init(Object initData) {
		this.preview = (AsyncConverter<Pair<File, File>, Pair<File, File>>) initData;
	}
	
	@Override
	protected ChainBuilding<String> fromStartToString(
			MultiStartLink<FileListConfig, File> start) 
	{
		return start
				.join(new Persist<>(PER_ORIG))
				.join(new SubChainRunner<>(ID_FILE_TO_STRING));
	}
	
	@Override
	protected void closeChain(ChainBuilding<String> chain) {
		chain.join(new SubChainRunner<String,File>(ID_STRING_TO_FILE))
			.join(ToPair.fillA(File.class, ParamElem.pers(PER_ORIG)))
			.join(preview)
			.close(new Rename());
	}
	
	@Override
	protected MultiStartLink<FileListConfig, File> createStartLink() {
		return new MultiStartLink<>(FileList::new);
	}

	public void runFromFiles(Collection<File> files, RegexRenameConfig config) {
		List<FileListConfig> input = files.stream()
				.filter(File::isFile)
				.map(FileListConfig::new)
				.collect(Collectors.toList());
		run(buidParams(input, config));
	}

	public void run(Collection<FileListConfig> input, RegexRenameConfig config) {
		run(buidParams(input, config));
	}
	
	protected ChainParams<Collection<FileListConfig>> buidParams(Collection<FileListConfig> input, 
			RegexRenameConfig config) 
	{
		ChainParams<Collection<FileListConfig>> params = new ChainParams<>(input);
		setParams(params, config);
		switch (config.getNameType()) {
		case FULL:
			params.set(ID_FILE_TO_STRING, fullInputChain);
			params.set(ID_STRING_TO_FILE, fullOutputChain);
			break;
		case NO_EXTENSION:
			params.set(ID_FILE_TO_STRING, noextInputChain);
			params.set(ID_STRING_TO_FILE, noextOutputChain);
			break;
		default:
			params.set(ID_FILE_TO_STRING, nameInputChain);
			params.set(ID_STRING_TO_FILE, nameOutputChain);
			break;
		}
		return params;
	}
	
	@ScriptParse
	public static void main(String[] args) throws Exception {
		if (args.length == 0) {
			System.err.println("Usage: [-debug] configFile [-f fileFilter] [-t full|name|noext] file1 [file2 [...]]");
			System.err.println("  path/to/folder    to include all the files in the folder");
			System.err.println("  path/to/folder/*  to recursively include all the files in the folder");
			System.err.println("  -t                part of file to consider");
			System.err.println("     full           full path");
			System.err.println("     name (default) filename only");
			System.err.println("     noext          filename without extension");
					
			return;
		} 
		int argIdx = 0;
		boolean debug = false;
		if ("-debug".equals(args[argIdx])) {
			debug = true;
			argIdx++;
		}
		
		RegexRenameConfig config = new RegexRenameConfig();
		config.load(new File(args[argIdx]));
		argIdx++;
		boolean optArgs = true;
		while(optArgs) {
			switch (args[argIdx]) {
				case "-f":
					config.setFilter(Pattern.compile(args[argIdx+1]));
					argIdx += 2;
					break;
				case "-t":
					switch (args[argIdx+1]) {
					case "full":
						config.setNameType(FileNameType.FULL);
						break;
					case "noext":
						config.setNameType(FileNameType.NO_EXTENSION);
						break;
					}
					argIdx += 2;
					break;
				default:
					optArgs = false;
					break;
			}
		}
		Collection<FileListConfig> fileConfigs = new ArrayList<>(args.length);
		for (int i=argIdx; i<args.length; i++) {
			String path = args[i];
			boolean recursive = false;
			if (path.endsWith("*")) {
				//recursive folder
				recursive = true;
				path = path.substring(0,path.length()-1);
			}
			fileConfigs.add(new FileListConfig(new File(path), recursive, false));
		}
		RegexRename ren;
		if (debug) {
			ren = new RegexRename(new AsyncConverter<Pair<File,File>, Pair<File,File>>() {
				@Override
				public void run() {
					getInput().stream().forEach(System.out::println);
				}
			});
			System.out.println("### Chain");
			ren.debug();
			System.out.println("### Files");
			System.out.println(fileConfigs);
			System.out.println("### Params");
			System.out.println(config);
			System.out.println("### Output");
		} else {
			ren = new RegexRename();
		}
		ren.run(fileConfigs, config);
	}

}
