package takutility.datachain.app.multiregex;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;

import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.link.ChainBuilding;
import takutility.datachain.chain.link.DefaultStartLink;
import takutility.datachain.in.FileLines;
import takutility.datachain.out.ToFile;
import takutility.datachain.parse.ScriptParse;

public class FileContentReplace extends MultiRegex<File, DefaultStartLink<File, String>> {

	@Override
	protected DefaultStartLink<File, String> createStartLink() {
		return new DefaultStartLink<>(new FileLines());
	}

	@Override
	protected ChainBuilding<String> fromStartToString(DefaultStartLink<File, String> start) {
		return start;
	}

	@Override
	protected void closeChain(ChainBuilding<String> chain) {
		chain.close(new ToFile());
	}

	public static void run(String input, String output, List<MultiRegexStep> steps) {
		new FileContentReplace().run(new File(input), new File(output), new MultiRegexConfig(steps));
	}

	public void run(File input, File output, MultiRegexConfig config) {
		ChainParams<File> params = new ChainParams<>(input);
		params.set(ToFile.class, output);
		setParams(params, config);
		run(params);
	}

	@ScriptParse
	public static void main(String[] args) throws Exception {
		if (args.length < 3) {
			System.err.println("Usage: regexFile [-f fileFilter] input output");
			return;
		} 
		int argIdx = 0;
		
		MultiRegexConfig config = new MultiRegexConfig();
		config.load(new File(args[argIdx]));
		argIdx++;
		if ("-f".equals(args[argIdx])) {
			config.setFilter(Pattern.compile(args[argIdx+1]));
			argIdx += 2;
		}
		File in = new File(args[argIdx]);
		argIdx++;
		File out = new File(args[argIdx]);
		
		FileContentReplace rep = new FileContentReplace();
		rep.run(in, out, config);
	}
}
