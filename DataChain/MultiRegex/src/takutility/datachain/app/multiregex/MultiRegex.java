package takutility.datachain.app.multiregex;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import takutility.composite.Pair;
import takutility.datachain.chain.ChainBuilder;
import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.SubChain;
import takutility.datachain.chain.link.ChainBuilding;
import takutility.datachain.chain.link.ChainLink;
import takutility.datachain.chain.link.DefaultStartLink;
import takutility.datachain.chain.link.InnerLink;
import takutility.datachain.chain.link.OptionalLink;
import takutility.datachain.chain.link.PrintableLink;
import takutility.datachain.chain.link.StartLink;
import takutility.datachain.filter.RegexFilter;
import takutility.datachain.filter.SubChainRunner;
import takutility.datachain.in.CollectionInput;
import takutility.datachain.out.DataStorage;
import takutility.datachain.parse.ScriptParse;

public abstract class MultiRegex<P,S extends StartLink<P> & PrintableLink> {

	protected static final String PARAM_REGEXES = "regexes";
	protected static final String PARAM_FILTER = "filter";
	
	private S startLink;

	public MultiRegex() {
		this(null, false);
	}

	public MultiRegex(Object initData) {
		this(initData, true);
	}

	public MultiRegex(Object initData, boolean callInit) {
		if (callInit)
			init(initData);
		startLink = createStartLink();
		ChainBuilding<String> link = fromStartToString(startLink);
		link = regexInnerChain(link);
		closeChain(link);
	}
	
	protected void init(Object initData) {}
	
	protected abstract S createStartLink();
	protected abstract ChainBuilding<String> fromStartToString(S start);
	protected abstract void closeChain(ChainBuilding<String> chain);
	

	protected ChainBuilding<String> regexInnerChain(ChainBuilding<String> link) {
		return link
				.join(new OptionalLink<>(new RegexFilter(PARAM_FILTER)))
				.join(new SubChainRunner<String,String>(PARAM_REGEXES));
	}

	protected static Pair<ChainLink<String>, ChainBuilding<String>> 
			chainFromRegex(List<MultiRegexStep> regexes) 
	{
		InnerLink<String,String> start = null;
		ChainBuilding<String> link = null;
		for (MultiRegexStep reg : regexes) {
			if (start == null) {
				start = new InnerLink<>(reg.toConverter());
				link = start;
			} else {
				link = link.join(reg.toConverter());
			}
		}
		return new Pair<>(start, link);
	}

	protected static void setParams(InnerChainParams params, MultiRegexConfig config) 
	{
		if (config.getFilter() != null)
			params.set(PARAM_FILTER, config.getFilter());
		
		ChainBuilder<String> chain = null;
		for (MultiRegexStep step : config.getRegexes()) {
			if (chain == null) 
				chain = new ChainBuilder<>(); 
			chain.build(String.class).join(step.toConverter());
		}
		SubChain<String, String> subChain;
		if (chain == null) 
			subChain = SubChain.empty();
		else
			subChain = chain.getSubChain();
		params.set(PARAM_REGEXES, subChain);
	}
	
	public void debug() {
		startLink.printChain();
	}

	protected void run(ChainParams<P> params) {
		startLink.start(params);
	}

	public static Collection<String> run(Collection<String> input, MultiRegexConfig config)
	{
		DefaultMultiRegex mr = new DefaultMultiRegex();
		ChainParams<Collection<String>> params = new ChainParams<>(input);
		setParams(params, config);
		mr.run(params);
		return mr.get();
	}
	
	@ScriptParse
	public static Object parse(String args, List<String> params) {
		MultiRegexConfig config = new MultiRegexConfig();
		if (params.size() == 0) {
			if (args.isEmpty())
				throw new IllegalArgumentException("Missing params");
			try {
				config.load(new File(args));
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}
		} else
			config.load(params);
		return new MultiRegexLink(config);
	}
	
	private static class DefaultMultiRegex extends MultiRegex<Collection<String>, DefaultStartLink<Collection<String>, String>> {
		private DataStorage<String> output = new DataStorage<>();
		
		@Override
		protected DefaultStartLink<Collection<String>, String> createStartLink() {
			return new DefaultStartLink<>(new CollectionInput<>());
		}
		
		@Override
		protected ChainBuilding<String> fromStartToString(DefaultStartLink<Collection<String>, String> start) {
			return start;
		}

		@Override
		protected void closeChain(ChainBuilding<String> chain) {
			chain.close(output);
		}
		
		public List<String> get() {
			return output.getData().stream()
					.map(Object::toString)
					.collect(Collectors.toList());
		}
	}
	
}