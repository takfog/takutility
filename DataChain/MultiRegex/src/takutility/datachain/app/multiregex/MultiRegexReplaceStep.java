package takutility.datachain.app.multiregex;

import java.util.regex.Pattern;

import takutility.datachain.filter.FullConverter;
import takutility.datachain.filter.RegexReplace;

public class MultiRegexReplaceStep extends MultiRegexStep {
	public static final String STEP_ID = "R";
	private String replace;

	public MultiRegexReplaceStep(String regex, String replace) {
		this(Pattern.compile(regex), replace);
	}

	public MultiRegexReplaceStep(Pattern pattern, String replace) {
		super(pattern);
		this.replace = replace;
	}

	public String getReplace() {
		return replace;
	}

	@Override
	public FullConverter<String, String> toConverter() {
		return new RegexReplace(pattern, replace);
	}

	@Override
	public String stepTypeId() {
		return STEP_ID;
	}

	@Override
	public String serialize() {
		return pattern.pattern()+"\t"+replace;
	}
	
	public static MultiRegexReplaceStep deserialize(String value) {
		String[] split = value.split("\t");
		Pattern pattern = Pattern.compile(split[0]);
		if (split.length > 1)
			return new MultiRegexReplaceStep(pattern, split[1]);
		else
			return new MultiRegexReplaceStep(pattern, "");
	}

}
