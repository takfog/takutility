package takutility.datachain.app.multiregex;

import java.util.regex.Pattern;

import takutility.datachain.filter.FullConverter;

public abstract class MultiRegexStep {
	protected Pattern pattern;

	public MultiRegexStep(Pattern pattern) {
		this.pattern = pattern;
	}

	public Pattern getPattern() {
		return pattern;
	}

	public abstract FullConverter<String, String> toConverter();
	public abstract String stepTypeId();
	public abstract String serialize();
}