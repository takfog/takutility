package takutility.datachain.app.multiregex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class MultiRegexConfig {
	private static final String ID_FILTER = "F";
	private List<MultiRegexStep> regexes;
	private Pattern filter;

	public MultiRegexConfig() {
		this(null,null);
	}

	public MultiRegexConfig(List<MultiRegexStep> regexes) {
		this(null, regexes);
	}

	public MultiRegexConfig(Pattern filter, List<MultiRegexStep> regexes) {
		this.regexes = regexes;
		this.filter = filter;
	}

	public List<MultiRegexStep> getRegexes() {
		return regexes == null ? Collections.emptyList() : regexes;
	}
	
	public void setRegexes(List<MultiRegexStep> regexes) {
		this.regexes = regexes;
	}
	
	public Pattern getFilter() {
		return filter;
	}
	
	public void setFilter(Pattern filter) {
		this.filter = filter;
	}
	
	public void save(File file) throws FileNotFoundException {
		try(PrintWriter w = new PrintWriter(file)) {
			saveProperties(w);
		}
	}
	
	protected void writeProperty(PrintWriter w, String id, String value) {
		w.println(id+"\t"+value);
	}
	
	protected void saveProperties(PrintWriter w) {
		if (filter != null)
			writeProperty(w, ID_FILTER, filter.pattern());
		if (regexes != null) {
			for (MultiRegexStep reg : regexes) {
				writeProperty(w, reg.stepTypeId(), reg.serialize());
			}
		}
	}
	
	public void load(File file) throws FileNotFoundException, IOException {
		regexes = new ArrayList<>();
		try(BufferedReader r = new BufferedReader(new FileReader(file))) {
			load(r.lines());
		}
	}
	
	public void load(Collection<String> lines) {
		load(lines.stream());
	}
	
	private void load(Stream<String> lines) {
		regexes = new ArrayList<>();
		lines.forEach(l -> {
			String[] split = l.split("\t", 2);
			if (split.length > 1)
				loadProperty(split[0], split[1]);
		});
	}
	
	protected void loadProperty(String id, String value) {
		switch (id) {
		case ID_FILTER:
			filter = Pattern.compile(value);
			break;
		case MultiRegexReplaceStep.STEP_ID:
			regexes.add(MultiRegexReplaceStep.deserialize(value));
			break;
		case MultiRegexSelectStep.STEP_ID:
			regexes.add(MultiRegexSelectStep.deserialize(value));
			break;
		}
	}
	
	@Override
	public String toString() {
		StringWriter sw = new StringWriter();
		saveProperties(new PrintWriter(sw));
		return sw.toString();
	}
}
