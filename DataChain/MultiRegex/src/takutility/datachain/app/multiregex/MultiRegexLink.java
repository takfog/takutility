package takutility.datachain.app.multiregex;

import java.util.Collection;

import takutility.datachain.Parametrized;
import takutility.datachain.chain.ChainParams;
import takutility.datachain.chain.ChainVerificationException;
import takutility.datachain.chain.InnerChainParams;
import takutility.datachain.chain.VerificationData;
import takutility.datachain.chain.link.ChainBuilding;
import takutility.datachain.chain.link.DefaultStartLink;
import takutility.datachain.data.Data;
import takutility.datachain.data.DataContainer;
import takutility.datachain.filter.FullConverter;
import takutility.datachain.in.DataInput;
import takutility.datachain.out.DataStorage;

public class MultiRegexLink extends MultiRegex<Collection<Data>, 
	DefaultStartLink<Collection<Data>, String>> 
	implements FullConverter<String, String>, Parametrized<MultiRegexConfig>
{
	private DataStorage<String> end = new DataStorage<>();
	
	private String id;
	private MultiRegexConfig defConfig;
	
	public MultiRegexLink(MultiRegexConfig config) {
		this(null, config);
	}
	
	public MultiRegexLink(String id) {
		this(id, null);
	}
	
	private MultiRegexLink(String id, MultiRegexConfig config) {
		if (id == null)
			this.id = Parametrized.defaultId(MultiRegexLink.class);
		else
			this.id = id;
		this.defConfig = config;
	}
	
	@Override
	protected DefaultStartLink<Collection<Data>, String> createStartLink() {
		return new DefaultStartLink<>(new DataInput<>());
	}

	@Override
	protected ChainBuilding<String> fromStartToString(DefaultStartLink<Collection<Data>, String> start) {
		return start;
	}

	@Override
	protected void closeChain(ChainBuilding<String> chain) {
		chain.close(end);
	}

	@Override
	public DataContainer fullConvert(InnerChainParams params, DataContainer in) {
		MultiRegexConfig config = params.get(this);
		if (config == null) config = defConfig;
		ChainParams<Collection<Data>> innerParams = new ChainParams<>(in.get());
		setParams(innerParams, config);
		run(innerParams);
		return end.getData();
	}

	@Override
	public String getLinkId() {
		return id;
	}

	@Override
	public boolean isParamRequired() {
		return defConfig == null;
	}

	@Override
	public void verify(VerificationData data) throws ChainVerificationException {
		data.verifyType(String.class, this);
	}
}
