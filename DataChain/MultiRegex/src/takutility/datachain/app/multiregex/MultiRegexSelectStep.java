package takutility.datachain.app.multiregex;

import java.util.regex.Pattern;

import takutility.datachain.filter.FullConverter;
import takutility.datachain.filter.RegexSelect;

public class MultiRegexSelectStep extends MultiRegexStep {
	public static final String STEP_ID = "S";
	private int group;

	public MultiRegexSelectStep(Pattern pattern) {
		this(pattern, 0);
	}

	public MultiRegexSelectStep(Pattern pattern, int group) {
		super(pattern);
		this.group = group;
	}

	@Override
	public FullConverter<String, String> toConverter() {
		return new RegexSelect(pattern, group);
	}
	
	public int getGroup() {
		return group;
	}

	@Override
	public String stepTypeId() {
		return STEP_ID;
	}

	@Override
	public String serialize() {
		if (group <= 0)
			return pattern.pattern();
		else
			return pattern.pattern()+"\t"+group;
	}
	
	public static MultiRegexSelectStep deserialize(String value) {
		String[] split = value.split("\t");
		Pattern pattern = Pattern.compile(split[0]);
		if (split.length > 1)
			return new MultiRegexSelectStep(pattern, Integer.parseInt(split[1]));
		else
			return new MultiRegexSelectStep(pattern);
	}

}
