package takutility.test;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import takutility.*;
import takutility.gui.CentraFinestra;
import takutility.gui.checkedlist.CheckedList;

@SuppressWarnings("serial")
public class TestCheckedList extends JFrame implements ListSelectionListener {

	JFrame child;
	JList l;
	
	public TestCheckedList() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		l = new CheckedList(
				new String[]{"uno", "due", "tre", "quattro"});
		l.addListSelectionListener(this);
		add(new JScrollPane(l));
		pack();
		CentraFinestra.schermo(this);
		setVisible(true);
		child = new JFrame();
		CentraFinestra.component(child, this);
		child.setVisible(true);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new TestCheckedList();
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (l.isSelectedIndex(e.getLastIndex()))
			CentraFinestra.schermo(this);
		else
			CentraFinestra.component(child, this);
	}

}
