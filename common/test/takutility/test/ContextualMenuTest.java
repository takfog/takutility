package takutility.test;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EmptyBorder;

import takutility.gui.textpopup.PopupAction;
import takutility.gui.textpopup.TextPopup;
import takutility.gui.textpopup.TextPopupFactory;

import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.BoxLayout;
import java.awt.Component;
import javax.swing.Box;

public class ContextualMenuTest extends JFrame {

	private JPanel contentPane;
	private JTextField txtFull;
	private JTextField txtNone;
	private JTextField txtReadonlyMenu;
	private JTextField txtReadOnlyField;
	private JTextField txtDisabled;
	private JTextField txtSelectOnly;
	private JTextField txtSelectUndo;
	private JTextField txtIta;
	private JTextField txtCcp;
	private JTextField txtUndoOnly;
	private JTextField txtUndoAndRedo;
	private JTextField txtSingleUndo;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField txtSelectUndoRedo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ContextualMenuTest frame = new ContextualMenuTest();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ContextualMenuTest() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		txtFull = new JTextField();
		txtFull.setText("full");
		contentPane.add(txtFull);
		txtFull.setColumns(10);
		
		txtNone = new JTextField();
		txtNone.setText("none");
		contentPane.add(txtNone);
		txtNone.setColumns(10);
		
		txtReadonlyMenu = new JTextField();
		txtReadonlyMenu.setText("readonly menu");
		contentPane.add(txtReadonlyMenu);
		txtReadonlyMenu.setColumns(10);
		
		txtReadOnlyField = new JTextField();
		txtReadOnlyField.setEditable(false);
		txtReadOnlyField.setText("read only field");
		contentPane.add(txtReadOnlyField);
		txtReadOnlyField.setColumns(10);
		
		txtDisabled = new JTextField();
		txtDisabled.setEnabled(false);
		txtDisabled.setText("disabled");
		txtDisabled.setColumns(10);
		contentPane.add(txtDisabled);
		
		txtSelectOnly = new JTextField();
		txtSelectOnly.setText("select only");
		txtSelectOnly.setColumns(10);
		contentPane.add(txtSelectOnly);
		
		txtSelectUndo = new JTextField();
		txtSelectUndo.setText("select undo");
		txtSelectUndo.setColumns(10);
		contentPane.add(txtSelectUndo);
		
		txtSelectUndoRedo = new JTextField();
		txtSelectUndoRedo.setText("select undo redo");
		txtSelectUndoRedo.setColumns(10);
		contentPane.add(txtSelectUndoRedo);
		
		txtIta = new JTextField();
		txtIta.setText("ita");
		txtIta.setColumns(10);
		contentPane.add(txtIta);
		
		txtCcp = new JTextField();
		txtCcp.setText("ccp");
		txtCcp.setColumns(10);
		contentPane.add(txtCcp);
		
		txtUndoOnly = new JTextField();
		txtUndoOnly.setText("undo only");
		txtUndoOnly.setColumns(10);
		contentPane.add(txtUndoOnly);
		
		txtUndoAndRedo = new JTextField();
		txtUndoAndRedo.setText("undo and redo");
		txtUndoAndRedo.setColumns(10);
		contentPane.add(txtUndoAndRedo);
		
		txtSingleUndo = new JTextField();
		txtSingleUndo.setText("single undo");
		txtSingleUndo.setColumns(10);
		contentPane.add(txtSingleUndo);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		contentPane.add(textField_2);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		contentPane.add(textField_1);
		
		
		TextPopupFactory.all().attachTo(txtFull);
		TextPopupFactory.empty().attachTo(txtNone);
		TextPopupFactory.empty().ccp().attachTo(txtCcp);
		new TextPopup(txtDisabled);
		TextPopupFactory.all()
				.setText(PopupAction.UNDO_REDO, "Annulla")
				.setText(PopupAction.COPY, "Copia")
				.setText(PopupAction.CUT, "Taglia")
				.setText(PopupAction.PASTE, "Incolla")
				.setText(PopupAction.SELECT_ALL, "Seleziona tutto")
				.attachTo(txtIta);
		new TextPopup(txtReadOnlyField);
		TextPopupFactory.empty().add(PopupAction.SELECT_ALL).attachTo(txtSelectOnly);
		TextPopupFactory.empty().add(PopupAction.SELECT_ALL).add(PopupAction.UNDO_REDO).attachTo(txtSelectUndo);
		TextPopupFactory.empty().selectAll().readOnly().attachTo(txtReadonlyMenu);
		TextPopupFactory.all().undoOnly().attachTo(txtUndoOnly);
		TextPopupFactory.empty().undoAndRedo().attachTo(txtUndoAndRedo);
		TextPopupFactory.all().singleUndo().attachTo(txtSingleUndo);
		TextPopupFactory.empty().undoAndRedo().add(PopupAction.SELECT_ALL).attachTo(txtSelectUndoRedo);
		
//		new TextPopupFactory().attachTo(txt);
	}
}
