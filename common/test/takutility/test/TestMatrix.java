package takutility.test;

import takutility.collections.matrix.*;
import takutility.collections.matrix.Matrix.Entry;

public class TestMatrix {

	public static void main(String[] args) {
		test(new MatrixList<String>(3));
		test(new SymMatrixList<String>(3));
		test(new SparseMatrix<String>());
		test(new SymSparseMatrix<String>());
	}
	
	public static void test(Matrix<String> matrix) {
		System.out.println("size 0 "+matrix.size());
		
		set(matrix,0,0);
		System.out.println("size 1 "+matrix.size());
		print(matrix);
		
		set(matrix,0,1);
		System.out.println("size 2 "+matrix.size());
		print(matrix);
		set(matrix,1,0);
		set(matrix,1,1);
		System.out.println("size 2 "+matrix.size());
		print(matrix);
		
		set(matrix,5,0);
		System.out.println("size 6 "+matrix.size());
		print(matrix);
		
		System.out.println("val 0;5 "+matrix.get(0, 5));
		System.out.println("########################");
	}
	
	public static void set(Matrix<String> mat, int a, int b) {
		String val = a+";"+b;
		mat.set(a, b, val);
	}
	
	public static void print(Matrix<String> mat) {
		for (Entry<String> e : mat.entrySet()) {
			System.out.println(e.getA()+"\t"+e.getB()+"\t= "+e.getValue());
		}
	}
}
