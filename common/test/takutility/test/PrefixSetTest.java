package takutility.test;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import takutility.collections.ExtremeSet;
import takutility.collections.PrefixSet;
import takutility.collections.SuffixSet;

public class PrefixSetTest {

	public static void main(String[] args) {
		Random rnd = new Random(201607051005L);
		int iterations = 50000;
		int chars = 8;
		int minLen = 3, maxLen = 10;
		
		
		for (int i = 0; i < iterations; i++) {
			if (i%100 == 0)
				System.out.println("it\t"+i+"/"+iterations);
			Set<String> fix = generateWords(rnd, (int)(rnd.nextDouble()+1*100), chars, minLen, maxLen);
			Set<String> words = generateWords(rnd, (int)(rnd.nextDouble()+1*500), chars, minLen, maxLen);
			
			PrefixSet pre = new PrefixSet(fix);
			SuffixSet suf = new SuffixSet(fix);
			
			for (String w : fix) {
				checkSet(i, w, pre, fix);
				checkSet(i, w, suf, fix);
			}
			
			for (String w : words) {
				try {
					checkWord(w, i, pre, fix); 
				} catch(IllegalStateException e) {
					pre.find(w);
				}
				try {
					checkWord(w, i, suf, fix); 
				} catch(IllegalStateException e) {
					suf.find(w);
				}
			}
		}
	}
	
	private static void checkSet(int iter, String word, ExtremeSet ext, Set<String> fix) {
		String found = ext.find(word);
		if (found == null)
			System.err.printf("It %d\tpart %s not found in %s%n",iter, word, ext.getClass().getSimpleName());
		else if (!ext.verify(word, found))
			System.err.printf("It %d\tpart %s found as %s in %s%n",iter, word, found, ext.getClass().getSimpleName());
		else if (!word.equals(found) && !fix.contains(found))
			System.err.printf("It %d\tpart %s found as unexisting %s in %s%n",iter, word, found, ext.getClass().getSimpleName());
		else
			return;
		System.err.printf("- %s %s%n",ext.getClass().getSimpleName(),ext);
		throw new RuntimeException();
	}

	private static Set<String> generateWords(Random rnd, int size, int chars, int minLen, int maxLen) {
		HashSet<String> set = new HashSet<String>();
		while (set.size() < size) {
			int len = minLen + rnd.nextInt(maxLen-minLen);
			StringBuilder sb = new StringBuilder(len);
			for (int j = 0; j < len; j++) {
				sb.append(rnd.nextInt(chars));
			}
			set.add(sb.toString());
		}
		return set;
	}
	
	private static void checkWord(String word, int iter, ExtremeSet fix, Set<String> orig) {
		String found = fix.find(word);
		if (found == null) {
			for (String s : orig) {
				if (fix.verify(word, s)) {
					System.err.printf("It %d\tword %s, missed %s%n",iter, word, s);
					System.err.printf("- set %s%n- %s %s%n",orig,fix.getClass().getSimpleName(),fix);
					throw new IllegalStateException();
				}
			}
		} else if (!fix.verify(word, found)) {
			System.err.printf("It %d\tword %s, not matching %s for %s%n",
					iter, word, found, fix.getClass().getName());
			throw new IllegalStateException();
		} else if (!orig.contains(found)) {
			System.err.printf("It %d\tword %s, not in set %s%n",iter, word, found);
			System.err.printf("- set %s%n- %s %s%n",orig,fix.getClass().getSimpleName(),fix);
			throw new IllegalStateException();
		}
		
	}
}

//interface ManualCheck {
//	boolean check(String word, String fix);
//}
