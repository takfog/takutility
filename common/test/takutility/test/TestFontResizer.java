package takutility.test;

import java.awt.GridLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JFrame;
import javax.swing.JLabel;

import takutility.gui.adaptivelabel.AdaptiveLabel;

public class TestFontResizer extends JFrame implements ComponentListener {
	private JLabel lbl;
	//private FontResizer fr;
	
	public TestFontResizer() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setLayout(new GridLayout(1,1));
		lbl = new AdaptiveLabel(); //new JLabel("Prova");
		lbl.setText("Prova lunga");		
		getContentPane().add(lbl);
		setVisible(true);
		pack();
		//lbl.addComponentListener(this);
		//fr = new FontResizer(this, lbl.getFont());
		//fr.resize(new Dimension(30,20), "Prova");
	}
	
	public static void main(String[] args) {
		new TestFontResizer();
		//System.exit(0);
	}

	@Override
	public void componentResized(ComponentEvent e) {
		//lbl.setFont(fr.resize(lbl.getSize(), lbl.getText()));
	}

	@Override
	public void componentMoved(ComponentEvent e) {}

	@Override
	public void componentShown(ComponentEvent e) {}

	@Override
	public void componentHidden(ComponentEvent e) {}
}
