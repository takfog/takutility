package takutility.collections;

import java.util.AbstractList;

// ########### begin import

import java.util.function.IntFunction;

//########### end import

public class ArrayOf<E> extends AbstractList<E> {
	private Object[] array;

	public ArrayOf(int length) {
		array = new Object[length];
	}

	// ################# begin varargs annot
	@SafeVarargs
	// ################# end varargs annot
	public ArrayOf(E... elements) {
		array = elements;
	}
	
	// ############ begin ctors
	public ArrayOf(int length, IntFunction<E> init) {
		this(length);
		for (int i = 0; i < length; i++) {
			array[i] = init.apply(i);
		}
	}
	// ################# end ctors

	@Override
	@SuppressWarnings("unchecked")
	public E get(int index) {
		return (E) array[index];
	}

	@Override
	@SuppressWarnings("unchecked")
	public E set(int index, E element) {
		E old = (E) array[index];
		array[index] = element;
		return old;
	}

	@Override
	public int size() {
		return array.length;
	}
}
