package takutility.collections;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class ArrayOfCollection<E,C extends Collection<E>> extends AbstractList<C> {
	private ArrayList<C> array;
	private Supplier<C> ctor;

	public ArrayOfCollection(int length, Supplier<C> ctor) {
		this.ctor = ctor;
		
		array = new ArrayList<>(length);
		for(int i=0; i<length; i++) {
			array.add(null);
		}
	}

	public ArrayOfCollection(ArrayOfCollection<E, C> base, UnaryOperator<C> elemCopy) {
		this.ctor = base.ctor;
		
		int length = base.size();
		array = new ArrayList<>(length);
		for (C e : base.array) {
			if (e == null)
				array.add(null);
			else
				array.add(elemCopy.apply(e));
		}
	}

	@Override
	public C get(int index) {
		C list = array.get(index);
		if (list == null) {
			list = ctor.get();
			array.set(index, list);
		}
		return list;
	}
	
	public boolean hasElements(int index) {
		C list = array.get(index);
		return list != null && list.size() > 0;
	}
	
	public boolean addElement(int index, E e) {
		return get(index).add(e);
	}
	
	public void addAllElements(ArrayOfCollection<E, C> other) {
		int i=0;
		for (C c : other.array) {
			if (c != null)
				get(i).addAll(c);
			i++;
		}
	}

	@Override
	public int size() {
		return array.size();
	}
	
	public int size(int index) {
		C list = array.get(index);
		if (list == null)
			return 0;
		else
			return list.size();
	}
	
	public void delete(int index) {
		array.set(index, null);
	}
	
	@Override
	public String toString() {
		return array.toString();
	}
}
