package takutility.collections;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

//################# begin import function

import java.util.function.Supplier;

//################# end import function

import takutility.ptr.Lazy;

public class LazyMap<K,V> extends Lazy<Map<K,V>> implements Map<K,V> {

	public LazyMap() {
		super(HashMap.class);
	}

	public LazyMap(boolean async) {
		super(async, HashMap.class);
	}

	public LazyMap(@SuppressWarnings("rawtypes") Class<? extends Map> klass) {
		super(klass);
	}

	public LazyMap(boolean async, @SuppressWarnings("rawtypes") Class<? extends Map> klass) {
		super(async, klass);
	}

	public LazyMap(boolean async, Supplier<Map<K, V>> factory) {
		super(async, factory);
	}

	public LazyMap(Supplier<Map<K, V>> factory) {
		super(factory);
	}

	@Override
	public void clear() {
		if (isCreated()) get().clear();
	}

	@Override
	public int size() {
		return isCreated() ? get().size() : 0;
	}

	@Override
	public boolean isEmpty() {
		return isCreated() ? get().isEmpty() : true;
	}

	@Override
	public boolean containsKey(Object key) {
		if (!isCreated()) return false;
		return get().containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		if (!isCreated()) return false;
		return get().containsValue(value);
	}

	@Override
	public V get(Object key) {
		if (!isCreated()) return null;
		return get().get(key);
	}

	@Override
	public V put(K key, V value) {
		return get().put(key, value);
	}

	@Override
	public V remove(Object key) {
		if (!isCreated()) return null;
		return get().remove(key);
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		get().putAll(m);
	}

	@Override
	public Set<K> keySet() {
		if (!isCreated()) return Collections.emptySet();
		return get().keySet();
	}

	@Override
	public Collection<V> values() {
		if (!isCreated()) return Collections.emptyList();
		return get().values();
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		if (!isCreated()) return Collections.emptySet();
		return get().entrySet();
	}

}
