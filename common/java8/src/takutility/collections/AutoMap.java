package takutility.collections;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

//################## begin import

import java.util.function.Function;

//################## end import

public class AutoMap<K, V> extends AbstractMap<K, V> {
	private Map<K, V> map;
	// ################# begin fields
	private Function<K,V> factory;
	// ################# end fields
	
	private static <V> Factory<V> factoryFromClass(final Class<? extends V> klass) {
		try {
			klass.getConstructor();
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
		return new Factory<V>(){
			@Override
			public V create() {
				try {
					return klass.newInstance();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		};
	}

	public AutoMap(Factory<V> factory) {
		this(factory, new HashMap<K, V>());
	}

	public AutoMap(Factory<V> factory, Map<K, V> baseMap) {
		// ################ begin factory-map ctor
		this(k -> factory.create(), baseMap);
		// ##### end
	}

	public AutoMap(Class<? extends V> klass) {
		this(factoryFromClass(klass));
	}

	// ################ begin ctors
	public AutoMap(Function<K,V> factory) {
		this(factory, new HashMap<K, V>());
	}
	
	public AutoMap(Function<K,V> factory, Map<K, V> baseMap) {
		this.map = baseMap;
		this.factory = factory;
	}
	// ################ end ctors
	
	// ############ begin get
	@Override
	@SuppressWarnings("unchecked")
	public V get(Object key) {
		V v;
		if (map.containsKey(key))
			v = map.get(key);
		else {
			K typedKey = (K)key;
			v = factory.apply(typedKey);
			map.put(typedKey, v);
		}
		return v;
	}
	// ############ end

	public V getIfPresent(Object key) {
		return getOrDefault(key, null);
	}

	// ############# begin getOrDefault override
	@Override
	// ############# end
	public V getOrDefault(Object key, V def) {
		if (map.containsKey(key))
			return map.get(key);
		else
			return def;
	}
	
	@Override
	public Set<Entry<K, V>> entrySet() {
		return map.entrySet();
	}

	@Override
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}
}
