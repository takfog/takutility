package takutility.collections;

import java.util.Iterator;
import java.util.function.Supplier;

public class IteratorFactory<T> implements Iterable<T> {
	private Supplier<Iterator<T>> factory;
	
	public IteratorFactory(Supplier<Iterator<T>> factory) {
		this.factory = factory;
	}

	@Override
	public Iterator<T> iterator() {
		return factory.get();
	}

}
