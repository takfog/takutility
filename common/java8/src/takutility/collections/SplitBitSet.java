package takutility.collections;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.IntStream;

public class SplitBitSet implements Cloneable {
	/** Number of bits allocated to a value in an index */
	private static final int VALUE_BITS = 20; // 1M values per bit set
	/** Mask for extracting values */
	private static final int VALUE_MASK = (1 << VALUE_BITS) - 1;

	/**
	 * Map from a value stored in high bits of a long index to a bit set mapped
	 * to the lower bits of an index. Bit sets size should be balanced - not to
	 * long (otherwise setting a single bit may waste megabytes of memory) but
	 * not too short (otherwise this map will get too big). Update value of
	 * {@code VALUE_BITS} for your needs. In most cases it is ok to keep 1M -
	 * 64M values in a bit set, so each bit set will occupy 128Kb - 8Mb.
	 */
	private Map<Integer, BitSet> m_sets = new HashMap<>(VALUE_BITS);
	private int size = 0;

	public SplitBitSet() {}
	
	private SplitBitSet(SplitBitSet other) {
		for (Entry<Integer, BitSet> e : other.m_sets.entrySet()) {
			m_sets.put(e.getKey(), (BitSet) e.getValue().clone());
		}
		this.size = other.size;
	}
	/**
	 * Get set index by long index (extract bits 20-63)
	 * 
	 * @param index
	 *            Long index
	 * @return Index of a bit set in the inner map
	 */
	private int getSetIndex(final int index) {
		return index >> VALUE_BITS;
	}
	
	private int getStartSetIndex(final int setIndex) {
		return setIndex << VALUE_BITS;
	}

	/**
	 * Get index of a value in a bit set (bits 0-19)
	 * 
	 * @param index
	 *            Long index
	 * @return Index of a value in a bit set
	 */
	private int getPos(final int index) {
		return index & VALUE_MASK;
	}

	/**
	 * Helper method to get (or create, if necessary) a bit set for a given long
	 * index
	 * 
	 * @param index
	 *            Long index
	 * @return A bit set for a given index (always not null)
	 */
	private BitSet bitSet(final int index) {
		final Integer iIndex = getSetIndex(index);
		BitSet bitSet = m_sets.get(iIndex);
		if (bitSet == null) {
			bitSet = new BitSet(1024);
			m_sets.put(iIndex, bitSet);
		}
		return bitSet;
	}

	/**
	 * Set a given index
	 * 
	 * @param index
	 *            Long index
	 */
	public void set(final int index) {
		BitSet bitSet = bitSet(index);
		int pos = getPos(index);
		if (bitSet.get(pos))
			return;
		size++;
		bitSet.set(pos);
	}
	
	public void clear(final int index) {
		BitSet bitSet = bitSet(index);
		int pos = getPos(index);
		if (!bitSet.get(pos))
			return;
		size--;
		bitSet.set(pos, false);
	}

	/**
	 * Get a value for a given index
	 * 
	 * @param index
	 *            Long index
	 * @return Value associated with a given index
	 */
	public boolean get(final int index) {
		final BitSet bitSet = m_sets.get(getSetIndex(index));
		return bitSet != null && bitSet.get(getPos(index));
	}
	
	public void or(SplitBitSet other) {
		size = -1;
		for (Entry<Integer, BitSet> e : other.m_sets.entrySet()) {
			if (m_sets.containsKey(e.getKey()))
				m_sets.get(e.getKey()).or(e.getValue());
			else 
				m_sets.put(e.getKey(), (BitSet) e.getValue().clone());
		}
	}
	
	public void xor(SplitBitSet other) {
		size = -1;
		Set<Integer> remove = null;
		for (Entry<Integer, BitSet> e : other.m_sets.entrySet()) {
			if (m_sets.containsKey(e.getKey())) {
				BitSet bitSet = m_sets.get(e.getKey());
				bitSet.xor(e.getValue());
				if (bitSet.isEmpty()) {
					if (remove == null)
						remove = new HashSet<>();
					remove.add(e.getKey());
				}
			} else 
				m_sets.put(e.getKey(), (BitSet) e.getValue().clone());
		}		
		if (remove != null) {
			for (Integer k : remove)
				m_sets.remove(k);
		}
	}
	
	@Override
	public SplitBitSet clone() {
		return new SplitBitSet(this);
	}

	public int cardinality() {
		if (size < 0)
			size = m_sets.values().stream().mapToInt(BitSet::cardinality).sum();
		return size;
	}
	
	public boolean isEmpty() {
		return cardinality() == 0;
	}

	public boolean intersects(SplitBitSet other) {
		if (other == null)
			return false;
		SplitBitSet min, max;
		if (this.m_sets.size() > other.m_sets.size()) {
			min = other;
			max = this;
		} else {
			min = this;
			max = other;
		}
		for (Entry<Integer, BitSet> e : min.m_sets.entrySet()) {
			BitSet bs = max.m_sets.get(e.getKey());
			if (bs != null && bs.intersects(e.getValue()))
				return true;
		}
		return false;
	}
	
	public IntStream stream() {
		if (m_sets.size() == 0)
			return IntStream.empty();
		
		IntStream stream = null;
		for (Entry<Integer, BitSet> e : m_sets.entrySet()) {
			int d = getStartSetIndex(e.getKey());
			IntStream s = e.getValue().stream().map(i -> i+d);
			if (stream == null)
				stream = s;
			else
				stream = IntStream.concat(stream, s);
		}
		return stream;
	}
	
	public void serialize(Writer w) {
		BitSet os = new BitSet(cardinality());
		stream().forEach(os::set);
		serialize(w, os);
	}

	public static void serialize(Writer w, BitSet bs) {
		long[] bits = bs.toLongArray();
		try {
			w.append(bits.length+"");
			boolean zero = false;
			for(int i=0; i<bits.length; i++) {
				if (bits[i] == 0) {
					zero = true;
					continue;
				}
				if (zero) {
					zero = false;
					w.append(" i"+i);
				}
				w.append(" "+bits[i]);
			}
		} catch(IOException e) {
			throw new UncheckedIOException(e);
		}
	}
	
	public static SplitBitSet deserialize(String line) {
		BitSet bitSet = deserializeBitSet(line);
		SplitBitSet out = new SplitBitSet();
		bitSet.stream().forEach(out::set);
		return out;
	}

	public static BitSet deserializeBitSet(String line) {
		String[] p = line.split(" ");
		int numWords = Integer.parseInt(p[0]);
		long[] bits = new long[numWords];
		int b = 0;
		for(int i=1; i<p.length; i++, b++) {
			if (p[i].startsWith("i"))
				b = Integer.parseInt(p[i].substring(1))-1;
			else
				bits[b] = Long.parseLong(p[i]);
		}
		
		BitSet bitSet = BitSet.valueOf(bits);
		return bitSet;
	}

	public void clear() {
		m_sets.clear();
		size = 0;
	}

	@Override
	public int hashCode() {
		return m_sets.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SplitBitSet other = (SplitBitSet) obj;
		if (size >= 0 && other.size >= 0 && size != other.size)
			return false;
		if (!m_sets.equals(other.m_sets))
			return false;
		return true;
	}
	
	
}