package takutility.function;

@FunctionalInterface
public interface IntBiFunction<T,R> {
	R apply(int value, T t);
}
