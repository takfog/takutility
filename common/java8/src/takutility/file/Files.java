package takutility.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import takutility.function.ThrowConsumer;
import takutility.io.UTF8BufferedReader;
import takutility.io.UTF8PrintWriter;

// ############### begin import

import java.util.function.Consumer;

//############### end import

public class Files {
	
	/**
	 * Creates a new file where the base path is replaced with the destination
	 * keeping the rest of the directory structure. <br/>
	 * E.g. file <tt>/original/path/inner/dir/file.txt</tt> with base 
	 * <tt>/original/path</tt> and destination <tt>/new/directory</tt> 
	 * would be transplanted in <tt>/new/directory/inner/dir/file.txt</tt> 
	 * @param file The file to transplant. Must be contained in <code>base</code>
	 * @param base the part of the path to replace
	 * @param dest the new directory
	 * @return
	 */
	public static File transplant(File file, File base, File dest) {
		return transplant(file, base, dest.getPath());
	}

	public static File transplant(File file, File base, String dest) {
		String path = file.getAbsolutePath();
		String basePath = base.getAbsolutePath();
		if (!path.startsWith(basePath))
			return file;
		path = path.replace(basePath, dest);
		return new File(path);
	}
	
	public static List<File> recursiveList(File dir) {
		return recursiveList(dir, new ArrayList<File>());
	}
	
	public static List<File> recursiveList(File dir, List<File> files) {
		for (File file : dir.listFiles()) {
			if (file.isDirectory())
				recursiveList(file, files);
			else
				files.add(file);
		}
		return files;
	}
	
	public static File commonRoot(File... files) {
		File root = null;
		for(int i=0; i<files.length; i++) {
			root = commonRootPair(root, files[i]);
		}
		return root;
	}
	
	private static File commonRootPair(File f1, File f2) {
		if (f1 == null) return f2;
		if (f2 == null) return f1;
		File[] f = new File[] {f1.getAbsoluteFile(), f2.getAbsoluteFile()};
		String[] p = new String[] {f1.getAbsolutePath(), f2.getAbsolutePath()};
		int sh, ln;
		if (p[0].length() > p[1].length()) {
			sh = 1;
			ln = 0;
		} else {
			sh = 0;
			ln = 1;
		}
		do {
			if (f[ln].equals(f[sh]))
				return f[0];
			
			if (!p[ln].startsWith(p[sh])) {
				f[sh] = f[sh].getParentFile();
				p[sh] = f[sh].getPath();
			}
			f[ln] = f[ln].getParentFile();
			p[ln] = f[ln].getPath();
			
			if (p[ln].length() < p[sh].length()) {
				sh = (sh+1)%2;
				ln = (ln+1)%2;
			}
		} while(p[sh].length() > 0);
		return f[sh];
	}
	
	public static void copy(File source, File dest) throws IOException {
		if (!source.isFile())
			throw new IllegalArgumentException(source+" is not a file");
		if (dest.exists() && !dest.isFile())
			throw new IllegalArgumentException(dest+" is not a file");
		// ########## begin copy
		java.nio.file.Files.copy(source.toPath(), dest.toPath());
		// ######## end
	}

	public static void copy(List<File> files, File source, File dest) throws IOException {
		for (File file : files) {
			File destFile = Files.transplant(file, source, dest);
			destFile.getParentFile().mkdirs();
			Files.copy(file, destFile);
		}
	}
	
	public static void copyDir(File source, File dest) throws IOException {
		if (!source.isDirectory())
			throw new IllegalArgumentException(source+" is not a directory");
		if (dest.exists() && !dest.isDirectory())
			throw new IllegalArgumentException(dest+" is not a directory");
		copyDir_inner(source, dest);
	}
	
	private static void copyDir_inner(File source, File dest) throws IOException {
		for (File f : source.listFiles()) {
			File d = new File(dest, f.getName());
			if (f.isDirectory()) {
				d.mkdirs();
				copyDir_inner(f, d);
			} else {
				copy(f, d);
			}
		}
	}
	
	public static void stringToFile(File file, String content) throws FileNotFoundException {
		PrintWriter w = new UTF8PrintWriter(file);
		try {
			w.print(content);
		} finally {
			w.close();
		}
	}
	
	public static String fileToString(File file) throws FileNotFoundException, IOException {
		if (file == null || !file.isFile()) 
			return null;
		BufferedReader r = new UTF8BufferedReader(file);
		StringBuffer sb = new StringBuffer();
		try {
			char[] buffer = new char[2048];
			int l;
			while ((l = r.read(buffer)) > 0) {
				sb.append(buffer, 0, l);
			}
		} finally {
			r.close();
		}
		return sb.toString();
	}

	public static <X extends Throwable> void tempFile(File file, 
			ThrowConsumer<File, X> consumer) throws X 
	{
		tempFile(file, new File(file.getParentFile(), file.getName()+".tmp"), consumer);
	}
	
	public static <X extends Throwable> void tempFile(File file, File tmp, 
			ThrowConsumer<File, X> consumer) throws X 
	{
		consumer.accept(tmp);
		file.delete();
		tmp.renameTo(file);
	}
	
	public static File addSuffix(File orig, String suffix) {
		String name = orig.getName();
		int extStart = name.lastIndexOf('.');
		if (extStart < 0)
			return new File(orig.getPath()+suffix);
		else
			return new File(orig.getParent(), 
					name.substring(0, extStart)+suffix+name.substring(extStart));
	}

	// ########### begin other
	public static void visit(File dir, Consumer<File> visitor) {
		for (File f : dir.listFiles()) {
			visitor.accept(f);
			if (f.isDirectory())
				visit(f, visitor);
		}
	}
	// ############## end other

}
