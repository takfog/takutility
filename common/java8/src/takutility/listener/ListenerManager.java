package takutility.listener;

//################# begin import function

import java.util.function.BiConsumer;

//################# end import function

public class ListenerManager<L,A> extends AbstractListenerManager<L> {
	private BiConsumer<L,A> listenerFunc;
	
	public ListenerManager(BiConsumer<L,A> listenerFunc) {
		this.listenerFunc = listenerFunc;
	}
	
	public void call() {
		call(null);
	}

	public void call(A arg) {
		if (listeners == null) return;
		for (L l : listeners) {
			listenerFunc.accept(l, arg);
		}
	}
}
