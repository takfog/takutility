package takutility.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//################# begin import function
//################# end import function

public class ActionListenerManager extends ListenerManager<ActionListener, ActionEvent> {

	public ActionListenerManager() {
		//################# begin super
		super(ActionListener::actionPerformed);
		//################# end super
	}

}
