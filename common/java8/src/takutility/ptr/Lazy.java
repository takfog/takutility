package takutility.ptr;

// ################# begin import function

import java.util.function.Supplier;

//################# end import function

public class Lazy<T> implements Supplier<T> {
	// ############## begin fields	
	private Supplier<T> get;
	// ############## end fields	
	private T val = null;
	private boolean created = false;
	private Supplier<T> factory;

	public Lazy(Supplier<T> factory) {
		this(false, factory);
	}
		
	public Lazy(boolean async, Supplier<T> factory) {
		this.factory = factory;
		// ############## begin async in ctor
		if (async)
			get = this::getAsync;
		else
			get = this::innerGet;
		// ############## end async in ctor
	}
	
	public Lazy(@SuppressWarnings("rawtypes") final Class klass) {
		this(false, klass);
	}
		
	@SuppressWarnings("unchecked")
	public Lazy(boolean async, @SuppressWarnings("rawtypes") final Class klass) {
		this(async, new Supplier<T>(){
			@Override
			public T get() {
				try {
					return (T) klass.newInstance();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
		try {
			klass.getConstructor();
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	}
	
	public T getActual() {
		return val;
	}
	
	public T getOrDefault(T defVal) {
		if (created)
			return val;
		else
			return defVal;
	}
	
	public T getOrDefault(Supplier<T> defVal) {
		if (created)
			return val;
		else
			return defVal.get();
	}
	
	public boolean isCreated() {
		return created;
	}

	protected T innerGet() {
		if (!created) {
			val = factory.get();
			created = true;
		}
		return val;
	}

	@Override
	public T get() {
		// ############# begin get
		return get.get();
		// ############# end
	}
	
	public T getAsync() {
		if (!created) {
			synchronized (this) {
				return innerGet();
			}
		}
		return val;
	}
	
	@Override
	public String toString() {
		return created ? val.toString() : "null";
	}
}
