package takutility.ptr;

import java.util.function.Supplier;

public class CloseableLazy<T extends AutoCloseable> extends Lazy<T> 
	implements AutoCloseable, Supplier<T>
{

	public CloseableLazy(Supplier<T> factory) {
		super(factory);
	}

	public CloseableLazy(boolean async, Supplier<T> factory) {
		super(async, factory);
	}

	@Override
	public void close() throws Exception {
		if (isCreated())
			get().close();
	}

}