package takutility;

// ######### begin import

import java.util.Comparator;
import java.util.function.Function;

//######### end import

public class Comparators {
	public static int compare(long x, long y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }
	
	public static int compare(int x, int y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }
	
	// ############ begin methods

	public static <T,V extends Comparable<V>> int compare(T o1, T o2, Function<T, V> func) {
		return func.apply(o1).compareTo(func.apply(o2));
	}
	
	public static <T,V> int compare(T o1, T o2, Function<T, V> func, Comparator<V> comp) {
		return comp.compare(func.apply(o1), func.apply(o2));
	}
	
	public static <T,V extends Comparable<V>> Comparator<T> function(Function<T, V> func) {
		return (o1,o2) -> compare(o1, o2, func);
	}
	
	public static <T,V> Comparator<T> function(Function<T, V> func, Comparator<V> comp) {
		return (o1,o2) -> compare(o1, o2, func, comp);
	}
	
	// ############ end methods
}
