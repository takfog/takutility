package takutility.collections;

import java.util.AbstractList;

// ################ insert import

public class ArrayOf<E> extends AbstractList<E> {
	private Object[] array;

	public ArrayOf(int length) {
		array = new Object[length];
	}

	// ################# insert varargs annot
	public ArrayOf(E... elements) {
		array = elements;
	}
	
	// ################## insert ctors

	@Override
	@SuppressWarnings("unchecked")
	public E get(int index) {
		return (E) array[index];
	}

	@Override
	@SuppressWarnings("unchecked")
	public E set(int index, E element) {
		E old = (E) array[index];
		array[index] = element;
		return old;
	}

	@Override
	public int size() {
		return array.length;
	}
}
