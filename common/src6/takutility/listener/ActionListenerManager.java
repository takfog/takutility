package takutility.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//################# begin import function

import takutility.function.java8.BiConsumer;

//################# end import function

public class ActionListenerManager extends ListenerManager<ActionListener, ActionEvent> {

	public ActionListenerManager() {
		//################# begin super
		super(new BiConsumer<ActionListener, ActionEvent>() {
			@Override
			public void accept(ActionListener listener, ActionEvent e) {
				listener.actionPerformed(e);
			}
		});
		//################# end super
	}

}
