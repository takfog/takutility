package takutility.listener;

//################# begin import function

import takutility.function.java8.BiConsumer;

//################# end import function

public class ListenerNManager<L> extends AbstractListenerManager<L> {
	private BiConsumer<L,Object[]> listenerFunc;
	
	public ListenerNManager(BiConsumer<L,Object[]> listenerFunc) {
		this.listenerFunc = listenerFunc;
	}

	public void call(Object... args) {
		if (listeners == null) return;
		for (L l : listeners) {
			listenerFunc.accept(l, args);
		}
	}
}
