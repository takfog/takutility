package takutility.ctr;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import takutility.collections.AutoMap;
import takutility.collections.Factory;
import takutility.composite.Pair;

// ############## insert import

public class CorrelationCounter<A,B> {
	protected Map<A, Map<B, Counter>> matrix;
	
	public CorrelationCounter() {
		matrix = new AutoMap<A, Map<B, Counter>>(new Factory<Map<B, Counter>>() {
			@Override
			public Map<B, Counter> create() {
				return new AutoMap<B, Counter>(Counter.class);
			}
		});
	}
	
	public void addAll(Collection<A> as, Collection<B> bs) {
		for (A a : as) {
			Map<B, Counter> mb = matrix.get(a);
			for (B b : bs) {
				mb.get(b).increment();
			}
		}
	}

	public void add(Pair<A,B> pair) {
		add(pair.a(), pair.b());
	}
	
	public void add(A a, B b) {
		matrix.get(a).get(b).increment();
	}

	public void add(Pair<A,B> pair, int value) {
		add(pair.a(), pair.b(), value);
	}
	
	public void add(A a, B b, int value) {
		matrix.get(a).get(b).increment(value);
	}

	public int get(Pair<A,B> pair) {
		return get(pair.a(), pair.b());
	}

	public int get(A a, B b) {
		if (!matrix.containsKey(a))
			return 0;
		Map<B, Counter> mb = matrix.get(a);
		if (!mb.containsKey(b))
			return 0;
		return mb.get(b).get();
	}
	
	public Set<A> getAValues() {
		return matrix.keySet();
	}
	
	public Set<B> getBValues() {
		HashSet<B> vals = new HashSet<B>();
		for (Map<B, Counter> mb : matrix.values()) {
			vals.addAll(mb.keySet());
		}
		return vals;
	}
	
	public double getMeanA(A a) {
		if (!matrix.containsKey(a))
			return Double.NaN;
		double tot = 0.;
		Collection<Counter> vals = matrix.get(a).values();
		for (Counter c: vals) {
			tot += c.doubleValue();
		}
		return tot / vals.size();
	}
	
	public int getCorrelationsA(A a) {
		if (!matrix.containsKey(a))
			return 0;
		return matrix.get(a).size();
	}
	
	public double getMeanB(B b) {
		double tot = 0.;
		int count = 0;
		for (Map<B, Counter> mb : matrix.values()) {
			if (mb.containsKey(b)) {
				count++;
				tot += mb.get(b).doubleValue();
			}
		}
		return tot / count;
	}
	
	public int getCorrelationsB(B b) {
		int count = 0;
		for (Map<B, Counter> mb : matrix.values()) {
			if (mb.containsKey(b)) {
				count++;
			}
		}
		return count;
	}
	
	public Map<B, Integer> getAllB(A a) {
		if (!matrix.containsKey(a))
			return Collections.emptyMap();
		HashMap<B, Integer> ret = new HashMap<B, Integer>();
		for (Entry<B, Counter> e : matrix.get(a).entrySet()) {
			ret.put(e.getKey(), e.getValue().get());
		}
		return ret;
	}
	
	public Map<A, Integer> getAllA(B b) {
		HashMap<A, Integer> ret = new HashMap<A, Integer>();
		for (Entry<A, Map<B, Counter>> e : matrix.entrySet()) {
			if (e.getValue().containsKey(b)) {
				ret.put(e.getKey(), e.getValue().get(b).get());
			}
		}
		return ret;
	}
	
	// ############ insert methods
}
