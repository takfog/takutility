package takutility.ctr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

// ############# insert import

public class SymmetricCorrelationCounter<E extends Comparable<E>> extends CorrelationCounter<E, E> {

	public void addAll(Collection<E> coll) {
		List<E> list = new ArrayList<E>(coll);
		Collections.sort(list);
		for(int i=0; i<list.size(); i++) {
			E e = list.get(i);
			for(int j=i+1; j<list.size(); j++) {
				super.add(e, list.get(j));
			}
		}
	}
	
	@Override
	public void add(E a, E b) {
		add(a, b, 1);
	}
	
	@Override
	public void add(E a, E b, int value) {
		int comp = a.compareTo(b);
		if (comp < 0)
			super.add(a, b, value);
		else if (comp > 0)
			super.add(b, a, value);
		else {
			if (!matrix.containsKey(a)) 
				super.add(b, a, value);
			else {
				Map<E, Counter> ma = matrix.get(a);
				if (ma.containsKey(b) || !matrix.containsKey(b)) 
					ma.get(b).increment(value);
				else 
					super.add(b, a, value);
			}
		}
	}
	
	@Override
	public int get(E a, E b) {
		int comp = a.compareTo(b);
		if (comp < 0)
			return super.get(a, b);
		else if (comp > 0)
			return super.get(b, a);
		else {
			if (!matrix.containsKey(a)) 
				return super.get(b, a);
			else {
				Map<E, Counter> ma = matrix.get(a);
				if (ma.containsKey(b)) 
					return ma.get(b).get();
				else 
					return super.get(b, a);
			}
		}
	}
	
	@Override
	public Map<E, Integer> getAllA(E e) {
		return getAll(e);
	}
	
	@Override
	public Map<E, Integer> getAllB(E e) {
		return getAll(e);
	}
	
	public Map<E, Integer> getAll(E e) {
		Map<E, Integer> ret = new HashMap<E, Integer>();
		
		for (Entry<E, Counter> en : matrix.get(e).entrySet()) {
			ret.put(en.getKey(), en.getValue().get());
		}
		for (E s : matrix.keySet()) {
			if (s.compareTo(e) < 0) {
				Map<E, Counter> map2 = matrix.get(s);
				if (map2.containsKey(e)) {
					ret.put(s, map2.get(e).get());
				}
			}
		}
		return ret;
	}
	
	public double getMean(E e) {
		double tot = 0.;
		int count = 0;
		if (matrix.containsKey(e)) {
			for (Counter c : matrix.get(e).values()) {
				count++;
				tot += c.doubleValue();
			}
		}
		for (E s : matrix.keySet()) {
			if (s.compareTo(e) < 0) {
				Map<E, Counter> map2 = matrix.get(s);
				if (map2.containsKey(e)) {
					count++;
					tot += map2.get(e).doubleValue();
				}
			}
		}
		return tot/count;
	} 
	
	@Override
	public double getMeanA(E e) {
		return getMean(e);
	}
	
	@Override
	public double getMeanB(E e) {
		return getMean(e);
	}
	
	public Set<E> getValues() {
		HashSet<E> vals = new HashSet<E>();
		vals.addAll(super.getAValues());
		vals.addAll(super.getBValues());
		return vals;
	}
	
	@Override
	public Set<E> getAValues() {
		return getValues();
	}
	
	@Override
	public Set<E> getBValues() {
		return getValues();
	}
	
	public int getCorrelations(E e) {
		int count = 0;
		if (matrix.containsKey(e))
			count = matrix.get(e).size();
		
		for (E s : matrix.keySet()) {
			if (s.compareTo(e) < 0) {
				Map<E, Counter> map2 = matrix.get(s);
				if (map2.containsKey(e)) {
					count++;
				}
			}
		}
		return count;
	}
	
	@Override
	public int getCorrelationsA(E e) {
		return getCorrelations(e);
	}
	
	@Override
	public int getCorrelationsB(E e) {
		return getCorrelations(e);
	}
	
	// ################### insert methods
}
