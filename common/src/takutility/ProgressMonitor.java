package takutility;

import java.util.Timer;
import java.util.TimerTask;

import takutility.ptr.IntPtr;
import takutility.ptr.Ptr;

public class ProgressMonitor {
	private static final int PAUSE_PRINT_TIME = 60;
	private Timer timer = new Timer(true);
	private final long rate = 1000L;

	public TimerTask add(Number c) {
		return add(c, null);
	}

	public TimerTask add(final Number c, final Ptr<?> ptr) {
		final IntPtr last = new IntPtr(c.intValue());
		final IntPtr lastEq = new IntPtr(0);
		return add(new Runnable() {
			@Override
			public void run() {
				int v = c.intValue();
				if (v == last.val) {
					lastEq.val++;
					if ((lastEq.val % PAUSE_PRINT_TIME) == 0)
						System.out.print(".");
					return;
				}
				if (lastEq.val >= PAUSE_PRINT_TIME) {
					System.out.println();
					lastEq.val = 0;
				}

				last.val = v;
				if (ptr == null)
					System.out.println(v);
				else
					System.out.println(v + "\t" + ptr);
			}
		});
	}

	public TimerTask add(final Runnable action) {
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				action.run();
				
			}
		};
		timer.scheduleAtFixedRate(task, rate, rate);
		return task;
	}

	public void cancelAll() {
		timer.cancel();
	}
}