package takutility.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class DoubleOutputStream extends FilterOutputStream {
	private OutputStream out2;
	
	public DoubleOutputStream(OutputStream out1, OutputStream out2) {
		super(out1);
		this.out2 = out2;
	}

	@Override
	public void close() throws IOException {
		out.close();
		out2.close();
	}
	
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		out.write(b, off, len);
		out2.write(b, off, len);
	}
	
	@Override
	public void write(int b) throws IOException {
		out.write(b);
		out2.write(b);
	}
	
	@Override
	public void write(byte[] b) throws IOException {
		out.write(b);
		out2.write(b);
	}
	
	@Override
	public void flush() throws IOException {
		out.flush();
		out2.flush();
	}
	
}
