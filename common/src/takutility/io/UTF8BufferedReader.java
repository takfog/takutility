package takutility.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

public class UTF8BufferedReader extends BufferedReader implements Iterable<String> {

	public UTF8BufferedReader(File file) throws FileNotFoundException {
		super(new UTF8Reader(file));
	}

	public UTF8BufferedReader(String file) throws FileNotFoundException {
		super(new UTF8Reader(file));
	}

	@Override
	public Iterator<String> iterator() {
		return new LineIterator();
	}

	private class LineIterator implements Iterator<String> {
		private String next;
		
		public LineIterator() {
			try {
				next = readLine();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		public boolean hasNext() {
			return next != null;
		}

		@Override
		public String next() {
			String last = next;
			try {
				next = readLine();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			return last;
		}

		@Override
		public void remove() {
	        throw new UnsupportedOperationException("remove");
	    }
	}
}