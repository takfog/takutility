package takutility.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class UnclosableOutputStream extends FilterOutputStream {

	protected UnclosableOutputStream(OutputStream in) {
		super(in);
	}

	@Override
	public void close() throws IOException {}
}
