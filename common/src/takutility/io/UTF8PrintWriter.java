package takutility.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class UTF8PrintWriter extends PrintWriter {
	
	public UTF8PrintWriter(File file) throws FileNotFoundException {
		super(new UTF8Writer(file));
	}

	public UTF8PrintWriter(String name) throws FileNotFoundException {
		super(new UTF8Writer(name));
	}
}