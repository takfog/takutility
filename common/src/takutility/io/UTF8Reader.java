package takutility.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class UTF8Reader extends InputStreamReader {

	public UTF8Reader(File file) throws FileNotFoundException {
		super(new FileInputStream(file), Charset.forName("UTF-8"));
	}

	public UTF8Reader(String file) throws FileNotFoundException {
		super(new FileInputStream(file), Charset.forName("UTF-8"));
	}

}