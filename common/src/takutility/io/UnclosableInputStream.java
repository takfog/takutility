package takutility.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class UnclosableInputStream extends FilterInputStream {

	protected UnclosableInputStream(InputStream in) {
		super(in);
	}

	@Override
	public void close() throws IOException {}
}
