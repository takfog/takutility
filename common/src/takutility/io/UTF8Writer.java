package takutility.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

public class UTF8Writer extends OutputStreamWriter {

	public UTF8Writer(File file) throws FileNotFoundException {
		super(new FileOutputStream(file), Charset.forName("UTF-8"));
	}

	public UTF8Writer(String name) throws FileNotFoundException {
		super(new FileOutputStream(name), Charset.forName("UTF-8"));
	}
}