package takutility.function;

public interface ThrowConsumer<T, X extends Throwable> {

	void accept(T t) throws X;
}
