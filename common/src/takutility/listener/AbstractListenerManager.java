package takutility.listener;

import takutility.collections.UnorderedArrayList;

public class AbstractListenerManager<L> {

	protected UnorderedArrayList<L> listeners = null;

	public AbstractListenerManager() {
		super();
	}

	public void add(L listener) {
		if (listeners == null)
			listeners = new UnorderedArrayList<L>();
		listeners.add(listener);
	}

	public boolean remove(L listener) {
		if (listeners == null) return false;
		return listeners.remove(listener);
	}

	public void clear() {
		if (listeners != null)
			listeners.clear();
	}

}