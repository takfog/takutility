package takutility.listener;

import takutility.function.TriConsumer;

public class Listener2Manager<L,A,B> extends AbstractListenerManager<L> {
	private TriConsumer<L,A,B> listenerFunc;
	
	public Listener2Manager(TriConsumer<L,A,B> listenerFunc) {
		this.listenerFunc = listenerFunc;
	}

	public void call(A arg1, B arg2) {
		if (listeners == null) return;
		for (L l : listeners) {
			listenerFunc.accept(l, arg1, arg2);
		}
	}
}
