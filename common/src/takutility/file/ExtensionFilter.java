package takutility.file;

import java.io.File;
import java.io.FilenameFilter;

public class ExtensionFilter implements FilenameFilter {
	private String ext;
	
	public ExtensionFilter(String ext) {
		if (ext.startsWith("."))
			this.ext = ext;
		else
			this.ext = "."+ext;
	}

	@Override
	public boolean accept(File dir, String name) {
		return name.endsWith(ext);
	}

}
