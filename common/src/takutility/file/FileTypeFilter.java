package takutility.file;

import java.io.File;
import java.io.FileFilter;

public class FileTypeFilter implements FileFilter {
	enum FileType {
		FILE, DIR
	}
	
	private FileType type;
	
	public static FileTypeFilter file() {
		return new FileTypeFilter(FileType.FILE);
	}
	
	public static FileTypeFilter directory() {
		return new FileTypeFilter(FileType.DIR);
	}
	
	public FileTypeFilter(FileType type) {
		this.type = type;
	}
	
	@Override
	public boolean accept(File pathname) {
		switch (type) {
		case FILE:
			return pathname.isFile();
		case DIR:
			return pathname.isDirectory();
		default:
			return false;
		}
	}

}
