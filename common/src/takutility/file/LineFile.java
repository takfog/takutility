package takutility.file;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class LineFile implements Closeable, Iterable<String> {
	private static final int STEP_BACK = 20;
	
	private RandomAccessFile file;
	private String encoding;
	private InputStreamReader reader, stepReader;
	private boolean fullRead = false;
	/**
	 * End of line position
	 */
	private ArrayList<Long> linePos = new ArrayList<Long>();

	public LineFile(File file, String encoding) throws FileNotFoundException, UnsupportedEncodingException {
		this.file = new RandomAccessFile(file, "r");
		init(encoding);
	}

	public LineFile(String pathname, String encoding) throws FileNotFoundException, UnsupportedEncodingException {
		this.file = new RandomAccessFile(pathname, "r");
		init(encoding);
	}
	
	private void init(String encoding) throws UnsupportedEncodingException {
		this.encoding = encoding;
		stepReader = new InputStreamReader(new RandomInputStream(true), encoding);
		reader = new InputStreamReader(new RandomInputStream(false), encoding);
	}
	
	public boolean goTo(int line) throws IOException {
		if (line >= linePos.size()) {
			if (fullRead)
				return false;
			if(linePos.size() == 0)
				file.seek(0);
			else
				file.seek(linePos.get(linePos.size()-1));
			
			long spos = file.getFilePointer();
			Scanner s = new Scanner(stepReader);
			BufferedReader br = new BufferedReader(reader);
			
			while(line >= linePos.size()) {
				//find line using buffer
				String bline = br.readLine();
				if (bline == null) {
					fullRead = true;
					break;
				}
				int lcount = bline.getBytes(encoding).length;
				//save buffer position
				long bpos = file.getFilePointer();
				
				//find exact position using scanner
				if (lcount > STEP_BACK)
					file.seek(spos + lcount - STEP_BACK); //move near the end of the line
				else
					file.seek(spos); //very short line
				s.nextLine();
				//save scanner position
				spos = file.getFilePointer();
				linePos.add(spos);
				
				//go back to buffer position
				file.seek(bpos);
			}
		}
		
		if (line == 0) {
			file.seek(0);
			return true;
		} else if (line < linePos.size()) {
			file.seek(linePos.get(line-1));
			return true;
		} else {
			fullRead = true;
			return false;
		}
	}
	
	public String get(int line) throws IOException {
		if (goTo(line)) {
			int size;
			if (line == 0)
				size = linePos.get(0).intValue();
			else
				size = (int)(linePos.get(line) - linePos.get(line-1));
			byte[] b = new byte[size];
			file.read(b, 0, size);
			return new String(b, encoding).replaceAll("\n|\r", "");
		} else
			throw new IndexOutOfBoundsException();
	}
	
	public int loadedLines() {
		return linePos.size();
	}
	
	public int size() {
		if (!fullRead) {
			try {
				goTo(Integer.MAX_VALUE);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		return linePos.size();
	}
	
	@Override
	public void close() throws IOException {
		file.close();
	}

	@Override
	public Iterator<String> iterator() {
		return new LineIterator();
	}
	
	private class RandomInputStream extends InputStream {
		private boolean singleStep;
		
		public RandomInputStream(boolean singleStep) {
			this.singleStep = singleStep;
		}

		@Override
		public int read() throws IOException {
			return file.read();
		}
		
		@Override
		public int read(byte[] b) throws IOException {
			return file.read(b, 0, singleStep ? 1 : b.length);
		}
		
		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			return file.read(b, off, singleStep ? 1 : len);
		}
	}
	
	private class LineIterator implements Iterator<String> {
		private int index = 0;
		
		@Override
		public boolean hasNext() {
			return !fullRead || index < size();
		}

		@Override
		public String next() {
			try {
				return get(index++);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}

class CharCounterInputStream extends FilterInputStream {
	private int count = 0;
	
	protected CharCounterInputStream(InputStream in) {
		super(in);
	}
	
	@Override
	public int read() throws IOException {
		count++;
		return super.read();
	}
	
	public int getCount() {
		return count;
	}
	
	public void resetCount() {
		count = 0;
	}
	
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		int r = super.read(b, off, 1);
		count += r;
		return r;
	}
	
	@Override
	public int read(byte[] b) throws IOException {
		int r = super.read(b, 0, 1);
		count += r;
		return r;
	}
	
	@Override
	public int available() throws IOException {
		int a = super.available();
		return a < 1 ? a : 1;
	}
}
