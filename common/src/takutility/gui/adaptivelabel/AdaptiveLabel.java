package takutility.gui.adaptivelabel;

import java.awt.Font;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JLabel;


public class AdaptiveLabel extends JLabel implements ComponentListener {
	private static final long serialVersionUID = -6732503634477850039L;
	private FontResizer fr;
	
	public AdaptiveLabel() {
		fr = new FontResizer(this, getFont());
		addComponentListener(this);
	}
	
	@Override
	public void setFont(Font font) {
		super.setFont(font);
		fr = new FontResizer(this, font);
		refreshSize();
	}
	
	@Override
	public void setText(String text) {
		super.setText(text);
		refreshSize();
	}
	
	public void refreshSize() {
		if (fr == null) return;
		super.setFont(fr.resize(getSize(), getText()));
	}

	@Override
	public void componentResized(ComponentEvent e) {
		refreshSize();
	}

	@Override
	public void componentMoved(ComponentEvent e) {}

	@Override
	public void componentShown(ComponentEvent e) {}

	@Override
	public void componentHidden(ComponentEvent e) {}
}
