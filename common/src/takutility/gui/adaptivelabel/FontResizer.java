package takutility.gui.adaptivelabel;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;

public class FontResizer {
	private static final float PRECISION = 5;
	private static final double MARGIN = 0;
	
	private Font base;
	private Component comp;
	
	public FontResizer(Component comp, Font base) {
		this.comp = comp;
		this.base = base;
	}
	
	public Font resize(Dimension size, String text) {
		float mh = getMaxSize(size.getHeight(), text, Float.MAX_VALUE, false);
		float mw = getMaxSize(size.getWidth() - MARGIN, text, mh, true);
		return base.deriveFont(Math.min(mw, mh));
	}
	
	public Font getFont() {
		return base;
	}

	/**
	 * Restituisce la dimensione massima del font per non
	 * superare la dimensione massima.
	 * @param maxdim la dimensione massima
	 * @param text il testo che dovra` essere scritto
	 * @param width true per misurare la larghezza, false per l'altezza
	 * @return la dimensione massima
	 */
	private float getMaxSize(double maxdim, String text, float max, boolean width) {
		if (maxdim < 0) return 0;
		float min = 0;
		float size = base.getSize2D();
		FontMetrics test = comp.getFontMetrics(base);
		while(true){//(max - min) > PRECISION && size > maxdim) {
			int fdim;
			if (width)
				fdim = test.stringWidth(text);
			else
				fdim = test.getHeight();
			if (fdim > maxdim) {
				//bisogna ridurre
				max = size;
				size = (min + size) / 2;
			} else if ((max - min) > PRECISION) {
				//bisogna aumentare
				min = size;
				if ((size * 2) > max) 
					size = (max + size) / 2;
				else 
					size *= 2;
			} else break; //ci siamo
			test = comp.getFontMetrics(base.deriveFont(size));
		}
		return size;
	}

}
