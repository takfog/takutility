package takutility.gui;

import java.awt.Component;

import javax.swing.JOptionPane;

public class JMessageDialog {
	private static String warnTitle = "Warning", errTitle = "Error";
	
	public static void setWarningTitle(String warnTitle) {
		JMessageDialog.warnTitle = warnTitle;
	}
	
	public static void setErrorTitle(String errTitle) {
		JMessageDialog.errTitle = errTitle;
	}

	public static void warning(Component parentComponent, Object message) {
		warning(parentComponent, message, null);
	}

	public static void warning(Component parentComponent, Object message, String title) {
		if (title == null) title = warnTitle;
		JOptionPane.showMessageDialog(parentComponent, message, title, JOptionPane.WARNING_MESSAGE);
	}

	public static void error(Component parentComponent, Object message) {
		error(parentComponent, message, null);
	}

	public static void error(Component parentComponent, Object message, String title) {
		if (title == null) title = errTitle;
		JOptionPane.showMessageDialog(parentComponent, message, title, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void error(Component parentComponent, Exception e, String title) {
		error(parentComponent, e, null, title);
	}
	
	public static void warning(Component parentComponent, Exception e, String title) {
		warning(parentComponent, e, null, title);
	}
	
	public static void error(Component parentComponent, Exception e, Object message, String title) {
		e.printStackTrace();
		if (message == null) message = e.getMessage();
		error(parentComponent, message, title);
	}
	
	public static void warning(Component parentComponent, Exception e, Object message, String title) {
		e.printStackTrace();
		if (message == null) message = e.getMessage();
		warning(parentComponent, message, title);
	}
	

}
