package takutility.gui;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;

/**
 * An extension of JLabel which looks like a link and responds appropriately
 * when clicked.
 * Note that because of the way this class is implemented, getText() will not
 * return correct values, use <code>getNormalText</code> instead.
 */
public class LinkLabel extends JLabel {
	private static final long serialVersionUID = 1733938268237535646L;
	private static final Cursor CUR_DEF = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
    private static final Cursor CUR_HAND = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

    /**
     * The normal text set by the user.
     */
    private String text;
    private int textSize = 0;
    private boolean isOver = false;

    public LinkLabel() {
        enableEvents(MouseEvent.MOUSE_EVENT_MASK | MouseEvent.MOUSE_MOTION_EVENT_MASK);
    }

    /**
     * Creates a new LinkLabel with the given text.
     */
    public LinkLabel(String text) {
        super(text);
        enableEvents(MouseEvent.MOUSE_EVENT_MASK);
    }

    /**
     * Sets the text of the label.
     */
    @Override
    public void setText(String text) {
        super.setText("<html><font color=\"#0000CF\"><u>" + text + "</u></font></html>"); //$NON-NLS-1$ //$NON-NLS-2$
        this.text = text;
        if (text != null && getFont() != null)
            textSize = getFontMetrics(getFont()).stringWidth(getNormalText());
    }

    private boolean isOverText(int x) {
        return x <= textSize;
    }

    /**
     * Returns the text set by the user.
     */
    public String getNormalText() {
        return text;
    }

    /**
     * Processes mouse events and responds to clicks.
     */
    @Override
    protected void processMouseEvent(MouseEvent evt) {
        super.processMouseEvent(evt);
        if (evt.getID() == MouseEvent.MOUSE_CLICKED && isOverText(evt.getX())) {
            fireActionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, getNormalText()));
        }
    }

    @Override
    protected void processMouseMotionEvent(MouseEvent evt) {
        //se non e` nella stessa posizione di prima
        if(evt.getID() == MouseEvent.MOUSE_MOVED && isOver != isOverText(evt.getX())){
            isOver = !isOver; //cambia posizione
            if (isOver)
                setCursor(CUR_HAND);
            else
                setCursor(CUR_DEF);
        }
    }

    /**
     * Adds an ActionListener to the list of listeners receiving notifications
     * when the label is clicked.
     */
    public void addActionListener(ActionListener listener) {
        listenerList.add(ActionListener.class, listener);
    }

    /**
     * Removes the given ActionListener from the list of listeners receiving
     * notifications when the label is clicked.
     */
    public void removeActionListener(ActionListener listener) {
        listenerList.remove(ActionListener.class, listener);
    }

    /**
     * Fires an ActionEvent to all interested listeners.
     */
    protected void fireActionPerformed(ActionEvent evt) {
        Object[] listeners = listenerList.getListenerList();
        for (int i = 0; i < listeners.length; i += 2) {
            if (listeners[i] == ActionListener.class) {
                ActionListener listener = (ActionListener) listeners[i + 1];
                listener.actionPerformed(evt);
            }
        }
    }
}
