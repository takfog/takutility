package takutility.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;

/**
 * Classe statica contenente metodi per il posizionamento delle finestre.
 */
public final class CentraFinestra {

	/**
	 * Posiziona una finestra al centro dello schermo.
	 * @param w la finestra da posizionare
	 */
	public static void schermo(Window w){
		area(w, Toolkit.getDefaultToolkit().getScreenSize(), 0, 0);
	}
	
	/**
	 * Posiziona una finestra al centro di un altro {@link Component}.
	 * @param w la finestra da posizionare
	 * @param parent il  {@link Component} da usare come riferimento
	 */
	public static void component(Window w, Component parent){
		area(w, parent.getSize(), parent.getX(), parent.getY());
	}
	
	/**
	 * Posiziona una finestra al centro di un'area.
	 * @param w la finestra da posizionare
	 * @param area le dimensioni dell'area
	 * @param offx l'offest sulla x dal bordo dello schermo
	 * @param offy l'offest sulla y dal bordo dello schermo
	 */
	public static void area(Window w, Dimension area, int offx, int offy) {
		w.setLocation((area.width - w.getWidth())/2 + offx, 
				(area.height - w.getHeight())/2 + offy);
	}

	private CentraFinestra() {}
}
