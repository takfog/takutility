package takutility.gui.checkedlist;

import javax.swing.DefaultListSelectionModel;


public class ToggleSelectionModel extends DefaultListSelectionModel
{
	private static final long serialVersionUID = -2553468705388661023L;
	boolean canChange = true, isAdj = false;

	@Override
    public void setSelectionMode(int selectionMode) {
        if (selectionMode == SINGLE_INTERVAL_SELECTION)
            throw new RuntimeException("Singolo intervallo non consentito");
        super.setSelectionMode(selectionMode);
    }
    
    @Override
    public void setSelectionInterval(int index0, int index1) {
    	if (!canChange) return;
    	canChange = !isAdj;
        if (isSelectedIndex(index1))
            super.removeSelectionInterval(index1, index1);
        else if (getSelectionMode() != SINGLE_SELECTION)
            super.addSelectionInterval(index1, index1);
        else
            super.setSelectionInterval(index1, index1);
    }

    @Override
    public void removeSelectionInterval(int index0, int index1) {
        super.removeSelectionInterval(index1, index1);
    }

    @Override
    public void addSelectionInterval(int index0, int index1) {
        super.addSelectionInterval(index1, index1);
    }
    
    @Override
	public void setValueIsAdjusting(boolean isAdjusting) {
    	isAdj = isAdjusting;
    	canChange = true;
    }
}
