package takutility.gui.checkedlist;

import java.awt.Component;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.ListCellRenderer;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;

/**
 * Mostra gli elementi di una lista come {@link JCheckBox} se 
 * la lista e` a selezione multipla, come {@link JOptionPane}
 * se e` a selezione singola.
 */
public class CheckListCellRenderer implements ListCellRenderer {
    private JToggleButton comp;
    private int oldSelMode = -1;
    
    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus)
    {
        if (list.getSelectionMode() != oldSelMode){
            oldSelMode = list.getSelectionMode();
            if (oldSelMode == ListSelectionModel.SINGLE_SELECTION)
                comp = new JRadioButton();
            else
                comp = new JCheckBox();
            comp.setOpaque(false);
        }
        //comp.setBackground(list.getBackground());
        comp.setText(value.toString());
        comp.setSelected(isSelected);

        return comp;
    }
}

