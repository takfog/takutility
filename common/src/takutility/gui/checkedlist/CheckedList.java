package takutility.gui.checkedlist;

import java.util.Vector;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;


/**
 * Un componente che mostra una lista di oggetti affiancati da una checkbox.
 */
public class CheckedList extends JList {
	private static final long serialVersionUID = 2305617751349577789L;

	private static void init(JList list) {
        list.setSelectionModel(new ToggleSelectionModel());
        list.setSelectionMode(ToggleSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        list.setCellRenderer(new CheckListCellRenderer());
    }

    public CheckedList() {
        init(this);
    }

    public CheckedList(ListModel dataModel) {
        super(dataModel);
        init(this);
   }

    public CheckedList(Object[] listData) {
        super(listData);
        init(this);
    }

    public CheckedList(Vector<?> listData) {
        super(listData);
        init(this);
    }
    
    public boolean isChecked(int index) {
    	return getSelectionModel().isSelectedIndex(index);
    }
    
    public void checkAll() {
    	ListSelectionModel m = getSelectionModel();
    	int size = getModel().getSize();
    	for (int i = 0; i < size; i++) {
    		m.addSelectionInterval(i, i);
		}
    }
    
    public void uncheckAll() {
    	ListSelectionModel m = getSelectionModel();
    	m.clearSelection();
    }
    
    public void setChecked(int index, boolean value) {
    	ListSelectionModel m = getSelectionModel();
    	if (value == m.isSelectedIndex(index)) return;
    	if (value)
    		m.addSelectionInterval(index, index);
    	else
    		m.removeSelectionInterval(index, index);
    }
}
