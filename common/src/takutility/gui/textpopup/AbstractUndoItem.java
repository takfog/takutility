package takutility.gui.textpopup;

import java.awt.event.ActionEvent;

import javax.swing.event.UndoableEditListener;
import javax.swing.text.JTextComponent;
import javax.swing.undo.UndoManager;

public abstract class AbstractUndoItem extends TextPopupItem {
	private static final long serialVersionUID = -3717364093781068609L;
	
	private UndoManager undoManager;

	public AbstractUndoItem(String text, JTextComponent component, UndoManager undoManager) {
		this(text, component, undoManager, undoManager);
	}

	public AbstractUndoItem(String text, JTextComponent component, UndoManager undoManager, 
			UndoableEditListener listener) 
	{
		super(text, component);
		this.undoManager = undoManager;
		UndoableEditListener list = listener == null ? undoManager : listener;
		component.getDocument().removeUndoableEditListener(list);
		component.getDocument().addUndoableEditListener(list);
	}

	public AbstractUndoItem(PopupAction action, JTextComponent component) {
		this(action.toString(), component, new UndoManager());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		act(undoManager);
	}

	@Override
	public void checkValidity() {
		setEnabled(component.isEditable() && canAct(undoManager));
	}
	
	protected abstract void act(UndoManager undoManager);
	
	protected abstract boolean canAct(UndoManager undoManager);

	@Override
	public abstract PopupAction getPopupAction();

}
