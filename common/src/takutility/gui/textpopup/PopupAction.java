package takutility.gui.textpopup;

public enum PopupAction {
	UNDO_REDO, UNDO, REDO, CUT, COPY, PASTE, SELECT_ALL;
	
	public boolean readOnly() {
		switch (this) {
		case COPY:
		case SELECT_ALL:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public String toString() {
		switch (this) {
		case UNDO_REDO:
			return "Undo";
		case UNDO:
			return "Undo";
		case REDO:
			return "Redo";
		case CUT:
			return "Cut";
		case COPY:
			return "Copy";
		case SELECT_ALL:
			return "Select all";
		case PASTE:
			return "Paste";
		default:
			return "";
		}
	}
}
