package takutility.gui.textpopup;

import javax.swing.event.UndoableEditListener;
import javax.swing.text.JTextComponent;
import javax.swing.undo.UndoManager;

public class RedoItem extends AbstractUndoItem {
	private static final long serialVersionUID = -463906423378698818L;

	public RedoItem(JTextComponent component) {
		super(PopupAction.REDO, component);
	}

	public RedoItem(String text, JTextComponent component, UndoManager undoManager, UndoableEditListener listener) {
		super(text, component, undoManager, listener);
	}

	public RedoItem(String text, JTextComponent component, UndoManager undoManager) {
		super(text, component, undoManager);
	}

	@Override
	protected void act(UndoManager undoManager) {
		undoManager.redo();
	}
	
	@Override
	protected boolean canAct(UndoManager undoManager) {
		return undoManager.canRedo();
	}

	@Override
	public PopupAction getPopupAction() {
		return PopupAction.REDO;
	}

}
