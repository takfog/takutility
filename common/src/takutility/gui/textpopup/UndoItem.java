package takutility.gui.textpopup;

import javax.swing.event.UndoableEditListener;
import javax.swing.text.JTextComponent;
import javax.swing.undo.UndoManager;

public class UndoItem extends AbstractUndoItem {
	private static final long serialVersionUID = 7813496360008795763L;

	public UndoItem(JTextComponent component) {
		super(PopupAction.UNDO, component);
	}

	public UndoItem(String text, JTextComponent component, UndoManager undoManager, UndoableEditListener listener) {
		super(text, component, undoManager, listener);
	}

	public UndoItem(String text, JTextComponent component, UndoManager undoManager) {
		super(text, component, undoManager);
	}

	@Override
	protected void act(UndoManager undoManager) {
		undoManager.undo();
	}
	
	@Override
	protected boolean canAct(UndoManager undoManager) {
		return undoManager.canUndo();
	}

	@Override
	public PopupAction getPopupAction() {
		return PopupAction.UNDO;
	}

}
