package takutility.gui.textpopup;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;

import javax.swing.text.DefaultEditorKit.PasteAction;
import javax.swing.text.JTextComponent;

class PasteItem extends TextPopupItem {
	private static final long serialVersionUID = 6559367027466119855L;
	private static PasteAction action = new PasteAction();

	public PasteItem(String text, JTextComponent component) {
		super(text, component);
	}

	public PasteItem(JTextComponent component) {
		this(PopupAction.PASTE.toString(), component);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		e.setSource(component);
		action.actionPerformed(e);
	}

	@Override
	public void checkValidity() {
		boolean enabled = true;
		enabled &= component.isEditable();
		if (enabled) {
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			enabled &= clipboard.isDataFlavorAvailable(DataFlavor.stringFlavor);
		}
		setEnabled(enabled);
	}

	@Override
	public PopupAction getPopupAction() {
		return PopupAction.PASTE;
	}
	
}
