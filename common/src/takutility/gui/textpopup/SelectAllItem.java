package takutility.gui.textpopup;

import java.awt.event.ActionEvent;

import javax.swing.text.JTextComponent;

class SelectAllItem extends TextPopupItem {
	private static final long serialVersionUID = 2928215093740129680L;

	public SelectAllItem(String text, JTextComponent component) {
		super(text, component);
	}
	
	public SelectAllItem(JTextComponent component) {
		this(PopupAction.SELECT_ALL.toString(), component);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		component.getCaret().setDot(0);
		component.getCaret().moveDot(component.getText().length());
	}

	@Override
	public void checkValidity() {
	}

	@Override
	public PopupAction getPopupAction() {
		return PopupAction.SELECT_ALL;
	}

}
