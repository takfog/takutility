package takutility.gui.textpopup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Properties;

import javax.swing.event.UndoableEditListener;
import javax.swing.text.JTextComponent;
import javax.swing.undo.UndoManager;

public class TextPopupFactory {
	private EnumSet<PopupAction> actions;
	private EnumMap<PopupAction, String> names = new EnumMap<PopupAction, String>(PopupAction.class);
	private UndoManager undoManager = null;
	private UndoableEditListener undoListener = null;
	
	protected TextPopupFactory() {}
	
	public static TextPopupFactory all() {
		return new TextPopupFactory().selectAll();
	}
	
	public static TextPopupFactory empty() {
		return new TextPopupFactory().removeAll();
	}

	public TextPopup build(JTextComponent comp) {
		EnumMap<PopupAction, TextPopupItem> items = new EnumMap<PopupAction, TextPopupItem>(PopupAction.class);
		UndoManager undoMan = undoManager;
		for (PopupAction act : actions) {
			String name;
			if (names.containsKey(act))
				name = names.get(act);
			else
				name = act.toString();
			TextPopupItem item = null;
			switch (act) {
			case COPY:
				item = new CopyItem(name, comp);
				break;
			case CUT:
				item = new CutItem(name, comp);
				break;
			case PASTE:
				item = new PasteItem(name, comp);
				break;
			case SELECT_ALL:
				item = new SelectAllItem(name, comp);
				break;
			case UNDO_REDO:
				if (undoMan == null) undoMan = new UndoManager();
				item = new UndoRedoItem(name, comp, undoMan, undoListener);
				break;
			case UNDO:
				if (undoMan == null) undoMan = new UndoManager();
				item = new UndoItem(name, comp, undoMan, undoListener);
				break;
			case REDO:
				if (undoMan == null) undoMan = new UndoManager();
				item = new RedoItem(name, comp, undoMan, undoListener);
				break;
			}
			items.put(act, item);
		}
		return new TextPopup(comp, items);
	}
	
	public TextPopupFactory attachTo(JTextComponent comp) {
		build(comp);
		return this;
	}
	
	public TextPopupFactory selectAll() {
		actions = EnumSet.allOf(PopupAction.class);
		actions.remove(PopupAction.UNDO_REDO);
		return this;
	}
	
	public TextPopupFactory removeAll() {
		actions = EnumSet.noneOf(PopupAction.class);
		return this;
	}
	
	public TextPopupFactory readOnly() {
		Iterator<PopupAction> it = actions.iterator();
		while (it.hasNext()) {
			if (!((PopupAction) it.next()).readOnly())
				it.remove();
		}
		return this;
	}
	
	public TextPopupFactory ccp() {
		actions.add(PopupAction.CUT);
		actions.add(PopupAction.COPY);
		actions.add(PopupAction.PASTE);
		return this;
	}
	
	public TextPopupFactory singleUndo() {
		actions.add(PopupAction.UNDO_REDO);
		actions.remove(PopupAction.UNDO);
		actions.remove(PopupAction.REDO);
		return this;
	}
	
	public TextPopupFactory undoAndRedo() {
		actions.remove(PopupAction.UNDO_REDO);
		actions.add(PopupAction.UNDO);
		actions.add(PopupAction.REDO);
		return this;
	}
	
	public TextPopupFactory undoOnly() {
		actions.remove(PopupAction.UNDO_REDO);
		actions.add(PopupAction.UNDO);
		actions.remove(PopupAction.REDO);
		return this;
	}
	
	public TextPopupFactory setText(PopupAction action, String text) {
		names.put(action, text);
		return this;
	}
	
	public TextPopupFactory add(PopupAction action) {
		actions.add(action);
		return this;
	}
	
	public TextPopupFactory add(PopupAction action, String text) {
		add(action);
		setText(action, text);
		return this;
	}
	
	public TextPopupFactory remove(PopupAction action) {
		actions.remove(action);
		return this;
	}

	public TextPopupFactory setLanguage(File file) throws FileNotFoundException, IOException {
		Properties prop = new Properties();
		FileReader reader = new FileReader(file);
		try {
			prop.load(reader);
		} finally {
			reader.close();
		}
		return setLanguage(prop);
	}
	
	public TextPopupFactory setLanguage(Properties prop) {
		for (PopupAction act : PopupAction.values()) {
			if (prop.containsKey(act.name()))
				names.put(act, prop.getProperty(act.name()));
		}
		return this;
	}

	
}
