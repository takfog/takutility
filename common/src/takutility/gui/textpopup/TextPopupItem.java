package takutility.gui.textpopup;

import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.text.JTextComponent;

abstract class TextPopupItem extends JMenuItem implements ActionListener {
	private static final long serialVersionUID = -1996554177029835822L;
	protected JTextComponent component;
	
	public TextPopupItem(String text, JTextComponent component) {
		super(text);
		this.component = component;
		addActionListener(this);
	}
	
	public abstract void checkValidity();

	public abstract PopupAction getPopupAction();
}
