package takutility.gui.textpopup;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;

import javax.swing.JPopupMenu;
import javax.swing.text.JTextComponent;

public class TextPopup {
	
	private JPopupMenu popup;
	private JTextComponent component;
	private Collection<TextPopupItem> items = new ArrayList<TextPopupItem>();
	
	private static EnumMap<PopupAction, TextPopupItem> getDefaultItems(JTextComponent component) {
		EnumMap<PopupAction, TextPopupItem> defaultItems = new EnumMap<PopupAction, TextPopupItem>(PopupAction.class);
		for (TextPopupItem item : new TextPopupItem[] {
				new CopyItem(component),
				new CutItem(component),
				new PasteItem(component),
				new SelectAllItem(component),
				new UndoRedoItem(component) 
			})
		{
			defaultItems.put(item.getPopupAction(), item);
		}
		return defaultItems;
	}

	public TextPopup(JTextComponent component) {
		this(component, getDefaultItems(component));
	}

	TextPopup(JTextComponent component, EnumMap<PopupAction, TextPopupItem> itemMap) {
		this.component = component;
		popup = new JPopupMenu();
		boolean itemBefore = false;
		
		itemBefore |= addItem(PopupAction.UNDO_REDO, itemMap, itemBefore); 
		itemBefore |= addItem(PopupAction.UNDO, itemMap, false); 
		itemBefore |= addItem(PopupAction.REDO, itemMap, false); 
		
		itemBefore |= addItem(PopupAction.CUT, itemMap, itemBefore); 
		itemBefore |= addItem(PopupAction.COPY, itemMap, false); 
		itemBefore |= addItem(PopupAction.PASTE, itemMap, false); 
		
		itemBefore |= addItem(PopupAction.SELECT_ALL, itemMap, itemBefore);

	    MouseListener popupListener = new PopupListener();
	    component.addMouseListener(popupListener);
	}

	private boolean addItem(PopupAction action, EnumMap<PopupAction, TextPopupItem> itemMap,
			boolean addSep) 
	{
		if (!itemMap.containsKey(action))
			return false;
		if (addSep) popup.addSeparator();
		TextPopupItem item = itemMap.get(action);
		items.add(item);
		popup.add(item);
		return true;
	}
	
	public void show(int x, int y) {
		for (TextPopupItem item : items)
			item.checkValidity();
		popup.show(component, x, y);
	}
	
	private class PopupListener extends MouseAdapter {
	    @Override
		public void mousePressed(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    @Override
		public void mouseReleased(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    private void maybeShowPopup(MouseEvent e) {
	        if (e.isPopupTrigger() && component.isEnabled()) {
	        	component.requestFocus();
	            show(e.getX(), e.getY());
	        }
	    }
	}
}
