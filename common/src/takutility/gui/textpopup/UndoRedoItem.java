package takutility.gui.textpopup;

import javax.swing.event.UndoableEditListener;
import javax.swing.text.JTextComponent;
import javax.swing.undo.UndoManager;

public class UndoRedoItem extends AbstractUndoItem {
	private static final long serialVersionUID = -6483904159749707883L;

	public UndoRedoItem(JTextComponent component) {
		super(PopupAction.UNDO_REDO, component);
	}

	public UndoRedoItem(String text, JTextComponent component, UndoManager undoManager, UndoableEditListener listener) {
		super(text, component, undoManager, listener);
	}

	public UndoRedoItem(String text, JTextComponent component, UndoManager undoManager) {
		super(text, component, undoManager);
	}

	@Override
	protected void act(UndoManager undoManager) {
		undoManager.undoOrRedo();
	}
	
	@Override
	protected boolean canAct(UndoManager undoManager) {
		return undoManager.canUndoOrRedo();
	}

	@Override
	public PopupAction getPopupAction() {
		return PopupAction.UNDO_REDO;
	}

}
