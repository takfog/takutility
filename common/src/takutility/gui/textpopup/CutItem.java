package takutility.gui.textpopup;

import java.awt.event.ActionEvent;

import javax.swing.text.DefaultEditorKit.CutAction;
import javax.swing.text.JTextComponent;

class CutItem extends TextPopupItem {
	private static final long serialVersionUID = 6559367027466119855L;
	private static CutAction action = new CutAction();

	public CutItem(String text, JTextComponent component) {
		super(text, component);
	}

	public CutItem(JTextComponent component) {
		this(PopupAction.CUT.toString(), component);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		e.setSource(component);
		action.actionPerformed(e);
	}

	@Override
	public void checkValidity() {
		setEnabled(component.getSelectedText() != null && component.isEditable());
	}

	@Override
	public PopupAction getPopupAction() {
		return PopupAction.CUT;
	}
	
}
