package takutility.gui.textpopup;

import java.awt.event.ActionEvent;

import javax.swing.text.DefaultEditorKit.CopyAction;
import javax.swing.text.JTextComponent;

class CopyItem extends TextPopupItem {
	private static final long serialVersionUID = 6559367027466119855L;
	private static CopyAction action = new CopyAction();

	public CopyItem(JTextComponent component) {
		this(PopupAction.COPY.toString(), component);
	}

	public CopyItem(String text, JTextComponent component) {
		super(text, component);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		e.setSource(component);
		action.actionPerformed(e);
	}

	@Override
	public void checkValidity() {
		setEnabled(component.getSelectedText() != null);
	}

	@Override
	public PopupAction getPopupAction() {
		return PopupAction.COPY;
	}

}
