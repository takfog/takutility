package takutility;

public class StringWrap<T> {
	private T obj;
	private String str;
	
	public StringWrap(T obj, String str) {
		this.obj = obj;
		this.str = str;
	}
	
	public StringWrap(T obj) {
		this(obj, obj.toString());
	}
	
	public T get() {
		return obj;
	}
	
	@Override
	public String toString() {
		return str;
	}
}
