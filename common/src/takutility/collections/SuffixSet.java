package takutility.collections;

import java.util.Collection;

public class SuffixSet extends ExtremeSet {

	public SuffixSet(Collection<String> words) {
		super(words);
	}

	@Override
	protected String getSub(String str, int remove, int len) {
		if (str.length() < remove+len)
			return "";
		return str.substring(str.length()-remove-len, str.length()-remove);
	}

	@Override
	public boolean verify(String word, String suffix) {
		return word.endsWith(suffix);
	}
}
