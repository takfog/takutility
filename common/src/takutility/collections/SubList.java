package takutility.collections;

import java.util.AbstractList;
import java.util.List;

public class SubList<E> extends AbstractList<E> implements Cloneable {
	private List<E> base;
	private int start;
	private int end;

	public SubList(List<E> base) {
		this(base, 0, base.size());
	}
	
	public SubList(List<E> base, int start) {
		this(base, start, base.size());
	}
	
	public SubList(List<E> base, int start, int end) {
		this.base = base;
		this.start = start;
		this.end = end;
	}
	
	public void setBounds(int start, int end) {
		if (start < 0) start = 0;
		if (end > base.size()) end = base.size();
		if (start > end) 
			throw new IllegalArgumentException("Start must be less than end");
		this.start = start;
		this.end = end;
	}
	
	public void resetStart() {
		start = 0;
	}
	
	public void setStart(int start) {
		setBounds(start, end);
	}
	
	public void reduceHead() {
		reduceHead(1);
	}
	
	public void reduceHead(int size) {
		setBounds(start+size, end);
	}
	
	public void resetEnd() {
		end = base.size();
		if (start > end)
			start = end;
	}
	
	public void reduceTail() {
		reduceTail(1);
	}
	
	public void reduceTail(int size) {
		setBounds(start, end-size);
	}
	
	public void setEnd(int end) {
		setBounds(start, end);
	}
	
	public void setRemovedTailSize(int size) {
		setBounds(start, base.size()-size);
	}
	
	@Override
	public E get(int index) {
		if (index >= size())
			throw new IndexOutOfBoundsException();
		return base.get(index+start);
	}

	@Override
	public int size() {
		//if base is shorter reset end
		if (end > base.size())
			resetEnd();
		return end-start;
	}

	@Override
	public SubList<E> clone() throws CloneNotSupportedException {
		return new SubList<E>(base, start, end);
	}
}
