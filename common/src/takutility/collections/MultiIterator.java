package takutility.collections;

import java.util.Iterator;

public class MultiIterator<E> implements Iterator<E> {
	private Iterator<? extends Iterable<? extends E>> mainIt;
	private Iterator<? extends E> subIt = null;
	
	public MultiIterator(Iterable<? extends Iterable<? extends E>> iters) {
		mainIt = iters.iterator();
		selectSub();
	}

	private void selectSub() {
		while (mainIt.hasNext() && (subIt == null || !subIt.hasNext()))
			subIt = mainIt.next().iterator();
	}
	
	@Override
	public boolean hasNext() {
		return subIt != null && subIt.hasNext();
	}

	@Override
	public E next() {
		E e = subIt.next();
		selectSub();
		return e;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
