package takutility.collections;

import java.util.AbstractCollection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Queue;

public class Buffer<E> extends AbstractCollection<E> implements Queue<E> {
	private E[] buffer;
	private int start = 0, next = 0;
	
	@SuppressWarnings("unchecked")
	public Buffer(int size) {
		buffer = (E[])new Object[size+1];
	}
	
	public E get(int index) {
		if (index < 0 || index >= size())
			throw new IndexOutOfBoundsException();
		return buffer[(start+index) % buffer.length];
	}
	
	public E getLast() {
		return getLast(0);
	}
	
	public E getLast(int index) {
		if (index < 0 || index >= size())
			throw new IndexOutOfBoundsException();
		int i = next - index - 1;
		if (i < 0) i += buffer.length;
		return buffer[i];
	}
	
	public void resize(int newSize) {
		int oldSize = size();
		if (newSize == buffer.length) return;
		@SuppressWarnings("unchecked")
		E[] tmp = (E[]) new Object[newSize];
		if (start != next) {
			int tmpStart;
			if (newSize >= oldSize) 
				tmpStart = start;
			else {
				tmpStart = (start + (oldSize-newSize)) % buffer.length;
				oldSize = newSize;
			}
			
			if (tmpStart < next) { 
				System.arraycopy(buffer, tmpStart, tmp, 0, oldSize);
			} else {
				System.arraycopy(buffer, tmpStart, tmp, 0, buffer.length-start);
				System.arraycopy(buffer, 0, tmp, buffer.length-start, next);
			}
		}
		start = 0;
		buffer = tmp;
		next = Math.min(oldSize, newSize);
	}
	
	public int capacity() {
		return buffer.length - 1;
	}

	@Override
	public int size() {
		return start <= next ? next - start : buffer.length - start + next;
	}
	
	@Override
	public boolean isEmpty() {
		return start == next;
	}
	
	@Override
	public boolean add(E e) {
		buffer[next] = e;
		next = move(next);
		if (start == next)
			start = move(start);
		return true;
	}

	@Override
	public boolean offer(E e) {
		return add(e);
	}

	@Override
	public E remove() {
		if (start == next)
			throw new NoSuchElementException();
        return innerPoll();
	}

	@Override
	public E poll() {
		if (start == next)
			return null;
        return innerPoll();
	}
	
	private E innerPoll() {
		next--;
		if (next < 0)
			next = buffer.length-1;
		return buffer[next];
	}
	
	@Override
	public void clear() {
		start = next = 0;
	}

	@Override
	public E element() {
		if (start == next)
			throw new NoSuchElementException();
		return innerPeek();
	}

	@Override
	public E peek() {
		if (start == next)
			return null;
		return innerPeek();
	}
	
	private E innerPeek() {
		if (next == 0)
			return buffer[buffer.length-1];
		else
			return buffer[next-1];
	}

	@Override
	public Iterator<E> iterator() {
		return new BufferIterator();
	}
	
	private int move(int ptr) {
		return (ptr + 1) % buffer.length;
	}

	private class BufferIterator implements Iterator<E> {
		private int pos;
		
		public BufferIterator() {
			pos = start;
		}
		
		@Override
		public boolean hasNext() {
			return pos != next;
		}

		@Override
		public E next() {
			E e = buffer[pos];
			pos = move(pos);
			return e;
		}
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException("remove");
		}
	}
}
