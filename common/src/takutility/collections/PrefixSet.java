package takutility.collections;

import java.util.Collection;

public class PrefixSet extends ExtremeSet {
	
	public PrefixSet(Collection<String> words) {
		super(words);
	}
	
	@Override
	protected String getSub(String str, int remove, int len) {
		if (str.length() < remove+len)
			return "";
		return str.substring(remove, remove+len);
	}

	@Override
	public boolean verify(String word, String prefix) {
		return word.startsWith(prefix);
	}
}
