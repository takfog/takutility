package takutility.collections.matrix;

public class SymSparseMatrix<E> extends SparseMatrix<E> {

	@Override
	public E get(int a, int b) {
		if (a <= b)
			return super.get(a, b);
		else
			return super.get(b, a);
	}
	
	@Override
	public E set(int a, int b, E val) {
		if (a <= b)
			return super.set(a, b, val);
		else
			return super.set(b, a, val);
	}
}
