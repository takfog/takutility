package takutility.collections.matrix;

import java.util.Set;

public interface Matrix<E> {

	E get(int a, int b);
	E set(int a, int b, E val);
	int size();
	Iterable<E> values();
	Set<Entry<E>> entrySet();

	interface Entry<E> {
		int getA();
		int getB();
		E getValue();
		E setValue(E val);
	}
}