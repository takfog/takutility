package takutility.collections.matrix;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SparseMatrix<E> implements Matrix<E> {
	private Map<Integer, Map<Integer, E>> matrix;
	private int size = 0;
	private Collection<E> values = null;
	private Set<Entry<E>> entries = null;
	
	public SparseMatrix() {
		matrix = new HashMap<Integer, Map<Integer,E>>();
	}
	
	@Override
	public E get(int a, int b) {
		if (a > size || a < 0)
			throw new IndexOutOfBoundsException(outOfBoundsMsg("a", a));
		if (b > size || b < 0)
			throw new IndexOutOfBoundsException(outOfBoundsMsg("b", b));
		
		Map<Integer, E> aval = matrix.get(a);
		if (aval == null)
			return null;
		return aval.get(b);
	}
	
	private String outOfBoundsMsg(String name, int val) {
        return "Index "+name+": "+val+", Size: "+size;
    }

	@Override
	public E set(int a, int b, E val) {
		if (a < 0)
			throw new IndexOutOfBoundsException(outOfBoundsMsg("a", a));
		if (b < 0)
			throw new IndexOutOfBoundsException(outOfBoundsMsg("b", b));
		
		values = null;
		entries = null;
		size = Math.max(size, Math.max(a, b)+1);
		Integer akey = new Integer(a);
		Map<Integer, E> aval = matrix.get(akey);
		if (aval == null) {
			aval = new HashMap<Integer, E>();
			matrix.put(akey, aval);
		}
		return aval.put(b, val);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Iterable<E> values() {
		if (values == null) {
			values = new ArrayList<E>();
			for (Map<Integer, E> av : matrix.values()) {
				values.addAll(av.values());
			}
		}
		return values;
	}

	@Override
	public Set<Entry<E>> entrySet() {
		if (entries == null) {
			entries = new HashSet<Entry<E>>();
			for (Map.Entry<Integer, Map<Integer, E>> ae : matrix.entrySet()) {
				int a = ae.getKey();
				for (Map.Entry<Integer, E> be : ae.getValue().entrySet()) {
					entries.add(new MapEntry(a, be));
				}
			}
		}
		return entries;
	}

	private class MapEntry implements Entry<E> {
		private int a;
		private Map.Entry<Integer, E> e;
		
		public MapEntry(int a, Map.Entry<Integer, E> e) {
			this.a = a;
			this.e = e;
		}
		
		@Override
		public int getA() {
			return a;
		}

		@Override
		public int getB() {
			return e.getKey().intValue();
		}

		@Override
		public E getValue() {
			return e.getValue();
		}

		@Override
		public E setValue(E val) {
			return e.setValue(val);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + a;
			result = prime * result + ((e == null) ? 0 : e.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			@SuppressWarnings("unchecked")
			MapEntry other = (MapEntry) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (a != other.a)
				return false;
			if (e == null) {
				if (other.e != null)
					return false;
			} else if (!e.equals(other.e))
				return false;
			return true;
		}

		private SparseMatrix<?> getOuterType() {
			return SparseMatrix.this;
		}
		
		
	}
}
