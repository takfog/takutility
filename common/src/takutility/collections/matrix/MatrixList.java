package takutility.collections.matrix;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MatrixList<E> implements Matrix<E> {
	private transient Object[] matrix;
	private int size;
	protected Set<Entry<E>> entries = null;
	
	public MatrixList() {
		this(10);
	}
	
	public MatrixList(int size) {
		this.size = size;
		matrix = new Object[arraySize(size)];
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public E get(int a, int b) {
		if (a > size || a < 0)
			throw new IndexOutOfBoundsException(outOfBoundsMsg("a", a));
		if (b > size || b < 0)
			throw new IndexOutOfBoundsException(outOfBoundsMsg("b", b));
		return (E)matrix[coord2index(a, b)];
	}
	
	@Override
	public E set(int a, int b, E val) {
		if (a < 0)
			throw new IndexOutOfBoundsException(outOfBoundsMsg("a", a));
		if (b < 0)
			throw new IndexOutOfBoundsException(outOfBoundsMsg("b", b));
		
		int idx = coord2index(a, b);
		if (idx >= matrix.length)
			grow(Math.max(a, b)+1);
		
		return set(idx, val);
	}

	private E set(int idx, E val) {
		@SuppressWarnings("unchecked")
		E old = (E) matrix[idx]; 
		matrix[idx] = val;
		return old;
	}
	
	@Override
	public int size() {
		return size;
	}
	
	private String outOfBoundsMsg(String name, int val) {
        return "Index "+name+": "+val+", Size: "+size;
    }
	
	protected int arraySize(int matrixSize) {
		return matrixSize*matrixSize;
	}
	
	protected int coord2index(int a, int b) {
		int max = Math.max(a, b);
		return max*(max+1)+a-b; 
	}
	
	private void grow(int newSize) {
		matrix = Arrays.copyOf(matrix, arraySize(newSize));
		size = newSize;
		entries = null;
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(matrix);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatrixList<?> other = (MatrixList<?>) obj;
		if (!Arrays.equals(matrix, other.matrix))
			return false;
		return true;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Iterable<E> values() {
		return (Iterable<E>) Arrays.asList(matrix);
	}

	@Override
	public Set<Entry<E>> entrySet() {
		if (entries == null)
			createEntrySet();
		return entries;
	}

	protected void createEntrySet() {
		entries = new HashSet<Entry<E>>();
		for(int a=0; a<size; a++) {
			for(int b=0; b<size; b++) {
				entries.add(new ListEntry(a, b));
			}
		}
	}
	
	protected class ListEntry implements Entry<E> {
		private int a, b, idx;
		
		public ListEntry(int a, int b) {
			this.a = a;
			this.b = b;
			idx = coord2index(a, b);
		}

		@Override
		public int getA() {
			return a;
		}

		@Override
		public int getB() {
			return b;
		}

		@Override
		@SuppressWarnings("unchecked")
		public E getValue() {
			return (E)matrix[idx];
		}

		@Override
		public E setValue(E val) {
			return set(idx, val);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + idx;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			@SuppressWarnings("unchecked")
			ListEntry other = (ListEntry) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (idx != other.idx)
				return false;
			return true;
		}

		private MatrixList<?> getOuterType() {
			return MatrixList.this;
		}
		
	}
}
