package takutility.collections.matrix;

import java.util.HashSet;


public class SymMatrixList<E> extends MatrixList<E> {

	public SymMatrixList() {
		super();
	}
	
	public SymMatrixList(int size) {
		super(size);
	}
	
	@Override
	protected int coord2index(int a, int b) {
		if (a == b) 
			return a*(a+1)/2;
		int max, min;
		if (a > b) {
			max = a;
			min = b;
		} else {
			max = b;
			min = a;
		}
		return max*(max+3)/2-min; 
	}
	
	@Override
	protected int arraySize(int matrixSize) {
		return matrixSize*(matrixSize+1)/2;
	}
	
	@Override
	protected void createEntrySet() {
		entries = new HashSet<Entry<E>>();
		int size = size();
		for(int a=0; a<size; a++) {
			for(int b=a; b<size; b++) {
				entries.add(new ListEntry(a, b));
			}
		}
	}
}
