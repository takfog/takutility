package takutility.collections.multimap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import takutility.collections.Factory;

public class ListMultiMap<K, V> extends TypedMultiMap<K, V, List<V>> {
	
	public ListMultiMap() {
		this(ArrayList.class);
	}

	@SuppressWarnings("unchecked")
	public ListMultiMap(@SuppressWarnings("rawtypes") Class<? extends List> cType) {
		super((Class<? extends List<V>>) cType);
	}

	public ListMultiMap(Factory<List<V>> factory, Map<K, List<V>> baseMap) {
		super(factory, baseMap);
	}

	public ListMultiMap(Factory<List<V>> factory) {
		super(factory);
	}

	public V getFirst(Object key) {
		List<V> list = get(key);
		if (list == null || list.isEmpty()) return null;
		return list.get(0);
	}

	public V getLast(Object key) {
		List<V> list = get(key);
		if (list == null || list.isEmpty()) return null;
		return list.get(list.size());
	}
}
