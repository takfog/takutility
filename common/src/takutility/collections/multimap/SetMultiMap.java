package takutility.collections.multimap;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import takutility.collections.Factory;

public class SetMultiMap<K, V> extends TypedMultiMap<K, V, Set<V>> {
	
	public SetMultiMap() {
		this(HashSet.class);
	}

	@SuppressWarnings("unchecked")
	public SetMultiMap(@SuppressWarnings("rawtypes") Class<? extends Set> cType) {
		super((Class<? extends Set<V>>) cType);
	}

	public SetMultiMap(Factory<Set<V>> factory, Map<K, Set<V>> baseMap) {
		super(factory, baseMap);
	}

	public SetMultiMap(Factory<Set<V>> factory) {
		super(factory);
	}

}
