package takutility.collections.multimap;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;

import takutility.collections.Factory;

public class DequeMultiMap<K, V> extends TypedMultiMap<K, V, Deque<V>> {
	
	public DequeMultiMap() {
		this(ArrayDeque.class);
	}

	@SuppressWarnings("unchecked")
	public DequeMultiMap(@SuppressWarnings("rawtypes") Class<? extends Deque> cType) {
		super((Class<? extends Deque<V>>) cType);
	}

	public DequeMultiMap(Factory<Deque<V>> factory, Map<K, Deque<V>> baseMap) {
		super(factory, baseMap);
	}

	public DequeMultiMap(Factory<Deque<V>> factory) {
		super(factory);
	}

}
