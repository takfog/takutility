package takutility.collections.multimap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import takutility.collections.Factory;

public class MultiMap<K, V> extends TypedMultiMap<K, V, Collection<V>> {

	public MultiMap() {
		this(ArrayList.class);
	}

	@SuppressWarnings("unchecked")
	public MultiMap(@SuppressWarnings("rawtypes") final Class<? extends Collection> cType) {
		super((Class<Collection<V>>) cType);
	}
	
	public MultiMap(Factory<Collection<V>> factory) {
		super(factory);
	}
	
	public MultiMap(Factory<Collection<V>> factory, Map<K, Collection<V>> baseMap) {
		super(factory, baseMap);
	}

}
