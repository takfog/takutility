package takutility.collections.multimap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import takutility.collections.AutoMap;
import takutility.collections.Factory;

public class TypedMultiMap<K, V, C extends Collection<V>> extends AutoMap<K, C> {

	public TypedMultiMap(final Class<? extends C> cType) {
		super(new Factory<C>() {
			@Override
			public C create() {
				try {
					return cType.newInstance();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
	}
	
	public TypedMultiMap(Factory<C> factory) {
		super(factory);
	}
	
	public TypedMultiMap(Factory<C> factory, Map<K, C> baseMap) {
		super(factory, baseMap);
	}

	public boolean add(K key, V element) {
		return get(key).add(element);
	}

	public boolean addAll(K key, Collection<? extends V> element) {
		return get(key).addAll(element);
	}

	public boolean removeElement(K key, V element) {
		if (containsKey(key))
			return get(key).add(element);
		else
			return false;
	}

	public void clear(K key) {
		if (containsKey(key))
			get(key).clear();
	}
	
	public void removeEmpty() {
		ArrayList<java.util.Map.Entry<K, C>> entries 
				= new ArrayList<Entry<K, C>>(entrySet());
		for (Entry<K, C> e : entries) {
			if (e.getValue().isEmpty())
				remove(e.getKey());
		}
	}
}
