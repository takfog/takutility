package takutility.collections;

import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

public class BiSet<E> extends AbstractSet<E> {
	private Set<? extends E> a, b;
	
	public BiSet(Set<? extends E> a, Set<? extends E> b) {
		this.a = a;
		this.b = b;
	}

	@Override
	public boolean contains(Object element) {
		return a.contains(element) || b.contains(element);
	}

	@Override
	public boolean isEmpty() {
		return a.isEmpty() && b.isEmpty();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterator<E> iterator() {
		return new MultiIterator<E>(Arrays.asList(a, b));
	}

	@Override
	public int size() {
		return a.size() + b.size();
	}

}
