package takutility.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

public class Maps {
	
	public static <K, V> Map<V, Collection<K>> reverseMulti(Map<K, Collection<V>> orig) {
		Map<V, Collection<K>> rev = new AutoMap<V, Collection<K>>(new Factory<Collection<K>>() {
			@Override
			public Collection<K> create() {
				return new ArrayList<K>();
			}
		});
		
		for (Entry<K, Collection<V>> listEntry : orig.entrySet()) {
			for (V user : listEntry.getValue()) {
				rev.get(user).add(listEntry.getKey());
			}
		}
		return rev;
	}
}
