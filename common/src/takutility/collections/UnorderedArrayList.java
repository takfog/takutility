package takutility.collections;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;

public class UnorderedArrayList<E> extends AbstractList<E> {
	private ArrayList<E> list;
	
	public UnorderedArrayList() {
		list = new ArrayList<E>();
	}
	
	public UnorderedArrayList(Collection<? extends E> c) {
		list = new ArrayList<E>(c);
	}
	
	public UnorderedArrayList(int initialCapacity) {
		list = new ArrayList<E>(initialCapacity);
	}
	
	@Override
	public E set(int index, E element) {
		return list.set(index, element);
	}
	
	@Override
	public E get(int index) {
		return list.get(index);
	}

	@Override
	public int size() {
		return list.size();
	}

	@Override
	public boolean add(E e) {
		return list.add(e);
	}
	
	/**
	 * Inserts the specified element at the specified position in this list. 
	 * The element previously at the specified position is moved to the end
	 * of the list.
	 */
	@Override
	public void add(int index, E element) {
		if (index >= list.size())
			list.add(element);
		list.add(list.get(index));
		list.set(index, element);
	}
	
	/**
	 * Removes the element at the specified position in this list, 
	 * then moves the last element of the list to the specified
	 * position
	 */
	@Override
	public E remove(int index) {
		int end = list.size()-1;
		if (index == end) 
			return list.remove(index);
		E v = list.get(index);
		list.set(index, list.get(end));
		list.remove(end);
		return v;
	}
	
	public void swap(int a, int b) {
		if (a == b) return;
		E v = list.get(a);
		list.set(a, list.get(a));
		list.set(b, v);
	}
	
	/**
	 * Removes all of the elements from this list. 
	 * The list will be empty after this call returns.
	 */
	@Override
	public void clear() {
		list.clear();
	}
}
