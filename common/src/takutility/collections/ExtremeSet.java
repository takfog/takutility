package takutility.collections;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public abstract class ExtremeSet extends AbstractSet<String> {
	private PrefixElem set;
	private int size;

	public ExtremeSet(Collection<String> words) {
		setData(words);
	}

	public void setData(Collection<String> words) {
		size = words.size();
		switch (words.size()) {
		case 0:
			set = new SingleElem(null);
			break;
		case 1:
			set = new SingleCheckedElem(words);
			break;
		default:
			set = new SubPrefix(words, 0);
			break;
		}
	}
	
	protected abstract String getSub(String str, int remove, int len);
	
	public abstract boolean verify(String word, String extreme);

	public String find(String word) {
		return set.get(word, 0);
	}

	public boolean contains(String word) {
		return find(word) != null;
	}

	@Override
	public boolean contains(Object word) {
		return contains(word.toString());
	}

	@Override
	public Iterator<String> iterator() {
		return set.iterator();
	}

	@Override
	public int size() {
		return size;
	}


	private static interface PrefixElem extends Iterable<String> {
		String get(String word, int remove);
	}
	
	private static class SingleElem implements PrefixElem {
		protected String elem;

		public SingleElem(String elem) {
			this.elem = elem;
		}

		@Override
		public String get(String word, int remove) {
			return elem;
		}

		@Override
		public String toString() {
			return elem;
		}

		@Override
		public Iterator<String> iterator() {
			if (elem == null)
				return Collections.<String>emptyList().iterator();
			return Collections.singleton(elem).iterator();
		}
	}

	private class SingleCheckedElem extends SingleElem {
		public SingleCheckedElem(Iterable<String> elemCont) {
			this(elemCont.iterator().next());
		}

		public SingleCheckedElem(String elem) {
			super(elem);
		}

		@Override
		public String get(String word, int remove) {
			return verify(word, elem) ? elem : null;
		}

	}

	private class SubPrefix implements PrefixElem {
		private Map<String, PrefixElem> map = new HashMap<String, PrefixElem>();
		private int keyLen;

		public SubPrefix(Collection<String> coll, int remove) {
			int maxLen = Integer.MAX_VALUE;
			for (String w : coll) {
				if (w.length() < maxLen)
					maxLen = w.length();
			}
			keyLen = maxLen - remove;

			AutoMap<String, ArrayList<String>> group = new AutoMap<String, ArrayList<String>>(
					new Factory<ArrayList<String>>() {
						@Override
						public ArrayList<String> create() {
							return new ArrayList<String>();
						}
					});

			for (String w : coll) {
				if (w.length() == maxLen)
					map.put(getSub(w, remove, w.length() - remove), new SingleElem(w));
				else {
					String key = getSub(w, remove, keyLen);
					group.get(key).add(w);
				}
			}
			for (Entry<String, ArrayList<String>> e : group.entrySet()) {
				if (map.containsKey(e.getKey()))
					continue;
				if (e.getValue().size() == 1)
					map.put(e.getKey(), new SingleCheckedElem(e.getValue()));
				else
					map.put(e.getKey(), new SubPrefix(e.getValue(), maxLen));
			}
		}

		@Override
		public String get(String word, int remove) {
			String key = getSub(word, remove, keyLen);
			if (!map.containsKey(key))
				return null;
			return map.get(key).get(word, remove + keyLen);
		}

		@Override
		public Iterator<String> iterator() {
			return new MultiIterator<String>(map.values());
		}
	}

}