package takutility.collections;

public interface Factory<V> {
	V create();
}
