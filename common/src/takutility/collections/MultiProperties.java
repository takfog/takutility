package takutility.collections;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import takutility.collections.multimap.ListMultiMap;
import takutility.io.UTF8Reader;

public class MultiProperties implements Map<String, String> {
	private Map<String, String> single = new HashMap<String, String>();
	private ListMultiMap<String, String> multi = new ListMultiMap<String, String>();
	private MultiProperties multiDef = null;
	private Properties defaults = null;
	
	private int multiSize = 0;
	
	public MultiProperties() {}
	
	public MultiProperties(Properties defaults) {
		this.defaults = defaults;
	}
	
	public MultiProperties(MultiProperties defaults) {
		this.multiDef = defaults;
	}
	
	public MultiProperties(File file) throws IOException {
		load(file);
	}
	
	public MultiProperties(Reader reader) throws IOException {
		load(reader);
	}
	
	public void load(File file) throws IOException {
		UTF8Reader reader = new UTF8Reader(file);
		load(reader);
		reader.close();
	}

    public void load(InputStream inStream) throws IOException {
        new NativePropLoad().load(inStream);
    }

    public void load(Reader reader) throws IOException {
        new NativePropLoad().load(reader);
    }
	
	public void store(OutputStream out, String comments) throws IOException {
		new NativePropStore().store(out, comments);
	}
	
	public void store(Writer writer, String comments) throws IOException {
		new NativePropStore().store(writer, comments);
	}

	@Override
	public void clear() {
		single.clear();
		multi.clear();
		multiSize = 0;
	}

	@Override
	public boolean containsKey(Object key) {
		return single.containsKey(key) || multi.containsKey(key);
	}

	public boolean containsSingleKey(Object key) {
		return single.containsKey(key);
	}

	public boolean containsMultiKey(Object key) {
		return multi.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		if (value instanceof String)
			return single.containsValue(value);
		else
			return multi.containsValue(value);	
	}

	@Override
	public Set<Entry<String, String>> entrySet() {
		Set<Entry<String, String>> set = new HashSet<Entry<String,String>>();
		set.addAll(single.entrySet());
		for (Entry<String, List<String>> e : multi.entrySet()) {
			for (String v : e.getValue()) {
				set.add(new SimpleEntry(e.getKey(), v));
			}
		}
		return set;
	}
	
	public Set<Entry<String, String>> singleEntrySet() {
		return Collections.unmodifiableMap(single).entrySet();
	}

	public Set<Entry<String, List<String>>> multiEntrySet() {
		return Collections.unmodifiableMap(multi).entrySet();
	}

	@Override
	public String get(Object key) {
		return get(key, null);
	}

	public String get(Object key, String defaultValue) {
		if (single.containsKey(key))
			return single.get(key);
		else if (multi.containsKey(key))
			return multi.getLast(key);
		else
			return defaultValue;
	}

	public String getProperty(String key) {
		return get(key);
	}
	
	public String getProperty(Object key, String defaultValue) {
		return get(key, defaultValue);
	}
	
	public List<String> getMulti(String key) {
		return getMulti(key, null);
	}
	
	public List<String> getMultiNonNull(String key) {
		return getMulti(key, Collections.<String>emptyList());
	}
	
	public List<String> getMulti(String key, List<String> defaultValue) {
		if (multi.containsKey(key))
			return Collections.unmodifiableList(multi.get(key));
		if (single.containsKey(key))
			return Collections.singletonList(single.get(key));
		if (multiDef != null)
			return multiDef.getMulti(key, defaultValue);
		if (defaults != null) {
			String retVal = defaults.getProperty(key);
			if (retVal != null)
				return Collections.singletonList(retVal);
		}
		return defaultValue;
	}

	@Override
	public boolean isEmpty() {
		return single.isEmpty() && multi.isEmpty();
	}

	@Override
	public Set<String> keySet() {
		return new BiSet<String>(single.keySet(), multi.keySet());
	}
	
	@Override
	public String put(String key, String val) {
		boolean inMulti = multi.containsKey(key);
		if (inMulti || single.containsKey(key)) {
			if (!inMulti) {
				multi.add(key, single.remove(key));
				multiSize++;
			}
			multi.add(key, val);
			multiSize++;
		} else 
			single.put(key, val);
		return null;
	}
	
	public void put(String key, Collection<String> val) {
		if (single.containsKey(key)) {
			multi.add(key, single.remove(key));
			multiSize++;
		}
		multi.addAll(key, val);
		multiSize += val.size();
	}

	@Override
	public void putAll(Map<? extends String, ? extends String> map) {
		for (Entry<? extends String, ? extends String> e : map.entrySet()) {
			put(e.getKey(), e.getValue());
		}
	}

	public void putAllMulti(Map<? extends String, ? extends List<String>> map) {
		for (java.util.Map.Entry<? extends String, ? extends List<String>> e : map.entrySet()) {
			put(e.getKey(), e.getValue());
		}
	}
	public void putAll(MultiProperties other) {
		putAll(other.single);
		putAllMulti(other.multi);
	}

	@Override
	public String remove(Object key) {
		if (single.containsKey(key))
			return single.remove(key);
		else if(multi.containsKey(key)) {
			List<String> list = multi.get(key);
			if (list.isEmpty()) {
				multi.remove(key);
				return null;
			} 
			multiSize--;
			if (list.size() > 1)
				return list.remove(list.size()-1);
			else {
				multi.remove(key);
				return list.get(0);
			}
		}
		return null;
	}

	@Override
	public int size() {
		return single.size() + multiSize;
	}

	@Override
	public Collection<String> values() {
		ArrayList<String> vals = new ArrayList<String>(size());
		vals.addAll(single.values());
		for (List<String> mv : multi.values()) {
			vals.addAll(mv);
		}
		return vals;
	}
	
	private class NativePropLoad extends Properties {
		private static final long serialVersionUID = 1L;
		
		@Override
		public synchronized Object put(Object key, Object val) {
			return MultiProperties.this.put(key.toString(), val.toString());
		}
	}
	
	private class NativePropStore extends Properties {
		private static final long serialVersionUID = 1L;
		private Entry<String,String> next;
		
		@Override
		public synchronized Enumeration<Object> keys() {
			final Iterator<Entry<String, String>> it 
					= MultiProperties.this.entrySet().iterator();
			
			return new Enumeration<Object>() {
				@Override
				public Object nextElement() {
					next = it.next();
					return next.getKey();
				}
				
				@Override
				public boolean hasMoreElements() {
					return it.hasNext();
				}
			};
		}
		
		@Override
		public synchronized Object get(Object key) {
			return next.getValue();
		}
	}
	
	private static class SimpleEntry implements Entry<String, String> {
		private String k,v;

		public SimpleEntry(String k, String v) {
			this.k = k;
			this.v = v;
		}

		@Override
		public String getKey() {
			return k;
		}

		@Override
		public String getValue() {
			return v;
		}

		@Override
		public String setValue(String arg0) {
			return null;
		}
		
	}
	
	public static void main(String[] args) throws IOException {
		MultiProperties p = new MultiProperties(new File("C:\\Users\\f.debenedictis\\Downloads\\strat.properties"));
		for (Entry<String, String> e : p.entrySet()) {
			System.out.println(e.getKey()+"="+e.getValue());
		}
		System.out.println();
		p.store(System.out, "store test");
	}

}
