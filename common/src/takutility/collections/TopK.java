package takutility.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class TopK<E> extends PriorityQueue<E> {
	private static final long serialVersionUID = -6950428981166023959L;
	
	private int maxSize;
	private Comparator<E> comp = null;
	
	public TopK(int maxSize) {
		this(maxSize, null);
	}

	@SuppressWarnings("unchecked")
	public TopK(int maxSize, Comparator<E> comp) {
		super(maxSize, Collections.reverseOrder(comp));
		this.maxSize = maxSize;
		this.comp = comp != null ? comp : new Comparator<E>() {
			@Override
			public int compare(E o1, E o2) {
				return ((Comparable<E>)o1).compareTo(o2);
			}
		};
	}
	
	@Override
	public boolean add(E e) {
		if (size() == maxSize) {
			if (comp.compare(e, peek()) >= 0)
				return false;
			poll();
		}
		return super.add(e);
	}
	
	public int maxSize() {
		return maxSize;
	}
	
	public List<E> toSortedList() {
		ArrayList<E> list = new ArrayList<E>(this);
		Collections.sort(list, comp);
		return list;
	}
}