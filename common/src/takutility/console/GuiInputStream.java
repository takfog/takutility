package takutility.console;

import java.io.IOException;
import java.io.InputStream;

import javax.swing.JOptionPane;

public class GuiInputStream extends InputStream {
	private byte[] buffer = new byte[0];
	private int index = 0;
	
	@Override
	public int read() throws IOException {
		load();
		return buffer[index++];
	}
	
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		load();
		int readLen = Math.min(available(), len);
		System.arraycopy(buffer, index, b, 0, readLen);
		index += readLen;
		return readLen;
	}
	
	@Override
	public int read(byte[] b) throws IOException {
		return read(b, 0, b.length);
	}
	
	@Override
	public int available() throws IOException {
		return buffer.length - index;
	}
	
	public boolean isEmpty() {
		return index >= buffer.length;
	}

	private void load() {
		if (!isEmpty()) return;
		while(true) {
			String input = JOptionPane.showInputDialog("Standard input");
			if (input != null) {
				input += "\n";
				buffer = input.getBytes();
				index = 0;
				return;
			}
		}
	}
	
	@Override
	public String toString() {
		return new String(buffer);
	}
	
	public static void main(String[] args) throws IOException {
		GuiInputStream is = new GuiInputStream();
		byte[] b = new byte[10];
		int read;
		
		read = is.read(b);
		System.out.println(read);
		System.out.println(new String(b, 0, read));
		
		read = is.read(b, 0, 3);
		System.out.println(read);
		System.out.println(new String(b, 0, read));
		is.close();
	}
}
