package takutility.console;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JFrame;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;


public class GuiOutputStream extends ByteArrayOutputStream {
	private Document doc;
	private Collection<FlushListener> flushList = null;

	public GuiOutputStream(Document doc) {
		this.doc = doc;
	}

	public GuiOutputStream(Document doc, final JScrollPane scrollPane) {
		this(doc);
		this.addFlushListener(new FlushListener() {
			@Override
			public void flushed() {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						JScrollBar vertical = scrollPane.getVerticalScrollBar();
						vertical.setValue( vertical.getMaximum() );
					}
				});
			}
		});
	}
	
	public void addFlushListener(FlushListener listener) {
		if (flushList == null) flushList = new ArrayList<FlushListener>();
		flushList.add(listener);
	}
	
	@Override
	public synchronized void flush() throws IOException {
		String txt = toString();
		reset();
		try {
			doc.insertString(doc.getLength(), txt, null);
		} catch (BadLocationException e) {}
		if (flushList != null) {
			for (FlushListener listener : flushList) {
				listener.flushed();
			}
		}
	}
	
	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setSize(300, 300);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JTextArea jta = new JTextArea();
		jta.setEditable(false);
		jta.setBackground(Color.BLACK);
		jta.setForeground(Color.WHITE);
		f.getContentPane().add(new JScrollPane(jta));
		f.setVisible(true);
		
		PrintStream ps = new PrintStream(new GuiOutputStream(jta.getDocument()), true);
		System.setOut(ps);
		System.setErr(ps);
		
		ps.println("Testo 1");
		ps.print("Testo 2\ntesto3");
		System.out.println("system");
		System.err.println("err");
		ps.print("2 sec sleep...");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ps.println(" awake");
		ps.println();
		System.out.println("sys2");
		ps.print("fine");
		//ps.flush();
	}
}
