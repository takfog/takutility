package takutility.console;

import java.awt.GridLayout;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class ConsoleFrame extends JFrame {
	private static final long serialVersionUID = 3935546780205890825L;
	JTextArea txtOut, txtErr;

	public ConsoleFrame(boolean out, boolean err) {
		this(out, err, true);
	}

	public ConsoleFrame(boolean out, boolean err, boolean keepStd) {
		if (!(out || err))
			throw new IllegalArgumentException("No stream enabled");
		
		JPanel pane = new JPanel();
		if (out && err)
			pane.setLayout(new GridLayout(2, 1));
		else
			pane.setLayout(new GridLayout(1, 1));
		
		if (out) {
			pane.add(new ConsolePanel(true, false, true, keepStd));
		}
		if (err) {
			pane.add(new ConsolePanel(false, true, true, keepStd));
		}
		setContentPane(pane);
	}
	
	public static void main(String[] args) {
		ConsoleFrame cf = new ConsoleFrame(true, true, true);
		cf.setSize(200, 200);
		cf.setDefaultCloseOperation(EXIT_ON_CLOSE);
		cf.setVisible(true);
		
		pause();
		System.out.println("out 1");
		pause();
		System.err.println("err 1");
		pause();
		System.out.println("out 2");
		pause();
		Exception ex = new Exception("err obj");
		ex.printStackTrace();
		pause();
		Scanner s = new Scanner(System.in);
		System.out.println(s.nextLine());
		for(int i=0; i<50; i++)
			System.out.println("scroll "+i);
		pause();
		for(int i=50; i<70; i++)
			System.out.println("scroll "+i);
	}
	
	private static void pause() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
