package takutility.console;

import java.awt.Color;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import takutility.io.DoubleOutputStream;

public class ConsolePanel extends JScrollPane {
	private static final long serialVersionUID = -4542831683677253741L;
	private JTextArea txt;
	private GuiOutputStream out;
	
	public ConsolePanel() {
		this(null, null);
	}
	
	public ConsolePanel(boolean std, boolean err) {
		this(null, null, std, err, true);
	}
	
	public ConsolePanel(boolean std, boolean err, boolean in, boolean keepStdOut) {
		this(null, null, std, err, in, keepStdOut);
	}

	public ConsolePanel(JTextArea txt) {
		this(txt, null);
	}
	
	public ConsolePanel(JTextArea txt, GuiOutputStream out) {
		this(txt, out, false, false, true);
	}

	public ConsolePanel(JTextArea txt, GuiOutputStream out, boolean std, boolean err, boolean keepStdOut) {
		this(txt, out, std, err, false, keepStdOut);
	}

	public ConsolePanel(JTextArea txt, GuiOutputStream out, boolean std, boolean err, boolean in, boolean keepStdOut) {
		super(txt);
		if (txt == null) {
			this.txt = new JTextArea();
			this.txt.setEditable(false);
			this.txt.setBackground(Color.BLACK); 
			if (!std && err) 
				this.txt.setForeground(Color.RED);
			else
				this.txt.setForeground(Color.WHITE);
		} else {
			this.txt = txt;
		}
		setViewportView(this.txt);
		
		if (out == null)
			this.out = new GuiOutputStream(this.txt.getDocument(), this);
		else
			this.out = out;
		
		if (std) {
			OutputStream newStd;
			if (keepStdOut)
				newStd = new DoubleOutputStream(System.out, this.out);
			else
				newStd = this.out;
			System.setOut(new PrintStream(newStd, true));
		}
		if (err) {
			OutputStream newStd;
			if (keepStdOut)
				newStd = new DoubleOutputStream(System.err, this.out);
			else
				newStd = this.out;
			System.setErr(new PrintStream(newStd, true));
		}
		if (in) {
			System.setIn(new GuiInputStream());
		}
	}
	
	public void clear() {
		txt.setText("");
	}

	public JTextArea getTextArea() {
		return txt;
	}

	public GuiOutputStream getOutputStream() {
		return out;
	}
	
}
