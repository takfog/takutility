package takutility.composite;

public class MutablePair<A, B> extends Pair<A, B> {
	
	public MutablePair() {
		super(null, null);
	}
	
	public MutablePair(A first, B second) {
		super(first, second);
	}

	public void setFirst(A val) {
		first = val;
	}
	
	public void setSecond(B val) {
		second = val;
	}
	
	public void set(A first, B second) {
		setFirst(first);
		setSecond(second);
	}
}
