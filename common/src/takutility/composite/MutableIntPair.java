package takutility.composite;

public class MutableIntPair extends IntPair {
	
	public MutableIntPair() {
		super(0, 0);
	}
	
	public MutableIntPair(int first, int second) {
		super(first, second);
	}

	public void setFirst(int val) {
		first = val;
	}
	
	public void setSecond(int val) {
		second = val;
	}
	
	public void set(int first, int second) {
		setFirst(first);
		setSecond(second);
	}
	
	public void swap() {
		int t = first;
		first = second;
		second = t;
	}
}
