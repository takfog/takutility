package takutility.composite;

public class IntPair {
	protected int first;
	protected int second;
	
	public IntPair(int first, int second) {
		this.first = first;
		this.second = second;
	}
	
	public int getFirst() {
		return first;
	}
	
	public int getSecond() {
		return second;
	}
	
	public int a() {
		return first;
	}
	
	public int b() {
		return second;
	}
	
	public int min() {
		if (first <= second)
			return first;
		else
			return second;
	}
	
	public int max() {
		if (first >= second)
			return first;
		else
			return second;
	}
	
	public float mean() {
		return (first+second)/2f;
	}	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + first;
		result = prime * result + second;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntPair other = (IntPair) obj;
		if (first != other.first)
			return false;
		if (second != other.second)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "("+first+","+second+")";
	}

}
