package takutility.composite;

public class KeyVal<K,V> {
	protected K key;
	protected V val;
	
	public KeyVal(K key) {
		this(key, null);
	}

	public KeyVal(K key, V value) {
		this.key = key;
		this.val = value;
	}
	
	public K getKey() {
		return key;
	}
	
	public V getValue() {
		return val;
	}
	
	public void setValue(V value) {
		this.val = value;
	}
	
	public void set(V value) {
		this.val = value;
	}
	
	public K key() {
		return key;
	}
	
	public V val() {
		return val;
	}

	@Override
	public int hashCode() {
		return key == null ? 0 : key.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KeyVal<?,?> other = (KeyVal<?,?>) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return key+"="+val;
	}
}
