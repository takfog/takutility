package takutility;

public interface ProgressNotifier<E> {
	void notify(E elem, int pos, int tot);
}
