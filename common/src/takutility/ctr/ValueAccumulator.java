package takutility.ctr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class ValueAccumulator<T, V extends Comparable<V>> implements Comparator<T> {
	protected Map<T, V> map = new HashMap<T, V>();
	
	public Set<T> getElements() {
		return map.keySet();
	}

	public List<T> getSortedElements() {
		ArrayList<T> t = new ArrayList<T>(map.keySet());
		Collections.sort(t, this);
		return t;
	}
	
	public List<T> getInverseSortedElements() {
		ArrayList<T> t = new ArrayList<T>(map.keySet());
		Collections.sort(t, new Comparator<T>() {
			@Override
			public int compare(T o1, T o2) {
				return compare(o2, o1);
			}
		});
		return t;
	}
	
	protected V getInner(T elem) {
		return map.get(elem);
	}
	
	/**
	 * Return 0 if val is equivalent to null, > 0 if val > null and < 0 if val < null
	 */
	protected abstract int compareNull(V val);

	@Override
	public int compare(T o1, T o2) {
		V c1, c2;
		c1 = map.get(o1);
		c2 = map.get(o2);
		if (c1 == null) {
			if (c2 == null)
				return 0;
			else
				return -compareNull(c2);
		} else if (c2 == null)
			return compareNull(c1);
		return c1.compareTo(c2);
	}

}
