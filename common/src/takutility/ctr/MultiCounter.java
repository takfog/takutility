package takutility.ctr;

public class MultiCounter<T> extends NumberAccumulator<T, Counter> {

	public int add(T elem) {
		return add(elem, 1);
	}
	
	public int add(T elem, int value) {
		Counter c = map.get(elem);
		if (c == null) {
			c = new Counter(value);
			map.put(elem, c);
		} else {
			c.increment(value);
		}
		return c.get();
	}
	
	public int get(T elem) {
		Counter c = getInner(elem);
		if (c == null) return 0;
		return c.get();
	}
	
	public int GCD() {
		int gcd = -1;
		for(Counter c : map.values()) {
			int v = c.get();
			if (v == 0)
				continue;
			else if(v < 0)
				v = -v;
				
			if (gcd < 0)
				gcd = v;
			else if(gcd > v)
				gcd = GCD(gcd, v);
			else
				gcd = GCD(v, gcd);
		}
		return gcd;
	}
	
	private int GCD(int a, int b) {
		while(b != 0) {
			int t = b;
			b = a%b;
			a = t;
		}
		return a;
	}

	@Override
	protected int compareNull(Counter val) {
		return val.get();
	}

}
