package takutility.ctr;

import takutility.Comparators;

public class Counter extends Number implements Comparable<Counter> {
	private static final long serialVersionUID = 3018293216719554275L;
	
	private int v;
	
	public Counter() {
		this(0);
	}
	
	public Counter(int v) {
		this.v = v;
	}
	
	public void increment(){
		v++;
	}
	
	public void increment(int i){
		v += i;
	}
	
	public void clear() {
		v = 0;
	}
	
	public void set(int val) {
		v = val;
	}
	
	public int get() {
		return v;
	}

	@Override
	public int intValue() {
		return v;
	}

	@Override
	public long longValue() {
		return v;
	}

	@Override
	public float floatValue() {
		return v;
	}

	@Override
	public double doubleValue() {
		return v;
	}

	@Override
	public int compareTo(Counter o) {
		return Comparators.compare(v, o.v);
	}
}
