package takutility.ctr;

import takutility.ptr.DoublePtr;

public class ScoreAccumulator<T> extends NumberAccumulator<T, DoublePtr> {
	private double total = 0.;
	
	public double add(T elem, int value) {
		DoublePtr c = map.get(elem);
		if (c == null) {
			c = new DoublePtr(value);
			map.put(elem, c);
		} else {
			c.val += value;
		}
		total += value;
		return c.val;
	}
	
	public double get(T elem) {
		DoublePtr c = getInner(elem);
		if (c == null) return 0.;
		return c.val;
	}
	
	public double getNormalized(T elem) {
		return get(elem)/total;
	}
	
	@Override
	protected int compareNull(DoublePtr val) {
		return Double.compare(val.val, 0.);
	}

}
