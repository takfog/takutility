package takutility.ctr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class NumberAccumulator<T,V extends Number & Comparable<V>> 
	extends ValueAccumulator<T, V> 
{

	public List<T> getSortedElements(final Comparator<? super Number> comparator) {
		ArrayList<T> t = new ArrayList<T>(map.keySet());
		Collections.sort(t, new Comparator<T>() {
			@Override
			public int compare(T o1, T o2) {
				return comparator.compare(getInner(o1), getInner(o2));
			}
		});
		return t;
	}

}
