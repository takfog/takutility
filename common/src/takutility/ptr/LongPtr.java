package takutility.ptr;

import takutility.Comparators;

public class LongPtr extends Number implements Comparable<LongPtr> {
	private static final long serialVersionUID = 7269402974963784060L;
	
	public long val;
	
	public LongPtr() {
		val = 0;
	}
	
	public LongPtr(long initVal) {
		val = initVal;
	}
	
	public void set(long val) {
		this.val = val;
	}

	public void swap(LongPtr other) {
		long tmp = val;
		val = other.val;
		other.val = tmp;
	}

	@Override
	public int hashCode() {
		return (int)(val ^ (val >>> 32));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntPtr other = (IntPtr) obj;
		if (val != other.val)
			return false;
		return true;
	}

	public boolean innerEquals(Number obj) {
		if (obj == null) return false; 
		return val == obj.longValue();
	}
	
	public boolean innerEquals(long val) {
		return this.val == val;
	}
	
	@Override
	public String toString() {
		return val+"";
	}

	@Override
	public int intValue() {
		return (int)val;
	}

	@Override
	public long longValue() {
		return val;
	}

	@Override
	public float floatValue() {
		return val;
	}

	@Override
	public double doubleValue() {
		return val;
	}

	@Override
	public int compareTo(LongPtr o) {
		return Comparators.compare(val, o.val);
	}
}
