package takutility.ptr;

public class DoublePtr extends Number implements Comparable<DoublePtr> {
	private static final long serialVersionUID = 7257501416733160337L;
	
	public double val;
	
	public DoublePtr() {
		val = 0;
	}
	
	public DoublePtr(long initVal) {
		val = initVal;
	}
	
	public void set(double val) {
		this.val = val;
	}

	public void swap(DoublePtr other) {
		double tmp = val;
		val = other.val;
		other.val = tmp;
	}

	@Override
	public int hashCode() {
		long bits = Double.doubleToLongBits(val);
        return (int)(bits ^ (bits >>> 32));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntPtr other = (IntPtr) obj;
		if (val != other.val)
			return false;
		return true;
	}

	public boolean innerEquals(Number obj) {
		if (obj == null) return false; 
		return val == obj.doubleValue();
	}
	
	public boolean innerEquals(double val) {
		return this.val == val;
	}
	
	@Override
	public String toString() {
		return val+"";
	}

	@Override
	public int intValue() {
		return (int)val;
	}

	@Override
	public long longValue() {
		return (long) val;
	}

	@Override
	public float floatValue() {
		return (float) val;
	}

	@Override
	public double doubleValue() {
		return val;
	}
	
	@Override
	public int compareTo(DoublePtr o) {
		return Double.compare(val, o.val);
	}
}
