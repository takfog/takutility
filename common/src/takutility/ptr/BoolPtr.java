package takutility.ptr;

public class BoolPtr {
	public boolean val;
	
	public BoolPtr() {
		val = false;
	}
	
	public BoolPtr(boolean initVal) {
		val = initVal;
	}
	
	public boolean get() {
		return val;
	}
	
	public void set(boolean val) {
		this.val = val;
	}

	public void swap(BoolPtr other) {
		if (val != other.val) {
			val = !val;
			other.val = !other.val;
		}
	}
	
	@Override
	public int hashCode() {
		return val ? 1 : 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return val == ((BoolPtr) obj).val;
	}

	public boolean innerEquals(Boolean obj) {
		if (obj == null) return false; 
		return obj.booleanValue() == val;
	}
	
	public boolean innerEquals(boolean val) {
		return this.val == val;
	}
	
	@Override
	public String toString() {
		return val+"";
	}
}
