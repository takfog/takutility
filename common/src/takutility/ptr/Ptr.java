package takutility.ptr;

public class Ptr<T> {
	public T val;
	
	public Ptr() {
		val = null;
	}
	
	public Ptr(T initVal) {
		val = initVal;
	}
	
	public T get() {
		return val;
	}
	
	public void set(T val) {
		this.val = val;
	}
	
	public boolean isNull() {
		return val == null;
	}

	public void swap(Ptr<T> other) {
		T tmp = val;
		val = other.val;
		other.val = tmp;
	}
	
	@Override
	public int hashCode() {
		if (val == null) return 0;
		return val.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ptr<?> other = (Ptr<?>) obj;
		if (val == null) {
			if (other.val == null) return true;
			else return false; 
		} else
			return val.equals(other.val);
	}
	
	public boolean equals(Ptr<T> other) {
		if (other == null)
			return false;
		return innerEquals(other.val);
	}

	public boolean innerEquals(T obj) {
		if (val == null) {
			if (obj == null) return true;
			else return false; 
		} else
			return val.equals(obj);
	}
	
	@Override
	public String toString() {
		if (val == null) return "null";
		return val.toString();
	}
}
