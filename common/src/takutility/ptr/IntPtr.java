package takutility.ptr;

import takutility.Comparators;

public class IntPtr extends Number implements Comparable<IntPtr> {
	private static final long serialVersionUID = -4039640095585780639L;
	
	public int val;
	
	public IntPtr() {
		val = 0;
	}
	
	public IntPtr(int initVal) {
		val = initVal;
	}
	
	public void set(int val) {
		this.val = val;
	}

	public void swap(IntPtr other) {
		int tmp = val;
		val = other.val;
		other.val = tmp;
	}
	
	@Override
	public int hashCode() {
		return val;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntPtr other = (IntPtr) obj;
		if (val != other.val)
			return false;
		return true;
	}

	public boolean innerEquals(Number obj) {
		if (obj == null) return false; 
		return val == obj.intValue();
	}
	
	public boolean innerEquals(int val) {
		return this.val == val;
	}
	
	@Override
	public String toString() {
		return val+"";
	}

	@Override
	public int intValue() {
		return val;
	}

	@Override
	public long longValue() {
		return val;
	}

	@Override
	public float floatValue() {
		return val;
	}

	@Override
	public double doubleValue() {
		return val;
	}

	@Override
	public int compareTo(IntPtr o) {
		return Comparators.compare(val, o.val);
	}
}
