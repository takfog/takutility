package takutility;

public class TakUtility {
	
	public static <T> T optional(T value, T def) {
		if (value == null) return def;
		else return value;
	}
	
	public static double optional(double value, double def) {
		if (Double.isNaN(value)) return def;
		else return value;
	}

	public static int parseInt(String s) {
		return parseInt(s, 0);
	}

	public static int parseInt(String s, int def) {
		if (s == null || s.isEmpty())
			return def;
		try {
			return Integer.parseInt(s);
		} catch(NumberFormatException e) {
			return def;
		}
	}

	public static long parseLong(String s) {
		return parseLong(s, 0L);
	}

	public static long parseLong(String s, long def) {
		if (s == null || s.isEmpty())
			return def;
		try {
			return Long.parseLong(s);
		} catch(NumberFormatException e) {
			return def;
		}
	}

	public static float parseFloat(String s) {
		return parseFloat(s, 0f);
	}

	public static float parseFloat(String s, float def) {
		if (s == null || s.isEmpty())
			return def;
		try {
			return Float.parseFloat(s);
		} catch(NumberFormatException e) {
			return def;
		}
	}
	
	public static double parseDouble(String s) {
		return parseDouble(s, 0.);
	}
	
	public static double parseDouble(String s, double def) {
		if (s == null || s.isEmpty())
			return def;
		try {
			return Double.parseDouble(s);
		} catch(NumberFormatException e) {
			return def;
		}
	}
	
	public static int inRange(int val, int min, int max) {
		if (val < min) 
			return min;
		if (val > max)
			return max;
		return val;
	}
	
	public static float inRange(float val, float min, float max) {
		if (val < min) 
			return min;
		if (val > max)
			return max;
		return val;
	}
	
	public static boolean isInRange(int val, int min, int max) {
		return val >= min && val <= max;
	}
	
	public static boolean isInRange(float val, float min, float max) {
		return val >= min && val <= max;
	}
	
	public static int max(int... values) {
		if (values.length == 0)
			throw new IllegalArgumentException("the array can't be empty");
		int max = values[0];
		for (int i = 1; i < values.length; i++) {
			if (values[i] > max)
				max = values[i];
		}
		return max;
	}
	
	public static int min(int... values) {
		if (values.length == 0)
			throw new IllegalArgumentException("the array can't be empty");
		int min = values[0];
		for (int i = 1; i < values.length; i++) {
			if (values[i] < min)
				min = values[i];
		}
		return min;
	}
}
