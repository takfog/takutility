package takutility.frankentext;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import takutility.collections.AutoMap;
import takutility.file.Files;
import takutility.io.UTF8BufferedReader;
import takutility.io.UTF8PrintWriter;

public class FrankenText {
	private Pattern frankenRow = Pattern.compile("\\s*//\\s*#+\\s+(\\S+)(?:\\s*|\\s+(\\S.*))$");
	
	public void updateFolder(File bodyDir, File... partDirs) throws FileNotFoundException, IOException {
		for (File body : Files.recursiveList(bodyDir)) {
			File[] parts = Arrays.stream(partDirs)
				.map(d -> Files.transplant(body, bodyDir, d))
				.filter(File::exists)
				.toArray(s -> new File[s]);
			if (parts.length > 0)
				update(body, parts);
		}
	}
	
	/**
	 * 
	 * @param body the file with the common text
	 * @param parts the files with the pieces to add
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void update(File body, File... parts) throws FileNotFoundException, IOException {
		Map<String, Map<File, String>> pieces = loadPieces(parts);
		if (pieces.size() == 0) 
			return;
		
		Map<File, UTF8PrintWriter> writers = new HashMap<>(parts.length);
		for (File file : parts) {
			writers.put(file, new UTF8PrintWriter(file));
		}
		
		try (UTF8BufferedReader r = new UTF8BufferedReader(body)) {
			String partName = null;
			Collection<UTF8PrintWriter> withoutPart = null;
			for (String line : r) {
				Matcher m = frankenRow.matcher(line);
				if (partName == null) {
					//not in part
					if (m.matches()) {
						switch (m.group(1)) {
						case "begin":
						case "insert":
							break;
						default:
							continue;
						}
						partName = m.group(2);
						Map<File, String> piece = null;
						if (pieces.containsKey(partName)) {
							piece = pieces.get(partName);
							for (Entry<File, String> e : piece.entrySet()) {
								writers.get(e.getKey()).print(e.getValue());
							}
						}
						if (piece == null || "insert".equals(m.group(1))) {
							partName = null;
						} else if (piece.size() < writers.size()) {
							Set<File> written = piece.keySet();
							withoutPart = writers.entrySet().stream()
									.filter(e -> !written.contains(e.getKey()))
									.map(Entry::getValue)
									.collect(Collectors.toList());
						}
					} else {
						writers.values().forEach(w -> w.println(line));
					}
				} else {
					//in part
					if (m.matches()) {
						if ("end".equals(m.group(1))) {
							partName = null;
							if (withoutPart != null) withoutPart.clear();
						}
					} else if (withoutPart != null)
						withoutPart.forEach(w -> w.println(line));
				}
			}
		} finally {
			writers.values().forEach(UTF8PrintWriter::close);
		}
	}

	public Map<String, Map<File, String>> loadPieces(File... parts) throws IOException, FileNotFoundException {
		AutoMap<String, Map<File, String>> pieces = new AutoMap<>(HashMap::new);
		
		for (File file : parts) {
			try(UTF8BufferedReader r = new UTF8BufferedReader(file)) {
				String pieceName = null;
				StringBuilder sb = null;
				for (String line : r) {
					Matcher m = frankenRow.matcher(line);
					if (pieceName == null) {
						//new piece
						if (m.matches()) {
							if ("begin".equals(m.group(1))) {
								pieceName = m.group(2);
								sb = new StringBuilder();
								sb.append(line).append(System.lineSeparator());
							}
						}
					} else {
						//inside of piece
						sb.append(line).append(System.lineSeparator());
						if (m.matches()) {
							if ("end".equals(m.group(1))) {
								pieces.get(pieceName).put(file, sb.toString());
								pieceName = null;
								sb = null;
							}
						}
					}
				}
			}
		}
		return pieces;
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		if (args.length < 2) {
			System.err.println("Usage: body part1 [part2 [...]]");
			return;
		}
		new FrankenText().updateFolder(new File(args[0]), Arrays.stream(args)
				.skip(1)
				.map(File::new)
				.toArray(n -> new File[n]));
	}
}
